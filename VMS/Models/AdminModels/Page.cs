﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class Page
    {
        public String ID
        { get; set; }

        public String NAME
        { get; set; }

        public String MODULE_NAME
        { get; set; }

        public String MODULE_ID
        { get; set; }

        public String URL
        { get; set; }

        public String ICON
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }

        public int SHOW_ON_MENU
        { get; set; }

    }
}