﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class Badge
    {
        public String BADGE_ID
        { get; set; }

        public String PHOTO_PATH
        { get; set; }

        public String DESCRIPTION
        { get; set; }

        public int MINIMUM_HOURS
        { get; set; }

        public int CAUSE_ID
        { get; set; }

        public String CAUSE_NAME
        { get; set; }

        public int QTY
        { get; set; }

        public String STATUS
        { get; set; }

        public int VOL_HAS
        { get; set; }
    }
}