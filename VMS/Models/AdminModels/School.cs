﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class School
    {
        public String SCHL_ID
        { get; set; }

        public String SCHL_NAME
        { get; set; }

        public String SCH_LVL
        { get; set; }

        public String LVL_OF_EDU_ID
        { get; set; }

        public String ADDRESS
        { get; set; }
    }
}