﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class Vocab
    {
        public int VOCAB_ID
        { get; set; }

        public String VOCAB_NAME
        { get; set; }

        public int FOR_INTRST_FORM
        { get; set; }
        
    }
}