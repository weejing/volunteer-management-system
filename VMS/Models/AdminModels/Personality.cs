﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class Personality
    {
        public String PERSONALITY_ID
        { get; set; }
        public String PERSONALITY_NAME
        { get; set; }
        public String PERSONALITY_CHAR
        { get; set; }
        public String DESCRIPTION
        { get; set; }
    }
}