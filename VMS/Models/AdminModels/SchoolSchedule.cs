﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class SchoolSchedule
    {
        public String SCHL_LVL_EXAM_PERIOD_ID
        { get; set; }
        public String START_DAY
        { get; set; }
        public String END_DATE
        { get; set; }
        public String SCH_LVL
        { get; set; }
        public String SCHL_NAME
        { get; set; }
        public String YEAR
        { get; set; }
    }
}