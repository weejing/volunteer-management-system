﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class InterestCat
    {
        public String INTEREST_CAT_ID
        { get; set; }

        public String INTEREST_CAT
        { get; set; }
    }
}