﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class ExamPeriod
    {
        public String EXAM_PERIOD_ID
        { get; set; }
        public DateTime START_DATE
        { get; set; }
        public DateTime END_DATE
        { get; set; }
        public String SCH_LVL
        { get; set; }

        public String LVL_OF_EDU_ID
        { get; set; }
    }
}