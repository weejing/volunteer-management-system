﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AdminModels
{
    public class VolEvtCause
    {
        public int CAUSE_ID
        { get; set; }

        public int QTY
        { get; set; }
    }
}