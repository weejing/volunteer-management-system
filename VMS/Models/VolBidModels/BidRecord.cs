﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.VolBidModels
{
    public class BidRecord
    {
        public String USR_BID_ID
        { get; set; }

        public String USR_ID
        { get; set; }

        public String USR_NAME
        { get; set; }

        public String BID_ITEM_ID
        { get; set; }

        public String ITEM_NAME
        { get; set; }

        public int BID_POINTS_AMOUNT
        { get; set; }

        public String DATE_TIME
        { get; set; }

        public String STATUS
        { get; set; }
    }
}