﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.VolBidModels
{
    public class BidPointTransaction
    {
        public String USR_POINT_ID
        { get; set; }

        public String USR_ID
        { get; set; }

        public String USR_NAME
        { get; set; }

        public int CHANGES
        { get; set; }

        public int BALANCE
        { get; set; }

        public String BID_ITEM_ID
        { get; set; }

        public String BID_ITEM_NAME
        { get; set; }

        public String EVENT_ID
        { get; set; }

        public String TRANSACTION_INFO
        { get; set; }

        public String TRANSACTION_DATETIME
        { get; set; }
    }
}