﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.NotificationModels
{
    public class Notification
    {
        public String NOTI_ID
        { get; set; }

        public String NOTIFICATION
        { get; set; }

        public String RECEIVER_ID
        { get; set; }

        public String TABLE_NAME
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }

        public String HAS_READ
        { get; set; }

        public DateTime TIMESTAMP
        { get; set; }
    }
}