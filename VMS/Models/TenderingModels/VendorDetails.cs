﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.TenderingModels
{
    public class VendorDetails
    {
        public VendorDetails()
        {

        }
        public string VDR_ID
        {
            get;
            set;
        }
        public string VDR_NAME
        {
            get;
            set;
        }
        public string VDR_EMAIL
        {
            get;
            set;
        }
        public string VDR_TEL
        {
            get;
            set;
        }
        public string VDR_WEBSITE
        {
            get;
            set;
        }
        public string PASSWORD
        {
            get;
            set;
        }
    }
}