﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.TenderingModels
{
    public class OrganisationDetails
    {
        public OrganisationDetails()
        {

        }
        public string ORG_ID
        {
            get;
            set;
        }
        public string ORG_NAME
        {
            get;
            set;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string LINK
        {
            get;
            set;
        }
        public string ADDRESS
        {
            get;
            set;
        }
        public string DESCRIPTION
        {
            get;
            set;
        }
        public string ORG_TYPE
        {
            get;
            set;
        }
        public string ORG_STATUS
        {
            get;
            set;
        }
        public string RECURRING_PAYMENT
        {
            get;
            set;
        }
    }
}