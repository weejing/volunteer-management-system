﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.TenderingModels
{
    public class TenderCategory
    {
        public TenderCategory()
        {

        }

        public string IVT_PRODUCT_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string LEVEL2_PARENT
        {
            get;
            set;
        }
        public string LEVEL1_PARENT
        {
            get;
            set;
        }
       
    }
}