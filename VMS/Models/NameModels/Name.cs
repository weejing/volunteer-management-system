﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.NameModels
{
    // this is a dummy model file
    public class Name
    {
        public String name
        { get; set; }
        public int age
        { get; set; }
        

        public Name()
        {
            this.name = "name";
            this.age = 12;
        }

    }
}