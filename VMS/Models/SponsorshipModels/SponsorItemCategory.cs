﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.SponsorshipModels
{
    public class SponsorItemCategory
    {
        public String SPR_ITEM_CATEGORY_ID
        { get; set; }

        public String ITEM_CATEGORY_NAME
        { get; set; }

    }
}