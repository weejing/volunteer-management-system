﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.SponsorshipModels
{
    public class SponsorshipRecord
    {
        public String SPONSOR_RECORD_ID
        { get; set; }

        public String ITEM_ID
        { get; set; }

        public String ITEM_NAME
        { get; set; }

        public String ITEM_DESCRIPTION
        { get; set; }

        public String SPR_ITEM_CATEGORY_ID
        { get; set; }

        public String ITEM_CATEGORY_NAME
        { get; set; }

        public int QTY
        { get; set; }

        public String SPONSOR_USR_ID
        { get; set; }

        public String SPONSOR_NAME
        { get; set; }

        public String SPONSORSHIP_DESCRIPTION
        { get; set; }

        public String COLLECTION_DESCRIPTION
        { get; set; }

        public String STATUS
        { get; set; }

        public String ACTION_ADMIN_ID
        { get; set; }

        public String ACTION_ADMIN_NAME
        { get; set; }

        public String REQUEST_DATE
        { get; set; }

        public String ADMIN_ACTION_DATE
        { get; set; }

        public String IMAGE
        { get; set; }

    }
}