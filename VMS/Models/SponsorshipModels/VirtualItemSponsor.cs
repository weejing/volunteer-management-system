﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.SponsorshipModels
{
    public class VirtualItemSponsor
    {
        public String SPR_ITEM_ID
        { get; set; }

        public String ITEM_NAME
        { get; set; }

        public int QTY_AVAILABLE
        { get; set; }

        public int QTY_ON_BIDDING
        { get; set; }

        public String ITEM_DESCRIPTION
        { get; set; }

        public String SPR_ITEM_CATEGROY_ID
        { get; set; }

        public String ITEM_CATEGORY_NAME
        { get; set; }

        public String SPONSOR_USR_ID
        { get; set; }

        public String SPONSOR_NAME
        { get; set; }

        public String IMAGE
        { get; set; }
    }
}