﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.ReportingModels
{
    public class ReportVendor
    {
        public ReportVendor()
        {

        }
        public string USR_ID
        { get; set; }
        public string REGISTRATION_DATE
        { get; set; }
        public string FIRM_NAME
        { get; set; }
        public DateTime REG_DATE
        { get; set; }
        //non-sql varaibles
        public string SPONSOR_COUNT
        { get; set; }
        public string VENDOR_RATING
        { get; set; }
    }
}