﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.ReportingModels
{
    public class ReportOverview
    {
        public ReportOverview()
        {

        }
        public string DATE
        { get; set; }
        public string VOL_NUM
        { get; set; }
        public string VENDOR_NUM
        { get; set; }
        public string ORG_NUM
        { get; set; }
        public string NUM
        { get; set; }
    }
}