﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.ReportingModels
{
    public class ReportVolunteers
    {
        public ReportVolunteers()
        {

        }
        public string VOLUNTEER_DATE
        { get; set; }
        public string VOLUNTEER_COUNT
        { get; set; }
        public string VOLUNTEER_INTEREST
        { get; set; }
        public string VOLUNTEER_INTEREST_CAT
        { get; set; }
    }
}