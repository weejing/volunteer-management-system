﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.ReportingModels;

namespace VMS.DataLayer.ReportingMgmt
{
    public class AVolunteerReport
    {
        public AVolunteerReport()
        {

        }
        public string TOTAL_VOLUNTEER_NUMBER
        { get; set; }

        public List<ItemSummary> VOLUNTEER_SUMMARY_INTEREST_CAT
        { get; set; }
        public List<ItemSummary> VOLUNTEER_SUMMARY_INTEREST
        { get; set; }
        public List<ItemSummary> VOLUNTEER_SUMMARY_MBTI
        { get; set; }
        public List<ItemSummary> VOLUNTEER_SUMMARY_PREFERRED_V_DAY
        { get; set; }
        public List<ItemSummary> VOLUNTEER_SUMMARY_SKILLS_CAT
        { get; set; }
        public List<ItemSummary> VOLUNTEER_SUMMARY_SKILLS
        { get; set; }
        
    }
}