﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.ReportingModels
{
    public class ReportNPO
    {
        public ReportNPO()
        {

        }
        public string VOLUNTEER_NO
        { get; set; }
        public string CONNECTION_STRING
        { get; set; }
        public string LINK
        { get; set; }
        public string ORG_TYPE
        { get; set; }
        public string ORG_ID
        { get; set; }
        public string ORG_STATUS_ACTIVE
        { get; set; }
        public string ORG_STATUS_INACTIVE
        { get; set; }
        public string ORG_NAME
        { get; set; }
    }
}