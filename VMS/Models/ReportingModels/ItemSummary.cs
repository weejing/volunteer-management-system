﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.ReportingModels
{
    public class ItemSummary
    {
        public string ITEM_NAME
        { get; set; }
        public string ITEM_AMOUNT
        { get; set; }
        public string ITEM_DESC
        { get; set; }
        public int ITEM_NUM
        { get; set; }
    }
}