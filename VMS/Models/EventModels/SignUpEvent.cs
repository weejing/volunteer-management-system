﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class SignUpEvent
    {

        public string evtUserId
        { get; set; }

        public String eventId
        { get; set; }

        public String evt_roleId
        { get; set; }

        public String sessionId
        { get; set; }

        public string eventdateList
        { get; set; }

        public String connectionString
        { get; set; }
    }

}