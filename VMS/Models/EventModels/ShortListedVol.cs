﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class ShortListedVol
    {
        public String VOL_ID
        { get; set; }

        public string traits
        { get; set; }

        public string email
        { get; set; }

        public List<DateTime> listOfDates
        { get; set; }

        public string eventId
        { get; set; }

        public List<AcceptedSession> listOfAcceptedSession
        { get; set; }

        public List<int> skillSets
        { get; set; }

        public List<int> listOfEventInterest
        {get; set;}

        public List<int> listOfEventRoleInterest
        { get; set; }
    }
}