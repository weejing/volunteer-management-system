﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class FeedBackDesign
    {
        public string sessionId
        { get; set; }

        public string question
        { get; set; }

        public int type
        { get; set; }

        public string category //never use this variable
        { get; set; }

        public int number
        { get; set; }
        
        public string fdback_detail_id
        { get; set; }

        //public List<String> listOfTextAns
        //{ get; set; }

        public String listOfTextAns
        { get; set; }

        public List<RatingCount> ratingCountList
        { get; set; }
    }
}