﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.Models.EventModels
{
    public class EventDetails
    {
        public String eventId
        { get; set; }

        public String eventName
        { get; set;}

        public String description
        { get; set;}

        public DateTime startDate
        { get; set;}

        public DateTime endDate
        { get; set;}

        public String status
        { get; set; }

        public List<int> listOfEventCause
        { get; set; }

        public List<String> listOfEventCauseName
        { get; set; }

        public List<SessionDetails> listOfSession
        { get; set; }

        public String connectionString
        { get; set; }

        public String orgName
        { get; set; }

        public int passingWeight
        { get; set; }

        /*this is organised by (1) traits, (2) event type interest, (3) role interest         
         */
        public  List<int> listOfWeights
        { get; set; }

        public string generateNewLink
        { get; set; }

        public int logsNum
        { get; set; }

        public string imagePath
        { get; set; }

        public List<Term> listofCauseName
        { get; set; }

    }
}