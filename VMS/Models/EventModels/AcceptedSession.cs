﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class AcceptedSession
    {
        public string sessionId
        { get; set; }

        // sessionRoles are arranged in role1,role2,role3
        public string sessionRoles
        { get; set; }

        // sessionDates are arranged in shortdate1, shortdate2, shortdate3
        public string sessionDates
        { get; set; }

        // shows the connectionString of the event that it is tag to
        public string connectionString
        { get; set; }

        public string percent
        { get; set; }
    }
}