﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class RatingCount
    {
        public int RATING
        { get; set; }

        public int COUNT
        { get; set; }
    }
}