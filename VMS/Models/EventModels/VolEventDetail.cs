﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.Models.EventModels
{
    public class VolEventDetail
    {
        public String eventId
        { get; set; }

        public String eventName
        { get; set; }

        public String description
        { get; set; }

        public DateTime startDate
        { get; set; }

        public DateTime endDate
        { get; set; }

        public List<VolSessionDetails> listOfSession
        { get; set; }

        public string imagePath
        { get; set; }

        public String generateNewLink
        { get; set; }

        public List<Term> listofCauseName
        { get; set; }
    }
}