﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class CheckIn
    {
        public string sessionId
        { get; set; }

        public DateTime date
        { get; set; }

        public int postalCode
        { get; set; }

        public DateTime checkInTime
        { get; set; }

        public string sessionDayId
        { get; set; }

        public String connectionString
        { get; set; }
       
    }
}