﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.EventModels
{
    public class SessionRoles
    {
        public String roleName
        { get; set; }

        public String personalityTraits
        { get; set;}

        public int quantity
        { get; set; }
           
        public int roleId
        { get; set; } 

        public String term_skills_id
        { get; set; }

        public String evt_roleId
        { get; set; }

        public String skillSets
        { get; set; }

        public String roleDesc
        { get; set; }

        // variable is in form "roleId-traits-skillset"
        public string concatenateRoleId
        { get; set; }

        public int percent
        { get; set; }
             
    }
}