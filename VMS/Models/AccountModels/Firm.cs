﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.AccountModels
{
    public class Firm
    {
        public String USR_ID
        { get; set; }

        public String EMAIL
        { get; set; }

        public String PASSWORD
        { get; set; }

        public DateTime REGISTRATION_DATE
        { get; set; }

        public String RESETPW_CODE
        { get; set; }

        public String FIRM_NAME
        { get; set; }
        public String ADDRESS
        { get; set; }

        public String PHONE
        { get; set; }
        public String FAX
        { get; set; }
        public String FIRM_DESCRIPTION
        { get; set; }
    }
}