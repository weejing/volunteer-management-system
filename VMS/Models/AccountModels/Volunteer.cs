﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.Models.AccountModels
{
    public class Volunteer
    {
        public String VOL_ID
        { get; set; }

        public String FULL_NAME
        { get; set; }

        public String SCHL_ID
        { get; set; }

        public String FACEBOOK_ID
        { get; set; }

        public String EMAIL
        { get; set; }

        public String NRIC
        { get; set; }

        public String PHONE_NO
        { get; set; }

        public String PASSWORD
        { get; set; }

        public DateTime DOB
        { get; set; }

        public String DIETARY
        { get; set; }

        public String PERSONALITY_CHAR
        { get; set; }


        public String VIA_TARGET
        { get; set; }

        public String RELIGION
        { get; set; }

        public DateTime REGISTRATION_DATE
        { get; set; }

        public String ACTIVATION_CODE
        { get; set; }

        public String RESETPW_CODE
        { get; set; }

        public List<TermRelation> volListofInts
        { get; set; }

        public List<Badge> volBadges
        { get; set; }

        public String SCHL_NAME
        { get; set; }

        public String SCH_LVL
        { get; set; }

        public String URL
        { get; set; }

        public String IMAGE
        { get; set; }

    }
}