﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserComments
    {
        public String USR_COMMENT_ID
        { get; set; }

        public String TO_USR_ID
        { get; set; }

        public String TO_USR_NAME
        { get; set; }

        public String FROM_USR_ID
        { get; set; }

        public String FROM_USR_NAME
        { get; set; }

        public String FROM_USR_IMAGE
        { get; set; }

        public String COMMENT
        { get; set; }

        public String COMMENT_DATE
        { get; set; }
    }
}