﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class FBFriends
    {
        public List<FBFriend> friendsListing { get; set; }
    }

    public class FBFriend
    {
        public string name { get; set; }
        public string id { get; set; }
    }
}