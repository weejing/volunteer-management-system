﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserCommentsReply
    {
        public String USR_ID
        { get; set; }

        public String USR_COMMENT_ID
        { get; set; }

        public String COMMENT
        { get; set; }

        public String COMMENT_DATE
        { get; set; }

        public String IS_DELETED
        { get; set; }
    }
}