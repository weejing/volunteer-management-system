﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserOrganisations
    {
        public String USR_ID
        { get; set; }

        public List<String> ORG_ID
        { get; set; }
    }
}