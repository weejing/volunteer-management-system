﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserEvent
    {
        public String USR_EVENT_ID
        { get; set; }

        public String USR_ID
        { get; set; }

        public String USR_FULL_NAME
        { get; set; }

        public String USR_IMAGE
        { get; set; }

        public String EVENT_ID
        { get; set; }

        public String CONNECTION_STRING
        { get; set; }
    }
}