﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserAcitvities
    {
        public String USR_ACTIVITY_ID
        { get; set; }

        public String USR_ID
        { get; set; }

        public String ACTIVITY_TYPE
        { get; set; }

        public String TARGET_USR_ID
        { get; set; }

        public String TARGET_USR_NAME
        { get; set; }

        public String EVENT_ID
        { get; set; }

        public String EVENT_NAME
        { get; set; }

        public String DATETIME
        { get; set; }

        public String USR_FULL_NAME
        { get; set; }

        public String MESSAGE_DISPLAY
        { get; set; }
    }
}