﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.CommunityModels
{
    public class UserFriends
    {
        public String OWN_USR_ID
        { get; set; }

        /*
        public List<String> FRIEND_USR_ID
        { get; set; }
        */

        public List<UserPortfolio> USR_FRIENDS
        { get; set; }
    }
}