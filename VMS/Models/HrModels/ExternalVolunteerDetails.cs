﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.HrModels
{
    public class ExternalVolunteerDetails
    {
        public string NAMELIST_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string PHONENO
        {
            get;
            set;
        }
        public ExternalVolunteerDetails(string Namelistid, string volName, string PhoneNo,string Email)
        {
            this.NAMELIST_ID = Namelistid;
            this.NAME = volName;
            this.EMAIL = Email;
            this.PHONENO = PhoneNo;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }

        public ExternalVolunteerDetails()
        {

        }
    }
}