﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.HrModels
{
    public class OpenEvents
    {
        public string EVENTNAME
        {
            get;
            set;
        }

        public DateTime EVENTDATE
        {
            get;
            set;
        }

        public string EVENTLINK
        {
            get;
            set;
        }

        public OpenEvents(string eventname, DateTime eventdate, string eventlink)
        {

            this.EVENTNAME = eventname;
            this.EVENTDATE = eventdate;
            this.EVENTLINK = eventlink;
        }
        public OpenEvents()
        {

        }
    }
}