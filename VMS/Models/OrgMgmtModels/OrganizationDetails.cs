﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.AuditMgmtModels;

namespace VMS.Models.OrgMgmtModels
{
    public class OrganizationDetails
    {
        public OrganizationDetails()
        {

        }
        public string ORG_ID
        {
            get;
            set;
        }
        public string ORG_NAME
        {
            get;
            set;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }
        public string IMAGE
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string LINK
        {
            get;
            set;
        }

        public string UEN
        {
            get;
            set;
        }

        public string ADDRESS
        {
            get;
            set;
        }
        public string DESCRIPTION
        {
            get;
            set;
        }

        public string ORG_STATUS
        {
            get;
            set;
        }
        public string ORG_TYPE
        {
            get;
            set;
        }
        public string IMAGE_PATH
        {
            get;
            set;
        }
        public string VERIFICATION
        {
            get;
            set;
        }
        public string RECURRING_PAYMENT
        {
            get;
            set;
        }
        public string AUDIT_MODULE_NAME
        {
            get;
            set;
        }
        public string AUDIT_MODULE_FEATURE
        {
            get;
            set;
        }
        public string AUDIT_ACTION
        {
            get;
            set;
        }
        public string AUDIT_SYSTEM_USER
        {
            get;
            set;
        }
        public string AUDIT_USER_ID
        {
            get;
            set;
        }
    }
}