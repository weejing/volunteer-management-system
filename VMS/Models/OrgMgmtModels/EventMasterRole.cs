﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.OrgMgmtModels
{
    public class EventMasterRole
    {
        public string roleTermId
        { get; set; }

        public string roleName
        {get; set;}

        public string traits
        { get; set; }

        public string skillSetList
        { get; set; }

    }
}