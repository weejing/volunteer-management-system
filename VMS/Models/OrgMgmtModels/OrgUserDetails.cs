﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.OrgMgmtModels
{
    public class OrgUserDetails
    {
        public OrgUserDetails()
        {

        }
        public string USR_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string PASSWORD
        {
            get;
            set;
        }
        public string ROLE_ID
        {
            get;
            set;
        }
        public DateTime REGISTRATION_DATE
        {
            get;
            set;
        }
        public string CONNECTION_STRING
        {
            get;
            set;
        }
    }
}