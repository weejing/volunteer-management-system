﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.OrgMgmtModels
{
    public class OrgUser
    {
        public OrgUser()
        {

        }
        public string ORG_USR_ID
        {
            get;
            set;
        }
        public string NAME
        {
            get;
            set;
        }
        public string EMAIL
        {
            get;
            set;
        }
        public string PASSWORD
        {
            get;
            set;
        }
        public string ROLE_ID
        {
            get;
            set;
        }
        public string REGISTRATION_DATE
        {
            get;
            set;
        }
    }
}