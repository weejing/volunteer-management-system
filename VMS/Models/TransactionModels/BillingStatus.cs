﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VMS.Models.TransactionModels
{
    public class BillingStatus
    {
        public BillingStatus()
        {

        }
        public string BIL_ID
        {
            get;
            set;
        }
        public string STATUS
        {
            get;
            set;
        }
        public string ORG_ID
        {
            get;
            set;
        }
        public int MONTH
        {
            get;
            set;
        }
        public int YEAR
        {
            get;
            set;
        }
        public string ORG_NAME
        {
            get;
            set;
        }
        public string PLAN_TYPE
        {
            get;
            set;
        }
        public DateTime PLAN_END
        {
            get;
            set;
        }
    }
}