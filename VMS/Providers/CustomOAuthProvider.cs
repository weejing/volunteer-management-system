﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;
using VMS.AccountMgmt;
using System.Linq;

namespace VMS.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
       
        // checks if web/mobile client has the access rights to recevie tokens from the seerver. Put this aside for now
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            //  client id wold be empty for now as no verfication have been implemented
            if (context.ClientId == null)
            {
                System.Diagnostics.Debug.WriteLine("client id is null");
            }

            string loginType = context.Parameters.Where(f => f.Key == "LoginType").Select(f => f.Value).SingleOrDefault()[0];
            context.OwinContext.Set<string>("LoginType", loginType);

            if(loginType.Equals("Organiser"))
            {
                string connectionString = context.Parameters.Where(f => f.Key == "connectionString").Select(f => f.Value).SingleOrDefault()[0];
                context.OwinContext.Set<string>("connectionString", connectionString.ToString());
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }


        // called when request have grant type of password
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });       
            string loginType = context.OwinContext.Get<string>("LoginType");
            string usr_ID = "";
            var identity = new ClaimsIdentity("JWT");


            if (loginType.Equals("Volunteer"))
            {
                usr_ID = AuthenticateUserBL.login(context.UserName, context.Password, "TABLE_USR_DETAILS");
            }

            if (loginType.Equals("Facebook"))
            {
               usr_ID = AuthenticateUserBL.facebookAuthentication(context.UserName, context.Password);
               loginType = "Volunteer";
            }

            if(loginType.Equals("Organiser"))
            {
                string connectionString = context.OwinContext.Get<string>("connectionString");
                System.Diagnostics.Debug.WriteLine("connectionString: " + connectionString);
                usr_ID = AuthenticateUserBL.orgUserLogin(context.UserName, context.Password, "TABLE_ORG_USR", connectionString);
                identity.AddClaim(new Claim(ClaimTypes.Actor,connectionString));
            }

            if (loginType.Equals("Firm"))
            {
                usr_ID = AuthenticateUserBL.vendorLogin(context.UserName, context.Password);
            }

            if(loginType.Equals("OneThird"))
            {
                usr_ID = AuthenticateUserBL.oneThirdLogin(context.UserName, context.Password);
            }
            
            if(usr_ID.Equals(""))
            {
                context.SetError( "Username or password is incorrect");
                return Task.FromResult<object>(null);
            }


            // add in the payload 
            identity.AddClaim(new Claim(ClaimTypes.Name, usr_ID));
            // specify the authorization  roles
            identity.AddClaim(new Claim(ClaimTypes.Role, loginType));
            // temprary use claimTypes.actor to store table name
            

            var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            // call protect() in CustomJwtFormat.cs to create the JWT
            context.Validated(ticket);

            return Task.FromResult<object>(null);
        }


    }
}