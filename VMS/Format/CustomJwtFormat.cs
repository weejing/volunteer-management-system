﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace VMS.Format
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _issuer = string.Empty;
        const string sec = "IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw";


        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            // set up a temproary secret key 

           // var secretKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(sec));
           // var signingKey = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256Signature);
           var secretKey = TextEncodings.Base64Url.Decode(sec);
           var signingKey = new HmacSigningCredentials(secretKey);

            // set up expiry dates 
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            // create the JWT
            var token = new JwtSecurityToken(_issuer, "Any", data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);
            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }

    }
}