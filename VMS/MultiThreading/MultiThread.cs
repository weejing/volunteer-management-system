﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using VMS.Database;
using System.Data.SqlClient;


namespace VMS.MultiThreading
{
    public class MultiThread
    {
        private  String[] db_array;

        public MultiThread()
        {
            // calls the console_vms database class to retrieve a list of the databasenames
            db_array = new String[] {"Test1","Testing","testing2", "testing3"};
        }

      
        // initiate the number of child threads that are to be run in parallel
        public void mainThread()
        {
            for(int count=0; count < 4; count++)
            {
                Thread childThread = new Thread(callChildThread);
                childThread.Start(db_array[count]);
            }
        }


        // contains the business logic in which the child thread will perform
        private void callChildThread(Object dbName)
        {
            String name = dbName.ToString();
            Database_Connect saasConfig = new Database_Connect(name);
            SqlConnection databaseConnection = saasConfig.getConnection();

            SqlCommand command = new SqlCommand("Select * from Event", databaseConnection);

            SqlDataReader dataReader = command.ExecuteReader();
            String response = "";
            while (dataReader.Read())
            {
                response = dataReader[0].ToString();
                System.Diagnostics.Debug.WriteLine("result is " + response);
            }
        }
    }
}