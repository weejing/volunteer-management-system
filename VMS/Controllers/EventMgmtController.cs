﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Models.EventModels;
using VMS.Models.AccountModels;
using System.Security.Claims;
using VMS.EventMgmt;
using System.Collections;
using VMS.Models.AdminModels;

// This will be written by weejing
namespace VMS.Controllers
{
    public class EventMgmtController : ApiController
    {


        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Program Management Module >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        /*
         * Create an event 
         */
        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("createEvent")]
        public String createEvent([FromBody] EventDetails eventDetails)
        {
            // get the web token user id out 
            var identity = User.Identity as ClaimsIdentity;
            String organiserId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            String role = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();


            System.Diagnostics.Debug.WriteLine("connectionString: " + connectionString);
            EventMgmtBL.createEvent(eventDetails, organiserId, connectionString);
            return "success";
        }

        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("editEvent")]
        public void editEvent([FromBody] EventDetails eventDetails)
        {

        }

        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("deleteEvent")]
        public void deleteEvent([FromBody]String eventId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String organiserId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            EventMgmtBL.deleteEventDetails(eventId, connectionString);
        }

        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("deleteEventSession")]
        public void deleteEventSession([FromBody] String sessionId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String organiserId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("retrieveListOfEvents")]
        public IEnumerable<EventDetails> retrieveOrganiserEvent()
        {

            var identity = User.Identity as ClaimsIdentity;
            String organiserId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            return EventMgmtBL.retrieveOrganiserEvents(organiserId, connectionString);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("retrieveSessionDetails")]
        public EventDetails getSessionDetails(String eventId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            return EventMgmtBL.retrieveOrgEventSessionDetails(connectionString, eventId, true);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("retrieveVolunteerList")]
        public List<Volunteer> getListOfVolunteers(String sessionDayId, String evtRoleId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            return EventMgmtBL.getListOfVolunteers(connectionString, sessionDayId, evtRoleId);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getListOfEventRoles")]
        public List<SessionRoles> getListOfEventRoles()
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            System.Diagnostics.Debug.WriteLine("before? ");
            return EventMgmtBL.getMasterListOfEventRoles(connectionString);
        }


        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getSessionBySessDay")]
        public SessionDetails getSessionBySessDay(String sessionDayId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();

            return EventMgmtBL.getSessionBySessDay(connectionString, sessionDayId);
        }


        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  search and explore module >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


        [Authorize(Roles = "Volunteer")]
        [HttpPost]
        [ActionName("registerEvent")]
        public void registerEvent([FromBody] SignUpEvent signUp)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;
            SearchExploreBL.volRegisterEvent(signUp, userId);
        }

        [Authorize(Roles ="Volunteer")]
        [HttpPost]
        [ActionName("registerInvitedEvent")]
        public void registerInvitedEvent([FromBody] SignUpEvent signUp)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            SearchExploreBL.volRegisterInvitedEvent(signUp, userId);

        }

        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("retrieveEventListForVol")]
        public List<List<EventDetails>> getEventListForVol()
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;
            return CollaboativeFilteringBL.collaboativeFiltering(userId);
        }

        [HttpGet]
        [ActionName("retrieveEventListForPublic")]
        public List<List<EventDetails>> getEventListForPublic()
        {
            return SearchExploreBL.retrieveEventsForPublic();
        }

        [HttpGet]
        [ActionName("getRecentEventsListByOrg")]
        public IEnumerable<EventDetails> getRecentEventsListByOrg(String connectionName)
        {
            //var identity = User.Identity as ClaimsIdentity;
            return SearchExploreBL.retrieveRecentEventsListByOrg(connectionName);
        }

        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("retrieveSessionDetailsForVol")]
        public VolEventDetail getEventDetailsForVol(String eventId, String connectionString)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            return SearchExploreBL.retrieveEventDetailForVol(userId, eventId, connectionString);
        }

        [HttpGet]
        [ActionName("retrieveSessionDetailsForPublic")]
        public VolEventDetail getEventDetailsForPublic(String eventId, String connectionString)
        {
            return SearchExploreBL.getEventDetailsForPublic(eventId, connectionString);
        }


        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("retrieveVolJoinEvent")]
        public List<List<EventDetails>> listOfVolJoinEvents()
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            return SearchExploreBL.retrieveListOfJoinEvents(userId);
        }


        [HttpPost]
        [ActionName("searchEvent")]
        public List<List<EventDetails>> searchEvents([FromBody] SearchEvent searchParams)
        {
            return SearchExploreBL.searchEvents(searchParams);
        }


        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("getInvitedEvent")]
        public EventDetails getInvitedEvent(string eventId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            return SearchExploreBL.getInvitedEvent(eventId, userId);
        }


        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< FeedBack mgmt module >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("createFeedBack")]
        public void createFeedBack([FromBody] List<FeedBackDesign> listOfFeedBackDetail)
        {
            var identity = User.Identity as ClaimsIdentity;
            String orgId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            FeedBackMgmtBL.createFeedBackDesign(listOfFeedBackDetail, connectionString);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getFeedBackDetail")]
        public List<FeedBackDesign> getFeedBack(String sessionId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FeedBackMgmtBL.getFeedBackDesign(connectionString, sessionId);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("deleteFeedBackDesign")]
        public void deleteFeedBackDesign(String sessionId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
        }

        [HttpGet]
        [ActionName("volGetFeedBack")]
        public List<FeedBackDesign> volgetFeedBack(String sessionId, String connectionString)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            return FeedBackMgmtBL.getFeedBackDesign(connectionString, sessionId);
        }

        [HttpGet]
        [ActionName("sendFeedBack")]
        public String sendFeedBack()
        {
            FeedBackMgmtBL.sendFeedBack();

            return "success";
        }

      
        [HttpPost]
        [ActionName("insertFeedBackResponse")]
        public void insertFeedBackResponse([FromBody] FeedBackResponse feedbackResponse)
        {        
            FeedBackMgmtBL.createFeedBackResponse(feedbackResponse);
        }

        //shihui start
        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getSessionInfo")]
        public SessionDetails getSessionInfo(String sessionId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FeedBackMgmtBL.getSessionDetails(connectionString, sessionId);
        }


        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getFdbackOverallResp")]
        public List<FeedBackDesign> getFdbackOverallResp(String sessionId)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FeedBackMgmtBL.getFdbackOverallResp(connectionString, sessionId);
        }


        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Mobile app >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        [Authorize(Roles = "Volunteer")]
        [HttpPost]
        [ActionName("getCheckInDetails")]
        public CheckIn getCheckInDetails()
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;
            System.Diagnostics.Debug.WriteLine("reacher");
            return SearchExploreBL.getTodayEvent(userId);
        }

        [Authorize(Roles = "Volunteer")]
        [HttpPost]
        [ActionName("checkInVol")]
        public void checkIn([FromBody] CheckIn checkIn)
        {
            var identity = User.Identity as ClaimsIdentity;
            String userId = identity.Name;

            SearchExploreBL.checkInVol(checkIn, userId);
        }

        [HttpGet]
        [ActionName("checkOutVol")]
        public List<Badge> checkOut(String userId)
        {
            return SearchExploreBL.checkOutVol(userId);
        }

    }

}
