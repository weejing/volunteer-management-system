﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.SponsorMgmt;
using VMS.Models.SponsorshipModels;

namespace VMS.Controllers
{
    // kaifu's codes

    public class SponsorshipMgmtController : ApiController
    {

        // 4) Submit sponsorship request (from vendor to SGServe)
        [HttpPost]
        [ActionName("addSponsorshipRequest")]
        public String addSponsorshipRequest(String itemID, int qty, String sponsorID, String collectionDesc, String sponsorshipDesc)
        {
            return SponsorshipBL.addSponsorshipRequest(itemID, qty, sponsorID, collectionDesc, sponsorshipDesc);
        }

        // 4) Submit sponsorship item (from vendor to SGServe)
        [HttpPost]
        [ActionName("createItemToRecord")]
        public String createItemToRecord(String itemName, String itemDesc, String categoryID, String sponsorID, String imagePath)
        {
            return SponsorshipBL.createItemToRecord(itemName, itemDesc, categoryID, sponsorID, imagePath);
        }


        // A2) Update (accept / reject) sponsorship request (from SGServe to vendor)
        // once accept, create or update virtual inventory item [SPR_ITEM]
        [HttpPost]
        [ActionName("acceptSponsorship")]
        public String acceptSponsorship(String sponsorRecordID, String itemName, int qty, String description, String categoryID, String sponsorID, String imagePath)
        {
            VirtualInventoryBL.addOrUpdateItem(itemName, qty, description, categoryID, sponsorID, imagePath);
            return SponsorshipBL.updateSponsorship("accepted", sponsorRecordID);
        }

        [HttpPost]
        [ActionName("rejectSponsorship")]
        public String rejectSponsorship(String sponsorRecordID)
        {
            return SponsorshipBL.updateSponsorship("rejected", sponsorRecordID);
        }



        // 1) Retrieve all sponsored item requests
        [HttpGet]
        [ActionName("getAllSponsoredRequest")]
        public List<SponsorshipRecord> getAllSponsoredRequest()
        {
            return SponsorshipBL.getAllSponsoredRequest();
        }

        // A1) Retreive all pending request
        [HttpGet]
        [ActionName("getAllPendingRequest")]
        public List<SponsorshipRecord> getAllPendingRequest()
        {
            return SponsorshipBL.getAllPendingRequest();
        }

        // 2) Retrieve all sponsored item requests of a category
        /*
        [HttpGet]
        [ActionName("getSponsoredRequestByCategory")]
        public List<SponsorshipRecord> getSponsoredRequestByCategory(String categoryID)
        {
            return SponsorshipBL.getSponsoredRequestByCategory(categoryID);
        }
        */

        // A1) Retrieve all sponsored item requests of a vendor
        [HttpGet]
        [ActionName("getSponsoredRequestByVendorID")]
        public List<SponsorshipRecord> getSponsoredRequestByVendorID(String sponsorID)
        {
            return SponsorshipBL.getSponsoredRequestByVendorID(sponsorID);
        }

        // A1.2) Retrieve all sponsored pending item requests of a vendor
        [HttpGet]
        [ActionName("getSponsoredPendingRequestByVendorID")]
        public List<SponsorshipRecord> getSponsoredPendingRequestByVendorID(String sponsorID)
        {
            return SponsorshipBL.getSponsoredRequestByVendorIDByStatus(sponsorID, "pending");
        }

        [HttpGet]
        [ActionName("getSponsoredAcceptedRequestByVendorID")]
        public List<SponsorshipRecord> getSponsoredAcceptedRequestByVendorID(String sponsorID)
        {
            return SponsorshipBL.getSponsoredRequestByVendorIDByStatus(sponsorID, "accepted");
        }

        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        [HttpGet]
        [ActionName("getSponsoredRequestByRequestID")]
        public SponsorshipRecord getSponsoredRequestByRequestID(String requestID)
        {
            return SponsorshipBL.getSponsoredRequestByRecordID(requestID);
        }


        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        [HttpGet]
        [ActionName("getSponsoredRequestByItem")]
        public SponsorshipRecord getSponsoredRequestByItem(String itemID)
        {
            return SponsorshipBL.getSponsoredRequestByItem(itemID);
        }


        [HttpGet]
        [ActionName("getItemIDbyRequestID")]
        public String getItemIDbyRequestID(String requestID)
        {
            return SponsorshipBL.getItemIDbyRequestID(requestID);
        }

        [HttpGet]
        [ActionName("getSponsorshipItemDetails")]
        public SponsorshipRecord getSponsorshipItemDetails(String itemID)
        {
            return SponsorshipBL.getSponsorshipItemDetails(itemID);
        }   

        // 7) Delete sposorship request (for vendor)
        // once accepted, cannot vendor cannot delete striaght from system, need to personally email to request
        [HttpPost]
        [ActionName("deleteSponsorshipRequestV")]
        public String deleteSponsorshipRequestV(String sponsorRecordID)
        {
            return SponsorshipBL.deleteSponsorshipRequest(sponsorRecordID);
        }

        // 7) Delete sposorship request (for SGServe admin)
        [HttpPost]
        [ActionName("deleteSponsorshipRequest")]
        public String deleteSponsorshipRequest(String sponsorRecordID)
        {
            return SponsorshipBL.deleteSponsorshipRequest(sponsorRecordID);
        }

        [HttpPost]
        [ActionName("removeAcceptedSponsorshipRequest")]
        public String removeAcceptedSponsorshipRequest(String sponsorRecordID)
        {
            return SponsorshipBL.removeAcceptedSponsorshipRequest(sponsorRecordID);
        }

        [HttpGet]
        [ActionName("getItemOnBiddingQtyByRecord")]
        public int getItemOnBiddingQtyByRecord(String sponsorRecordID)
        {
            return SponsorshipBL.getItemOnBiddingQtyByRecord(sponsorRecordID);
        }


        [HttpGet]
        [ActionName("getItemNameList")]
        public List<SponsorshipRecord> getItemNameList(String sponsorVendorID)
        {
            return SponsorshipBL.getItemNameList(sponsorVendorID);
        }

        [HttpGet]
        [ActionName("getIndidivudalSponsoredRequest")]
        public SponsorshipRecord getIndidivudalSponsoredRequest(String requestID)
        {
            return SponsorshipBL.getSponsoredRequestByRecordID(requestID);
        }

        [HttpGet]
        [ActionName("getItemCategoryList")]
        public List<SponsorItemCategory> getItemCategoryList()
        {
            return SponsorshipBL.getItemCategoryList();
        }

        [HttpGet]
        [ActionName("getItemCategoryListForSearch")]
        public List<SponsorItemCategory> getItemCategoryListForSearch()
        {
            return SponsorshipBL.getItemCategoryListForSearch();
        }

        // 6) Update sponsorship request 
        [HttpPost]
        [ActionName("updateSponsorshipRequest")]
        public String updateSponsorshipRequest(String sponsorRecordID, String sponsorItemRecordID, int quantity, String sponsorshipDesc, String collectionDesc)
        {
            return SponsorshipBL.updateSponsorshipRequest(sponsorRecordID, sponsorItemRecordID, quantity, sponsorshipDesc, collectionDesc);
        }

        [HttpPost]
        [ActionName("updateSponsorshipItem")]
        public String updateSponsorshipItem(String sponsorItemRecordID, String itemName, String itemDesc, String itemCatID, String imagePath)
        {
            return SponsorshipBL.updateSponsorshipItem(sponsorItemRecordID, itemName, itemDesc, itemCatID, imagePath);
        }

        [HttpGet]
        [ActionName("searchSponsorshipRequests")]
        public List<SponsorshipRecord> searchSponsorshipRequests(String sponsorName, String itemName, String itemCatID)
        {
            return SponsorshipBL.searchSponsorshipRequests(sponsorName, itemName, itemCatID);
        }

        [HttpGet]
        [ActionName("searchSponsoredRequestByVendor")]
        public List<SponsorshipRecord> searchSponsoredRequestByVendor(String itemName, String itemCatID, String sponsorVendorID)
        {
            return SponsorshipBL.searchSponsoredRequestByVendor(itemName, itemCatID, sponsorVendorID);
        }

    }
}