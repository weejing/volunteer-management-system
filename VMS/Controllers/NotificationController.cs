﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.NotificationMgmt;
using VMS.Models.NotificationModels;

namespace VMS.Controllers
{
    public class NotificationController : ApiController
    {

        [Authorize(Roles = "OneThird, Volunteer, Organiser, Firm")]
        [HttpPost]
        [ActionName("createExamPeriod")]
        public string createNotif([FromBody] Notification notification)
        {          
            return NotificationBL.createNotif(notification);
        }

        [Authorize(Roles = "OneThird, Volunteer, Organiser, Firm")]
        [HttpPost]
        [ActionName("updateToRead")]
        public int updateToRead([FromBody] List<Notification> NotificationList)
        {
            return NotificationBL.updateToRead(NotificationList);
        }

        [Authorize(Roles = "OneThird, Volunteer, Organiser, Firm")]
        [HttpGet]
        [ActionName("getNotifList")]
        public List<Notification> getNotifList(string connection_string, string tablename)
        {
            var identity = User.Identity as ClaimsIdentity;
            String usr_id = identity.Name;
            return NotificationBL.getNotifList(connection_string, tablename, usr_id);
        }

        [Authorize(Roles = "OneThird, Volunteer, Organiser, Firm")]
        [HttpGet]
        [ActionName("getNumUnreadNotif")]
        public int getNumUnreadNotif(string connection_string, string tablename)
        {
            var identity = User.Identity as ClaimsIdentity;
            String usr_id = identity.Name;
            return NotificationBL.getNumUnreadNotif(connection_string, tablename, usr_id);
        }
        }
}