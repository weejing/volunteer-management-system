﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.BusinessLayer.ReportingMgmt;
using VMS.DataLayer.ReportingMgmt;
using VMS.Models.ReportingModels;

namespace VMS.Controllers
{
    public class ReportingController : ApiController
    {
        //Account Management
        //[Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getVolunteerData")]
        public AVolunteerReport getVolunteerData()
        {
            return ReportingBL.getVolunteerDataBL();
        }

        //Volunteer reporting
        [HttpGet]
        [ActionName("getVolNumData")]
        public List<ReportVolunteers> getVolNumData()
        {
            return ReportingBL.getVolNumDataBL(null);
        }

        [HttpGet]
        [ActionName("getVolNumData")]
        public List<ReportVolunteers> getVolNumData(string orgID)
        {
            return ReportingBL.getVolNumDataBL(orgID);
        }

        [HttpGet]
        [ActionName("getVolInterestData")]
        public List<ReportVolunteers> getVolInterestData()
        {
            return ReportingBL.getVolInterestDataBL(null);
        }

        [HttpGet]
        [ActionName("getVolInterestData")]
        public List<ReportVolunteers> getVolInterestData(string orgID)
        {
            return ReportingBL.getVolInterestDataBL(orgID);
        }


        //NPO Reporting
        [HttpGet]
        [ActionName("getTransactionData")]
        public List<ReportTransaction> getTransactionData()
        {
            return ReportingBL.getTransactionDataBL();
        }


        [HttpGet]
        [ActionName("getOrgNumData")]
        public List<ReportNPO> getOrgNumData()
        {
            return ReportingBL.getOrgNumDataBL();
        }

        //EVENT reporting
        [HttpGet]
        [ActionName("getEventData")]
        public List<ReportEvent> getEventData()
        {
            return ReportingBL.getEventDataBL();
        }

        [HttpGet]
        [ActionName("getEventTurnUpData")]
        public List<ReportEvent> getEventTurnUpData()
        {
            return ReportingBL.getEventTurnUpDataBL();
        }

        //NPO-calls
        [HttpGet]
        [ActionName("getNPOEventData")]
        public List<ReportEvent> getNPOEventData(string connectionStr)
        {
            return ReportingBL.getNPOEventDataBL(connectionStr);
        }

        [HttpGet]
        [ActionName("getNPOEventTurnUp")]
        public List<ReportEvent> getNPOEventTurnUp(string orgID, string connectionStr)
         {
            return ReportingBL.getNPOEventTurnUpBL(orgID, connectionStr);
        }

        //VENDOR reporting
        [HttpGet]
        [ActionName("getVendorData")]
        public List<ReportVendor> getVendorData()
        {
            return ReportingBL.getVendorDataBL();
        }

        //overview

        [HttpGet]
        [ActionName("getOverviewData")]
        public List<ReportOverview> getOverviewData()
        {
            return ReportingBL.getOverviewDataBL();
        }

    }
}
