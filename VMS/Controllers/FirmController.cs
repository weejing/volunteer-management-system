﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.AccountMgmt;
using VMS.Models.AccountModels;


namespace VMS.Controllers
{
    public class FirmController : ApiController
    {

        /*Retrieve firm details through token*/
        [Authorize]
        [HttpGet]
        [ActionName("getOwnDetail")]
        public Firm getOwnDetail()
        {
           var identity = User.Identity as ClaimsIdentity;          
            String usr_id = identity.Name;
            return getFirmDetails(usr_id);
        }

        /*Create account*/
        [HttpPost]
        [ActionName("createAccount")]
        public string createAccount([FromBody] Firm firm)
        {
            return FirmBL.createAccount(firm);
        }

        [HttpPost]
        [ActionName("updateAccDetail")]
        public int updateAccDetail([FromBody] Firm firm)
        {
            return FirmBL.updateAccDetail(firm);
        }

        /*Delete Account*/
        /*Role:Admin*/
        [HttpGet]
        [ActionName("deleteAcc")]
        public int deleteAcc(string USR_ID)
        {
            return FirmBL.deleteAcc("CONSOLE_VMS", USR_ID);
        }

        /*Get list of Firms*/
        [HttpGet]
        [ActionName("searchFirms")]
        public IEnumerable<Firm> searchFirms(string firm_name)
        {
            List<Firm> firmList = FirmBL.searchFirms("CONSOLE_VMS", firm_name);
            return firmList;
        }

        /*Get list of Firms*/
        //[HttpGet]
        //[ActionName("getAllFirms")]
        //public IEnumerable<Firm> getAllFirms()
        //{
        //    List<Firm> firmList = FirmBL.getAllFirms("CONSOLE_VMS");
        //    return firmList;
        //}

        /*get employee detail*/
        /*Role:ALL ROLE*/
        [HttpGet]
        [ActionName("getFirmDetails")]
        public Firm getFirmDetails(string USR_ID)
        {
            return FirmBL.getFirmDetails("CONSOLE_VMS", USR_ID);
        }      

        /*Check existing employee email*/
        [HttpGet]
        [ActionName("checkEmailExist")]
        public Boolean checkEmailExist(string EMAIL)
        {
            Boolean gotemail = FirmBL.checkEmailExist(EMAIL);
            return gotemail;
        }

        /*Employee request to reset password*/
        [HttpGet]
        [ActionName("reqResetPw")]
        public Boolean reqResetPw(string url, string email)
        {
            return FirmBL.reqResetPw(url, email);
        }

        /*Employee reset password*/
        [HttpGet]
        [ActionName("resetPw")]
        public Boolean resetPw(string resetpw_code, string newpassword)
        {
            return FirmBL.resetPW(resetpw_code, newpassword);
        }

        /*Employee change password*/
        [HttpGet]
        [ActionName("changePw")]
        public Boolean changePw(string email, string newpassword, string oldpassword)
        {
            return FirmBL.changePw(email, newpassword, oldpassword);
        }
    }
}