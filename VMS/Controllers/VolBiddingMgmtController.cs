﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.VolBiddingMgmt;
using VMS.Models.VolBidModels;

namespace VMS.Controllers
{
    public class VolBiddingMgmtController : ApiController
    {
        /*<<<<<<<<<<    Items    >>>>>>>>>>*/

        [HttpPost]
        [ActionName("createBidItem")]
        public String createBidItem(String itemID, int allocatingAmt, String description, String startDate)
        {
            return BidItemBL.createBidItem(itemID, allocatingAmt, description, startDate);
        }

        [HttpPost]
        [ActionName("updateBidItem")]
        public String updateBidItem(String bidItemID, String description, String startDate)
        {
            return BidItemBL.updateBidItem(bidItemID, description, startDate);
        }

        [HttpGet]
        [ActionName("getAllCurrentBidItems")]
        public List<BidItem> getAllCurrentBidItems()
        {
            return BidItemBL.getAllCurrentBidItems();
        }

        [HttpGet]
        [ActionName("getAllPublishedBidItems")]
        public List<BidItem> getAllPublishedBidItems()
        {
            return BidItemBL.getAllPublishedBidItems();
        }

        [HttpGet]
        [ActionName("getBidItemDetails")]
        public List<BidItem> getBidItemDetails(String bidItemID)
        {
            return BidItemBL.getBidItemDetails(bidItemID);
        }

        [HttpGet]
        [ActionName("getAllBidItems")]
        public List<BidItem> getAllBidItems()
        {
            return BidItemBL.getAllBidItems();
        }


        /*<<<<<<<<<<    Transactions    >>>>>>>>>>*/

        [HttpPost]
        [ActionName("addPointsForCompleteEvent")]
        public String addPointsForCompleteEvent(String userID, int hoursCompleted, String eventID)
        {
            return BiddingTransactionBL.addPointsForCompleteEvent(userID, hoursCompleted, eventID);
        }

        [HttpPost]
        [ActionName("addPointsForSpinWheel")]
        public String addPointsForSpinWheel(String userID, int pointsEarned)
        {
            return BiddingTransactionBL.addPointsForSpinWheel(userID, pointsEarned);
        }

        [HttpPost]
        [ActionName("updateVolVIAhours")]
        public String updateVolVIAhours(int hoursCompleted, String userID)
        {
            return BiddingTransactionBL.updateVolVIAhours(hoursCompleted, userID);
        }

        [HttpPost]
        [ActionName("createPlaceBid")]
        public String createPlaceBid(String userID, int bidAmount, String bidItemID)
        {
            return BiddingTransactionBL.createPlaceBid(userID, bidAmount, bidItemID);
        }

        [HttpPost]
        [ActionName("updatePointsForBidChanges")]
        public String updatePointsForBidChanges(String userID, int bidAmount, int changeInBidAmt, String bidItemID)
        {
            return BiddingTransactionBL.updatePointsForBidChanges(userID, bidAmount, changeInBidAmt, bidItemID);
        }

        [HttpPost]
        [ActionName("updatePointsForWithdrawBid")]
        public String updatePointsForWithdrawBid(String userID, int withdrawBidAmount, String bidItemID)
        {
            return BiddingTransactionBL.withdrawBidandUpdatePoints(userID, withdrawBidAmount, bidItemID);
        }

        [HttpGet]
        [ActionName("getVolBidPointsTransaction")]
        public List<BidPointTransaction> getVolBidPointsTransaction(String userID)
        {
            return BiddingTransactionBL.getVolBidPointsTransaction(userID);
        }

        [HttpGet]
        [ActionName("canUserSpinWheel")]
        public String canUserSpinWheel(String userID)
        {
            return BiddingTransactionBL.canUserSpinWheel(userID);
        }

        [HttpGet]
        [ActionName("getVolAllBidItemRecord")]
        public List<BidRecord> getVolAllBidItemRecord(String userID)
        {
            return BiddingTransactionBL.getVolAllBidItemRecord(userID);
        }

        [HttpGet]
        [ActionName("getVolBidItemRecordWon")]
        public List<BidRecord> getVolBidItemRecordWon(String userID)
        {
            return BiddingTransactionBL.getVolBidItemRecordWon(userID);
        }

        [HttpGet]
        [ActionName("getVolCurrentBidItemRecord")]
        public List<BidRecord> getVolCurrentBidItemRecord(String userID)
        {
            return BiddingTransactionBL.getVolCurrentBidItemRecord(userID);
        }

        [HttpGet]
        [ActionName("getVolCurrentBidItemRecordDetails")]
        public List<BidRecord> getVolCurrentBidItemRecordDetails(String userID, String bidItemID)
        {
            return BiddingTransactionBL.getVolCurrentBidItemRecordDetails(userID, bidItemID);
        }

        [HttpGet]
        [ActionName("getVolunteerCurrentBidPoints")]
        public int getVolunteerCurrentBidPoints(String userID)
        {
            return BiddingTransactionBL.getVolunteerCurrentBidPoints(userID);
        }

        [HttpGet]
        [ActionName("getBidItemRecordDetails")]
        public List<BidRecord> getBidItemRecordDetails(String bidItemID)
        {
            return BiddingTransactionBL.getBidItemRecordDetails(bidItemID);
        }

        [HttpGet]
        [ActionName("systemDetermineWinner")]
        public String systemDetermineWinner()
        {
            BiddingTransactionBL.systemDetermineWinner();
            return "done";
        }


    }
}