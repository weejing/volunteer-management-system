﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AuditMgmt;
using VMS.BusinessLayer.HrMgmt;
using VMS.Models.HrModels;

//This will be written by debbie
namespace VMS.Controllers
{
    public class HRController : ApiController
    {
        /// <summary>Controller: Insert a new external volunteer record</summary>
        /// <Role> HR Officer</Role>
        [HttpPost]
        [ActionName("addOneRecord")]
        public void addOneRecord([FromBody] ExternalVolunteerDetails newInfo)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            AuditMgmtBL.createAuditObj(3, 5, "Create New External Volunteer", "HRController.addOneRecord", "NPO", userID, newInfo.CONNECTION_STRING);
            HrMgmtBL.createOneRecord(newInfo);
        }
        /// <summary>Controller: Check if external volunteer exist</summary>
        [HttpGet]
        [ActionName("checkVolunteerExist")]
        public Boolean checkVolunteerExist(string name, string contact, string email, string connectionstring)
        {
            Boolean gotItem = HrMgmtBL.checkVolunteerExist(name,email,contact, connectionstring);
            return gotItem;
        }
        /// <summary>Controller: Get list of External Volunteer List from database</summary>
        //[Authorize]
        [HttpGet]
        [ActionName("getExternalVolList")]
        public IEnumerable<ExternalVolunteerDetails> getExternalVolList(string connectionstring)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            AuditMgmtBL.createAuditObj(3, 5, "Get List of External Volunteer", "HRController.getExternalVolList", "NPO", userID, connectionstring);
            List<ExternalVolunteerDetails> externalvollist = HrMgmtBL.getExternalVolList(connectionstring);
            return externalvollist;
        }
        /// <summary>Controller: Delete external volunteer</summary>
        /// <Role> HR Officer</Role>
        [HttpGet]
        [ActionName("deleteExternalVol")]
        public int deleteExternalVol(string NAMELIST_ID, string connectionstring)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            AuditMgmtBL.createAuditObj(3, 5, "Delete External Volunteer Record", "HRController.deleteExternalVol", "NPO", userID, connectionstring);
            return HrMgmtBL.deleteExternalVol(NAMELIST_ID, connectionstring);
        }

        /// <summary>Controller: Get event info</summary>
        /// <Role> HR Officer</Role>
        [HttpGet]
        [ActionName("getUpcomingEvents")]
        public IEnumerable<OpenEvents> getUpcomingEvents(string connectionstring)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            AuditMgmtBL.createAuditObj(3, 5, "Get List of Upcoming Events", "HRController.getUpcomingEvents", "NPO", userID, connectionstring);
            List<OpenEvents> eventinfo = HrMgmtBL.getUpcomingEvents(connectionstring);
            return eventinfo;
        }
    }
}
