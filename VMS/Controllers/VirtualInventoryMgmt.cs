﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.SponsorMgmt;
using VMS.Models.SponsorshipModels;

namespace VMS.Controllers
{
    public class VirtualInventoryMgmtController : ApiController
    {
        [HttpGet]
        [ActionName("getAllItems")]
        public List<VirtualItemSponsor> getAllItems()
        {
            return VirtualInventoryBL.getAllItems();
        }

        [HttpGet]
        [ActionName("searchItemsList")]
        public List<VirtualItemSponsor> searchItemsList(String itemName, String categoryID)
        {
            return VirtualInventoryBL.searchItemsList(itemName, categoryID);
        }

        [HttpGet]
        [ActionName("getIndidivudalItem")]
        public VirtualItemSponsor getIndidivudalItem(String itemID)
        {
            return VirtualInventoryBL.getIndidivudalItem(itemID);
        }

        [HttpPost]
        [ActionName("returnQuantityToInventory")]
        public String returnQuantityToInventory(String itemID, String bidItemID, int returningAmt)
        {
            return VirtualInventoryBL.returnQuantityToInventory(itemID, bidItemID, returningAmt);
        }

        [HttpGet]
        [ActionName("removeSponsoredItem")]
        public void removeSponsoredItem(String itemID)
        {
            //return VirtualInventoryBL.removeSponsoredItem(itemID);
        }
    }
}