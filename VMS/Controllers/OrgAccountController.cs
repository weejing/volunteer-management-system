﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AdminMgmt;
using VMS.BusinessLayer.AuditMgmt;
using VMS.BusinessLayer.OrgAccountMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.Controllers
{
    // this controller handles events organisation account mgmt like logging out, account creation
    public class OrgAccountController : ApiController
    {
        /*Admin create employee account*/
        /*Role:Admin*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("createAccount")]
        public string createAccount([FromBody] OrgEmployee employee)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string stakeholder = "NPO";
            if (connectionString == null || connectionString.Equals(""))
            {
                stakeholder = "Internal";
            }
            AuditMgmtBL.createAuditObj(1, 1, "Create "+employee.NAME+" account", "OrgAccountController.createAccount", stakeholder, userID, connectionString);
            return OrgAccBL.createAccount(employee);
        }

        /*Update Account detail*/
        /*Role:ALL ROLES*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("updateAccDetail")]
        public int updateAccDetail([FromBody] OrgEmployee employee)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string stakeholder = "NPO";
            if (connectionString == null || connectionString.Equals(""))
            {
                stakeholder = "Internal";
            }
            AuditMgmtBL.createAuditObj(1, 1, "Update " + employee.NAME + " account", "OrgAccountController.updateAccDetail", stakeholder, userID, connectionString);
            return OrgAccBL.updateAccDetail(employee);
        }

        /*Delete Account*/
        /*Role:Admin*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("deleteAcc")]
        public int deleteAcc(string connection_string, string USR_ID)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string stakeholder = "NPO";
            if (connectionString == null || connectionString.Equals(""))
            {
                stakeholder = "Internal";
            }
            AuditMgmtBL.createAuditObj(1, 1, "Delete account", "OrgAccountController.deleteAcc", stakeholder, userID, connectionString);
            return OrgAccBL.deleteAcc(connection_string,USR_ID);
        }

        /*Get list of employees*/
        /*Role:Admin*/
        [HttpGet]
        [ActionName("getEmployees")]
        public IEnumerable<OrgEmployee> getEmployees(string connection_string)
        {
            List<OrgEmployee> employeeList = OrgAccBL.getEmployees(connection_string);
            return employeeList;
        }

        /*get employee detail*/
        /*Role:ALL ROLE*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getEmpParticular")]
        public OrgEmployee getEmpParticular(string connection_string, string USR_ID) {
            return OrgAccBL.getEmpParticular(connection_string,USR_ID);
        }

        /*Search for employees*/
        [HttpGet]
        [ActionName("searchEmployees")]
        public IEnumerable<OrgEmployee> searchEmployees(string connection_string, string name)
        {
            return OrgAccBL.searchEmployees(connection_string,name);
        }

        /*Check existing employee email*/
        [HttpGet]
        [ActionName("checkEmailExist")]
        public Boolean checkEmailExist(string connection_string, string EMAIL)
        {
            Boolean gotemail = OrgAccBL.checkEmailExist(connection_string,EMAIL);
            return gotemail;
        }

        /*Retrieve login employee particulars*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getLoginEmpDetails")]
        public OrgEmployee getLoginEmpDetails(string connection_string)
        {
            var identity = User.Identity as ClaimsIdentity;
            String usr_id = identity.Name;
            return OrgAccBL.getEmpParticular(connection_string,usr_id);
        }

        /*Employee request to reset password*/
        [HttpGet]
        [ActionName("reqResetPw")]
        public Boolean reqResetPw(string connection_string, string url, string email)
        {
            return OrgAccBL.reqResetPw(connection_string, url,email);
        }

        /*Employee reset password*/
        [HttpGet]
        [ActionName("resetPw")]
        public Boolean resetPw(string connection_string, string resetpw_code, string newpassword)
        {
            return OrgAccBL.resetPW(connection_string, resetpw_code, newpassword);
        }

        /*Employee change password*/
        [HttpGet]
        [ActionName("changePw")]
        public Boolean changePw(string connection_string, string email, string newpassword, string oldpassword)
        {
            return OrgAccBL.changePw(connection_string, email, newpassword, oldpassword);
        }

        #region Role <-> Pages access rights
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getPageAccessRights")]
        public IEnumerable<Page> getPageAccessRights(string usr_id, string CONNECTION_STRING)
        {
            return RoleBL.getPageAccessRights(usr_id, CONNECTION_STRING);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getBillPage")]
        public IEnumerable<Page> getBillPage()
        {
            var identity = User.Identity as ClaimsIdentity;
            String organiserId = identity.Name;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return RoleBL.getBillPage(connectionString);
        }
        #endregion
    }
}
