﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Models.TransactionModels;
using VMS.BusinessLayer.TransactionMgmt;
using VMS.Models.TenderingModels;
using VMS.Models.AdminModels;
using VMS.DataLayer.ReportingMgmt;
using VMS.BusinessLayer.ReportingMgmt;
using VMS.BusinessLayer.AuditMgmt;
using System.Security.Claims;

namespace VMS.Controllers
{
    // jaslyn write your codes here
    public class TransactionController : ApiController
    {
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("insertNewPayment")]
        public String insertNewPayment([FromBody] OrgPayment newPayment)
        {
            return TransactionBL.insertNewPayment(newPayment);
        }


        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("checkBillStatus")]
        public BillingStatus checkBillStatus([FromBody] BillingStatus bill)
        {
            return TransactionBL.checkBillStatus(bill);
        }

        //PAYMENT PLANS -RELATED CODES
        //===============================
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("addPlan")]
        public List<OrgPayment> addPlan(string planName, string amount, string duration, string feature1, string feature2, string feature3)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Add New Payment Plan [Plan Name: " + planName + "]", "OrgMgmtController.addPlan", "Internal", userID, connectionString);

            return TransactionBL.addPlanBL(planName, amount, duration, feature1, feature2, feature3);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getAllPaymentPlan")]
        public List<OrgPayment> getAllPaymentPlan()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string role = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();

            AuditMgmtBL.createAuditObj(9, 18, "Get All Payment Plan", "OrgMgmtController.getAllPaymentPlan", "Internal", userID, connectionString);

            return TransactionBL.getAvaliablePaymentPlan("0");
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("updatePaymentPlan")]
        public List<OrgPayment> updatePaymentPlan(string id, string feature1, string feature2, string feature3)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Update Payment Plan [Plan ID: " + id + "]", "OrgMgmtController.updatePaymentPlan", "Internal", userID, connectionString);

            return TransactionBL.updatePaymentPlanBL(id, feature1, feature2, feature3);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("updatePaymentPlanStatus")]
        public List<OrgPayment> updatePaymentPlanStatus(string id, string status)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Update Payment Plan Status [Plan ID: " + id + "]", "OrgMgmtController.updatePaymentPlanStatus", "Internal", userID, connectionString);

            return TransactionBL.updatePaymentPlanStatusBL(id, status);
        }

        //TRANSACTION-RELATED CODES
        //===============================
        [HttpPost]
        [ActionName("createNewTransaction")]
        public Transaction createNewTransaction([FromBody] OrgPayment OrgPayment)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "NPO make payment to SGServe", "OrgMgmtController.createNewTransaction", "NPO", userID, connectionString);

            return TransactionBL.createNewTransaction(OrgPayment);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getAllTransactions")]
        public List<OrgPayment> getAllTransactions()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get all transactions", "OrgMgmtController.getAllTransactions", "Internal", userID, connectionString);

            return TransactionBL.getTransactionsListBL(0, null, null, null);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getTransactionsSearch")]
        public List<OrgPayment> getTransactionsSearch(String name, String start, String end)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get transaction search result", "OrgMgmtController.getTransactionsSearch", "Internal", userID, connectionString);

            return TransactionBL.getTransactionsListBL(1, name, start, end);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getIndividualTransaction")]
        public OrgPayment getIndividualTransaction(String org_pay_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get Individual Transaction", "OrgMgmtController.getIndividualTransaction", "Internal", userID, connectionString);

            return TransactionBL.getIndividualTransactionBL(org_pay_id);
        }

        //report-related codes
        //------------------------
        //Pre-condtion: none
        //TO DO: Get the number of organisations vs expected in SGServe
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getPaymentTrend")]
        public void getPaymentTrend()
        {
            //return TransactionBL.getPaymentTrendBL();
        }

        //RELATIONSHIP MANAGEMENT RELATED CODES
        //======================================

        //get organisation list for relationship management
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getOrganisationList")]
        public List<OrganisationDetails> getOrganisationList()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get List of Active Organisation", "OrgMgmtController.getOrganisationList", "Internal", userID, connectionString);

            return TransactionBL.getOrganisationListBL();
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getOrgPlanInfo")]
        public List<string> getOrgPlanInfo(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "Get plan that NPO has", "OrgMgmtController.getOrgPlanInfo", "NPO", userID, connectionString);

            return TransactionBL.getOrgPlanInfoBL(org_id);
        }


        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getProfileID")]
        public string getProfileID(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "Get NPO's PayPal Profile ID", "OrgMgmtController.getProfileID", "NPO", userID, connectionString);

            return TransactionBL.getProfileBL(org_id);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("cancelPayment")]
        public string cancelPayment(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "NPO Cancel Payment Plan", "OrgMgmtController.cancelPayment", "NPO", userID, connectionString);
            return TransactionBL.cancelPaymentBL(org_id);
        }

        //search for organisation that matches criteria
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getOrganisationSearch")]
        public List<OrganisationDetails> getOrganisationSearch(String name, String email, String type, String check)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get NPO Search Result", "OrgMgmtController.getOrganisationSearch", "Internal", userID, connectionString);

            return TransactionBL.getOrganisationSearchBL(name, email, type, check);
        }

        //get transaction search for org
        [Authorize(Roles = "OneThird, Organiser")]
        [HttpGet]
        [ActionName("getOrgTransaction")]
        public List<OrgPayment> getOrgTransaction(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "Get NPO's Transaction", "OrgMgmtController.getOrgTransaction", "", userID, connectionString);

            return TransactionBL.getOrgTransactionBL(org_id);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getOrgDetails")]
        public OrgPayment getOrgDetails(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            //moduleType, moduleFeatureType, action, description, systemUser, userID, connection
            AuditMgmtBL.createAuditObj(1, 1, "Get Organisation Details", "OrgMgmtController.getOrgDetails", "Internal", userID, connectionString);

            return TransactionBL.getOrgDetailsBL(org_id);
        }

        [HttpGet]
        [ActionName("getAvaliablePaymentPlan")]
        public List<OrgPayment> getAvaliablePaymentPlan()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string role = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 10, "Get All Avaliable Payment Plan", "OrgMgmtController.getAvaliablePaymentPlan", "", userID, connectionString);
            
            return TransactionBL.getAvaliablePaymentPlan("1");
        }


        [Authorize(Roles = "OneThird, Organiser")]
        [HttpGet]
        [ActionName("getOrgDiscountData")]
        public List<CRM> getOrgDiscountData(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            string role = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();

            AuditMgmtBL.createAuditObj(9, 18, "Get NPO's Discounts", "OrgMgmtController.getOrgDiscountData", "", userID, connectionString);

            return TransactionBL.getOrgDiscountDataBL(org_id);
        }


        //update and return crm table
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getUpdatedCRM")]
        public List<CRM> getUpdatedCRM(string org_id, string amt, string date, string plan_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Update CRM", "OrgMgmtController.getUpdatedCRM", "Internal", userID, connectionString);

            return TransactionBL.getUpdatedCRMBL(0, org_id, amt, date, null, plan_id);
        }

        //return crm table
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getCRM")]
        public List<CRM> getCRM(string org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get All CRM", "OrgMgmtController.getCRM", "Internal", userID, connectionString);

            return TransactionBL.getUpdatedCRMBL(1, org_id, null, null, null, null);
        }

        //delete crm record
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("deleteCRM")]
        public List<CRM> deleteCRM(string org_id, string crm_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Delete CRM", "OrgMgmtController.deleteCRM", "Internal", userID, connectionString);

            return TransactionBL.getUpdatedCRMBL(2, org_id, null, null, crm_id, null);
        }

        //update org verification
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("updateVerificationStatus")]
        public String updateVerificationStatus(string org_id, string verify)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Update Organisation Verify Status", "OrgMgmtController.updateVerificationStatus", "Internal", userID, connectionString);

            return TransactionBL.updateVerificationStatusBL(org_id, verify);
        }

        //BILLING RELATED CODES
        //=========================================
        //working on this -- to give to front end
        //Pre-condition: 
        // (current_month, current_year, search_type)
        // 0, 0 - current month and year
        //for search type
        //-1 - earlier than that date
        // 0 - exact month
        // 1 - later than that date
        // 2 - show all recordss
        //TO DO: get all the transactions
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getAllBillings")]
        public List<BillingStatus> getAllBillings()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get All Sales Transaction", "OrgMgmtController.getAllBillings", "Internal", userID, connectionString);

            return TransactionBL.getBillingStatusListBL(0, 0, 2);
        }

        //Pre-condition: 
        // (current_month, current_year, search_type)
        // 0, 0 - current month and year
        //for search type
        //-1 - earlier than that date
        // 0 - exact month
        // 1 - later than that date
        // 2 - show all recordss
        //TO DO: get unpaid organisation
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("getUnpaidBillList")]
        public List<BillingStatus> getUnpaidBillList()
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get all unpaid bills", "OrgMgmtController.getUnpaidBillList", "Internal", userID, connectionString);

            return TransactionBL.getUnpaidBillListBL(0, 0, 2);
        }

        //For Organisation only
        [Authorize(Roles = "Organisation")]
        [HttpGet]
        [ActionName("getOrgBillDetails")]
        public List<OrgPayment> getOrgBillDetails(String org_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(9, 18, "Get all Organisation bills", "OrgMgmtController.getOrgBillDetails", "NPO", userID, connectionString);

            return TransactionBL.getOrgBillDetailsBL(org_id);
        }


        [HttpGet]
        [ActionName("getTimeTriggerEvents")]
        public string getTimeTriggerEvents(string key)
        {
            return TransactionBL.getTimeTriggerEvents(key);
        }
    }
}
