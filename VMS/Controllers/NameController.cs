﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using VMS.Models.NameModels;
using System.Collections;
using Newtonsoft.Json;

namespace VMS.Controllers
{
    // this is a dummy controller file for you guys to refer to 
    public class NameController : ApiController
    {
        [Authorize]
        [HttpGet]
        [ActionName("Namesss")]
        public IEnumerable<object> method()
        {
            var identity = User.Identity as ClaimsIdentity;
            String name = identity.Name;

            String table_name = identity.Claims.Where(c => c.Type == ClaimTypes.Actor ).Select(c => c.Value).SingleOrDefault();


            // name is the user_id
            System.Diagnostics.Debug.WriteLine("name:" + name);
            System.Diagnostics.Debug.WriteLine("table name: " + table_name);

            return identity.Claims.Select(c => new
            {             
                Type = c.Type,
                Value = c.Value
            });
           // return "hello world";
        }


        [HttpGet]
        [ActionName("key")]
        public HttpResponseMessage Get()
        {
           // var identity = User.Identity
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
            response.Content = new StringContent("hello", Encoding.Unicode);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };
            return response;
        }

        [HttpGet]
        [ActionName("String")]
        public String getString(String hello, String world)
        {
            return hello +" " + world;
        }

        
        [HttpGet]
        [ActionName("Object")]
        public IEnumerable<object> getObject()
        {
            Name name = new Name();
            List<Name> listOfNames = new List<Name>();
            listOfNames.Add(new Name{name ="name", age=30 });
            listOfNames.Add(name);

            return listOfNames;
        }


        // A name class must be created, as asp.net will handle the seralization of the data for you 
        [HttpPost]
        [ActionName("PostName")]
        public void doPost([FromBody] Name n )
        {
            System.Diagnostics.Debug.WriteLine(n.name);
            System.Diagnostics.Debug.WriteLine(n.age);
        }

        [HttpPost]
        [ActionName("complex")]
        public void complexList(ArrayList paramList)
        {
            for(int count =0; count < paramList.Count; count++)
            {
                Name name = JsonConvert.DeserializeObject<Name>(paramList[count].ToString());
                System.Diagnostics.Debug.WriteLine(name.name);
                System.Diagnostics.Debug.WriteLine(name.age);

            }
        }
        
    }
}
