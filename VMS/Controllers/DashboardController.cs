﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AccountMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.Controllers
{
    public class DashboardController : ApiController
    {
        [HttpGet]
        [ActionName("getNPO")]
        public IEnumerable<String> getNPO(string volunteerid)
        {
            return DashboardBL.getNPO(volunteerid);
        }

        [HttpGet]
        [ActionName("getCompletedVIA")]
        public int getCompletedVIA(string connectionstring, string volunteerid)
        {
            return DashboardBL.getCompletedVIA(connectionstring, volunteerid);
        }

        [HttpGet]
        [ActionName("getCompletedEvents")]
        public IEnumerable<UserPastEvents> getCompletedEvents(string connectionstring, string volunteerid)
        {
            List<UserPastEvents> listOfPastEvent = DashboardBL.getCompletedEvents(connectionstring, volunteerid);
            System.Diagnostics.Debug.WriteLine("count: " +  listOfPastEvent.Count);
            return listOfPastEvent;
        }

        [HttpGet]
        [ActionName("getUpcomingEvents")]
        public IEnumerable<UserPastEvents> getUpcomingEvents(string connectionstring, string volunteerid)
        {
            return DashboardBL.getUpcomingEvents(connectionstring, volunteerid);
        }
    }
}