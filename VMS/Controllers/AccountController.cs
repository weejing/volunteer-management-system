﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AccountMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.Controllers
{
   // shi hui write your account mgmt codes here 
    public class AccountController : ApiController
    {
  
        [HttpGet]
        [ActionName("VolcheckEmailExist")]
        public Boolean VolcheckEmailExist(string EMAIL)
        {
            Boolean gotemail = VolBL.checkEmailExist(EMAIL);
            return gotemail;
        }

         /*volunteer register with email*/
        [HttpPost]
        [ActionName("createAccNoFB")]
        public string createAccNoFB([FromBody] Volunteer volunteer)
        {
            return VolBL.createAccNoFB(volunteer);
        }

        /*volunteer register with facebook*/
        [HttpPost]
        [ActionName("createAccFB")]
        public string volRegisterFB([FromBody] Volunteer volunteer)
        {
            return VolBL.createAccFB(volunteer);
        }

        /*Volunter activate account*/
        [HttpGet]
        [ActionName("updateAccActivated")]
        public int updateAccActivated(string ActivationCode)
        {
            return VolBL.updateAccActivated(ActivationCode);
        }

        /*Retrieve volunteer particulars
         Role: Volunteer*/
        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("retrieveUserDetails")]
        public Volunteer retrieveUserID()
        {
            var identity = User.Identity as ClaimsIdentity;
            String usr_id = identity.Name;
             /*get volunteer particulars*/
            return VolBL.getVolParticular(usr_id);
        }


        /*Volunter request to reset password*/
        [HttpGet]
        [ActionName("reqResetPw")]
        public Boolean reqResetPw(string email,string url)
        {
            return VolBL.reqResetPw(email, url);
        }

        /*Volunter reset password*/
        [HttpGet]
        [ActionName("resetPw")]
        public Boolean resetPw(string resetpw_code, string newpassword)
        {
            return VolBL.resetPW(resetpw_code, newpassword);
        }

        /*Volunter change password*/
        [HttpGet]
        [ActionName("changePw")]
        public Boolean changePw(string email, string newpassword, string oldpassword)
        {
            return VolBL.changePw(email, newpassword, oldpassword);
        }

        /*Update Account detail*/
        [HttpPost]
        [ActionName("updateAccDetail")]
        public int updateAccDetail([FromBody] Volunteer volunteer)
        {
            return VolBL.updateAccDetail(volunteer);
        }

        /*Get Volunteer Interest form records - Cause, Skills, roles */
        [HttpGet]
        [ActionName("getVolInterests")]
        public IEnumerable<TermRelation> getVolInterests(string usr_id, string vocab_name)
        {
            return VolBL.getVolInterestForm(usr_id, vocab_name);
        }

        /*Update volunteer interest form records - Interests, Skills, Preferred Days*/
        [HttpPost]
        [ActionName("updateInterestForm")]
        public int updateInterestForm([FromBody] Volunteer volunteer)
        {
            return VolBL.updateInterestForm(volunteer);
        }

        /*set VIA goals */
        //[HttpPost]
        //[ActionName("setVIAgoal")]
        //public void setVIAgoal([FromBody] Volunteer volunteer)
        //{
        //    VolBL.setVIAgoal(volunteer.VOL_ID, volunteer.VIA_TARGET);
        //}

        /*Get list of voluneers*/
        /*Role:Admin*/
        [HttpGet]
        [ActionName("getVolunteers")]
        public IEnumerable<Volunteer> getVolunteers(string connection_string)
        {
            List<Volunteer> volunteerList = VolBL.getVolunteers(connection_string);
            return volunteerList;
        }

        /*Get volunteer particulars*/
        [HttpGet]
        [ActionName("getVolParticular")]
        public Volunteer getVolParticular(string usr_id)
        {
            return VolBL.getVolParticular(usr_id);
        }

        /*Get volunteer by name LIKE*/
        [HttpGet]
        [ActionName("searchVol")]
        public IEnumerable<Volunteer> searchVol(string name)
        {
            return VolBL.searchVol(name);
        }

        /*Get volunteer completed event*/
        [HttpGet]
        [ActionName("getUserPastEvents")]
        public IEnumerable<UserPastEvents> getUserPastEvents(string connectionstring, string volunteerid)
        {
            return VolBL.getUserPastEvents(connectionstring, volunteerid);
        }

        /*Get volunteer completed event details*/
        [HttpGet]
        [ActionName("getUserPastEventDetails")]
        public IEnumerable<UserPastEvents> getUserPastEventDetails(string connectionstring, string eventid)
        {
            return VolBL.getUserPastEventDetails(connectionstring, eventid);
        }

    }
}
