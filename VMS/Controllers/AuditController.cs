﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VMS.BusinessLayer.AuditMgmt;
using VMS.DataLayer.AuditMgmt;
using VMS.Models.AuditMgmtModels;

namespace VMS.Controllers
{
    public class AuditController : ApiController
    {
        [HttpPost]
        [ActionName("getAuditSaaSList")]
        public IEnumerable<AuditTrail> getAuditSaaSList([FromBody] AuditTrail connect)
        {
            string connection_str = connect.CONNECTION_STRING;
            List<AuditTrail> auditList = AuditMgmtBL.getAuditListSaaS(connection_str);
            return auditList;
        }
    }
}