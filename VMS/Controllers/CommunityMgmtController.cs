﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using VMS.BusinessLayer.AuditMgmt;
using VMS.BusinessLayer.CommunityMgmt;
using VMS.Models.CommunityModels;
using VMS.Models.OrgMgmtModels;

namespace VMS.Controllers
{
    // kaifu write your codes here
    public class CommunityMgmtController : ApiController
    {
        /*<<<<<<<<<<    Organisation & Volunteer Lists    >>>>>>>>>>*/
            
        [HttpGet]
        [ActionName("getUserIDs")]
        public List<String> getUserIDs()
        {
            return UserPortfolioBL.getUserIDs();
        }

        [HttpGet]
        [ActionName("getListofUsersBySearch")]
        public List<UserPortfolio> getListofUsersBySearch(String name)
        {
            return UserPortfolioBL.getListofUsersBySearch(name);
        }

        [HttpGet]
        [ActionName("getAllListofOrganisations")]
        public List<OrganizationDetails> getAllListofOrganisations()
        {
            return UserPortfolioBL.getAllListofOrganisations();
        }

        [HttpGet]
        [ActionName("getListofOrganisationsBySearch")]
        public List<OrganizationDetails> getListofOrganisationsBySearch(String name)
        {
            return UserPortfolioBL.getListofOrganisationsBySearch(name);
        }


        /*<<<<<<<<<<    Volunteer Portfolio    >>>>>>>>>>*/

        [HttpGet]
        [ActionName("getUserDetails")]
        public UserPortfolio getUserDetails(String userID)
        {
            UserPortfolio up = new UserPortfolio();
            up = UserPortfolioBL.getUserDetails(userID);
            up.userInterest = UserPortfolioBL.getUserInterests(userID);
            up.userSkill = UserPortfolioBL.getUserSkills(userID);
            return up;
        }

        [HttpPost]
        [ActionName("updateUserPortfolio")]
        public String updateUserPortfolio(String userID, String description)
        {
            return UserPortfolioBL.updateUserPortfolio(userID, description);
        }


        [HttpGet]
        [ActionName("getUserFriends")]
        public List<UserPortfolio> getUserFriends(String userID)
        {        
            return UserPortfolioBL.getUserFriends(userID);
        }

        [HttpGet]
        [ActionName("getUserFriendsRequest")]
        public List<UserPortfolio> getUserFriendsRequest(String userID)
        {
            return UserPortfolioBL.getUserFriendsRequest(userID);
        }

        [HttpGet]
        [ActionName("checkfriends")]
        public String checkfriends(String userID1, String userID2)
        {
            String status = UserPortfolioBL.checkfriends(userID1, userID2);

            if (status == "not friends")
            {
                return UserPortfolioBL.checkfriends(userID2, userID1);
            }
            else
            {
                return status;
            }
        }


        [HttpPost]
        [ActionName("addUserFriend")]
        public String addUserFriend(String userID1, String userID2)
        {
            return UserPortfolioBL.addUserFriend(userID1, userID2);
        }

        //junhau pls call this
        [HttpGet]
        [ActionName("addUserFriendWithQR")]
        public String addUserFriendWithQR(String userID1, String userID2)
        {
            return UserPortfolioBL.addUserFriendWithQR(userID1, userID2);
        }

        [HttpPost]
        [ActionName("acceptFriendStatus")]
        public String acceptFriendStatus(String userID1, String userID2)
        {
            String reply = UserPortfolioBL.acceptFriendStatus(userID1, userID2);

            if (reply.Equals(null))
            {
                return UserPortfolioBL.acceptFriendStatus(userID2, userID1);
            }
            else
            {
                return reply;
            }
        }
        [HttpGet]
        [ActionName("acceptFriendStatusWithQR")]
        public String acceptFriendStatusWithQR(String userID1, String userID2)
        {
            String reply = UserPortfolioBL.acceptFriendStatus(userID1, userID2);

            if (reply.Equals(null))
            {
                return UserPortfolioBL.acceptFriendStatus(userID2, userID1);
            }
            else
            {
                return reply;
            }
        }
        [HttpPost]
        [ActionName("deleteFriend")]
        public String deleteFriend(String userID1, String userID2)
        {
            String reply = UserPortfolioBL.checkfriendID(userID1, userID2);

            if (reply.Equals(""))
            {
                NewsfeedBL.removeActivity(UserPortfolioBL.checkfriendID(userID2, userID1));
                return UserPortfolioBL.deleteFriend(userID2, userID1);
            }
            else
            {
                NewsfeedBL.removeActivity(UserPortfolioBL.checkfriendID(userID1, userID2));
                return UserPortfolioBL.deleteFriend(userID1, userID2);
            }
        }

        [HttpPost]
        [ActionName("removeFriend")]
        public String removeFriend(String userID1, String userID2)
        {
            String reply = UserPortfolioBL.checkfriendID(userID1, userID2);

            if (reply.Equals(""))
            {
                return UserPortfolioBL.deleteFriend(userID2, userID1);
            }
            else
            {
                return UserPortfolioBL.deleteFriend(userID1, userID2);
            }
        }

        /*
        [HttpPost]
        [ActionName("followOrganisation")]
        public String followOrganisation(String userID, String orgID)
        {
            return UserPortfolioBL.followOrganisation(userID, orgID);
        }
        */

        [HttpPost]
        [ActionName("followOrganisation")]
        public String followOrganisation(String userID, String orgID)
        {
            if (UserPortfolioBL.userFollowOrganisation(userID, orgID))
            {
                return UserPortfolioBL.unfollowOrganisation(userID, orgID);
            }
            else
            {
                return UserPortfolioBL.followOrganisation(userID, orgID);
            }
        }

        [HttpGet]
        [ActionName("getOrgF")]
        public Boolean getOrgF(String userID, String orgID)
        {
            return UserPortfolioBL.userFollowOrganisation(userID, orgID);
        }


        [HttpPost]
        [ActionName("unfollowOrganisation")]
        public static String unfollowOrganisation(String userID, String orgID)
        {
            return UserPortfolioBL.unfollowOrganisation(userID, orgID);
        }

        [HttpGet]
        [ActionName("getUserOrganisations")]
        public List<OrganizationDetails> getUserOrganisations(String userID)
        {
            return UserPortfolioBL.getUserOrganisations(userID);
        }

        [HttpGet]
        [ActionName("getUsersFollowedForOrganisation")]
        public List<UserPortfolio> getUsersFollowedForOrganisation(String orgID, String userID)
        {
            AuditMgmtBL.createAuditObj(7, 14, "Get list of followers", "CommunityMgmtController.getUsersFollowedForOrganisation", "", userID, "");
            return UserPortfolioBL.getUsersFollowedForOrganisation(orgID);
        }

        [HttpGet]
        [ActionName("getCommentsForUser")]
        public List<UserComments> getCommentsForUser(String toUserID)
        {
            return UserPortfolioBL.getCommentsForUser(toUserID);
        }

        [HttpGet]
        [ActionName("getCommentsByUser")]
        public List<UserComments> getCommentsByUser(String fromUserID)
        {
            return UserPortfolioBL.getCommentsByUser(fromUserID);
        }

        [HttpGet]
        [ActionName("getCommentDatetimeByUserForUser")]
        public String getCommentDatetimeByUserForUser(String fromUserID, String toUserID)
        {
            return UserPortfolioBL.getCommentDatetimeByUserForUser(fromUserID, toUserID);
        }

        [HttpPost]
        [ActionName("addComments")]
        public String addComments(String toUserID, String fromUserID, String comment)
        {
            return UserPortfolioBL.addComments(toUserID, fromUserID, comment);
        }

        [HttpPost]
        [ActionName("removeComments")]
        public String removeComments(String commentID)
        {
            NewsfeedBL.removeActivity(UserPortfolioBL.getActivityIDfromComment(commentID));
            return UserPortfolioBL.removeComment(commentID);
        }

        [HttpGet]
        [ActionName("getUserInterests")]
        public List<String> getUserInterests(String userID)
        {
            return UserPortfolioBL.getUserInterests(userID);
        }

        
        [HttpGet]
        [ActionName("getUserActivities")]
        public List<UserNewsfeed> getUserActivities(String userID)
        {
            return NewsfeedBL.getUserActivities(userID);
        }


        [HttpGet]
        [ActionName("getListofOrganisationsWithFollow")]
        public List<OrganizationCommunityDetails> getListofOrganisationsWithFollow(String userID)
        {
            return UserPortfolioBL.getListofOrganisationsWithFollow(userID);
        }

        [HttpGet]
        [ActionName("getListofOrganisationsWithFollowBySearch")]
        public List<OrganizationCommunityDetails> getListofOrganisationsWithFollowBySearch(String userID, String name)
        {
            return UserPortfolioBL.getListofOrganisationsWithFollowBySearch(userID, name);
        }

        [HttpGet]
        [ActionName("getOrganisationDetailsWithFollow")]
        public OrganizationCommunityDetails getOrganisationDetailsWithFollow(String userID, String orgID)
        {
            OrganizationCommunityDetails od = UserPortfolioBL.getOrganisationDetailsWithFollow(userID, orgID);
            if (od.UNFOLLOW_STATUS.Equals("follow"))
            {
                return UserPortfolioBL.getOrganisationDetailsWithFollow2(orgID);
            }
            else
            {
                return od;
            }
        }

        /*
        [HttpPost]
        [ActionName("uploadImage")]
        public String uploadImage(String userID, String imageByte)
        {
            imageByte = imageByte.Replace('-', '+').Replace('_', '/');
            return UserPortfolioBL.uploadImage(userID, imageByte);
        }
        */

        [HttpPost]
        [ActionName("uploadImage")]
        public String uploadImage(String userID, String imagePath)
        {
            return UserPortfolioBL.uploadImage(userID, imagePath);
        }

        /*
        [HttpPost]
        [ActionName("addUserActivity")]
        public String addUserActivity(String userID, String activityType, String targetUserID, String targetUserName, String eventID, String eventName)
        {
            return NewsfeedBL.addUserActivity(userID, activityType, targetUserID, targetUserName, eventID, eventName);
        }

        [HttpPost]
        [ActionName("addUserActivityUser")]
        public String addUserActivityUser(String userID, String activityType, String targetUserID)
        {
            NewsfeedBL.addUserActivityUser(userID, activityType, targetUserID);
            return NewsfeedBL.updateActivityIDtoUserFriend(userID, activityType, targetUserID);
        }

        [HttpPost]
        [ActionName("addUserActivityEvent")]
        public String addUserActivityEvent(String userID, String activityType, String eventID, String eventName)
        {
            return NewsfeedBL.addUserActivityEvent(userID, activityType, eventID, eventName);
        }
        */

        /*<<<<<<<<<<    Newsfeed Activities     >>>>>>>>>>*/

        [HttpGet]
        [ActionName("getUserFriendsActivities")]
        public List<UserNewsfeed> getUserFriendsActivities(String userID)
        {
            return NewsfeedBL.getUserFriendsActivities(userID);
        }

        [HttpGet]
        [ActionName("getTopUsersVIA")]
        public List<UserPortfolio> getTopUsersVIA(int size)
        {
            return NewsfeedBL.getTopUsersVIA(size);
        }

        [HttpGet]
        [ActionName("getUserFacebookID")]
        public String getUserFacebookID(String userID)
        {
            return NewsfeedBL.getUserFacebookID(userID);
        }

        [HttpGet]
        [ActionName("getListOfRecommendedUsersByNetwork")]
        public List<UserPortfolioRecommendationModel> getListOfRecommendedUsersByNetwork(String userID)
        {
            return NewsfeedBL.getListOfRecommendedUsersByNetwork(userID);
        }

        [HttpGet]
        [ActionName("getListOfRecommendedUsersByFacebook")]
        public List<UserPortfolioRecommendationModel> getListOfRecommendedUsersByFacebook(String userID, String facebookID, String accessToken)
        {
            return NewsfeedBL.getListOfRecommendedUsersByFacebook(userID, facebookID, accessToken);
        }

    }
}
