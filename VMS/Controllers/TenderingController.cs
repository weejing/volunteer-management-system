﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AuditMgmt;
using VMS.BusinessLayer.TenderingMgmt;
using VMS.Models.TenderingModels;

namespace VMS.Controllers
{
    public class TenderingController : ApiController
    {
        /// <summary>Controller: Get list of tender header list from database</summary>
        [HttpPost]
        [ActionName("getTenderHeaderList")]
        public IEnumerable<TenderHeader> getTenderHeaderList()
        {
            List<TenderHeader> tenderHeaderList = TenderingMgmtBL.getTenderHeaderList();
            return tenderHeaderList;
        }

        /// <summary>Controller: Get list of tender header from database</summary>
        [HttpPost]
        [ActionName("getTenderHeader")]
        public TenderHeader getTenderHeader([FromBody] TenderHeader headerID)
        {
            TenderHeader tenderHeader= TenderingMgmtBL.getTenderHeader(headerID);
            return tenderHeader;
        }

        /// <summary>Controller: Get list of tender item list from database</summary>
        [HttpPost]
        [ActionName("getTenderItemList")]
        public IEnumerable<TenderItemDetails> getOrgTenderItemList([FromBody] TenderHeader item)
        {
            List<TenderItemDetails> tenderItemList = TenderingMgmtBL.getTenderItemList(item);
            return tenderItemList;
        }

        /// <summary>Controller: Get list of saas tender item list from database</summary>
        [HttpPost]
        [ActionName("getSaaSTenderItemList")]
        public IEnumerable<TenderItemDetails> getSaaSTenderItemList([FromBody] TenderHeader item)
        {
            List<TenderItemDetails> tenderItemList = TenderingMgmtBL.getSaaSTenderItemList(item);
            return tenderItemList;
        }

        /// <summary>Controller: get tender lowest bid (group price)</summary>
        [HttpPost]
        [ActionName("getTenderLowestBid")]
        public IEnumerable<VendorBidsDetails> getTenderLowestBid([FromBody] TenderItemDetails item)
        {
            List<VendorBidsDetails> tenderItemList = TenderingMgmtBL.getTenderLowestBid(item);
            return tenderItemList;
        }

        /// <summary>Controller: get tender lowest bid (individual price)</summary>
        [HttpPost]
        [ActionName("getTenderLowestBidSolo")]
        public IEnumerable<VendorBidsDetails> getTenderLowestBidSolo([FromBody] TenderItemDetails item)
        {
            List<VendorBidsDetails> tenderItemList = TenderingMgmtBL.getTenderLowestBidSolo(item);
            return tenderItemList;
        }

        /// <summary>Controller: update vendor bid status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateVendorBidStatus")]
        public void updateVendorBidStatus([FromBody] VendorBidsDetails item)
        {
           TenderingMgmtBL.updateVendorBidStatus(item);   
                   
        }

        /// <summary>Controller: update tender item status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderItemStatus")]
        public void updateTenderItemStatus([FromBody] TenderItemDetails item)
        {
            TenderingMgmtBL.updateTenderItemStatus(item);
        }

        /// <summary>Controller: update tender head status</summary>
        [HttpPost]
        [ActionName("updateTenderHeadStatus")]
        public void updateTenderHeadStatus([FromBody] TenderHeader item)
        {
            TenderingMgmtBL.updateTenderHeadStatus(item);

        }

        /// <summary>Controller: update tender vendor bid packed</summary>
        [HttpPost]
        [ActionName("updateVendorBidPacked")]
        public void updateVendorBidPacked([FromBody] VendorBidsDetails item)
        {
            TenderingMgmtBL.updateVendorBidPacked(item);
        }

        /// <summary>Controller: update tender bid delivered</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateVendorBidDelivered")]
        public void updateVendorBidDelivered([FromBody] VendorBidsDetails item)
        {
            TenderingMgmtBL.updateVendorBidDelivered(item);

        }

        /// <summary>Controller: get bidder SaaS</summary>
        [HttpPost]
        [ActionName("getBidderSaaS")]
        public IEnumerable<BidCombineView> getBidderSaaS([FromBody] BidCombineView item)
        {
            List<BidCombineView> tenderList = TenderingMgmtBL.getBidderSaaS(item);
            return tenderList;
        }

        /// <summary>Controller: get tender category list</summary>
        [HttpPost]
        [ActionName("getTenderCategoryList")]
        public IEnumerable<TenderCategory> getTenderCategoryList()
        {
            List<TenderCategory> tenderCatList = TenderingMgmtBL.getTenderCategoryList();
            return tenderCatList;
        }

        /// <summary>Controller: get bid item SaaS</summary>
        [HttpPost]
        [ActionName("getBidItemSaaS")]
        public BidCombineView getBidItemSaaS([FromBody] BidCombineView item)
        {
            BidCombineView tenderList = TenderingMgmtBL.getBidItemSaaS(item);
            return tenderList;
        }

        /// <summary>Controller: get supplier rating</summary>
        [HttpPost]
        [ActionName("getRating")]
        public BidCombineView getRating([FromBody] BidCombineView supplier)
        {
            BidCombineView tenderList = TenderingMgmtBL.getRating(supplier);
            return tenderList;
        }

        /// <summary>Controller: get tender list saas</summary>
        [HttpPost]
        [ActionName("getTenderListSaaS")]
        public IEnumerable<TenderItemDetails> getTenderListSaaS([FromBody] TenderItemDetails item)
        {
            List<TenderItemDetails> tenderList = TenderingMgmtBL.getTenderListSaaS(item);
            return tenderList;
        }

        /// <summary>Controller: get bid tender list</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getBiddedTenderList")]
        public IEnumerable<TenderItemDetails> getBiddedTenderList([FromBody] VendorDetails orgDetails)
        {
            List<TenderItemDetails> tenderList = TenderingMgmtBL.getBiddedTenderList(orgDetails);
            return tenderList;
        }

        /// <summary>Controller: create tender header</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("createTenderHeader")]
        public String createTenderHeader([FromBody] TenderHeader newTender)
        {
            return TenderingMgmtBL.createTenderHeader(newTender);
         
        }

        /// <summary>Controller: create tender item</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("createTenderItem")]
        public void createTenderItem([FromBody] TenderItemDetails newTender)
        {
            TenderingMgmtBL.createTenderItem(newTender);
            
        }

        /// <summary>Controller: Update tender entry (created by volunteer organization)
        /// Only can update before tender starts</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTender")]
        public void updateTender([FromBody] TenderItemDetails newTender)
        {
            TenderingMgmtBL.updateTender(newTender);
            
        }

        /// <summary>Controller: Update tender entry status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderStatusStart")]
        public void updateTenderStatusStart()
        {
            TenderingMgmtBL.updateTenderStatusStart();
        }

        /// <summary>Controller: Update tender entry status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderStatusClosed")]
        public void updateTenderStatusClosed()
        {
            TenderingMgmtBL.updateTenderStatusClosed();
        }

        /// <summary>Controller: Update tender winner</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderWinner")]
        public void updateTenderWinner([FromBody] String tenderID, [FromBody] String venderID, [FromBody] DateTime winnerDate)
        {
            TenderingMgmtBL.updateTenderWinner(tenderID, venderID, winnerDate);
        }

        /// <summary>Controller: Delete tender</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("deleteTender")]
        public void deleteTender([FromBody] TenderItemDetails tenderID)
        {
            TenderingMgmtBL.deleteTender(tenderID);
        }

        /// <summary>Controller: Delete bid</summary>
        [HttpPost]
        [ActionName("deleteBid")]
        public void deleteBid([FromBody] VendorBidsDetails tenderID)
        {
            TenderingMgmtBL.deleteBid(tenderID);
        }

        /// <summary>Controller: Get tender's volunteer organization information</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getTenderOrganizationInfo")]
        public OrganisationDetails getTenderOrganizationInfo([FromBody] String connectionString)
        {
            return TenderingMgmtBL.getTenderOrganizationInfo(connectionString);
        }

        /// <summary>Controller: Vendor sign up new account</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("vendorSignUp")]
        public void vendorSignUp([FromBody] VendorDetails vendorDetails)
        {
            TenderingMgmtBL.vendorSignUp(vendorDetails);
        }

        /// <summary>Controller: vendor forgot password</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateVendorPassword")]
        public void updateVendorPassword([FromBody] String tenderID, [FromBody] String password)
        {
            TenderingMgmtBL.updateVendorPassword(tenderID, password);
        }

        /// <summary>Controller: vendor update password</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("forgotVendorPassword")]
        public void forgotVendorPassword([FromBody] String tenderID, [FromBody] String password)
        {
            TenderingMgmtBL.forgotVendorPassword(tenderID, password);
        }

        /// <summary>Controller: vendor update particulars</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateVendorParticulars")]
        public void updateVendorParticulars([FromBody] VendorDetails orgDetails)
        {
            TenderingMgmtBL.updateVendorParticulars(orgDetails);
        }

        /// <summary>Controller: vendor submit bid</summary>
       // [Authorize]
        [HttpPost]
        [ActionName("insertVendorBid")]
        public void insertVendorBid([FromBody] VendorBidsDetails newBid)
        {
            TenderingMgmtBL.insertVendorBid(newBid);
        }

        /// <summary>Controller: vendor update bid</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateVendorBid")]
        public void updateVendorBid([FromBody] VendorBidsDetails bidsDetails)
        {
            TenderingMgmtBL.updateVendorBid(bidsDetails);
        }

        /// <summary>Controller: get list of submitted bid by vendor</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getSubmittedBidsList")]
        public IEnumerable<VendorBidsDetails> getSubmittedBidsList([FromBody] TenderItemDetails vendorID)
        {
            return TenderingMgmtBL.getSubmittedBidsList(vendorID);
        }

        /// <summary>Controller: get list of submitted bid by vendor</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getSubmittedBid")]
        public VendorBidsDetails getSubmittedBid([FromBody] VendorBidsDetails tenderItemID)
        {
            return TenderingMgmtBL.getSubmittedBid(tenderItemID);
        }

        /// <summary>Controller: update tender status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderStatus")]
        public String updateTenderStatus([FromBody] TenderItemDetails bids)
        {
            return TenderingMgmtBL.updateTenderStatus(bids);
        }

        /// <summary>Controller: update bid status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateBidStatus")]
        public String updateBidStatus([FromBody] VendorBidsDetails bids)
        {
            return TenderingMgmtBL.updateBidStatus(bids);
        }

        /// <summary>Controller: update tender status</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateTenderStatusStatus")]
        public String updateTenderStatusStatus()
        {
            return TenderingMgmtBL.updateTenderStatusStatus();
        }
    }
}
