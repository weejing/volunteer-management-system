﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMS.Models.OrgMgmtModels;
using VMS.BusinessLayer.OrgMgmt;
using VMS.DataLayer.AuditMgmt;
using System.Security.Claims;
using VMS.BusinessLayer.AuditMgmt;

namespace VMS.Controllers
{ //SaaS
    public class OrgMgmtController : ApiController
    {
        /// <summary>Controller: insert new organisation</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("insertOrganization")]
        public String insertOrganization([FromBody] OrganizationDetails newOrg)
        {
            return OrgMgmtBL.insertOrganization(newOrg);
        }

        /// <summary>Controller: launch new organisation database instance</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("insertOrgInstance")]
        public String insertOrgInstance([FromBody] OrgUserDetails newOrg)
        {
           System.Diagnostics.Debug.WriteLine("manage to send http request here ");
            return OrgMgmtBL.insertOrgInstance(newOrg);
        }

        /// <summary>Controller: insert organisation master admin</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("insertMasterAdmin")]
        public String insertMasterAdmin([FromBody] OrgUserDetails orgUser)
        {
            // System.Diagnostics.Debug.WriteLine("result: " + result);
            return OrgMgmtBL.insertMasterAdmin(orgUser);
        }

        /// <summary>Controller: get organisation information</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getOrgInfo")]
        public OrganizationDetails getOrgInfo([FromBody] OrganizationDetails org)
        {
            return OrgMgmtBL.getOrgInfo(org);
        }

        [HttpPost]
        [ActionName("getOrgInfo2")]
        public OrganizationDetails getOrgInfo2([FromBody] OrganizationDetails org)
        {
            return OrgMgmtBL.getOrgInfo2(org);
        }

        /// <summary>Controller: get organisation information list</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("getOrgList")]
        public List<OrganizationDetails> getOrgList()
        {
            return OrgMgmtBL.getOrgList();
        }

        /*search for org*/
        [HttpGet]
        [ActionName("searchOrg")]
        public List<OrganizationDetails> searchOrg(string connection_string, string name)
        {
            return OrgMgmtBL.searchOrg(connection_string, name);
        }
        /// <summary>Controller: update organisation information</summary>
        //[Authorize]
        [HttpPost]
        [ActionName("updateOrganization")]
        public String updateOrganization([FromBody] OrganizationDetails newOrg)
        {
            return OrgMgmtBL.updateOrganization(newOrg);
        }
    }
}
