﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using VMS.BusinessLayer.AdminMgmt;
using VMS.BusinessLayer.AuditMgmt;
using VMS.Models.AdminModels;

namespace VMS.Controllers
{

    public class AdminController : ApiController
    {
        #region School 

        [HttpGet]
        [ActionName("getSchools")]
        public IEnumerable<School> getSchools(string SCH_LVL)
        {
            List<School> schList = SchoolBL.getSchools(SCH_LVL);
            return schList;
        }

        [HttpGet]
        [ActionName("searchSchools")]
        public IEnumerable<School> searchSchools(string SCH_LVL,string SCHL_NAME)
        {
            List<School> schList = SchoolBL.searchSchools(SCH_LVL, SCHL_NAME);
            return schList;
        }

        /*Create school record*/
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("createSchool")]
        public string createSchool([FromBody] School school)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Create "+school.SCHL_NAME+" School", "AdminController.createSchool", "Internal", userID, connectionString);

            return SchoolBL.createSchool(school);
        }

        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("editSchool")]
        public int editSchool([FromBody] School school)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Edit " + school.SCHL_NAME + " School", "AdminController.editSchool", "Internal", userID, connectionString);
            return SchoolBL.editSchool(school);
        }

        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("deleteSchool")]
        public int deleteSchool(string schl_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Delete School", "AdminController.deleteSchool", "Internal", userID, connectionString);
            return SchoolBL.deleteSchool(schl_id);
        }

        /*get school detail*/
        [HttpGet]
        [ActionName("getSchByID")]
        public School getSchByID(string SCHL_ID)
        {
            return SchoolBL.getSchByID( SCHL_ID);
        }
        #endregion

        #region Exam period 
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("createExamPeriod")]
        public string createExamPeriod([FromBody] ExamPeriod examPeriod)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Create Exam Period", "AdminController.createExamPeriod", "Internal", userID, connectionString);
            return ExamPeriodBL.createExamPeriod(examPeriod);
        }

        /*Update exam detail*/
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("updateExamPeriod")]
        public int updateExamPeriod([FromBody] ExamPeriod examPeriod)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Update Exam Period", "AdminController.updateExamPeriod", "Internal", userID, connectionString);
            return ExamPeriodBL.updateExamPeriod(examPeriod);
        }

        /*Delete exam record*/
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("deleteExamPeriod")]
        public int deleteExamPeriod(string exam_period_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Delete Exam Period", "AdminController.deleteExamPeriod", "Internal", userID, connectionString);
            return ExamPeriodBL.deleteExamPeriod(exam_period_id);
        }

        /*get exam detail*/
        [HttpGet]
        [ActionName("getExamPeriod")]
        public ExamPeriod getExamPeriod(string exam_period_id)
        {
            return ExamPeriodBL.getExamPeriod(exam_period_id);
        }

        /*Get list of exam details*/
        [HttpGet]
        [ActionName("getExamPeriodList")]
        public IEnumerable<ExamPeriod> getExamPeriodList()
        {
            return ExamPeriodBL.getExamPeriodList();
        }
        #endregion

        #region Interest Form

        [HttpGet]
        [ActionName("getVocabID")]
        public int getVocabID(String vocab_name)
        {
            return TermBL.getVocabID(vocab_name);
        }
        [HttpGet]
        [ActionName("getVocabList")]
        public IEnumerable<Vocab> getVocabList()
        {
            return TermBL.getVocabList();
        }

        [HttpGet]
        [ActionName("getTermList")]
        public IEnumerable<Term> getTermList(int vocab_id)
        {
            return TermBL.getTermList(vocab_id);
        }

        [HttpGet]
        [ActionName("getTermListByVocabName")]
        public IEnumerable<Term> getTermListByVocabName(string vocab_name)
        {
            return TermBL.getTermListByVocabName(vocab_name);
        }

        /*Get term*/
        [HttpGet]
        [ActionName("getTerm")]
        public Term getTerm(string term_id)
        {
            return TermBL.getTerm(term_id);
        }

        /*Create term */
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("createTerm")]
        public Boolean createTerm([FromBody] Term term)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Create "+term.TERM_NAME+" interest attribute", "AdminController.createTerm", "Internal", userID, connectionString);
            return TermBL.createTerm( term);
        }

        /*Update term*/
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("updateTerm")]
        public int updateTerm([FromBody] Term term)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Update " + term.TERM_NAME + " interest attribute", "AdminController.updateTerm", "Internal", userID, connectionString);
            return TermBL.updateTerm( term);
        }

        /*Delete term*/
        [Authorize(Roles = "OneThird")]
        [HttpGet]
        [ActionName("deleteTerm")]
        public Boolean deleteTerm(int term_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 24, "Delete interest attribute", "AdminController.deleteTerm", "Internal", userID, connectionString);
            return TermBL.deleteTerm( term_id);
        }

        #endregion

        #region OneThird Event Role

        /*Get all event role*/
        [HttpGet]
        [ActionName("getEvtRoleList")]
        public IEnumerable<EventRole> getEvtRoleList(string connection_string)
        {
            return EventRoleBL.getEvtRoleList(connection_string);
        }

        /*Get event role traits*/
        [HttpGet]
        [ActionName("getEvtRoleSkills")]
        public EventRole getEvtRoleSkills(int term_id, string connection_string)
        {
            return EventRoleBL.getEvtRoleSkills(term_id, connection_string);
        }

        /*Update event role traits*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("updateEvtRole")]
        public int updateEvtRole([FromBody] EventRole evtRole)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 9, "Update event role traits", "AdminController.updateEvtRole", "Internal", userID, connectionString);
            return EventRoleBL.updateEvtRole(evtRole);
        }

        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("createEvtRoleTraits")]
        public void createEvtRoleTraits([FromBody] EventRole evtRole)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 9, "Create event role traits", "AdminController.createEvtRoleTraits", "Internal", userID, connectionString);
            EventRoleBL.createEvtRoleTraits(evtRole);
        }
        #endregion

        #region Organiser Event Role
        //Organiser get event roles (org copy + those that org have not modified from master copy)
        [HttpGet]
        [ActionName("OrgGetEvtRoleList")]
        public IEnumerable<EventRole> OrgGetEvtRoleList(string connection_string)
        {
            return EventRoleBL.OrgGetEvtRoleList(connection_string);
        }

        /*Update event role traits*/
        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("OrgUpdateEvtRole")]
        public int OrgUpdateEvtRole([FromBody] EventRole evtRole)
        {
            var identity = User.Identity as ClaimsIdentity;
            string userID = identity.Name;
            string connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            AuditMgmtBL.createAuditObj(5, 9, "Update event role traits", "AdminController.OrgUpdateEvtRole", "NPO", userID, connectionString);
            return EventRoleBL.OrgUpdateEvtRole(evtRole);
        }

        /*Get event role traits*/
        [HttpGet]
        [ActionName("OrgGetEvtRoleSkills")]
        public EventRole OrgGetEvtRoleSkills(int term_id, string connection_string)
        {

            return EventRoleBL.OrgGetEvtRoleSkills(term_id, connection_string);
        }
        #endregion

        #region Organization Role

        /*Get list of roles within the organization*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getRoleList")]
        public IEnumerable<Role> getRoleList(String CONNECTION_STRING)
        {
            return RoleBL.getRoleList(CONNECTION_STRING);
        }

        /*Get role*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getRole")]
        public Role getRole(string role_id, String CONNECTION_STRING)
        {
            return RoleBL.getRole(role_id, CONNECTION_STRING);
        }

        /*Create term */
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("createRole")]
        public string createRole([FromBody] Role role)
        {
            return RoleBL.createRole(role);
        }

        /*Update role*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("updateRole")]
        public int updateRole([FromBody] Role role)
        {
            return RoleBL.updateRole(role);
        }

        /*Delete role*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("deleteRole")]
        public int deleteRole([FromBody] Role role)
        {
            return RoleBL.deleteRole(role);
        }
        #endregion

        #region Pages
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getModuleList")]
        public IEnumerable<Module> getModuleList(String CONNECTION_STRING)
        {
            return PageBL.getModuleList(CONNECTION_STRING);
        }

        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getPageList")]
        public IEnumerable<Page> getPageList(String CONNECTION_STRING)
        {
            return PageBL.getPageList(CONNECTION_STRING);
        }

        #endregion

        #region Role <-> Modules
        /*Get role access rights*/
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpGet]
        [ActionName("getRoleModList")]
        public IEnumerable<Module> getRoleModList(string role_id, string CONNECTION_STRING)
        {
            return RoleBL.getRoleModList(role_id, CONNECTION_STRING);
        }

        /*Assign role access rights to the selected mods */
        [Authorize(Roles = "OneThird,Organiser")]
        [HttpPost]
        [ActionName("createRoleMod")]
        public void createRoleMod([FromBody] Role role)
        {
            RoleBL.createRoleMod(role);
        }

        #endregion


        #region Badge

        [HttpGet]
        [ActionName("getMinHourBadgeList")]
        public IEnumerable<Badge> getMinHourBadgeList()
        {
            return BadgeBL.getMinHourBadgeList();
        }

        [HttpGet]
        [ActionName("getPublishedBadgeList")]
        public IEnumerable<Badge> getPublishedBadgeList()
        {
            return BadgeBL.getPublishedBadgeList();
        }

        [HttpGet]
        [ActionName("getBadge")]
        public Badge getBadge(string badge_id)
        {
            return BadgeBL.getBadge(badge_id);
        }

    
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("createBadge")]
        public string createBadge([FromBody] Badge badge)
        {
            return BadgeBL.createBadge(badge);
        }

        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("updateBadge")]
        public int updateBadge([FromBody] Badge badge)
        {
            return BadgeBL.updateBadge(badge);
        }

        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("updateBadgeNoPhoto")]
        public int updateBadgeNoPhoto([FromBody] Badge badge)
        {
            return BadgeBL.updateBadgeNoPhoto(badge);
        }

        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("updateStatus")]
        public int updateStatus([FromBody] Badge badge)
        {
            return BadgeBL.updateStatus(badge);
        }

        /*Delete term*/
        [Authorize(Roles = "OneThird")]
        [HttpPost]
        [ActionName("deleteBadge")]
        public Boolean deleteBadge([FromBody] Badge badge)
        {
            return BadgeBL.deleteBadge(badge.BADGE_ID);
        }

        #endregion

        #region Vol Badge
        [HttpGet]
        [ActionName("issueBadges")]
        public IEnumerable<Badge> issueBadges(string userId)
        {
           return VolBadgeBL.issueBadges(userId);
        }

        [Authorize(Roles = "Volunteer")]
        [HttpGet]
        [ActionName("displayVolBadge")]
        public IEnumerable<Badge> displayVolBadge()
        {
            var identity = User.Identity as ClaimsIdentity;
            String usr_id = identity.Name;
            return VolBadgeBL.displayVolBadge(usr_id);
        }
        #endregion

        #region Feedback category

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getFdbackCatList")]
        public IEnumerable<FdbackCat> getFdbackCatList()
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FdbackCatBL.getFdbackCatList(connectionString);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("getCat")]
        public FdbackCat getCat(string cat_id)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FdbackCatBL.getCat(connectionString,cat_id);
        }

        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("createCat")]
        public Boolean createCat([FromBody] FdbackCat cat)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FdbackCatBL.createCat(connectionString, cat);
        }

        [Authorize(Roles = "Organiser")]
        [HttpPost]
        [ActionName("updateCat")]
        public int updateCat([FromBody] FdbackCat cat)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FdbackCatBL.updateCat(connectionString, cat);
        }

        [Authorize(Roles = "Organiser")]
        [HttpGet]
        [ActionName("deleteCat")]
        public int deleteCat(string FDBACK_CAT_ID)
        {
            var identity = User.Identity as ClaimsIdentity;
            String connectionString = identity.Claims.Where(c => c.Type == ClaimTypes.Actor).Select(c => c.Value).SingleOrDefault();
            return FdbackCatBL.deleteCat(connectionString, FDBACK_CAT_ID);
        }

        #endregion
    }
}
