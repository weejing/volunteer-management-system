﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using FluentScheduler;
using VMS.App_Start;

[assembly: OwinStartup(typeof(VMS.Startup))]

namespace VMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //JobManager.Initialize(new MyRegistry());
        }
    }
}
