﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;
using System.Threading;
using VMS.BusinessLayer.TransactionMgmt;
using VMS.BusinessLayer.VolBiddingMgmt;

namespace VMS.App_Start
{
    public class MyRegistry : Registry
    {
        public MyRegistry()
        {
            // Schedule an ITask to run at an interval

            Schedule<TimeTriggerBL>().ToRunNow().AndEvery(1).Days();
            //Schedule<TimeTriggerBL>().ToRunEvery(1).Days().At(00, 00);

            Schedule<TimeTriggerBidsTransBL>().ToRunNow().AndEvery(6).Hours();
            //Schedule<TimeTriggerBidsTransBL>().ToRunEvery(1).Days().At(21, 15);

            // Schedule a simple task to run at a specific time
            Schedule(() => Console.WriteLine("Timed Task - Will run every day at 9:15pm: " + DateTime.Now)).ToRunEvery(1).Days().At(21, 15);


            // Schedule a more complex action to run immediately and on an monthly interval
            Schedule(() =>
            {
                Console.WriteLine("Complex Action Task Starts: " + DateTime.Now);
                Thread.Sleep(1000);
                Console.WriteLine("Complex Action Task Ends: " + DateTime.Now);
            }).ToRunNow().AndEvery(1).Months().OnTheFirst(DayOfWeek.Monday).At(3, 0);
        }
    }
}