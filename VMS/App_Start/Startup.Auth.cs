S﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Jwt;
using Owin;
using VMS.Providers;
//using VMS.Models;
using VMS.Format;
using Microsoft.Owin.Security.DataHandler.Encoder;

namespace VMS
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            var secret = TextEncodings.Base64Url.Decode("key");
            String localhost = "http://sgserveportal.azurewebsites.net";
            //String localhost = "http://localhost/";


            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { "Any" },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                     new SymmetricKeyIssuerSecurityTokenProvider(localhost, secret)
                }
            });


            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true, 
                TokenEndpointPath = new PathString("/Authorise/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(90),
                Provider = new CustomOAuthProvider(),
                AccessTokenFormat = new CustomJwtFormat(localhost)
            };

            app.UseOAuthAuthorizationServer(OAuthOptions);

            /*
            app.UseFacebookAuthentication(
                appId: "",
                appSecret: "");
                */

        }
    }
}
