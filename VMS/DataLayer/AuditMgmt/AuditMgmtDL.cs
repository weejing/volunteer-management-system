﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AuditMgmtModels;

namespace VMS.DataLayer.AuditMgmt
{
    public class AuditMgmtDL
    {
        /// <summary>DAL: Insert ew organisation account</summary>
        public static String createAuditTrailSaaS(AuditTrail audit)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                bool userID = true;
                SqlConnection dbConnection = audit.dbConnection;
                if (audit.MODULE_NAME == null | audit.MODULE_NAME.Equals(""))
                {
                    return "";
                }
                string sql_str = @"INSERT INTO TABLE_AUDIT_TRAIL
           (AUDIT_TRAIL_ID
           ,MODULE_NAME
           ,MODULE_FEATURE
           ,ACTION
           ,DESCRIPTION
           ,TIMESTAMP
           ,SYSTEM_USR
           ,USR_ID)
     VALUES
           (@AUDIT_TRAIL_ID
           ,@MODULE_NAME
           ,@MODULE_FEATURE
           ,@ACTION
           ,@DESCRIPTION
           ,@TIMESTAMP
           ,@SYSTEM_USR
           ,TRY_CONVERT(UNIQUEIDENTIFIER,@USR_ID))";


                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                SqlParameter AUDIT_TRAIL_ID = new SqlParameter("AUDIT_TRAIL_ID", SqlDbType.NVarChar, 50);
                AUDIT_TRAIL_ID.Value = newID;
                command.Parameters.Add(AUDIT_TRAIL_ID);

                SqlParameter MODULE_NAME = new SqlParameter("MODULE_NAME", SqlDbType.NVarChar, 250);
                MODULE_NAME.Value = audit.MODULE_NAME;
                command.Parameters.Add(MODULE_NAME);

                SqlParameter MODULE_FEATURE = new SqlParameter("MODULE_FEATURE", SqlDbType.NVarChar, 250);
                MODULE_FEATURE.Value = audit.MODULE_FEATURE;
                command.Parameters.Add(MODULE_FEATURE);

                SqlParameter ACTION = new SqlParameter("ACTION", SqlDbType.NVarChar, 100);
                ACTION.Value = audit.ACTION;
                command.Parameters.Add(ACTION);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 250);
                DESCRIPTION.Value = audit.DESCRIPTION;
                command.Parameters.Add(DESCRIPTION);

                DateTime regDatetimeTemp = DateTime.Now;
                string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);

                SqlParameter TIMESTAMP = new SqlParameter("TIMESTAMP", SqlDbType.DateTime);
                TIMESTAMP.Value = regDate;
                command.Parameters.Add(TIMESTAMP);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 250);
                USR_ID.Value = audit.USR_ID;
                command.Parameters.Add(USR_ID);

                SqlParameter SYSTEM_USR = new SqlParameter("SYSTEM_USR", SqlDbType.NVarChar, 250);
                SYSTEM_USR.Value = audit.SYSTEM_USR;
                command.Parameters.Add(SYSTEM_USR);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertOrganization---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return newID;
        }
        /// <summary>DAL: Get list of tender list from database</summary>
        public static List<AuditTrail> getAuditListSaaS(SqlConnection dbConnection)
        {
            List<AuditTrail> AuditList = new List<AuditTrail>();

            try
            {
                string sqlStr = @"select a.AUDIT_TRAIL_ID, a.MODULE_NAME, a.MODULE_FEATURE, a.ACTION, a.DESCRIPTION, a.TIMESTAMP, a.SYSTEM_USR, a.USR_ID, u.NAME  from TABLE_AUDIT_TRAIL a
left join TABLE_ORG_USR u on a.USR_ID = u.USR_ID
ORDER BY TIMESTAMP DESC";
                SqlCommand command = new SqlCommand(sqlStr, dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AuditTrail auditItem = new AuditTrail();

                    auditItem.AUDIT_TRAIL_ID = reader["AUDIT_TRAIL_ID"].ToString();
                    auditItem.MODULE_NAME = reader["MODULE_NAME"].ToString();
                    auditItem.MODULE_FEATURE = reader["MODULE_FEATURE"].ToString();
                    auditItem.ACTION = reader["ACTION"].ToString();
                    auditItem.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    auditItem.TIMESTAMP = Convert.ToDateTime(reader["TIMESTAMP"]);
                    auditItem.SYSTEM_USR = reader["SYSTEM_USR"].ToString();
                    auditItem.USR_ID = reader["USR_ID"].ToString();
                    auditItem.NAME = reader["NAME"].ToString();

                    AuditList.Add(auditItem);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getTenderList---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return AuditList;
        }
    }
}