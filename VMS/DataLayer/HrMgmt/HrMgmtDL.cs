﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.HrModels;


namespace VMS.DataLayer.HrMgmt
{
    public class HrMgmtDL
    {
        /// <summary>DAL: Insert ONE new External Volunteer record/summary>
        public static String createOneRecord(SqlConnection dbConnection, ExternalVolunteerDetails newVolunteerRecord)
        {

            string namelistid = Guid.NewGuid().ToString();
            string name = newVolunteerRecord.NAME;
            string email = newVolunteerRecord.EMAIL;
            string phoneno = newVolunteerRecord.PHONENO;
            try
            {
                SqlCommand command = new SqlCommand(null, dbConnection);
                command.CommandText = "INSERT INTO TABLE_ORG_NAMELIST (NAMELIST_ID, PHONENO, NAME, EMAIL) VALUES (@NAMELIST_ID, @PHONENO, @NAME, @EMAIL)";

                SqlParameter NAMELIST_ID = new SqlParameter("NAMELIST_ID", SqlDbType.NVarChar, 100);
                NAMELIST_ID.Value = namelistid; 
                command.Parameters.Add(NAMELIST_ID);

                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 50);
                NAME.Value = name;
                command.Parameters.Add(NAME);

                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 250);
                EMAIL.Value = email;
                command.Parameters.Add(EMAIL);

                SqlParameter PHONENO= new SqlParameter("PHONENO", SqlDbType.NVarChar, 50);
                PHONENO.Value = phoneno;
                command.Parameters.Add(PHONENO);
                command.ExecuteScalar();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);

            }
            return namelistid;
        }

        /// <summary>DAL: check if external volunteer exist</summary>
        public static Boolean checkVolunteerExist( string name, string contact,string email, SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand("Select * from TABLE_ORG_NAMELIST WHERE NAME = @NAME AND PHONENO =@PHONENO AND EMAIL =@EMAIL;", dataConnection);
            SqlParameter Namepara = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
            Namepara.Value = name;
            command.Parameters.Add(Namepara);
            SqlParameter PhoneNopara = new SqlParameter("PHONENO", SqlDbType.NVarChar, 100);
            PhoneNopara.Value = contact;
            command.Parameters.Add(PhoneNopara);
            SqlParameter Emailpara = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            Emailpara.Value = email;
            command.Parameters.Add(Emailpara);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
                return true;
            else
                return false;
        }
        /// <summary>DAL: Get list of external volunteers from database</summary>
        public static List<ExternalVolunteerDetails> getExternalVolList(SqlConnection dbConnection)
        {
            List<ExternalVolunteerDetails> listofVolunteers = new List<ExternalVolunteerDetails>();

            try
            {
                SqlCommand command = new SqlCommand("Select * FROM TABLE_ORG_NAMELIST", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExternalVolunteerDetails externalvol = new ExternalVolunteerDetails();

                    externalvol.NAMELIST_ID = reader["NAMELIST_ID"].ToString().ToLower();
                    externalvol.NAME = reader["NAME"].ToString().ToLower();
                    externalvol.PHONENO = reader["PHONENO"].ToString();
                    externalvol.EMAIL = reader["EMAIL"].ToString();
                    listofVolunteers.Add(externalvol);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---InventoryMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getEventLogisticList---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listofVolunteers;
        }

        /// <summary>DAL: delete external volunteer</summary>
        public static int deleteExternalVol(SqlConnection dbConnection, string NAMELIST_ID)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_ORG_NAMELIST WHERE NAMELIST_ID= @NAMELIST_ID", dbConnection);
                SqlParameter namelistid = new SqlParameter("NAMELIST_ID", SqlDbType.NVarChar, 100);
                namelistid.Value = NAMELIST_ID;
                command.Parameters.Add(namelistid);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }


        /// <summary>DAL: check if event session available</summary>
        public static List<OpenEvents> getUpcomingEvents(SqlConnection dataConnection)
        {
            List<OpenEvents> listOfevents = new List<OpenEvents>();
            SqlCommand command = new SqlCommand("Select EVT_NAME,STARTDATE,UNIQUE_LINK from TABLE_EVT_DETAILS WHERE STARTDATE >= GETDATE()", dataConnection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                OpenEvents events = new OpenEvents();

                events.EVENTNAME= reader["EVT_NAME"].ToString();
                events.EVENTDATE = DateTime.Parse(reader["STARTDATE"].ToString());
                events.EVENTLINK = reader["UNIQUE_LINK"].ToString();
                listOfevents.Add(events);
            }
            return listOfevents;
        }

    }


}