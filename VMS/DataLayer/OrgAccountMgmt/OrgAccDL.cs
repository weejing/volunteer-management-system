﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.OrgAccountMgmt
{
    public class OrgAccDL
    {
        public static String getSalt(SqlConnection databaseConnection, String tableName, String userName)
        {
            String salt = "";
            System.Diagnostics.Debug.WriteLine("userName: " + userName);

            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "Select registration_date from " + tableName + " Where EMAIL = @user AND IS_DELETED=0";
            SqlParameter userParam = new SqlParameter("user", SqlDbType.NVarChar, 100);
            userParam.Value = userName;
            command.Parameters.Add(userParam);

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                salt = reader[0].ToString().Trim();
                System.Diagnostics.Debug.WriteLine("salt:" + salt);
            }
            reader.Close();
            return salt;
        }

        /*Get salt by resetpw_code to reset password*/
        public static String getSaltByResetCode(SqlConnection databaseConnection, String tablename, String resetpw_code)
        {
            String salt = "";

            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "Select registration_date from " + tablename + " Where RESETPW_CODE = @RESETPW_CODE AND IS_DELETED=0";
            SqlParameter RESETPW_CODE = new SqlParameter("RESETPW_CODE", SqlDbType.NVarChar, 100);
            RESETPW_CODE.Value = resetpw_code;
            command.Parameters.Add(RESETPW_CODE);

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                salt = reader[0].ToString();
                System.Diagnostics.Debug.WriteLine("salt:" + salt);
            }
            reader.Close();
            return salt;
        }

        /*check existing email*/
        public static Boolean checkEmailExist(SqlConnection dataConnection, string tablename, string EMAIL)
        {
            SqlCommand command = new SqlCommand("Select * from " + tablename + " Where Email = @EMAIL AND IS_DELETED=0", dataConnection);
            SqlParameter EMAILpara = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            EMAILpara.Value = EMAIL;
            command.Parameters.Add(EMAILpara);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read() == false)
            {
                reader.Close();
                return false;
            }
            else
            {
                reader.Close();
                return true;
            }

        }

        public static String checkPassword(String userName, String passwordAndSalt, SqlConnection dataConnection, string tableName)
        {
            try
            {
                //String hashedPassWord = generateHashPassword(passwordAndSalt);
                SqlCommand command = new SqlCommand(null, dataConnection);

                command.CommandText = "Select USR_ID from " + tableName + " Where Email = @user AND password = @password AND IS_DELETED=0";
                SqlParameter userParams = new SqlParameter("user", SqlDbType.NVarChar, 100);
                SqlParameter passwordParams = new SqlParameter("password", SqlDbType.NVarChar, 100);

                System.Diagnostics.Debug.WriteLine("userName: " + userName + " password: " + passwordAndSalt);

                userParams.Value = userName;
                passwordParams.Value = passwordAndSalt;
                command.Parameters.Add(userParams);
                command.Parameters.Add(passwordParams);

                command.Prepare();
                SqlDataReader reader = command.ExecuteReader();

                String usr_ID = "";

                while (reader.Read())
                {
                    usr_ID = reader["USR_ID"].ToString();
                }

                reader.Close();

                return usr_ID;
            } catch (Exception ex)
            {

            }
            return "";
        }

        /*Create Account */
        public static string createAccount(SqlConnection dbConnection, OrgEmployee employee)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_ORG_USR (USR_ID, NAME,EMAIL,PASSWORD,REGISTRATION_DATE) OUTPUT INSERTED.USR_ID VALUES (NEWID(), @NAME,@EMAIL,@PASSWORD,@REGISTRATION_DATE)";
            SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
            PASSWORD.Value = employee.PASSWORD;
            command.Parameters.Add(PASSWORD);
            SqlParameter FULL_NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
            FULL_NAME.Value = employee.NAME;
            command.Parameters.Add(FULL_NAME);
            SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            EMAIL.Value = employee.EMAIL;        
            command.Parameters.Add(EMAIL);
            SqlParameter REGISTRATION_DATE = new SqlParameter("REGISTRATION_DATE", SqlDbType.DateTime);
            REGISTRATION_DATE.Value = employee.REGISTRATION_DATE;
            command.Parameters.Add(REGISTRATION_DATE);
            

            string volid = command.ExecuteScalar().ToString();
            return volid;
        }

        /*Update Account detail*/
        public static int updateAccDetail(SqlConnection dbConnection, OrgEmployee emp)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_ORG_USR SET NAME = @NAME, EMAIL=@EMAIL Where USR_ID= @USR_ID", dbConnection);
                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
                NAME.Value = emp.NAME;
                command.Parameters.Add(NAME);
                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
                EMAIL.Value = emp.EMAIL;
                command.Parameters.Add(EMAIL);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = emp.USR_ID;
                command.Parameters.Add(USR_ID);

                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete Account*/
        public static int deleteAcc(SqlConnection dbConnection, string usr_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_ORG_USR SET IS_DELETED=1 Where USR_ID=@USR_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Get list of employees*/
        public static List<OrgEmployee> getEmployees(SqlConnection dbConnection)
        {
            List<OrgEmployee> listOfEmployees = new List<OrgEmployee>();

            try
            {
                SqlCommand command=new SqlCommand("SELECT * FROM TABLE_ORG_USR WHERE IS_DELETED=0", dbConnection);       

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrgEmployee employee = new OrgEmployee();
                    employee.EMAIL = reader["EMAIL"].ToString();
                    employee.NAME = reader["NAME"].ToString();
                    employee.USR_ID= reader["USR_ID"].ToString();
                    employee.PASSWORD = reader["PASSWORD"].ToString();
                    employee.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    //employee.ROLEE = reader["ROLEE"].ToString();
                    //employee.ROLE_ID = reader["ROLE_ID"].ToString();
                    //employee.ROLE_NAME = reader["ROLE_NAME"].ToString();
                    employee.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    listOfEmployees.Add(employee);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
            }
            return listOfEmployees;
        }

        /*get employee detail*/
        public static OrgEmployee getEmpParticular(SqlConnection dbConnection, string usr_id)
        {
            OrgEmployee employee = new OrgEmployee();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_ORG_USR WHERE USR_ID=@USR_ID ORDER BY NAME ASC;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    employee.EMAIL = reader["EMAIL"].ToString();
                    employee.NAME = reader["NAME"].ToString();
                    employee.USR_ID = reader["USR_ID"].ToString();
                    employee.PASSWORD = reader["PASSWORD"].ToString();
                    employee.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    //employee.ROLE_ID = reader["ROLE_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
            }
            return employee;
        }
        
        /*Get list of employees*/
        public static List<OrgEmployee> getEmpByNameSearch(SqlConnection dbConnection, string name)
        {
            List<OrgEmployee> listOfEmployees = new List<OrgEmployee>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_ORG_USR WHERE NAME LIKE @NAME AND IS_DELETED=0;", dbConnection);
               
                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@NAME", "%" + name + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrgEmployee employee = new OrgEmployee();
                    employee.EMAIL = reader["EMAIL"].ToString();
                    employee.NAME = reader["NAME"].ToString();
                    employee.USR_ID = reader["USR_ID"].ToString();
                    employee.PASSWORD = reader["PASSWORD"].ToString();
                    employee.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    //employee.ROLEE = reader["ROLEE"].ToString();
                    //employee.ROLE_ID = reader["ROLE_ID"].ToString();
                    //employee.ROLE_NAME = reader["ROLE_NAME"].ToString();
                    employee.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    listOfEmployees.Add(employee);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfEmployees;
        }

        #region usrRole 
        /*Assign roles to user */
        public static void createEmpRoles(SqlConnection dbConnection, string usr_id,List<Role> roleList)
        {
            foreach (Role role in roleList)
            {
                try
                {
                    SqlCommand command = new SqlCommand(null, dbConnection);
                command.CommandText = "INSERT INTO TABLE_ORG_USR_ROLE (USR_ID,ROLE_ID) VALUES ( @USR_ID,@ROLE_ID)";
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role.ROLE_ID;
                command.Parameters.Add(ROLE_ID);
                command.ExecuteScalar();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
                }
            }
        }

        /*Delete user roles*/
        public static int deleteUsrRoles(SqlConnection dbConnection, string usr_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_ORG_USR_ROLE WHERE USR_ID=@USR_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Get list of employees*/
        public static List<Role> getEmpRoles(SqlConnection dbConnection,string usr_id)
        {
            List<Role> listOfRoles = new List<Role>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT ROLE_NAME,UR.ROLE_ID AS ROLE_ID FROM TABLE_ORG_USR_ROLE UR INNER JOIN TABLE_ORG_USR U ON U.USR_ID=UR.USR_ID INNER JOIN TABLE_ORG_ROLE R ON R.ROLE_ID=UR.ROLE_ID WHERE U.USR_ID=@USR_ID AND R.IS_DELETED=0", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Role role = new Role();
                    role.ROLE_NAME = reader["ROLE_NAME"].ToString();
                    role.ROLE_ID = reader["ROLE_ID"].ToString();
                    listOfRoles.Add(role);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfRoles;
        }
        #endregion

        /*check organization status*/
        public static string getOrgStatus(SqlConnection dbConnection, string connection_string)
        {
            string status = "INACTIVE";

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_DETAILS WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = connection_string;
                command.Parameters.Add(CONNECTION_STRING);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    status = reader["ORG_STATUS"].ToString();
                }
                dbConnection.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:"+e.Message);
            }
            return status;
        }

    }
}