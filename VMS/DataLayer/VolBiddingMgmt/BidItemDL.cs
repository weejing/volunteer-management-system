﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.VolBidModels;

namespace VMS.DataLayer.VolBiddingMgmt
{
    public class BidItemDL
    {
        static String imageHost = "http://localhost:50348/";
        // 1.4) Publish sponsored items to be bid by volunteers
        // trigers updating of items avaliable

        public static String createBidItem(SqlConnection dataConnection, String itemID, String description, int qty, String startDate)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_USR_BID_ITEM (BID_ITEM_ID, SPR_ITEM_ID, DESCRIPTION, QTY, START_DATE, END_DATE, MIN_POINT, STATUS) VALUES (NEWID(), @SPR_ITEM_ID, @DESCRIPTION, @QTY, @START_DATE, @END_DATE, @MIN_POINT, @STATUS)";

            SqlParameter SPR_ITEM_ID = new SqlParameter("SPR_ITEM_ID", SqlDbType.NVarChar, 100);
            SPR_ITEM_ID.Value = itemID;
            command.Parameters.Add(SPR_ITEM_ID);

            SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
            DESCRIPTION.Value = description;
            command.Parameters.Add(DESCRIPTION);

            SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int, 5);
            QTY.Value = qty;
            command.Parameters.Add(QTY);

            SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.DateTime);
            START_DATE.Value = startDate;
            command.Parameters.Add(START_DATE);

            SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.DateTime);
            END_DATE.Value = System.DateTime.Parse(startDate).AddDays(7);
            command.Parameters.Add(END_DATE);

            SqlParameter MIN_POINT = new SqlParameter("MIN_POINT", SqlDbType.Int, 5);
            MIN_POINT.Value = 1;
            command.Parameters.Add(MIN_POINT);

            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
            STATUS.Value = "bidding";
            command.Parameters.Add(STATUS);

            command.ExecuteNonQuery();

            return "added new bid item";
        }


        // 1.5) Edit sponsored items bid
        public static String updateBidItem(SqlConnection dataConnection, String bidItemID, String description, String startDate)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_ITEM SET DESCRIPTION=@DESCRIPTION, START_DATE=@START_DATE, END_DATE=@END_DATE WHERE BID_ITEM_ID=@BID_ITEM_ID", dataConnection);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
                DESCRIPTION.Value = description;
                command.Parameters.Add(DESCRIPTION);

                SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.DateTime);
                START_DATE.Value = startDate;
                command.Parameters.Add(START_DATE);

                SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.DateTime);
                END_DATE.Value = System.DateTime.Parse(startDate).AddDays(7);
                command.Parameters.Add(END_DATE);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bid item updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "bid item failed to update";
        }


        // 1.6) Remove sponsored items bid
        // trigers updating of items avaliable
        public static String deleteBidItem(SqlConnection dataConnection, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_SPR_USR_BID_ITEM WHERE BID_ITEM_ID=@BID_ITEM_ID", dataConnection);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "deleted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "failed to delete";
        }


        // Retrieve all current items
        /*DL : */
        public static List<BidItem> getAllCurrentBidItems(SqlConnection dataConnection)
        {
            List<BidItem> listOfItems = new List<BidItem>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                //command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID AND END_DATE >= @0";
                command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS, si.IMAGE as IMAGE FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID AND START_DATE <= @0 AND END_DATE >= @0";
                command.Parameters.AddWithValue("@0", System.DateTime.Now);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidItem item = new BidItem();
                    item.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    item.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    item.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    item.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    item.ITEM_DESCRIPTION = reader["DESCRIPTION"].ToString();
                    item.QTY = (int)reader["QTY"];
                    item.SPONSOR_NAME = reader["FIRM_NAME"].ToString();
                    item.START_DATE = reader["START_DATE"].ToString();
                    item.END_DATE = reader["END_DATE"].ToString();
                    item.MIN_POINT = (int)reader["MIN_POINT"];
                    item.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(item);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retrieve all published items
        /*DL : */
        public static List<BidItem> getAllPublishedBidItems(SqlConnection dataConnection)
        {
            List<BidItem> listOfItems = new List<BidItem>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS, ubi.IMAGE FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID AND END_DATE >= @0";
                //command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID AND START_DATE <= @0 AND END_DATE >= @0";
                command.Parameters.AddWithValue("@0", System.DateTime.Now);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidItem item = new BidItem();
                    item.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    item.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    item.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    item.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    item.ITEM_DESCRIPTION = reader["DESCRIPTION"].ToString();
                    item.QTY = (int)reader["QTY"];
                    item.SPONSOR_NAME = reader["FIRM_NAME"].ToString();
                    item.START_DATE = reader["START_DATE"].ToString();
                    item.END_DATE = reader["END_DATE"].ToString();
                    item.MIN_POINT = (int)reader["MIN_POINT"];
                    item.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(item);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retrieve a published bidding item
        /*DL : */
        public static List<BidItem> getBidItemDetails(SqlConnection dataConnection, String bidItemID)
        {
            List<BidItem> listOfItems = new List<BidItem>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS, ubi.NUM_BIDDERS, si.IMAGE FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID AND ubi.BID_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidItem item = new BidItem();
                    item.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    item.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    item.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    item.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    item.ITEM_DESCRIPTION = reader["DESCRIPTION"].ToString();
                    item.QTY = (int)reader["QTY"];
                    item.SPONSOR_NAME = reader["FIRM_NAME"].ToString();
                    item.START_DATE = reader["START_DATE"].ToString();
                    item.END_DATE = reader["END_DATE"].ToString();
                    item.MIN_POINT = (int)reader["MIN_POINT"];
                    item.IMAGE = imageHost + reader["IMAGE"].ToString();
                    item.STATUS = reader["STATUS"].ToString();
                    item.NUM_BIDDERS = (int)reader["NUM_BIDDERS"];
                    listOfItems.Add(item);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retrieve all items
        /*DL : */
        public static List<BidItem> getAllBidItems(SqlConnection dataConnection)
        {
            List<BidItem> listOfItems = new List<BidItem>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT ubi.BID_ITEM_ID, ubi.SPR_ITEM_ID, si.ITEM_NAME, ubi.DESCRIPTION, CATEGORY_NAME, ubi.QTY, START_DATE, END_DATE, MIN_POINT, FIRM_NAME, STATUS, si.IMAGE FROM TABLE_SPR_USR_BID_ITEM ubi, TABLE_SPR_ITEM si, TABLE_SPR_ITEM_CATEGORY ic, TABLE_FIRM_DETAILS fd WHERE ubi.SPR_ITEM_ID=si.SPR_ITEM_ID AND si.SPR_ITEM_CATEGORY_ID=ic.SPR_ITEM_CATEGORY_ID AND si.SPONSOR_USR_ID=fd.USR_ID ORDER BY START_DATE desc";
                command.Parameters.AddWithValue("@0", System.DateTime.Now);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidItem item = new BidItem();
                    item.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    item.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    item.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    item.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    item.ITEM_DESCRIPTION = reader["DESCRIPTION"].ToString();
                    item.QTY = (int)reader["QTY"];
                    item.SPONSOR_NAME = reader["FIRM_NAME"].ToString();
                    item.START_DATE = reader["START_DATE"].ToString();
                    item.END_DATE = reader["END_DATE"].ToString();
                    item.MIN_POINT = (int)reader["MIN_POINT"];
                    item.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(item);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }



    }
}