﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using VMS.Models.VolBidModels;

namespace VMS.DataLayer.VolBiddingMgmt
{
    public class BiddingTransactionDL
    {
        // retrieve current point
        public static int getVolunteerCurrentBidPoints(SqlConnection dataConnection, String userID)
        {
            int points;

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT BID_POINTS FROM TABLE_USR_DETAILS WHERE USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    points = Int32.Parse(reader["BID_POINTS"].ToString());
                    reader.Close();
                    return points;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return -1;
        }



        // update points
        public static String updateVolBidPoint(SqlConnection dataConnection, int bidPoints, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET BID_POINTS = @BID_POINTS WHERE USR_ID = @USR_ID", dataConnection);

                SqlParameter BID_POINTS = new SqlParameter("BID_POINTS", SqlDbType.Int, 5);
                BID_POINTS.Value = bidPoints;
                command.Parameters.Add(BID_POINTS);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bid points updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bid points failed to update";
        }

        // update VIA hours
        public static String updateVolVIAhours(SqlConnection dataConnection, int viaHours, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET VIA_HOURS = @VIA_HOURS WHERE USR_ID = @USR_ID", dataConnection);

                SqlParameter VIA_HOURS = new SqlParameter("VIA_HOURS", SqlDbType.Int, 5);
                VIA_HOURS.Value = viaHours;
                command.Parameters.Add(VIA_HOURS);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "VIA hours updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "VIA hours failed to update";
        }

        // add transaction record for event
        public static String createTransactionRecordForEvent(SqlConnection dataConnection, String userID, int changes, int balance, String userEventID)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_USR_POINT_RECORD(USR_POINT_ID, USR_ID, CHANGES, BALANCE, EVENT_ID, TRANSACTION_INFO, TRANSACTION_DATETIME) VALUES (NEWID(), @USR_ID, @CHANGES, @BALANCE, @EVENT_ID, @TRANSACTION_INFO, @TRANSACTION_DATETIME)";

            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);

            SqlParameter CHANGES = new SqlParameter("CHANGES", SqlDbType.Int, 5);
            CHANGES.Value = changes;
            command.Parameters.Add(CHANGES);

            SqlParameter BALANCE = new SqlParameter("BALANCE", SqlDbType.Int, 5);
            BALANCE.Value = balance;
            command.Parameters.Add(BALANCE);

            SqlParameter EVENT_ID = new SqlParameter("EVENT_ID", SqlDbType.NVarChar, 100);
            EVENT_ID.Value = userEventID;
            command.Parameters.Add(EVENT_ID);

            SqlParameter TRANSACTION_INFO = new SqlParameter("TRANSACTION_INFO", SqlDbType.NVarChar, 100);
            TRANSACTION_INFO.Value = "complete event";
            command.Parameters.Add(TRANSACTION_INFO);

            SqlParameter TRANSACTION_DATETIME = new SqlParameter("TRANSACTION_DATETIME", SqlDbType.DateTime, 100);
            TRANSACTION_DATETIME.Value = System.DateTime.Now.ToString();
            command.Parameters.Add(TRANSACTION_DATETIME);

            command.ExecuteNonQuery();

            return "create new transaction record for completed event";
        }


        // add transaction record for bidding
        public static String createTransactionRecordForBidding(SqlConnection dataConnection, String userID, int changes, int balance, String bidItemID, String transactionInfo)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_USR_POINT_RECORD(USR_POINT_ID, USR_ID, CHANGES, BALANCE, BID_ITEM_ID, TRANSACTION_INFO, TRANSACTION_DATETIME) VALUES (NEWID(), @USR_ID, @CHANGES, @BALANCE, @BID_ITEM_ID, @TRANSACTION_INFO, @TRANSACTION_DATETIME)";

            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);

            SqlParameter CHANGES = new SqlParameter("CHANGES", SqlDbType.Int, 5);
            CHANGES.Value = changes;
            command.Parameters.Add(CHANGES);

            SqlParameter BALANCE = new SqlParameter("BALANCE", SqlDbType.Int, 5);
            BALANCE.Value = balance;
            command.Parameters.Add(BALANCE);

            SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
            BID_ITEM_ID.Value = bidItemID;
            command.Parameters.Add(BID_ITEM_ID);

            SqlParameter TRANSACTION_INFO = new SqlParameter("TRANSACTION_INFO", SqlDbType.NVarChar, 100);
            TRANSACTION_INFO.Value = transactionInfo;
            command.Parameters.Add(TRANSACTION_INFO);

            SqlParameter TRANSACTION_DATETIME = new SqlParameter("TRANSACTION_DATETIME", SqlDbType.DateTime, 100);
            TRANSACTION_DATETIME.Value = System.DateTime.Now.ToString();
            command.Parameters.Add(TRANSACTION_DATETIME);

            command.ExecuteNonQuery();

            return "create new transaction record for bidding";
        }


        // add transaction record for spin wheel
        public static String createTransactionRecordForBonusSpin(SqlConnection dataConnection, String userID, int changes, int balance)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_USR_POINT_RECORD(USR_POINT_ID, USR_ID, CHANGES, BALANCE, TRANSACTION_INFO, TRANSACTION_DATETIME) VALUES (NEWID(), @USR_ID, @CHANGES, @BALANCE, @TRANSACTION_INFO, @TRANSACTION_DATETIME)";

            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);

            SqlParameter CHANGES = new SqlParameter("CHANGES", SqlDbType.Int, 5);
            CHANGES.Value = changes;
            command.Parameters.Add(CHANGES);

            SqlParameter BALANCE = new SqlParameter("BALANCE", SqlDbType.Int, 5);
            BALANCE.Value = balance;
            command.Parameters.Add(BALANCE);

            SqlParameter TRANSACTION_INFO = new SqlParameter("TRANSACTION_INFO", SqlDbType.NVarChar, 100);
            TRANSACTION_INFO.Value = "spin wheel";
            command.Parameters.Add(TRANSACTION_INFO);

            SqlParameter TRANSACTION_DATETIME = new SqlParameter("TRANSACTION_DATETIME", SqlDbType.DateTime, 100);
            TRANSACTION_DATETIME.Value = System.DateTime.Now.ToString();
            command.Parameters.Add(TRANSACTION_DATETIME);

            command.ExecuteNonQuery();

            return "create new transaction record for bonus wheel spinned";
        }

        // add bidding record for bidding
        public static String createBiddingRecord(SqlConnection dataConnection, String userID, String bidItemID, int bidPointAmount)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_USR_BID_RECORD(USR_BID_ID, USR_ID, BID_ITEM_ID, BID_POINTS_AMOUNT, DATE_TIME, STATUS) VALUES (NEWID(), @USR_ID , @BID_ITEM_ID, @BID_POINTS_AMOUNT, @DATE_TIME, @STATUS)";

            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);

            SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
            BID_ITEM_ID.Value = bidItemID;
            command.Parameters.Add(BID_ITEM_ID);

            SqlParameter BID_POINTS_AMOUNT = new SqlParameter("BID_POINTS_AMOUNT", SqlDbType.Int, 5);
            BID_POINTS_AMOUNT.Value = bidPointAmount;
            command.Parameters.Add(BID_POINTS_AMOUNT);

            SqlParameter DATETIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime, 100);
            DATETIME.Value = System.DateTime.Now.ToString();
            command.Parameters.Add(DATETIME);

            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);
            STATUS.Value = "bidding";
            command.Parameters.Add(STATUS);

            command.ExecuteNonQuery();

            return "create new bidding record";
        }


        // Update bidding record
        public static String updateBidItemRecord(SqlConnection dataConnection, int bidPointAmount, String bidItemID, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET BID_POINTS_AMOUNT = @BID_POINTS_AMOUNT, DATE_TIME = @DATE_TIME WHERE BID_ITEM_ID = @BID_ITEM_ID AND USR_ID = @USR_ID AND STATUS <> 'WITHDRAW'", dataConnection);

                SqlParameter BID_POINTS_AMOUNT = new SqlParameter("BID_POINTS_AMOUNT", SqlDbType.Int, 5);
                BID_POINTS_AMOUNT.Value = bidPointAmount;
                command.Parameters.Add(BID_POINTS_AMOUNT);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                SqlParameter DATE_TIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime);
                DATE_TIME.Value = System.DateTime.Now.ToString();
                command.Parameters.Add(DATE_TIME);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bid record updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bid record failed to update";
        }


        // Update (Remove) bidding record
        public static String removeBidItemRecord(SqlConnection dataConnection, String bidItemID, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET STATUS = 'WITHDRAW', BID_POINTS_AMOUNT = 0, DATE_TIME = @DATE_TIME WHERE BID_ITEM_ID = @BID_ITEM_ID AND USR_ID = @USR_ID", dataConnection);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                SqlParameter DATE_TIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime);
                DATE_TIME.Value = System.DateTime.Now.ToString();
                command.Parameters.Add(DATE_TIME);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bid record withdrawn";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bid record failed to update";
        }


        // Get bid item minimum point and number of bidders
        public static BidItem getBidItemDetails(SqlConnection dataConnection, String bidItemID)
        {
            BidItem item = new BidItem();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT MIN_POINT, NUM_BIDDERS FROM TABLE_SPR_USR_BID_ITEM WHERE BID_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    item.MIN_POINT = (int)reader["MIN_POINT"];
                    item.NUM_BIDDERS = (int)reader["NUM_BIDDERS"];
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return item;
        }


        // check if number of bidders is more or equal than quantity avaliable
        public static Boolean checkIfBiddersMoreThanQuantity(SqlConnection dataConnection, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT QTY, NUM_BIDDERS FROM TABLE_SPR_USR_BID_ITEM WHERE BID_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int quantity = (int)reader["QTY"];
                    int numBidders = (int)reader["NUM_BIDDERS"];
                    if (numBidders >= quantity)
                    {
                        reader.Close();
                        return true;
                    }
                    else
                    {
                        reader.Close();
                        return false;
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return false;
        }

        // get quantity 
        public static int getQuantityForBidItem(SqlConnection dataConnection, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT QTY FROM TABLE_SPR_USR_BID_ITEM WHERE BID_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int quantity = (int)reader["QTY"];
                    reader.Close();
                    return quantity;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return 0;
        }

        // check if number of bidders is more or equal than quantity avaliable
        public static int getMinimumBidPointsForBidItem(SqlConnection dataConnection, String bidItemID, int quantity)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT MIN(BID_POINTS_AMOUNT) AS MINPOINTS FROM TABLE_SPR_USR_BID_RECORD WHERE BID_POINTS_AMOUNT IN (SELECT TOP "+ quantity +" BID_POINTS_AMOUNT FROM TABLE_SPR_USR_BID_RECORD WHERE BID_ITEM_ID = @0 ORDER BY BID_POINTS_AMOUNT desc)";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
    
                    int minPoints = (int)reader["MINPOINTS"];
                    reader.Close();
                    return minPoints;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return 0;
        }

        // Update bid item minimum point
        public static String updateBidItemMinPoint(SqlConnection dataConnection, int minPoints, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_ITEM SET MIN_POINT = @MIN_POINT WHERE BID_ITEM_ID = @BID_ITEM_ID", dataConnection);

                SqlParameter MIN_POINT = new SqlParameter("MIN_POINT", SqlDbType.Int, 5);
                MIN_POINT.Value = minPoints;
                command.Parameters.Add(MIN_POINT);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bit item minimum points updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bit item minimum points failed to update";
        }

        // Update bid item minimum point
        public static String updateBidItemNumBidders(SqlConnection dataConnection, int numBidders, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_ITEM SET NUM_BIDDERS = @NUM_BIDDERS WHERE BID_ITEM_ID = @BID_ITEM_ID", dataConnection);

                SqlParameter NUM_BIDDERS = new SqlParameter("NUM_BIDDERS", SqlDbType.Int, 5);
                NUM_BIDDERS.Value = numBidders;
                command.Parameters.Add(NUM_BIDDERS);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "number of bidders updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bit item minimum points failed to update";
        }

        // Update volunteer bid item point
        public static String updateVolBidItemPoint(SqlConnection dataConnection, int bidPoints, String userID, String bidItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET BID_POINTS_AMOUNT = @BID_POINTS_AMOUNT WHERE USR_ID = @USR_ID AND BID_ITEM_ID = @BID_ITEM_ID", dataConnection);

                SqlParameter BID_POINTS_AMOUNT = new SqlParameter("BID_POINTS_AMOUNT", SqlDbType.Int, 5);
                BID_POINTS_AMOUNT.Value = bidPoints;
                command.Parameters.Add(BID_POINTS_AMOUNT);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "bid item points updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "bid item points failed to update";
        }

        public static int getVolunteerCurrentVIAHours(SqlConnection dataConnection, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT VIA_HOURS FROM TABLE_USR_DETAILS WHERE USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int viaHours = (int)reader["VIA_HOURS"];
                    reader.Close();
                    return viaHours;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return -1;
        }


        // Retrieve points transaction (by user)
        public static List<BidPointTransaction> getVolBidPointsTransaction(SqlConnection dataConnection, String userID)
        {
            List<BidPointTransaction> listOfRecords = new List<BidPointTransaction>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "(SELECT USR_POINT_ID, USR_ID, CHANGES, BALANCE, TRANSACTION_INFO +' for ' + ITEM_NAME as TRANSACTION_DESC, TRANSACTION_DATETIME FROM TABLE_SPR_USR_POINT_RECORD p, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s WHERE p.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND USR_ID = @0 UNION SELECT USR_POINT_ID, USR_ID, CHANGES, BALANCE, TRANSACTION_INFO as TRANSACTION_DESC, TRANSACTION_DATETIME FROM TABLE_SPR_USR_POINT_RECORD WHERE USR_ID = @0 AND BID_ITEM_ID IS NULL) ORDER BY TRANSACTION_DATETIME desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidPointTransaction record = new BidPointTransaction();
                    record.USR_POINT_ID = reader["USR_POINT_ID"].ToString();
                    record.USR_ID = reader["USR_ID"].ToString();
                    record.CHANGES = (int)reader["CHANGES"];
                    record.BALANCE = (int)reader["BALANCE"];
                    record.TRANSACTION_INFO = reader["TRANSACTION_DESC"].ToString();
                    record.TRANSACTION_DATETIME = reader["TRANSACTION_DATETIME"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        public static String getLastSpinWheelDateByUser(SqlConnection dataConnection, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT Top 1 TRANSACTION_DATETIME FROM TABLE_SPR_USR_POINT_RECORD WHERE USR_ID = @0 AND TRANSACTION_INFO = 'spin wheel' ORDER BY TRANSACTION_DATETIME desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return reader["TRANSACTION_DATETIME"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "no spin";
        }


        // Retrieve all bidding items history (by user)
        public static List<BidRecord> getVolAllBidItemRecord(SqlConnection dataConnection, String userID)
        {
            List<BidRecord> listOfRecords = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_BID_ID,i.BID_ITEM_ID,s.ITEM_NAME, BID_POINTS_AMOUNT,DATE_TIME,r.STATUS FROM TABLE_SPR_USR_BID_RECORD r, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s WHERE r.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND r.STATUS <> 'WITHDRAW' AND USR_ID = @0 ORDER BY DATE_TIME";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord record = new BidRecord();
                    record.USR_BID_ID = reader["USR_BID_ID"].ToString();
                    record.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    record.DATE_TIME = reader["DATE_TIME"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        // Retrieve bidding items that won (by user)
        public static List<BidRecord> getVolBidItemRecordWon(SqlConnection dataConnection, String userID)
        {
            List<BidRecord> listOfRecords = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_BID_ID,i.BID_ITEM_ID,s.ITEM_NAME, BID_POINTS_AMOUNT,DATE_TIME,r.STATUS FROM TABLE_SPR_USR_BID_RECORD r, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s WHERE r.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND USR_ID = @0 AND r.STATUS = 'won' ORDER BY DATE_TIME";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord record = new BidRecord();
                    record.USR_BID_ID = reader["USR_BID_ID"].ToString();
                    record.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    record.DATE_TIME = reader["DATE_TIME"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        // Retrieve current bidding items (by user)
        public static List<BidRecord> getVolCurrentBidItemRecord(SqlConnection dataConnection, String userID)
        {
            List<BidRecord> listOfRecords = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_BID_ID,i.BID_ITEM_ID,s.ITEM_NAME, BID_POINTS_AMOUNT,DATE_TIME,r.STATUS FROM TABLE_SPR_USR_BID_RECORD r, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s WHERE r.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND USR_ID = @0 AND r.STATUS = 'bidding' ORDER BY DATE_TIME";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord record = new BidRecord();
                    record.USR_BID_ID = reader["USR_BID_ID"].ToString();
                    record.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    record.DATE_TIME = reader["DATE_TIME"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        public static List<BidRecord> getVolCurrentBidItemRecordDetails(SqlConnection dataConnection, String userID, String bidItemID)
        {
            List<BidRecord> listOfRecords = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_BID_ID,i.BID_ITEM_ID,s.ITEM_NAME, BID_POINTS_AMOUNT,DATE_TIME,r.STATUS FROM TABLE_SPR_USR_BID_RECORD r, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s WHERE r.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND USR_ID = @0 AND i.BID_ITEM_ID=@1 AND r.STATUS <> 'WITHDRAW' ORDER BY DATE_TIME";
                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord record = new BidRecord();
                    record.USR_BID_ID = reader["USR_BID_ID"].ToString();
                    record.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    record.DATE_TIME = reader["DATE_TIME"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        // Retrieve a bid item bidding details (by admin)
        public static List<BidRecord> getBidItemRecordDetails(SqlConnection dataConnection, String bidItemID)
        {
            List<BidRecord> listOfRecords = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_BID_ID,i.BID_ITEM_ID, FULL_NAME, s.ITEM_NAME, BID_POINTS_AMOUNT,DATE_TIME,r.STATUS FROM TABLE_SPR_USR_BID_RECORD r, TABLE_SPR_USR_BID_ITEM i, TABLE_SPR_ITEM s, TABLE_USR_DETAILS u WHERE r.BID_ITEM_ID = i.BID_ITEM_ID AND s.SPR_ITEM_ID = i.SPR_ITEM_ID AND u.USR_ID = r.USR_ID AND i.BID_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord record = new BidRecord();
                    record.USR_BID_ID = reader["USR_BID_ID"].ToString();
                    record.BID_ITEM_ID = reader["BID_ITEM_ID"].ToString();
                    record.USR_NAME = reader["FULL_NAME"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    record.DATE_TIME = reader["DATE_TIME"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        // Update bidding record, select winners
        public static String updateBidItemRecordSelectWinners(SqlConnection dataConnection, String bidItemID, int minPoint, int quantity)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET STATUS = 'won' WHERE USR_BID_ID IN (SELECT TOP " + quantity + " USR_BID_ID FROM TABLE_SPR_USR_BID_RECORD WHERE BID_ITEM_ID = @BID_ITEM_ID AND STATUS <> 'WITHDRAW' AND BID_POINTS_AMOUNT >= 1 ORDER BY BID_POINTS_AMOUNT desc, DATE_TIME)", dataConnection);
                //SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET STATUS = 'won' WHERE BID_ITEM_ID = @BID_ITEM_ID AND STATUS <> 'WITHDRAW' AND BID_POINTS_AMOUNT >= @BID_POINTS_AMOUNT", dataConnection);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                SqlParameter MIN_POINT = new SqlParameter("BID_POINTS_AMOUNT", SqlDbType.Int, 5);
                MIN_POINT.Value = minPoint;
                command.Parameters.Add(MIN_POINT);

                command.Prepare();
                command.ExecuteNonQuery();
                return "winners selected";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "winners failed to select";
        }


        // Update bidding record, select winners
        public static String updateBidItemRecordSelectLosers(SqlConnection dataConnection, String bidItemID, int minPoint)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_RECORD SET STATUS = 'unsuccessful' WHERE BID_ITEM_ID = @BID_ITEM_ID AND STATUS = 'bidding'", dataConnection);

                SqlParameter BID_ITEM_ID = new SqlParameter("BID_ITEM_ID", SqlDbType.NVarChar, 100);
                BID_ITEM_ID.Value = bidItemID;
                command.Parameters.Add(BID_ITEM_ID);

                SqlParameter MIN_POINT = new SqlParameter("BID_POINTS_AMOUNT", SqlDbType.Int, 5);
                MIN_POINT.Value = minPoint;
                command.Parameters.Add(MIN_POINT);

                command.Prepare();
                command.ExecuteNonQuery();
                return "losers selected";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "losers failed to select";
        }


        // Get list of winners
        public static List<BidRecord> getWinnersFromBiddingRecord(SqlConnection dataConnection, String bidItemID)
        {
            List<BidRecord> listOfUsers = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_ID, BID_POINTS_AMOUNT FROM TABLE_SPR_USR_BID_RECORD WHERE BID_ITEM_ID = @0 AND STATUS = 'won'";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord userRecord = new BidRecord();
                    userRecord.USR_ID = reader["USR_ID"].ToString();
                    userRecord.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    listOfUsers.Add(userRecord);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfUsers;
        }


        // Get list of losers
        public static List<BidRecord> getLosersFromBiddingRecord(SqlConnection dataConnection, String bidItemID)
        {
            List<BidRecord> listOfUsers = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_ID, BID_POINTS_AMOUNT FROM TABLE_SPR_USR_BID_RECORD WHERE BID_ITEM_ID = @0 AND STATUS = 'unsuccessful'";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord userRecord = new BidRecord();
                    userRecord.USR_ID = reader["USR_ID"].ToString();
                    userRecord.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    listOfUsers.Add(userRecord);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfUsers;
        }


        // Get user points
        public static int getUserPoints(SqlConnection dataConnection, String userID)
        {
            int currentPoints = 0;

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT BID_POINTS FROM TABLE_USR_DETAILS WHERE USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    currentPoints = (int)reader["BID_POINTS"];
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return currentPoints;
        }


        // Get list of winners
        public static List<BidRecord> getBidItemsThatEnd(SqlConnection dataConnection, String bidItemID)
        {
            List<BidRecord> listOfUsers = new List<BidRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_ID, BID_POINTS_AMOUNT FROM TABLE_SPR_USR_BID_RECORD WHERE BID_ITEM_ID = @0 AND STATUS = 'won'";
                command.Parameters.AddWithValue("@0", bidItemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidRecord userRecord = new BidRecord();
                    userRecord.USR_ID = reader["USR_ID"].ToString();
                    userRecord.BID_POINTS_AMOUNT = (int)reader["BID_POINTS_AMOUNT"];
                    listOfUsers.Add(userRecord);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfUsers;
        }


        // Update bid item status
        public static void updateBidItemStatusToCheck(SqlConnection dataConnection)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_ITEM SET STATUS = 'checking' WHERE END_DATE < @CURRENT_DATE AND STATUS = 'bidding'", dataConnection);

                SqlParameter CURRENT_DATE = new SqlParameter("CURRENT_DATE", SqlDbType.DateTime, 5);
                CURRENT_DATE.Value = System.DateTime.Now;
                command.Parameters.Add(CURRENT_DATE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


        // Get bid items that just end
        public static List<String> getBidItemThatJustEnd(SqlConnection dataConnection)
        {
            List<String> listOfItemsID = new List<String>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT BID_ITEM_ID FROM TABLE_SPR_USR_BID_ITEM WHERE STATUS = 'checking'";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listOfItemsID.Add(reader["BID_ITEM_ID"].ToString());
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItemsID;
        }


        // Update bid item status
        public static void updateBidItemStatusToEnd(SqlConnection dataConnection)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_USR_BID_ITEM SET STATUS = 'end' WHERE STATUS = 'checking'", dataConnection);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }


    }
}