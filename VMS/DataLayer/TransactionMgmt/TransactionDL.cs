﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.TransactionModels;
using System.Collections;
using VMS.Models.TenderingModels;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.TransactionMgmt
{
    public class TransactionDL
    {
        /// <summary>DAL: Insert new billing record (for checking)</summary>
        public static void insertBilling(SqlConnection dbConnection, BillingStatus billStatus)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_ORG_BIL VALUES (@BIL_ID, @STATUS, @ORG_ID, @MONTH, @YEAR)", dbConnection);

                SqlParameter BIL_ID = new SqlParameter("BIL_ID", SqlDbType.NVarChar, 50);
                BIL_ID.Value = newID;
                command.Parameters.Add(BIL_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);
                STATUS.Value = billStatus.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 50);
                ORG_ID.Value = billStatus.ORG_ID;
                command.Parameters.Add(ORG_ID);

                SqlParameter MONTH = new SqlParameter("MONTH", SqlDbType.Int);
                MONTH.Value = billStatus.MONTH;
                command.Parameters.Add(MONTH);

                SqlParameter YEAR = new SqlParameter("YEAR", SqlDbType.Int);
                YEAR.Value = billStatus.YEAR;
                command.Parameters.Add(YEAR);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertBilling---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        /// <summary>DAL: Insert new payment</summary>
        public static String insertPayment(SqlConnection dbConnection, OrgPayment orgPayment)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_ORG_PAYMENT VALUES (@ORG_PAYMENT_ID, @ORG_ID, @AMT, @PAYMENT_TIME, @PLAN_TYPE,@FROM_DATE,@TO_DATE)", dbConnection);

                SqlParameter ORG_PAYMENT_ID = new SqlParameter("ORG_PAYMENT_ID", SqlDbType.NVarChar, 50);
                ORG_PAYMENT_ID.Value = newID;
                command.Parameters.Add(ORG_PAYMENT_ID);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 50);
                ORG_ID.Value = orgPayment.ORG_ID;
                command.Parameters.Add(ORG_ID);

                SqlParameter AMT = new SqlParameter("AMT", SqlDbType.Decimal);
                AMT.Precision = 18;
                AMT.Scale = 2;
                AMT.Value = Convert.ToDecimal(orgPayment.AMT);
                command.Parameters.Add(AMT);

                SqlParameter PAYMENT_TIME = new SqlParameter("PAYMENT_TIME", SqlDbType.DateTime);
                PAYMENT_TIME.Value = DateTime.Now;
                command.Parameters.Add(PAYMENT_TIME);

                SqlParameter PLAN_TYPE = new SqlParameter("PLAN_TYPE", SqlDbType.NVarChar, 50);
                PLAN_TYPE.Value = orgPayment.PLAN_TYPE;
                command.Parameters.Add(PLAN_TYPE);

                DateTime theDateNow = DateTime.Now;
                SqlParameter FROM_DATE = new SqlParameter("FROM_DATE", SqlDbType.DateTime);
                FROM_DATE.Value = theDateNow;
                command.Parameters.Add(FROM_DATE);

                DateTime yearFuture = theDateNow.AddYears(1);
                SqlParameter TO_DATE = new SqlParameter("TO_DATE", SqlDbType.DateTime);
                TO_DATE.Value = yearFuture;
                command.Parameters.Add(TO_DATE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }

        /// <summary>DAL: Get tendering item details from volunteer organisation table</summary>
        public static BillingStatus checkBillStatus(SqlConnection dbConnection, BillingStatus bill)
        {
            BillingStatus billDetails = new BillingStatus();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_BIL WHERE MONTH=@MONTH AND YEAR=@YEAR AND ORG_ID=@ORG_ID", dbConnection);

                SqlParameter MONTH = new SqlParameter("MONTH", SqlDbType.NVarChar, 100);
                MONTH.Value = bill.MONTH;
                command.Parameters.Add(MONTH);

                SqlParameter YEAR = new SqlParameter("YEAR", SqlDbType.NVarChar, 100);
                YEAR.Value = bill.YEAR;
                command.Parameters.Add(YEAR);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 100);
                ORG_ID.Value = bill.ORG_ID;
                command.Parameters.Add(ORG_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    billDetails.BIL_ID = reader["BIL_ID"].ToString();
                    billDetails.STATUS = reader["STATUS"].ToString();
                    billDetails.ORG_ID = reader["ORG_ID"].ToString();
                    billDetails.MONTH = Convert.ToInt32(reader["MONTH"].ToString());
                    billDetails.YEAR = Convert.ToInt32(reader["YEAR"].ToString());
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getInventoryItemInfo---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return billDetails;
        }

        //get individual latest transaction information
        public static Transaction getOrgLatestTransactionDL(SqlConnection dbConnection, String org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"select * from TABLE_ORG_PAYMENT p1
left join TABLE_ORG_PLAN_TYPE t ON p1.PLAN_TYPE = t.PLAN_TYPE_ID
where p1.ORG_ID = @org_id and p1.TO_DATE = (
select max(p2.TO_DATE) from TABLE_ORG_PAYMENT p2 where p2.ORG_ID =  @org_id
)";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_id;
                command.Parameters.Add(sql_name);

                SqlDataReader dataReader = command.ExecuteReader();
                Transaction payment = null;

                while (dataReader.Read())
                {
                    payment = new Transaction();
                    payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    payment.AMT = dataReader["AMT"].ToString();
                    payment.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    payment.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    payment.CRM_ID = dataReader["CRM_ID"].ToString();
                    payment.FROM_DATE = correctDateTimeFormat(0, dataReader["FROM_DATE"].ToString());
                    payment.TO_DATE = correctDateTimeFormat(0, dataReader["TO_DATE"].ToString());
                    payment.PLAN_NAME = dataReader["PLAN_NAME"].ToString();
                    payment.PROFILE_ID = dataReader["PAYPAL_PROFILE"].ToString();
                }
                dataReader.Close();
                return payment;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }

        public static string correctDateTimeFormat(int type, string dateStr)
        {
            DateTime date = new DateTime();
            DateTime.TryParse(dateStr, out date);

            if (type == 0)
            {
                return date.ToString("dd/MM/yyyy");
            }
            else
            {
                return date.ToString("dd/MM/yyyy HH':'mm");
            }

        }
        //create new transaction
        public static String createNewTransaction(SqlConnection dbConnection, OrgPayment orgPayment)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                string sqlString = "";

                if (orgPayment.CRM_ID == null || orgPayment.CRM_ID.Equals(""))
                {
                    sqlString = @"INSERT INTO TABLE_ORG_PAYMENT
(ORG_PAYMENT_ID,ORG_ID,AMT,PAYMENT_TIME ,PLAN_TYPE ,FROM_DATE ,TO_DATE,PAYMENT_TYPE,PAYPAL_FEE,PAYPAL_ID, PAYPAL_PROFILE)
VALUES (@ORG_PAYMENT_ID, @ORG_ID, @AMT, @PAYMENT_TIME, @PLAN_TYPE,@FROM_DATE,@TO_DATE, @PAYMENT_TYPE, @PAYPAL_FEE, @PAYPAL_ID, @PROFILE_ID)";
                }
                else
                {
                    sqlString = @"INSERT INTO TABLE_ORG_PAYMENT VALUES 
(@ORG_PAYMENT_ID, @ORG_ID, @AMT, @PAYMENT_TIME, @PLAN_TYPE,@FROM_DATE,@TO_DATE, @CRM_ID, @PAYMENT_TYPE, @PAYPAL_FEE, @PAYPAL_ID, @PROFILE_ID)";
                }


                SqlCommand command = new SqlCommand(sqlString, dbConnection);

                SqlParameter ORG_PAYMENT_ID = new SqlParameter("ORG_PAYMENT_ID", SqlDbType.NVarChar, 50);
                ORG_PAYMENT_ID.Value = newID;
                command.Parameters.Add(ORG_PAYMENT_ID);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 50);
                ORG_ID.Value = orgPayment.ORG_ID;
                command.Parameters.Add(ORG_ID);

                SqlParameter AMT = new SqlParameter("AMT", SqlDbType.Decimal);
                AMT.Precision = 18;
                AMT.Scale = 2;
                AMT.Value = Convert.ToDecimal(orgPayment.AMT);
                command.Parameters.Add(AMT);

                SqlParameter PAYMENT_TIME = new SqlParameter("PAYMENT_TIME", SqlDbType.DateTime);
                PAYMENT_TIME.Value = DateTime.Now;
                command.Parameters.Add(PAYMENT_TIME);

                SqlParameter PLAN_TYPE = new SqlParameter("PLAN_TYPE", SqlDbType.NVarChar, 50);
                PLAN_TYPE.Value = orgPayment.PLAN_TYPE_ID;
                command.Parameters.Add(PLAN_TYPE);

                SqlParameter FROM_DATE = new SqlParameter("FROM_DATE", SqlDbType.NVarChar, 50);
                FROM_DATE.Value = orgPayment.FROM_DATE;
                command.Parameters.Add(FROM_DATE);

                SqlParameter TO_DATE = new SqlParameter("TO_DATE", SqlDbType.NVarChar, 50);
                TO_DATE.Value = orgPayment.TO_DATE;
                command.Parameters.Add(TO_DATE);

                if (orgPayment.CRM_ID != null)
                {
                    SqlParameter CRM_ID = new SqlParameter("CRM_ID", SqlDbType.NVarChar, 50);
                    CRM_ID.Value = orgPayment.CRM_ID;
                    command.Parameters.Add(CRM_ID);
                }

                SqlParameter PAYMENT_TYPE = new SqlParameter("PAYMENT_TYPE", SqlDbType.NVarChar, 50);
                PAYMENT_TYPE.Value = orgPayment.PAYMENT_TYPE;
                command.Parameters.Add(PAYMENT_TYPE);

                SqlParameter PAYPAL_FEE = new SqlParameter("PAYPAL_FEE", SqlDbType.Decimal);
                PAYPAL_FEE.Precision = 18;
                PAYPAL_FEE.Scale = 2;
                PAYPAL_FEE.Value = Convert.ToDecimal(orgPayment.PAYPAL_FEE);
                command.Parameters.Add(PAYPAL_FEE);

                SqlParameter PAYPAL_ID = new SqlParameter("PAYPAL_ID", SqlDbType.NVarChar, 25);
                PAYPAL_ID.Value = orgPayment.PAYPAL_ID;
                command.Parameters.Add(PAYPAL_ID);

                SqlParameter PROFILE_ID = new SqlParameter("PROFILE_ID", SqlDbType.NVarChar, 25);
                PROFILE_ID.Value = orgPayment.PROFILE_ID;
                command.Parameters.Add(PROFILE_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }

        //TIME TRIGGER EVENT
        //get organisations that should be revoked
        public static List<String> getOrganisationRevokeListDL(SqlConnection dbConnection, string date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "select * from TABLE_ORG_PAYMENT m1 left join TABLE_ORG_PAYMENT m2 ON(m1.ORG_ID = m2.ORG_ID AND m1.PAYMENT_TIME < m2.PAYMENT_TIME) left join TABLE_ORG_DETAILS d ON m1.ORG_ID = d.ORG_ID where m2.ORG_PAYMENT_ID is null and d.ORG_STATUS = 'ACTIVE' and m1.TO_DATE < @date";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_date = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_date.Value = date;
                command.Parameters.Add(sql_date);

                SqlDataReader dataReader = command.ExecuteReader();
                List<String> crm_list = new List<String>();


                while (dataReader.Read())
                {
                    String temp_payment = dataReader["ORG_ID"].ToString();
                    crm_list.Add(temp_payment);
                }
                dataReader.Close();
                return crm_list;

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return null;
            }
        }
        //revoke organisation status
        public static void revokeOrgDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "update TABLE_ORG_DETAILS set ORG_STATUS = 'INACTIVE' where ORG_ID = @org_id";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
            }
        }
        //revoke organisation status
        public static List<Transaction> getRecurringOrg(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                string date = DateTime.Now.ToString("yyyy-M-dd");
                date = "2016-11-06"; // this is for system release 3 only
                //string date = DateTime.Now.AddDays(-1).ToString("MM/dd/yy");
                string str_sql = @"select DISTINCT(o.ORG_ID), p.PAYPAL_PROFILE, p.PLAN_TYPE, t.PER, p.CRM_ID
from TABLE_ORG_DETAILS o
left join TABLE_ORG_PAYMENT p ON o.ORG_ID = p.ORG_ID
left join TABLE_ORG_PLAN_TYPE t ON t.PLAN_TYPE_ID = p.PLAN_TYPE
where o.RECURRING_PAYMENT = '1' and  CONVERT(CHAR(10),p.TO_DATE,120) >= @date"; //for purpose of system release 3, data after above @date would be called. else CONVERT(CHAR(10),p.TO_DATE,120) = @date
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sqldate = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sqldate.Value = date;
                command.Parameters.Add(sqldate);

                SqlDataReader dataReader = command.ExecuteReader();
                List<Transaction> list = new List<Transaction>();

                while (dataReader.Read())
                {
                    Transaction item = new Transaction();
                    item.ORG_ID = dataReader["ORG_ID"].ToString();
                    item.PROFILE_ID = dataReader["PAYPAL_PROFILE"].ToString();
                    item.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    item.PER = dataReader["PER"].ToString();
                    item.CRM_ID = dataReader["CRM_ID"].ToString();
                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
            }
            return null;
        }
        //create recurring transaction
        public static void createRecurringTransaction(SqlConnection dbConnection, Transaction item, string startDate, string endDate)
        {
            String newID = Guid.NewGuid().ToString();
            try
            {
                bool crmBool = true;
                string crmID = item.CRM_ID;
                String str_sql = @"INSERT INTO TABLE_ORG_PAYMENT
           (ORG_PAYMENT_ID
           ,ORG_ID
           ,AMT
           ,PAYMENT_TIME
           ,PLAN_TYPE
           ,FROM_DATE
           ,TO_DATE
           ,CRM_ID
           ,PAYMENT_TYPE
           ,PAYPAL_FEE
           ,PAYPAL_ID
           ,PAYPAL_PROFILE)
     VALUES
           (@newID
           ,@orgID
           ,@amt
           ,@paymentTime
           ,@planType
           ,@fromDate
           ,@toDate
           ,@crmID
           ,@paymentType
           ,@paypalFee
           ,@paypalID
           ,@paypalProfile)";

                if (crmID == null || crmID.Equals(""))
                {
                    crmBool = false;
                    str_sql = @"INSERT INTO TABLE_ORG_PAYMENT
           (ORG_PAYMENT_ID
           ,ORG_ID
           ,AMT
           ,PAYMENT_TIME
           ,PLAN_TYPE
           ,FROM_DATE
           ,TO_DATE
           ,PAYMENT_TYPE
           ,PAYPAL_FEE
           ,PAYPAL_ID
           ,PAYPAL_PROFILE)
     VALUES
           (@newID
           ,@orgID
           ,@amt
           ,@paymentTime
           ,@planType
           ,@fromDate
           ,@toDate
           ,@paymentType
           ,@paypalFee
           ,@paypalID
           ,@paypalProfile)";
                }
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter newID_sql = new SqlParameter("newID", SqlDbType.NVarChar, 50);
                newID_sql.Value = newID;
                command.Parameters.Add(newID_sql);

                SqlParameter orgID_sql = new SqlParameter("orgID", SqlDbType.NVarChar, 50);
                orgID_sql.Value = item.ORG_ID;
                command.Parameters.Add(orgID_sql);

                SqlParameter AMT = new SqlParameter("amt", SqlDbType.Decimal);
                AMT.Precision = 18;
                AMT.Scale = 2;
                AMT.Value = Convert.ToDecimal(item.AMT);
                command.Parameters.Add(AMT);

                SqlParameter paymentTime_sql = new SqlParameter("paymentTime", SqlDbType.NVarChar, 50);
                paymentTime_sql.Value = item.PAYMENT_TIME;
                command.Parameters.Add(paymentTime_sql);

                SqlParameter planType_sql = new SqlParameter("planType", SqlDbType.NVarChar, 50);
                planType_sql.Value = item.PLAN_TYPE;
                command.Parameters.Add(planType_sql);

                SqlParameter fromDate_sql = new SqlParameter("fromDate", SqlDbType.NVarChar, 50);
                fromDate_sql.Value = startDate;
                command.Parameters.Add(fromDate_sql);

                SqlParameter toDate_sql = new SqlParameter("toDate", SqlDbType.NVarChar, 50);
                toDate_sql.Value = endDate;
                command.Parameters.Add(toDate_sql);

                if (!crmBool)
                {
                    SqlParameter crmID_sql = new SqlParameter("crmID", SqlDbType.NVarChar, 50);
                    crmID_sql.Value = item.CRM_ID;
                    command.Parameters.Add(crmID_sql);
                }


                SqlParameter paymentType_sql = new SqlParameter("paymentType", SqlDbType.NVarChar, 50);
                paymentType_sql.Value = "RECURRING";
                command.Parameters.Add(paymentType_sql);

                SqlParameter paypalFee_sql = new SqlParameter("paypalFee", SqlDbType.NVarChar, 50);
                paypalFee_sql.Value = item.PAYPAL_FEE.Substring(1);
                command.Parameters.Add(paypalFee_sql);

                SqlParameter paypalID_sql = new SqlParameter("paypalID", SqlDbType.NVarChar, 50);
                paypalID_sql.Value = item.PAYPAL_ID;
                command.Parameters.Add(paypalID_sql);

                SqlParameter paypalProfile_sql = new SqlParameter("paypalProfile", SqlDbType.NVarChar, 50);
                paypalProfile_sql.Value = item.PROFILE_ID.Split(' ')[0];
                command.Parameters.Add(paypalProfile_sql);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        //PAYMENT PLANS -RELATED CODES
        //===============================
        //add payment plan
        public static void addPlanDL(SqlConnection dbConnection, string planName, string amount, string duration, string featuresHtml)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"INSERT INTO TABLE_ORG_PLAN_TYPE(PLAN_TYPE_ID,PLAN_NAME ,AMOUNT,DATE_CREATED ,ACTIVE,FEATURE,PER)
     VALUES (@newID ,@planName,@amount,GETDATE() ,'1' ,@featuresHtml ,@duration)";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("newID", SqlDbType.NVarChar, 50);
                sql_name.Value = newID;
                command.Parameters.Add(sql_name);

                SqlParameter sql_planName = new SqlParameter("planName", SqlDbType.NVarChar, 50);
                sql_planName.Value = planName;
                command.Parameters.Add(sql_planName);

                SqlParameter sql_amount = new SqlParameter("amount", SqlDbType.NVarChar, 50);
                sql_amount.Value = amount;
                command.Parameters.Add(sql_amount);

                SqlParameter sql_featuresHtml = new SqlParameter("featuresHtml", SqlDbType.NVarChar, 50);
                sql_featuresHtml.Value = featuresHtml;
                command.Parameters.Add(sql_featuresHtml);

                SqlParameter sql_duration = new SqlParameter("duration", SqlDbType.NVarChar, 50);
                sql_duration.Value = duration;
                command.Parameters.Add(sql_duration);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

            }
        }
        //get avaliable payment plan
        public static List<OrgPayment> getAvaliablePaymentPlanDL(SqlConnection dbConnection, string type)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = "";

                if (type.Equals("0"))
                {
                    str_sql = "select * from TABLE_ORG_PLAN_TYPE order by ACTIVE desc, AMOUNT desc";
                }
                else
                {
                    str_sql = @"select *
  from TABLE_ORG_PLAN_TYPE
  where ACTIVE = '1'
  order by ACTIVE desc, AMOUNT desc";
                }

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> planList = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_org = new OrgPayment();

                    temp_org.AMT = dataReader["AMOUNT"].ToString();
                    temp_org.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();
                    temp_org.PLAN_TYPE_ID = dataReader["PLAN_TYPE_ID"].ToString();
                    temp_org.PLAN_ACTIVE = dataReader["ACTIVE"].ToString();
                    temp_org.PLAN_DESCRIPTION = dataReader["FEATURE"].ToString();
                    temp_org.PER = dataReader["PER"].ToString();

                    planList.Add(temp_org);
                }
                dataReader.Close();
                return planList;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //update payment plan
        public static String updatePaymentPlanDL(SqlConnection dbConnection, string id, string featuresHtml)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //sql command
                SqlCommand command = new SqlCommand(@"UPDATE TABLE_ORG_PLAN_TYPE
   SET FEATURE = @FEATURE
 WHERE PLAN_TYPE_ID = @PLAN_TYPE_ID", dbConnection);

                //insert into sql
                SqlParameter PLAN_TYPE_ID = new SqlParameter("PLAN_TYPE_ID", SqlDbType.NVarChar, 50);
                PLAN_TYPE_ID.Value = id;
                command.Parameters.Add(PLAN_TYPE_ID);

                SqlParameter FEATURE = new SqlParameter("FEATURE", SqlDbType.NVarChar);
                FEATURE.Value = featuresHtml;
                command.Parameters.Add(FEATURE);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }
        //update payment plan status
        public static String updatePaymentPlanStatusDL(SqlConnection dbConnection, string id, string status)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //sql command
                SqlCommand command = new SqlCommand(@"UPDATE TABLE_ORG_PLAN_TYPE
   SET ACTIVE = @status
 WHERE PLAN_TYPE_ID = @PLAN_TYPE_ID", dbConnection);

                //insert into sql
                SqlParameter PLAN_TYPE_ID = new SqlParameter("PLAN_TYPE_ID", SqlDbType.NVarChar, 50);
                PLAN_TYPE_ID.Value = id;
                command.Parameters.Add(PLAN_TYPE_ID);

                SqlParameter sql_status = new SqlParameter("status", SqlDbType.NVarChar, 50);
                sql_status.Value = status;
                command.Parameters.Add(sql_status);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }
        //get subscriber number
        public static string getPlanSubscriberDL(SqlConnection dbConnection, string payID)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //sql command
                SqlCommand command = new SqlCommand(@"select COUNT(DISTINCT ORG_ID) as SUB_NUM
from TABLE_ORG_PAYMENT
where TO_DATE > GETDATE() and PLAN_TYPE = @payID", dbConnection);

                //insert into sql
                SqlParameter sql_payID = new SqlParameter("payID", SqlDbType.NVarChar, 50);
                sql_payID.Value = payID;
                command.Parameters.Add(sql_payID);

                SqlDataReader dataReader = command.ExecuteReader();
                string ans = "";
                while (dataReader.Read())
                {
                    ans = dataReader["SUB_NUM"].ToString();
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }

        //ORGANISATION -RELATED METHODS

        //get all organisation list in database
        public static List<OrganisationDetails> getOrganisationListDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = @"select  o.ORG_ID, o.ORG_NAME , o.CONNECTION_STRING, o.EMAIL, o.LINK,  o.ADDRESS, o.DESCRIPTION, o.ORG_TYPE, 
o.ORG_STATUS, o.VERIFICATION, o.RECURRING_PAYMENT, p.TO_DATE 
from TABLE_ORG_DETAILS o LEFT JOIN TABLE_ORG_PAYMENT p ON o.ORG_ID = p.ORG_ID 
where p.TO_DATE IS NOT NULL and o.ORG_STATUS = 'ACTIVE' 
and p.TO_DATE = (select  MAX(p1.TO_DATE) from TABLE_ORG_DETAILS o1 LEFT JOIN TABLE_ORG_PAYMENT p1 ON o.ORG_ID = p1.ORG_ID ) 
order by p.TO_DATE ASC";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();
                List<OrganisationDetails> org_list = new List<OrganisationDetails>();

                while (dataReader.Read())
                {

                    OrganisationDetails temp_org = new OrganisationDetails();

                    temp_org.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_org.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_org.EMAIL = dataReader["EMAIL"].ToString();
                    temp_org.LINK = dataReader["LINK"].ToString();
                    temp_org.ADDRESS = dataReader["ADDRESS"].ToString();
                    temp_org.DESCRIPTION = dataReader["DESCRIPTION"].ToString();
                    temp_org.ORG_TYPE = dataReader["ORG_TYPE"].ToString();
                    temp_org.ORG_STATUS = dataReader["ORG_STATUS"].ToString();
                    temp_org.RECURRING_PAYMENT = dataReader["RECURRING_PAYMENT"].ToString();

                    org_list.Add(temp_org);
                }
                dataReader.Close();
                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get transactions related to organisation
        public static List<OrgPayment> getOrgTransactionDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"SELECT * FROM TABLE_ORG_DETAILS 
left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_CRM on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_CRM.CRM_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_DETAILS.ORG_ID = @org_id order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";
                //string str_sql = "SELECT * FROM TABLE_ORG_DETAILS left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID left join TABLE_ORG_CRM on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_CRM.CRM_ID left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID where TABLE_ORG_DETAILS.ORG_ID = @org_id and ORG_STATUS = @org_status order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_id;
                command.Parameters.Add(sql_name);


                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> org_list = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_org = new OrgPayment();

                    temp_org.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_org.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_org.AMT = dataReader["AMT"].ToString();
                    temp_org.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    temp_org.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    temp_org.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();
                    temp_org.CRM_ID = dataReader["CRM_ID"].ToString();
                    temp_org.DISCOUNT = dataReader["DISCOUNT"].ToString();
                    temp_org.CRM_STATUS = dataReader["CRM_STATUS"].ToString();
                    temp_org.VERIFY = dataReader["VERIFICATION"].ToString();
                    temp_org.FROM_DATE = correctDateTimeFormat(0, dataReader["FROM_DATE"].ToString().Split(' ')[0]);
                    temp_org.TO_DATE = correctDateTimeFormat(0, dataReader["TO_DATE"].ToString().Split(' ')[0]);
                    temp_org.PLAN_AMOUNT = dataReader["AMOUNT"].ToString();
                    temp_org.PER = dataReader["PER"].ToString();


                    org_list.Add(temp_org);
                }
                dataReader.Close();



                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get latest payment plan
        public static List<CRM> getOrgDiscountDataDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"select *
from TABLE_ORG_CRM 
where ORG_ID = @org_id and CRM_STATUS = 'NOT USED'";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_id;
                command.Parameters.Add(sql_name);

                SqlDataReader dataReader = command.ExecuteReader();
                List<CRM> crmList = new List<CRM>();

                while (dataReader.Read())
                {
                    CRM temp = new CRM();

                    temp.CRM_ID = dataReader["CRM_ID"].ToString();
                    temp.DISCOUNT = dataReader["DISCOUNT"].ToString();
                    temp.PLAN_TYPE_ID = dataReader["PLAN_TYPE_ID"].ToString().Split(' ')[0];
                    temp.CRM_STATUS = dataReader["CRM_STATUS"].ToString();
                    temp.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp.EXPIRED_DATE = correctDateTimeFormat(0, dataReader["EXPIRED_DATE"].ToString());

                    crmList.Add(temp);
                }
                dataReader.Close();
                return crmList;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get transactions related to organisation
        public static OrgPayment getOrgDetailsDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"select * from TABLE_ORG_DETAILS where ORG_ID = @org_id";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_id;
                command.Parameters.Add(sql_name);


                SqlDataReader dataReader = command.ExecuteReader();
                OrgPayment orgdetail = new OrgPayment();

                while (dataReader.Read())
                {

                    orgdetail.ORG_ID = dataReader["ORG_ID"].ToString();
                    orgdetail.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    orgdetail.ORG_STATUS = dataReader["ORG_STATUS"].ToString();
                    orgdetail.PAYMENT_TYPE = dataReader["RECURRING_PAYMENT"].ToString();
                    orgdetail.LINK = dataReader["LINK"].ToString();
                    orgdetail.ADDRESS = dataReader["ADDRESS"].ToString();
                    orgdetail.EMAIL = dataReader["EMAIL"].ToString();
                    orgdetail.ORG_TYPE = dataReader["ORG_TYPE"].ToString();
                    orgdetail.VERIFICATION = dataReader["VERIFICATION"].ToString();
                }
                dataReader.Close();
                return orgdetail;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get organisation list search result
        public static List<OrganisationDetails> getOrganisationSearchDL(SqlConnection dbConnection, string name, string email, string type, string org_status)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                Boolean bool_name = false;
                Boolean bool_email = false;
                Boolean bool_type = false;
                Boolean bool_status = false;

                //defining sql statements
                string str_sql_command = "select  o.ORG_ID, o.ORG_NAME , o.CONNECTION_STRING, o.EMAIL, o.LINK, o.ADDRESS, o.DESCRIPTION, o.ORG_TYPE, o.ORG_STATUS, o.VERIFICATION, p.TO_DATE from TABLE_ORG_DETAILS o LEFT JOIN TABLE_ORG_PAYMENT p ON o.ORG_ID = p.ORG_ID where p.TO_DATE IS NOT NULL and p.TO_DATE = (select  MAX(p1.TO_DATE) from TABLE_ORG_DETAILS o1 LEFT JOIN TABLE_ORG_PAYMENT p1 ON o.ORG_ID = p1.ORG_ID)";
                List<String> sql_array = new List<String>();

                if (org_status.Equals("ACTIVE"))
                {
                    bool_status = true;
                    str_sql_command += "and ORG_STATUS = @org_status";
                }

                if (name != null)
                {
                    bool_name = true;
                    sql_array.Add(" ORG_NAME LIKE @org_name ");
                }
                if (email != null)
                {
                    bool_email = true;
                    sql_array.Add(" EMAIL LIKE @org_email ");
                }
                if (type != null)
                {
                    bool_type = true;
                    sql_array.Add(" ORG_TYPE LIKE @org_type ");
                }

                for (int i = 0; i < sql_array.Count; i++)
                {
                    str_sql_command += " and " + sql_array[i];
                }

                str_sql_command += " order by p.TO_DATE ASC";

                //calling sql statements
                SqlCommand command = new SqlCommand(str_sql_command, dbConnection);


                if (bool_status)
                {
                    SqlParameter sql_name0 = new SqlParameter("org_status", SqlDbType.NVarChar, 50);
                    sql_name0.Value = org_status;
                    command.Parameters.Add(sql_name0);
                }

                if (bool_name)
                {
                    SqlParameter sql_name = new SqlParameter("org_name", SqlDbType.NVarChar, 50);
                    sql_name.Value = "%" + name + "%";
                    command.Parameters.Add(sql_name);
                }
                if (bool_email)
                {
                    SqlParameter sql_name = new SqlParameter("org_email", SqlDbType.NVarChar, 50);
                    sql_name.Value = "%" + email + "%";
                    command.Parameters.Add(sql_name);
                }
                if (bool_type)
                {
                    SqlParameter sql_name = new SqlParameter("org_type", SqlDbType.NVarChar, 50);
                    sql_name.Value = "%" + type + "%";
                    command.Parameters.Add(sql_name);
                }

                SqlDataReader dataReader = command.ExecuteReader();
                List<OrganisationDetails> org_list = new List<OrganisationDetails>();

                while (dataReader.Read())
                {
                    OrganisationDetails temp_org = new OrganisationDetails();

                    temp_org.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_org.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_org.EMAIL = dataReader["EMAIL"].ToString();
                    temp_org.LINK = dataReader["LINK"].ToString();
                    temp_org.ADDRESS = dataReader["ADDRESS"].ToString();
                    temp_org.DESCRIPTION = dataReader["DESCRIPTION"].ToString();
                    temp_org.ORG_TYPE = dataReader["ORG_TYPE"].ToString();
                    temp_org.ORG_STATUS = dataReader["ORG_STATUS"].ToString();

                    org_list.Add(temp_org);
                }
                dataReader.Close();
                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //update organisation status
        //update organisation status
        public static string updateOrgStatusDL(SqlConnection dbConnection, string orgID)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //sql command
                SqlCommand command = new SqlCommand(@"UPDATE TABLE_ORG_DETAILS
   SET ORG_STATUS = 'ACTIVE' ,RECURRING_PAYMENT = '1'
 WHERE ORG_ID = @orgID", dbConnection);

                //insert into sql
                SqlParameter sql_payID = new SqlParameter("orgID", SqlDbType.NVarChar, 50);
                sql_payID.Value = orgID;
                command.Parameters.Add(sql_payID);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }
        //update organisation status
        public static string updateCRMStatusDL(SqlConnection dbConnection, int state, string crmID)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //sql command
                string sqlStr = "";
                if (state == 0)
                {
                    sqlStr = @"UPDATE TABLE_ORG_CRM
   SET CRM_STATUS = 'USED'
 WHERE CRM_ID = @crmID";
                }

                SqlCommand command = new SqlCommand(sqlStr, dbConnection);

                //insert into sql
                SqlParameter sql_payID = new SqlParameter("crmID", SqlDbType.NVarChar, 50);
                sql_payID.Value = crmID;
                command.Parameters.Add(sql_payID);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SaaSMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertPayment---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }

            return "Success";

        }

        //TRANSACTION-RELATED METHODS
        //=============================

        //search type parameter
        // 0 - name, 1 - date, 2, name and date
        // <summary>DAL: get all transactions</summary>
        //TRANSACTION-RELATED METHODS
        //================================
        public static List<OrgPayment> getAllTransactionListDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select * from  TABLE_ORG_DETAILS 
left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null 
order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> payment_list = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_payment = new OrgPayment();
                    temp_payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_payment.AMT = dataReader["AMT"].ToString();
                    temp_payment.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    temp_payment.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();
                    temp_payment.CRM_ID = dataReader["CRM_ID"].ToString();
                    temp_payment.PAYPAL_ID = dataReader["PAYPAL_ID"].ToString();
                    temp_payment.PROFILE_ID = dataReader["PAYPAL_PROFILE"].ToString();
                    temp_payment.PAYPAL_FEE = dataReader["PAYPAL_FEE"].ToString();
                    temp_payment.PLAN_AMOUNT = dataReader["AMOUNT"].ToString();

                    payment_list.Add(temp_payment);
                }
                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<OrgPayment> getTransactionsSearchDL(SqlConnection dbConnection, int search_type, String name, String start, String end)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statementse
                string str_sql = "";
                if (search_type == 0)
                {
                    str_sql = @"select * from  TABLE_ORG_DETAILS 
left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ORG_NAME LIKE @name 
order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";

                }
                else if (search_type == 1)
                {
                    str_sql = @"select * from  TABLE_ORG_DETAILS 
left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and TABLE_ORG_PAYMENT.PAYMENT_TIME between @start and @end 
order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";
                }
                else
                {
                    str_sql = @"select * from  TABLE_ORG_DETAILS 
left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ORG_NAME LIKE @name and TABLE_ORG_PAYMENT.PAYMENT_TIME between @start and @end 
order by TABLE_ORG_PAYMENT.PAYMENT_TIME desc";
                }

                SqlCommand command = new SqlCommand(str_sql, dbConnection);


                if (search_type != 1)
                {
                    SqlParameter sql_name = new SqlParameter("name", SqlDbType.NVarChar, 50);
                    sql_name.Value = "%" + name + "%";
                    command.Parameters.Add(sql_name);
                }

                if (search_type != 0)
                {
                    SqlParameter sql_start = new SqlParameter("start", SqlDbType.NVarChar, 50);
                    sql_start.Value = start;
                    command.Parameters.Add(sql_start);

                    SqlParameter sql_end = new SqlParameter("end", SqlDbType.NVarChar, 50);
                    sql_end.Value = end;
                    command.Parameters.Add(sql_end);
                }

                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> payment_list = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_payment = new OrgPayment();
                    temp_payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_payment.AMT = dataReader["AMT"].ToString();
                    temp_payment.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    temp_payment.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();

                    temp_payment.CRM_ID = dataReader["CRM_ID"].ToString();
                    temp_payment.PAYPAL_ID = dataReader["PAYPAL_ID"].ToString();
                    temp_payment.PROFILE_ID = dataReader["PAYPAL_PROFILE"].ToString();
                    temp_payment.PAYPAL_FEE = dataReader["PAYPAL_FEE"].ToString();
                    temp_payment.PLAN_AMOUNT = dataReader["AMOUNT"].ToString();


                    payment_list.Add(temp_payment);
                }
                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get individual transaction information
        public static OrgPayment getIndividualTransactionDL(SqlConnection dbConnection, String org_pay_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"select * from TABLE_ORG_PAYMENT 
left join TABLE_ORG_DETAILS on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_DETAILS.ORG_ID 
left join TABLE_ORG_CRM on TABLE_ORG_PAYMENT.CRM_ID = TABLE_ORG_CRM.CRM_ID 
left join TABLE_ORG_PLAN_TYPE on TABLE_ORG_PAYMENT.PLAN_TYPE = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
where TABLE_ORG_PAYMENT.ORG_PAYMENT_ID = @org_pay_id ";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_pay_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_pay_id;
                command.Parameters.Add(sql_name);

                SqlDataReader dataReader = command.ExecuteReader();
                OrgPayment payment = null;

                while (dataReader.Read())
                {
                    payment = new OrgPayment();
                    payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    payment.AMT = dataReader["AMT"].ToString();
                    payment.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    payment.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();
                    payment.CRM_ID = dataReader["CRM_ID"].ToString();
                    payment.DISCOUNT = dataReader["DISCOUNT"].ToString();
                    payment.CRM_STATUS = dataReader["CRM_STATUS"].ToString();
                    payment.FROM_DATE = correctDateTimeFormat(0, dataReader["FROM_DATE"].ToString());
                    payment.TO_DATE = correctDateTimeFormat(0, dataReader["TO_DATE"].ToString());
                    payment.LINK = dataReader["LINK"].ToString();
                    payment.EMAIL = dataReader["EMAIL"].ToString();
                    payment.ADDRESS = dataReader["ADDRESS"].ToString();
                    payment.ORG_STATUS = dataReader["ORG_STATUS"].ToString();

                    payment.PAYPAL_ID = dataReader["PAYPAL_ID"].ToString();
                    payment.PROFILE_ID = dataReader["PAYPAL_PROFILE"].ToString();
                    payment.PAYPAL_FEE = dataReader["PAYPAL_FEE"].ToString();
                    payment.PLAN_AMOUNT = dataReader["AMOUNT"].ToString();
                }
                dataReader.Close();
                return payment;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }

        //REPORTING-RELATED METHODS
        //------------------------
        //---------------------------
        // <summary>DAL: get payment trend for one year</summary>
        public static int getPaymentTrendDL(SqlConnection dbConnection, int current_month, int current_year)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                SqlCommand command = new SqlCommand("select count(*) from TABLE_ORG_BIL where MONTH = @month and YEAR = @year", dbConnection);

                SqlParameter month = new SqlParameter("month", SqlDbType.NVarChar, 50);
                month.Value = current_month;
                command.Parameters.Add(month);

                SqlParameter year = new SqlParameter("year", SqlDbType.NVarChar, 50);
                year.Value = current_year;
                command.Parameters.Add(year);

                var firstColumn = command.ExecuteScalar();

                //storing data
                if (firstColumn != null)
                {
                    int result = (int)firstColumn;
                    return (int)firstColumn;
                }
                return 0;
            }
            catch (Exception e)
            {

                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPaymentTrendDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return -1;
            }
        }

        //CRM-RELATED METHODS
        //update crm within the table
        public static void insertCRMDL(SqlConnection dbConnection, string org_id, string amt, string date, string plan_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = "INSERT INTO TABLE_ORG_CRM (CRM_ID,  DISCOUNT, CRM_STATUS, ORG_ID, EXPIRED_DATE, PLAN_TYPE_ID) VALUES (@newID,  @amt, 'NOT USED', @org_id, @date, @plan_id) ";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("newID", SqlDbType.NVarChar, 50);
                sql_name.Value = newID;
                command.Parameters.Add(sql_name);

                SqlParameter sql_amt = new SqlParameter("amt", SqlDbType.NVarChar, 50);
                sql_amt.Value = amt;
                command.Parameters.Add(sql_amt);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlParameter sql_date = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_date.Value = date;
                command.Parameters.Add(sql_date);

                SqlParameter sql_plan_id = new SqlParameter("plan_id", SqlDbType.NVarChar, 50);
                sql_plan_id.Value = plan_id;
                command.Parameters.Add(sql_plan_id);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        public static List<CRM> showCRMDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = @"select * from TABLE_ORG_CRM  
                     left join TABLE_ORG_PLAN_TYPE on  TABLE_ORG_CRM.PLAN_TYPE_ID = TABLE_ORG_PLAN_TYPE.PLAN_TYPE_ID 
                     left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.CRM_ID = TABLE_ORG_CRM.CRM_ID 
                     where TABLE_ORG_CRM.ORG_ID = @org_id 
                    order by TABLE_ORG_CRM.EXPIRED_DATE desc, TABLE_ORG_PLAN_TYPE.PLAN_NAME asc";
                //order by TABLE_ORG_CRM.CRM_STATUS asc, TABLE_ORG_CRM.EXPIRED_DATE desc, TABLE_ORG_PLAN_TYPE.PLAN_NAME asc";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_name = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_name.Value = org_id;
                command.Parameters.Add(sql_name);

                SqlDataReader dataReader = command.ExecuteReader();
                List<CRM> crm_list = new List<CRM>();


                while (dataReader.Read())
                {
                    CRM temp_payment = new CRM();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.CRM_ID = dataReader["CRM_ID"].ToString();
                    temp_payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    temp_payment.DISCOUNT = dataReader["DISCOUNT"].ToString();
                    temp_payment.CRM_STATUS = dataReader["CRM_STATUS"].ToString();
                    temp_payment.EXPIRED_DATE = correctDateTimeFormat(0, dataReader["EXPIRED_DATE"].ToString().Split(' ')[0]);
                    temp_payment.PLAN_NAME = dataReader["PLAN_NAME"].ToString();
                    temp_payment.AMOUNT = dataReader["AMOUNT"].ToString();
                    temp_payment.PLAN_TYPE_ID = dataReader["PLAN_TYPE_ID"].ToString();
                    crm_list.Add(temp_payment);
                }
                dataReader.Close();
                return crm_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //delete crm within the table
        public static void deleteCRMDL(SqlConnection dbConnection, string crm_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = "delete from TABLE_ORG_CRM where CRM_ID = @crm_id and CRM_STATUS = 'NOT USED'";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_crmid = new SqlParameter("crm_id", SqlDbType.NVarChar, 50);
                sql_crmid.Value = crm_id;
                command.Parameters.Add(sql_crmid);


                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        //VERIFICATION RELATED METHODS
        //update verification status of organisation
        public static void updateVerificationStatusDL(SqlConnection dbConnection, string org_id, string verify)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = "update TABLE_ORG_DETAILS set VERIFICATION = @verify where ORG_ID = @org_id";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlParameter sql_verify = new SqlParameter("verify", SqlDbType.NVarChar, 50);
                sql_verify.Value = verify;
                command.Parameters.Add(sql_verify);


                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        //get verification status of organisation
        public static String getVerificationStatusDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = "select * from TABLE_ORG_DETAILS where ORG_ID = @org_id";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 100);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlDataReader dataReader = command.ExecuteReader();
                String ans = "";
                while (dataReader.Read())
                {
                    ans = dataReader["VERIFICATION"].ToString();
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return null;
            }
        }

        //get ORGANISATION PLAN RELATED INFO
        //get organisation plan of organisation
        public static string getOrgPlanInfoDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = @"select * from TABLE_ORG_DETAILS o
LEFT JOIN TABLE_ORG_CRM c ON o.ORG_ID = c.ORG_ID 
where o.ORG_ID = @org_id";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 100);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlDataReader dataReader = command.ExecuteReader();
                string ans = "";
                while (dataReader.Read())
                {
                    ans = dataReader["RECURRING_PAYMENT"].ToString();
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return null;
            }
        }
        //get cem related info of organisation
        public static string getOrgPlanInfoCRMDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = @"select * from TABLE_ORG_CRM
where ORG_ID = @org_id and CRM_STATUS = 'NOT USED' ";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 100);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlDataReader dataReader = command.ExecuteReader();
                string ans = "";
                while (dataReader.Read())
                {
                    ans = dataReader["CRM_ID"].ToString();
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return null;
            }
        }

        //ACTIONS
        //cancel organisation recurring plan
        public static string cancelPaymentDL(SqlConnection dbConnection, string org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                String str_sql = @"update TABLE_ORG_DETAILS
SET RECURRING_PAYMENT = '0'
where ORG_ID =  @org_id";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 100);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();
                return "success";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return "not success";
            }
        }

        //BILLING-RELATED METHODS
        //=============================

        //BILLING -RELATED METHODS
        //=============================
        // <summary>DAL: get non-payments</summary>
        // <summary>DAL: get all billings </summary>
        public static List<BillingStatus> getBillingStatusListDL(SqlConnection dbConnection, int current_month, int current_year, int search_type)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "";
                if (search_type == 2)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else if (search_type == 1)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ((YEAR = @year and MONTH > @month) or (YEAR > @year)) order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else if (search_type == 0)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and MONTH = @month and YEAR = @year order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ((YEAR = @year and MONTH < @month) or (YEAR < @year)) order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                if (search_type < 2)
                {
                    SqlParameter month = new SqlParameter("month", SqlDbType.NVarChar, 50);
                    month.Value = current_month;
                    command.Parameters.Add(month);

                    SqlParameter year = new SqlParameter("year", SqlDbType.NVarChar, 50);
                    year.Value = current_year;
                    command.Parameters.Add(year);
                }


                SqlDataReader dataReader = command.ExecuteReader();
                List<BillingStatus> payment_list = new List<BillingStatus>();

                while (dataReader.Read())
                {
                    BillingStatus temp_payment = new BillingStatus();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_payment.STATUS = dataReader["STATUS"].ToString();
                    temp_payment.MONTH = (int)dataReader["MONTH"];
                    temp_payment.YEAR = (int)dataReader["YEAR"];
                    temp_payment.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    DateTime plan_end = (DateTime)dataReader["PAYMENT_TIME"];
                    temp_payment.PLAN_END = plan_end.AddYears(1);
                    payment_list.Add(temp_payment);
                }

                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }

        public static List<BillingStatus> getUnpaidBillListDL(SqlConnection dbConnection, int current_month, int current_year, int search_type)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "";
                if (search_type == 2)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else if (search_type == 1)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ((YEAR = @year and MONTH > @month) or (YEAR > @year)) order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else if (search_type == 0)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and MONTH = @month and YEAR = @year order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                else if (search_type == -1)
                {
                    str_sql = "select * from  TABLE_ORG_BIL left join TABLE_ORG_PAYMENT on TABLE_ORG_PAYMENT.ORG_ID = TABLE_ORG_BIL.ORG_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_BIL.ORG_ID where TABLE_ORG_PAYMENT.PAYMENT_TIME is not null and ((YEAR = @year and MONTH < @month) or (YEAR < @year)) order by TABLE_ORG_BIL.YEAR desc, TABLE_ORG_BIL.MONTH desc";
                }
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                if (search_type < 2)
                {
                    SqlParameter month = new SqlParameter("month", SqlDbType.NVarChar, 50);
                    month.Value = current_month;
                    command.Parameters.Add(month);

                    SqlParameter year = new SqlParameter("year", SqlDbType.NVarChar, 50);
                    year.Value = current_year;
                    command.Parameters.Add(year);
                }


                SqlDataReader dataReader = command.ExecuteReader();
                List<BillingStatus> payment_list = new List<BillingStatus>();

                while (dataReader.Read())
                {
                    BillingStatus temp_payment = new BillingStatus();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_payment.STATUS = dataReader["STATUS"].ToString();
                    temp_payment.MONTH = (int)dataReader["MONTH"];
                    temp_payment.YEAR = (int)dataReader["YEAR"];
                    temp_payment.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    DateTime plan_end = (DateTime)dataReader["PAYMENT_TIME"];
                    temp_payment.PLAN_END = plan_end.AddYears(1);
                    payment_list.Add(temp_payment);
                }

                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<OrgPayment> getOrgBillDetailsDL(SqlConnection dbConnection, String org_id)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "select * from TABLE_ORG_PAYMENT m1 left join TABLE_ORG_PAYMENT m2 ON(m1.ORG_ID = m2.ORG_ID AND m1.PAYMENT_TIME < m2.PAYMENT_TIME) left join TABLE_ORG_DETAILS d ON m1.ORG_ID = d.ORG_ID left join TABLE_ORG_CRM c ON d.ORG_ID = c.ORG_ID  left join TABLE_ORG_PLAN_TYPE t ON t.PLAN_TYPE_ID = m1.PLAN_TYPE where m2.ORG_PAYMENT_ID is null and m1.ORG_ID = @org_id and ( c.EXPIRED_DATE  >= @date or c.CRM_ID IS NULL)";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlParameter sql_date = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_date.Value = DateTime.Now.ToString();
                command.Parameters.Add(sql_date);

                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> payment_list = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_payment = new OrgPayment();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    temp_payment.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    DateTime plan_end = (DateTime)dataReader["PAYMENT_TIME"];
                    temp_payment.TO_DATE = dataReader["TO_DATE"].ToString();
                    payment_list.Add(temp_payment);
                }

                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //get organisations that are due to pay in one month's time
        public static List<OrgPayment> getDueOrganisationDL(SqlConnection dbConnection, String date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                String str_sql = "select * from TABLE_ORG_PAYMENT left join TABLE_ORG_BIL on TABLE_ORG_BIL.ORG_PAYMENT_ID = TABLE_ORG_PAYMENT.ORG_PAYMENT_ID left join TABLE_ORG_DETAILS on TABLE_ORG_DETAILS.ORG_ID = TABLE_ORG_PAYMENT.ORG_ID where TABLE_ORG_BIL.STATUS is null and TO_DATE < @date";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_date = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_date.Value = date;
                command.Parameters.Add(sql_date);


                SqlDataReader dataReader = command.ExecuteReader();
                List<OrgPayment> payment_list = new List<OrgPayment>();

                while (dataReader.Read())
                {
                    OrgPayment temp_payment = new OrgPayment();
                    temp_payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    temp_payment.AMT = dataReader["AMT"].ToString();
                    temp_payment.PAYMENT_TIME = correctDateTimeFormat(1, dataReader["PAYMENT_TIME"].ToString());
                    temp_payment.PLAN_TYPE = dataReader["PLAN_TYPE"].ToString();
                    temp_payment.FROM_DATE = correctDateTimeFormat(0, dataReader["FROM_DATE"].ToString());
                    temp_payment.TO_DATE = correctDateTimeFormat(0, dataReader["TO_DATE"].ToString());
                    temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();


                    payment_list.Add(temp_payment);
                }

                dataReader.Close();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //generate billing for organisations
        public static String generateBillingDL(SqlConnection dbConnection, string org_id, string from_sub, string to_sub)
        {
            String newID = "-1";
            newID = Guid.NewGuid().ToString();

            try
            {
                string status = "NOT PAID";
                String str_sql = "INSERT INTO TABLE_ORG_BIL (BIL_ID,STATUS,ORG_ID,FROM_SUBCRIPTION, TO_SUBSCRIPTION) VALUES(@newID, @status, @org_id, @from_sub, @to_sub) ";
                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlParameter sql_new_id = new SqlParameter("newID", SqlDbType.NVarChar, 50);
                sql_new_id.Value = newID;
                command.Parameters.Add(sql_new_id);

                SqlParameter sql_status = new SqlParameter("status", SqlDbType.NVarChar, 50);
                sql_status.Value = status;
                command.Parameters.Add(sql_status);

                SqlParameter sql_org_id = new SqlParameter("org_id", SqlDbType.NVarChar, 50);
                sql_org_id.Value = org_id;
                command.Parameters.Add(sql_org_id);

                SqlParameter sql_from_sub = new SqlParameter("from_sub", SqlDbType.NVarChar, 50);
                sql_from_sub.Value = from_sub;
                command.Parameters.Add(sql_from_sub);

                SqlParameter sql_to_sub = new SqlParameter("to_sub", SqlDbType.NVarChar, 50);
                sql_to_sub.Value = to_sub;
                command.Parameters.Add(sql_to_sub);


                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Close();

                return newID;

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getPayments---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return null;
            }
        }


    }
}