﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using VMS.AccountMgmt;
using VMS.Database;
using VMS.Models.OrgMgmtModels;
using VMS.Models.TenderingModels;

namespace VMS.DataLayer.OrgMgmt
{
    public class OrgMgmtDL
    {
        /// <summary>DAL: Insert ew organisation account</summary>
        public static String insertOrganization(SqlConnection dbConnection, OrganizationDetails newOrg)
        {
            DateTime regDatetimeTemp = DateTime.Now;
            string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy");
            DateTime regDate = new DateTime();
            regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy", null);

            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_ORG_DETAILS VALUES (@ORG_ID, @ORG_NAME, @CONNECTION_STRING, @EMAIL, @LINK, @IMAGE_PATH, @ADDRESS, @DESCRIPTION, @ORG_TYPE,'ACTIVE','0', '0', @DATE_REG)", dbConnection);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 50);
                ORG_ID.Value = newID;
                command.Parameters.Add(ORG_ID);

                SqlParameter ORG_NAME = new SqlParameter("ORG_NAME", SqlDbType.NVarChar, 250);
                ORG_NAME.Value = newOrg.ORG_NAME;
                command.Parameters.Add(ORG_NAME);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 250);
                CONNECTION_STRING.Value = newOrg.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
                EMAIL.Value = newOrg.EMAIL;
                command.Parameters.Add(EMAIL);

                SqlParameter LINK = new SqlParameter("LINK", SqlDbType.NVarChar, 250);
                LINK.Value = newOrg.LINK;
                command.Parameters.Add(LINK);

                SqlParameter IMAGE_PATH = new SqlParameter("IMAGE_PATH", SqlDbType.NVarChar, 350);
                IMAGE_PATH.Value = "../SaaS/images/orgs/2268b167f96843bbbff8f5bfb83e4850.jpg";
                command.Parameters.Add(IMAGE_PATH);

                SqlParameter ADDRESS = new SqlParameter("ADDRESS", SqlDbType.NVarChar, 250);
                ADDRESS.Value = newOrg.ADDRESS;
                command.Parameters.Add(ADDRESS);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 3990);
                DESCRIPTION.Value = newOrg.DESCRIPTION;
                command.Parameters.Add(DESCRIPTION);

                SqlParameter ORG_TYPE = new SqlParameter("ORG_TYPE", SqlDbType.NVarChar, 50);
                ORG_TYPE.Value = newOrg.ORG_TYPE;
                command.Parameters.Add(ORG_TYPE);

                SqlParameter DATE_REG = new SqlParameter("DATE_REG", SqlDbType.DateTime);
                DATE_REG.Value = regDate;
                command.Parameters.Add(DATE_REG);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertOrganization---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return newID;
        }

        /// <summary>DAL: get organisation information</summary>
        public static OrganizationDetails getOrgInfo(SqlConnection dbConnection, OrganizationDetails org)
        {
            OrganizationDetails orgDetails = new OrganizationDetails();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_DETAILS WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = org.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orgDetails.ORG_ID = reader["ORG_ID"].ToString();
                    orgDetails.ORG_NAME = reader["ORG_NAME"].ToString();
                    orgDetails.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    orgDetails.EMAIL = reader["EMAIL"].ToString();
                    orgDetails.LINK = reader["LINK"].ToString();
                    //orgDetails.UEN = reader["UEN"].ToString();
                    orgDetails.ADDRESS = reader["ADDRESS"].ToString();
                    orgDetails.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    orgDetails.ORG_TYPE = reader["ORG_TYPE"].ToString();
                    orgDetails.ORG_STATUS = reader["ORG_STATUS"].ToString();
                    orgDetails.IMAGE = reader["IMAGE_PATH"].ToString().Replace('+', '-').Replace('/', '_');
                    orgDetails.RECURRING_PAYMENT = reader["RECURRING_PAYMENT"].ToString();
                    orgDetails.AUDIT_USER_ID = org.AUDIT_USER_ID;
                }
                reader.Close();
                dbConnection.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getOrgInfo---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return orgDetails;
        }

        public static OrganizationDetails getOrgInfo2(SqlConnection dbConnection, OrganizationDetails org)
        {
            OrganizationDetails orgDetails = new OrganizationDetails();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_DETAILS WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = org.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orgDetails.ORG_ID = reader["ORG_ID"].ToString();
                    orgDetails.ORG_NAME = reader["ORG_NAME"].ToString();
                    orgDetails.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    orgDetails.EMAIL = reader["EMAIL"].ToString();
                    orgDetails.LINK = reader["LINK"].ToString();
                    orgDetails.ADDRESS = reader["ADDRESS"].ToString();
                    orgDetails.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    orgDetails.ORG_TYPE = reader["ORG_TYPE"].ToString();
                    orgDetails.ORG_STATUS = reader["ORG_STATUS"].ToString();
                    orgDetails.IMAGE = reader["IMAGE_PATH"].ToString();
                    orgDetails.VERIFICATION = reader["VERIFICATION"].ToString();
                }
                reader.Close();
                dbConnection.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getOrgInfo---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return orgDetails;
        }


        /// <summary>DAL: Update organisation details
        /// </summary>
        public static String updateOrganization(SqlConnection dbConnection, OrganizationDetails newOrg)
        {
            try
            {
                SqlCommand command;
                if (newOrg.IMAGE_PATH==null)
                {
                    command = new SqlCommand("UPDATE TABLE_ORG_DETAILS SET ORG_NAME=@ORG_NAME, EMAIL=@EMAIL, LINK=@LINK, ADDRESS=@ADDRESS, DESCRIPTION=@DESCRIPTION, ORG_TYPE=@ORG_TYPE WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);
                }
                else {
                    command = new SqlCommand("UPDATE TABLE_ORG_DETAILS SET ORG_NAME=@ORG_NAME, EMAIL=@EMAIL, LINK=@LINK, IMAGE_PATH=@IMAGE_PATH, ADDRESS=@ADDRESS, DESCRIPTION=@DESCRIPTION, ORG_TYPE=@ORG_TYPE WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                    SqlParameter IMAGE_PATH = new SqlParameter("IMAGE_PATH", SqlDbType.NVarChar, 350);
                    IMAGE_PATH.Value = newOrg.IMAGE_PATH;
                    command.Parameters.Add(IMAGE_PATH);
                }
                 

                SqlParameter ORG_NAME = new SqlParameter("ORG_NAME", SqlDbType.NVarChar, 100);
                ORG_NAME.Value = newOrg.ORG_NAME;
                command.Parameters.Add(ORG_NAME);

                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
                EMAIL.Value = newOrg.EMAIL;
                command.Parameters.Add(EMAIL);

                SqlParameter LINK = new SqlParameter("LINK", SqlDbType.NVarChar, 100);
                LINK.Value = newOrg.LINK;
                command.Parameters.Add(LINK);

                SqlParameter ADDRESS = new SqlParameter("ADDRESS", SqlDbType.NVarChar, 250);
                ADDRESS.Value = newOrg.ADDRESS;
                command.Parameters.Add(ADDRESS);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 200);
                DESCRIPTION.Value = newOrg.DESCRIPTION;
                command.Parameters.Add(DESCRIPTION);

                SqlParameter ORG_TYPE = new SqlParameter("ORG_TYPE", SqlDbType.NVarChar, 100);
                ORG_TYPE.Value = newOrg.ORG_TYPE;
                command.Parameters.Add(ORG_TYPE);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = newOrg.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: updateOrganization---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return "Not Success";
            }
            return "Success";
        }

        /// <summary>DAL: Update organisation details
        /// </summary>
        public static String updateOrganizationStatus(SqlConnection dbConnection, String connection)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_ORG_DETAILS SET ORG_STATUS='ACTIVE' WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = connection;
                command.Parameters.Add(CONNECTION_STRING);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: updateOrganization---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return "Not Success";
            }
            return "Success";
        }


        /// <summary>DAL: Launch new organisation instances</summary>
        public static String insertOrgInstance(SqlConnection dbConnection, String newOrg)
        {
            try
            {
                String Scommand = "CREATE DATABASE " + newOrg;
                SqlCommand command = new SqlCommand(Scommand, dbConnection);
                command.CommandTimeout = 200;

                command.Prepare();
                command.ExecuteNonQuery();
                dbConnection.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: launchNewOrgInstance---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }
            return "Success";
        }

        /// <summary>DAL: Launch new organisation instances</summary>
        public static String feedNewTable(SqlConnection dbConnection)
        {
            try
            {
                String Scommand = @"


CREATE TABLE [dbo].[TABLE_AUDIT_TRAIL](
	[AUDIT_TRAIL_ID] [uniqueidentifier] NOT NULL,
	[MODULE_NAME] [nvarchar](50) NOT NULL,
	[MODULE_FEATURE] [nvarchar](250) NOT NULL,
	[ACTION] [nvarchar](50) NOT NULL,
	[DESCRIPTION] [nvarchar](250) NOT NULL,
	[TIMESTAMP] [datetime] NOT NULL,
	[SYSTEM_USR] [nvarchar](50) NOT NULL,
	[USR_ID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TABLE_AUDIT_TRAIL] PRIMARY KEY CLUSTERED 
(
	[AUDIT_TRAIL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



CREATE TABLE [dbo].[TABLE_EVT_CAUSE](
	[EVT_ID] [uniqueidentifier] NOT NULL,
	[CAUSE_ID] [int] NOT NULL,
 CONSTRAINT [PK_TABLE_EVT_CAUSE] PRIMARY KEY CLUSTERED 
(
	[EVT_ID] ASC,
	[CAUSE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_EVT_DETAILS](
	[EVT_ID] [uniqueidentifier] NOT NULL,
	[EVT_NAME] [nvarchar](100) NOT NULL,
	[STARTDATE] [date] NOT NULL,
	[ENDDATE] [date] NOT NULL,
	[EVT_DESC] [nvarchar](250) NOT NULL,
	[EVT_IMAGE] [nvarchar](max) NULL,
    [UNIQUE_LINK] [nvarchar](250) NULL, 
 CONSTRAINT [PK_TABLE_EVT_DETAILS] PRIMARY KEY CLUSTERED 
(
	[EVT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_EVT_SESSION](
	[EVT_SESSION_ID] [uniqueidentifier] NOT NULL,
	[EVT_ID] [uniqueidentifier] NOT NULL,
	[SESSION_NAME] [nvarchar](100) NOT NULL,
	[SESSION_DESC] [nvarchar](250) NOT NULL,
	[START_TIME] [datetime] NOT NULL,
	[END_TIME] [datetime] NOT NULL,
	[LOCATION] [nvarchar](100) NOT NULL,
    [POSTAL] [nchar](10) NULL,	
	[SESSION_SIZE] [int] NOT NULL,
	[COMPULSORY] [int] NULL,
 CONSTRAINT [PK_TABLE_EVT_SESSION] PRIMARY KEY CLUSTERED 
(
	[EVT_SESSION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_EVT_SESSION_DAY](
	[SESSIONDAY_ID] [uniqueidentifier] NOT NULL,
	[EVT_SESSION_ID] [uniqueidentifier] NOT NULL,
	[EVT_DATE] [date] NOT NULL,
	[QRCODE] [nvarchar](50) NULL,
 CONSTRAINT [PK_TABLE_EVT_SESSION_DAY] PRIMARY KEY CLUSTERED 
(
	[SESSIONDAY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_FDBACK_DETAILS](
	[FDBACK_DETAIL_ID] [uniqueidentifier] NOT NULL,
	[EVT_SESSION_ID] [uniqueidentifier] NOT NULL,
	[TYPE] [nvarchar](50) NOT NULL,
	[QUESTION] [nvarchar](50) NOT NULL,
	[NUMBER] [int] NOT NULL,
 CONSTRAINT [PK_TABLE_FDBACK_DETAILS] PRIMARY KEY CLUSTERED 
(
	[FDBACK_DETAIL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_EVT_SESSION_ROLE](
	[EVT_ROLE_ID] [uniqueidentifier] NOT NULL,
	[EVT_SESSION_ID] [uniqueidentifier] NOT NULL,
	[TERM_ROLE_ID] [int] NOT NULL,
	[QTY] [int] NOT NULL,
	[ROLE_DESC] [nvarchar](50) NOT NULL,
    [TRAITS] [nchar](10) NULL,
 CONSTRAINT [PK_TABLE_EVT_ROLE] PRIMARY KEY CLUSTERED 
(
	[EVT_ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_USR_FDBACK](
	[USR_FDBACK_ID] [uniqueidentifier] NOT NULL,
	[USRID] [uniqueidentifier] NOT NULL,
	[FDBACK_DETAIL_ID] [uniqueidentifier] NOT NULL,
	[ANSWER] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_TABLE_USR_FDBACK_1] PRIMARY KEY CLUSTERED 
(
	[USR_FDBACK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)




CREATE TABLE [dbo].[TABLE_ORG_EVT_USR](
	[USR_ID] [uniqueidentifier] NOT NULL,
	[EVT_ID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Table_ORG_EVT_USR] PRIMARY KEY CLUSTERED 
(
	[USR_ID] ASC,
	[EVT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_ORG_NAMELIST](
	[NAMELIST_ID] [uniqueidentifier] NOT NULL,
	[EMAIL] [nvarchar](100) NOT NULL,
	[NAME] [nvarchar](100) NOT NULL,
	[PHONENO] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_TABLE_ORG_NAMELIST] PRIMARY KEY CLUSTERED 
(
	[NAMELIST_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_ORG_ROLE](
	[ROLE_ID] [uniqueidentifier] NOT NULL,
	[ROLE_NAME] [nvarchar](100) NOT NULL,
	[IS_DELETED] [bit] NOT NULL,
 CONSTRAINT [PK_TABLE_ROLE] PRIMARY KEY CLUSTERED 
(
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



CREATE TABLE [dbo].[TABLE_ORG_USR](
	[USR_ID] [uniqueidentifier] NOT NULL,
	[NAME] [nvarchar](100) NOT NULL,
	[EMAIL] [nvarchar](100) NOT NULL,
	[PASSWORD] [nvarchar](max) NOT NULL,
	[REGISTRATION_DATE] [datetime] NOT NULL,
	[RESETPW_CODE] [uniqueidentifier] NULL,
	[IS_DELETED] [bit] NOT NULL,
 CONSTRAINT [PK_TABLE_ORG_USR] PRIMARY KEY CLUSTERED 
(
	[USR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

CREATE TABLE [dbo].[TABLE_ORG_USR_ROLE](
	[USR_ID] [uniqueidentifier] NOT NULL,
	[ROLE_ID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TABLE_ORG_USR_ROLE] PRIMARY KEY CLUSTERED 
(
	[USR_ID] ASC,
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



CREATE TABLE [dbo].[TABLE_VIA_TPLATE](
	[VIA_TPLATE_ID] [uniqueidentifier] NOT NULL,
	[ORG_ID] [uniqueidentifier] NOT NULL,
	[TEMPLATE] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TABLE_VIA_TPLATE] PRIMARY KEY CLUSTERED 
(
	[VIA_TPLATE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_VOL_EVT_ROLE](
	[VOL_EVT_ROLE_ID] [uniqueidentifier] NOT NULL,
	[USR_ID] [uniqueidentifier] NOT NULL,
	[SESSION_DAY_ID] [uniqueidentifier] NOT NULL,
	[EVT_ROLE_ID] [uniqueidentifier] NOT NULL,
	[TIME_IN] [datetime] NULL,
	[TIME_OUT] [datetime] NULL,
 CONSTRAINT [PK_TABLE_VOL_EVT_ROLE_1] PRIMARY KEY CLUSTERED 
(
	[VOL_EVT_ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_MODULE](
	[MODULE_ID] [uniqueidentifier] NOT NULL,
	[MODULE_NAME] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TABLE_MODULE] PRIMARY KEY CLUSTERED 
(
	[MODULE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

CREATE TABLE [dbo].[TABLE_EVT_ROLE_SKILL](
	[TERM_ROLE_ID] [int] NOT NULL,
	[TRAITS] [nvarchar](10) NOT NULL,
	[TERM_SKILL_IDS] [nvarchar](250) NULL,
 CONSTRAINT [PK_TABLE_EVT_ROLE_SKILL] PRIMARY KEY CLUSTERED 
(
	[TERM_ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



CREATE TABLE [dbo].[TABLE_PAGE](
	[PAGE_ID] [uniqueidentifier] NOT NULL,
	[MODULE_ID] [uniqueidentifier] NOT NULL,
	[PAGE_NAME] [nvarchar](100) NOT NULL,
	[URL] [nvarchar](50) NOT NULL,
	[SHOW_ON_MENU] [int] NOT NULL,
	[ICON] [nvarchar](50) NULL,
 CONSTRAINT [PK_TABLE_PAGE] PRIMARY KEY CLUSTERED 
(
	[PAGE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[TABLE_ROLE_MODULE](
	[ROLE_ID] [uniqueidentifier] NOT NULL,
	[MODULE_ID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TABLE_ROLE_PAGE] PRIMARY KEY CLUSTERED 
(
	[ROLE_ID] ASC,
	[MODULE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



CREATE TABLE [dbo].[TABLE_ORG_NOTIF](
	[NOTI_ID] [uniqueidentifier] NOT NULL,
	[NOTIFICATION] [nvarchar](250) NOT NULL,
	[RECEIVER_ID] [uniqueidentifier] NOT NULL,
	[HAS_READ] [nchar](10) NOT NULL,
	[TIMESTAMP] [datetime] NOT NULL,
 CONSTRAINT [PK_TABLE_ORG_NOTIF] PRIMARY KEY CLUSTERED 
(
	[NOTI_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)



ALTER TABLE [dbo].[TABLE_AUDIT_TRAIL]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_AUDIT_TRAIL_TABLE_ORG_USR] FOREIGN KEY([USR_ID])
REFERENCES [dbo].[TABLE_ORG_USR] ([USR_ID])

ALTER TABLE [dbo].[TABLE_AUDIT_TRAIL] CHECK CONSTRAINT [FK_TABLE_AUDIT_TRAIL_TABLE_ORG_USR]


ALTER TABLE [dbo].[TABLE_EVT_CAUSE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_EVT_CAUSE_TABLE_EVT_DETAILS] FOREIGN KEY([EVT_ID])
REFERENCES [dbo].[TABLE_EVT_DETAILS] ([EVT_ID])
ON DELETE CASCADE


ALTER TABLE [dbo].[TABLE_EVT_CAUSE] CHECK CONSTRAINT [FK_TABLE_EVT_CAUSE_TABLE_EVT_DETAILS]


ALTER TABLE [dbo].[TABLE_EVT_SESSION]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_EVT_SESSION_TABLE_EVT_DETAILS] FOREIGN KEY([EVT_ID])
REFERENCES [dbo].[TABLE_EVT_DETAILS] ([EVT_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_EVT_SESSION] CHECK CONSTRAINT [FK_TABLE_EVT_SESSION_TABLE_EVT_DETAILS]


ALTER TABLE [dbo].[TABLE_EVT_SESSION_ROLE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_EVT_SESSION_ROLE_TABLE_EVT_SESSION] FOREIGN KEY([EVT_SESSION_ID])
REFERENCES [dbo].[TABLE_EVT_SESSION] ([EVT_SESSION_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_EVT_SESSION_ROLE] CHECK CONSTRAINT [FK_TABLE_EVT_SESSION_ROLE_TABLE_EVT_SESSION]


ALTER TABLE [dbo].[TABLE_EVT_SESSION_DAY]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_EVT_SESSION_DAY_TABLE_EVT_SESSION] FOREIGN KEY([EVT_SESSION_ID])
REFERENCES [dbo].[TABLE_EVT_SESSION] ([EVT_SESSION_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_EVT_SESSION_DAY] CHECK CONSTRAINT [FK_TABLE_EVT_SESSION_DAY_TABLE_EVT_SESSION]

ALTER TABLE [dbo].[TABLE_FDBACK_DETAILS]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_FDBACK_DETAILS_TABLE_EVT_SESSION] FOREIGN KEY([EVT_SESSION_ID])
REFERENCES [dbo].[TABLE_EVT_SESSION] ([EVT_SESSION_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_FDBACK_DETAILS] CHECK CONSTRAINT [FK_TABLE_FDBACK_DETAILS_TABLE_EVT_SESSION]


ALTER TABLE [dbo].[TABLE_USR_FDBACK]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_USR_FDBACK_TABLE_FDBACK_DETAILS] FOREIGN KEY([FDBACK_DETAIL_ID])
REFERENCES [dbo].[TABLE_FDBACK_DETAILS] ([FDBACK_DETAIL_ID])
ON DELETE CASCADE


ALTER TABLE [dbo].[TABLE_USR_FDBACK] CHECK CONSTRAINT [FK_TABLE_USR_FDBACK_TABLE_FDBACK_DETAILS]


ALTER TABLE [dbo].[TABLE_ORG_EVT_USR]  WITH CHECK ADD  CONSTRAINT [FK_Table_ORG_EVT_USR_TABLE_EVT_DETAILS] FOREIGN KEY([EVT_ID])
REFERENCES [dbo].[TABLE_EVT_DETAILS] ([EVT_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_ORG_EVT_USR] CHECK CONSTRAINT [FK_Table_ORG_EVT_USR_TABLE_EVT_DETAILS]

ALTER TABLE [dbo].[TABLE_ORG_EVT_USR]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ORG_EVT_USR_TABLE_ORG_USR] FOREIGN KEY([USR_ID])
REFERENCES [dbo].[TABLE_ORG_USR] ([USR_ID])

ALTER TABLE [dbo].[TABLE_ORG_EVT_USR] CHECK CONSTRAINT [FK_TABLE_ORG_EVT_USR_TABLE_ORG_USR]

ALTER TABLE [dbo].[TABLE_ORG_USR] ADD  CONSTRAINT [DF_TABLE_ORG_USR_IS_DELETED]  DEFAULT ((0)) FOR [IS_DELETED]

ALTER TABLE [dbo].[TABLE_ORG_USR_ROLE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ORG_USR_ROLE_TABLE_ORG_ROLE] FOREIGN KEY([ROLE_ID])
REFERENCES [dbo].[TABLE_ORG_ROLE] ([ROLE_ID])

ALTER TABLE [dbo].[TABLE_ORG_USR_ROLE] CHECK CONSTRAINT [FK_TABLE_ORG_USR_ROLE_TABLE_ORG_ROLE]

ALTER TABLE [dbo].[TABLE_ORG_USR_ROLE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ORG_USR_ROLE_TABLE_ORG_USR_ROLE] FOREIGN KEY([USR_ID])
REFERENCES [dbo].[TABLE_ORG_USR] ([USR_ID])
ON DELETE CASCADE


ALTER TABLE [dbo].[TABLE_ORG_USR_ROLE] CHECK CONSTRAINT [FK_TABLE_ORG_USR_ROLE_TABLE_ORG_USR_ROLE]



ALTER TABLE [dbo].[TABLE_ROLE_MODULE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ROLE_PAGE_TABLE_ORG_ROLE] FOREIGN KEY([ROLE_ID])
REFERENCES [dbo].[TABLE_ORG_ROLE] ([ROLE_ID])

ALTER TABLE [dbo].[TABLE_ROLE_MODULE] CHECK CONSTRAINT [FK_TABLE_ROLE_PAGE_TABLE_ORG_ROLE]

ALTER TABLE [dbo].[TABLE_ROLE_MODULE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ROLE_PAGE_TABLE_PAGE] FOREIGN KEY([MODULE_ID])
REFERENCES [dbo].[TABLE_MODULE] ([MODULE_ID])

ALTER TABLE [dbo].[TABLE_ROLE_MODULE] CHECK CONSTRAINT [FK_TABLE_ROLE_PAGE_TABLE_PAGE]


ALTER TABLE [dbo].[TABLE_VOL_EVT_ROLE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_VOL_EVT_ROLE_TABLE_EVT_SESSION_DAY] FOREIGN KEY([SESSION_DAY_ID])
REFERENCES [dbo].[TABLE_EVT_SESSION_DAY] ([SESSIONDAY_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_VOL_EVT_ROLE] CHECK CONSTRAINT [FK_TABLE_VOL_EVT_ROLE_TABLE_EVT_SESSION_DAY]

ALTER TABLE [dbo].[TABLE_VOL_EVT_ROLE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_VOL_EVT_ROLE_TABLE_EVT_SESSION_ROLE] FOREIGN KEY([EVT_ROLE_ID])
REFERENCES [dbo].[TABLE_EVT_SESSION_ROLE] ([EVT_ROLE_ID])

ALTER TABLE [dbo].[TABLE_VOL_EVT_ROLE] CHECK CONSTRAINT [FK_TABLE_VOL_EVT_ROLE_TABLE_EVT_SESSION_ROLE]



ALTER TABLE [dbo].[TABLE_ORG_NOTIF] ADD  CONSTRAINT [DF_TABLE_ORG_NOTIF_HAS_READ]  DEFAULT ((0)) FOR [HAS_READ]

ALTER TABLE [dbo].[TABLE_ORG_NOTIF]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_ORG_NOTIF_TABLE_ORG_USR] FOREIGN KEY([NOTI_ID])
REFERENCES [dbo].[TABLE_ORG_USR] ([USR_ID])
ON DELETE CASCADE

ALTER TABLE [dbo].[TABLE_ORG_NOTIF] CHECK CONSTRAINT [FK_TABLE_ORG_NOTIF_TABLE_ORG_USR]

ALTER TABLE [dbo].[TABLE_PAGE] ADD  CONSTRAINT [DF_TABLE_PAGE_SHOW_ON_MENU]  DEFAULT ((1)) FOR [SHOW_ON_MENU]

ALTER TABLE [dbo].[TABLE_PAGE]  WITH CHECK ADD  CONSTRAINT [FK_TABLE_PAGE_TABLE_MODULE] FOREIGN KEY([MODULE_ID])
REFERENCES [dbo].[TABLE_MODULE] ([MODULE_ID])

ALTER TABLE [dbo].[TABLE_PAGE] CHECK CONSTRAINT [FK_TABLE_PAGE_TABLE_MODULE]


INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'731b21e0-59e0-4358-9ea7-01a41ee41ec6', N'Billing Management')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'1b71c457-50e9-434c-80d3-19c7017c6388', N'Event Management')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'91d45e9a-0f1b-4fd3-9e7a-42cc7d3ebdfb', N'Account Management')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'e49608d9-b2f1-4bb9-b288-4fce56bb718b', N'Organisation Profile')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'45906fb6-b629-4b19-b08c-5eaa149ed5f5', N'Audit Management')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'd23f9e75-686b-4b26-a005-c9b87821494e', N'HR Management')
INSERT [dbo].[TABLE_MODULE] ([MODULE_ID], [MODULE_NAME]) VALUES (N'e55931cf-e285-4ba4-b278-e62f00846e85', N'Admin Management')

INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'180834a6-85aa-43c1-9895-09505c6231f2', N'731b21e0-59e0-4358-9ea7-01a41ee41ec6', N'Bills', N'ManageBill.aspx', 1, N'money')
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'f1bdb23b-730d-4cc7-b1ec-098fcbbaa730', N'1b71c457-50e9-434c-80d3-19c7017c6388', N'Event Roles', N'EventRoles', 1, N'life-ring')
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'a1e890ff-265d-4edc-868b-09c442a7fb35', N'1b71c457-50e9-434c-80d3-19c7017c6388', N'Events', N'ViewEventList.aspx', 1, N'calendar')
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'93823f34-ebde-4fac-9636-1a0e2512351f', N'd23f9e75-686b-4b26-a005-c9b87821494e', N'Followers ', N'UsersFollowed.aspx', 1, N'users')
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'd47607b6-70e2-427a-ada3-40bbaef65d8f', N'1b71c457-50e9-434c-80d3-19c7017c6388', N'Create Event', N'CreateEvent.aspx', 1, N'tag')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'09dd0717-d68e-4457-a3c4-7525fbed601c', N'e55931cf-e285-4ba4-b278-e62f00846e85', N'Roles', N'rolelist.aspx', 1, N'user')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'21ea6b15-3ca8-4ae2-ab4b-8da853fbeca5', N'd23f9e75-686b-4b26-a005-c9b87821494e', N'Manpower', N'ManageExternalVolunteers.aspx', 1, N'users')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'5ab08847-65c9-430c-9e1b-a0c76dbf98e8', N'e49608d9-b2f1-4bb9-b288-4fce56bb718b', N'Organisation Profile', N'UpdateOrgInfo.aspx', 1, N'university')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'3c3c2551-29dd-49cd-92fe-b09a6a8cfa94', N'45906fb6-b629-4b19-b08c-5eaa149ed5f5', N'Audit Trail', N'AuditMgmtSaaS.aspx', 1, N'binoculars')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (N'e20ae678-5e0c-47e7-a348-bb8c0d95394d', N'91d45e9a-0f1b-4fd3-9e7a-42cc7d3ebdfb', N'Employees', N'ManageAccount.aspx', 1, N'user')
 
INSERT [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (NEWID(),'D23F9E75-686B-4B26-A005-C9B87821494E','VIA Letter','CreateVIATemplate.aspx','1','envelope-o')

INSERT INTO [dbo].[TABLE_PAGE] ([PAGE_ID], [MODULE_ID], [PAGE_NAME], [URL], [SHOW_ON_MENU], [ICON]) VALUES (NEWID(),'E55931CF-E285-4BA4-B278-E62F00846E85','Reporting','Reporting.aspx','1','line-chart') 

INSERT [dbo].[TABLE_ORG_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_DELETED]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'Admin', 0)
  
INSERT [dbo].[TABLE_ORG_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_DELETED]) VALUES (N'36b7139b-8318-47df-b663-cdd5b1848477', N'Finance Officer', 0)
 
INSERT [dbo].[TABLE_ORG_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_DELETED]) VALUES (N'850bb0eb-0cc2-41ff-9ca7-e5898c923bca', N'Event Organizer', 0)
 
INSERT [dbo].[TABLE_ORG_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_DELETED]) VALUES (N'88709ac2-2673-4ac3-a7c0-f98c8d368462', N'Human Resource', 0)
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'731b21e0-59e0-4358-9ea7-01a41ee41ec6')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'91d45e9a-0f1b-4fd3-9e7a-42cc7d3ebdfb')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'e49608d9-b2f1-4bb9-b288-4fce56bb718b')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'45906fb6-b629-4b19-b08c-5eaa149ed5f5')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'84b02464-33bc-43f0-a233-08944cbed370', N'e55931cf-e285-4ba4-b278-e62f00846e85')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'aba862de-ab67-429b-bbba-55b82def1344', N'7b1180b8-ba90-463f-898c-3bd29eb71bea')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'aba862de-ab67-429b-bbba-55b82def1344', N'83552b76-7f22-4b0a-9f2e-693ba20418d7')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'36b7139b-8318-47df-b663-cdd5b1848477', N'731b21e0-59e0-4358-9ea7-01a41ee41ec6')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'850bb0eb-0cc2-41ff-9ca7-e5898c923bca', N'1b71c457-50e9-434c-80d3-19c7017c6388')
 
INSERT [dbo].[TABLE_ROLE_MODULE] ([ROLE_ID], [MODULE_ID]) VALUES (N'88709ac2-2673-4ac3-a7c0-f98c8d368462', N'd23f9e75-686b-4b26-a005-c9b87821494e')

INSERT INTO TABLE_PAGE (PAGE_ID,MODULE_ID,PAGE_NAME,URL,SHOW_ON_MENU,ICON)
VALUES (NEWID(),'D23F9E75-686B-4B26-A005-C9B87821494E','External Volunteers List','ViewExternalVolunteer.aspx','1','list-alt');";

                
                

                if (dbConnection.State == System.Data.ConnectionState.Open)
                {
                    SqlCommand command = new SqlCommand(Scommand, dbConnection);
                    command.CommandTimeout = 300;

                    command.Prepare();
                    command.ExecuteNonQuery(); 
                }
                else
                {
                    dbConnection.Open();
                    SqlCommand command = new SqlCommand(Scommand, dbConnection);
                    command.CommandTimeout = 300;

                    command.Prepare();
                    command.ExecuteNonQuery();
                }          
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: launchNewOrgInstance---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return "Not Success";
            }
            return "Success";
        }

        // This method retrieves the master list of event roles that is currently governed by sgserve
        public static List<EventMasterRole> getEventRoleMasterList(SqlConnection dbConnection)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = @"SELECT t1.TERM_ID, t1.TERM_NAME, t2.TRAITS, t2.TERM_SKILL_IDS FROM  
                                    TABLE_TERM t1, TABLE_EVT_ROLE_SKILL t2
                                    WHERE t1.TERM_ID = t2.TERM_ROLE_ID";

            SqlDataReader dataReader = command.ExecuteReader();
            List<EventMasterRole> listOfEventMasterRole = new List<EventMasterRole>();
            while(dataReader.Read())
            {
                EventMasterRole masterRole = new EventMasterRole();
                masterRole.roleTermId = dataReader["TERM_ID"].ToString();
                masterRole.roleName = dataReader["TERM_NAME"].ToString();
                masterRole.traits = dataReader["TRAITS"].ToString();
                masterRole.skillSetList = dataReader["TERM_SKILL_IDS"].ToString();
                listOfEventMasterRole.Add(masterRole);
            }
            dataReader.Close();
            return listOfEventMasterRole;
        }

        // this method inserts a the roles table into the newly created Saas databases
        public static void importEventRoleTable(SqlConnection dbConnection, List<EventMasterRole> listOfEventMasterRoles)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dbConnection);
                command.CommandText = "INSERT INTO TABLE_EVT_ROLE_SKILL (TERM_ROLE_ID, TRAITS, TERM_SKILL_IDS) VALUES ";

                int count = 1;
                string termRole = "@termRole";
                String traits = "@traits";
                String skills = "@skills";
                foreach (EventMasterRole role in listOfEventMasterRoles)
                {
                    command.CommandText += "(" + termRole + count + ", " + traits + count + ", " + skills + count + " )";

                    SqlParameter termRoleParam = new SqlParameter(termRole + count, SqlDbType.Int);
                    termRoleParam.Value = role.roleTermId;
                    command.Parameters.Add(termRoleParam);


                    SqlParameter traitsParam = new SqlParameter(traits + count, SqlDbType.NVarChar, 10);
                    traitsParam.Value = role.traits;
                    command.Parameters.Add(traitsParam);

                    SqlParameter skillsParam = new SqlParameter(skills + count, SqlDbType.NVarChar, 50);
                    skillsParam.Value = role.skillSetList;
                    command.Parameters.Add(skillsParam);

                    if (count != listOfEventMasterRoles.Count)
                        command.CommandText += ", ";
                    count++;

                }

                command.ExecuteNonQuery();
            } catch (Exception ex)
            {

            }
        }

        /// <summary>DAL: Insert new organisation admin account</summary>
        public static String insertMasterAdmin(SqlConnection dbConnection, OrgUserDetails newUser)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_ORG_USR (USR_ID, NAME, EMAIL, PASSWORD, REGISTRATION_DATE, RESETPW_CODE) VALUES (@USR_ID, @NAME, @EMAIL, @PASSWORD, @REGISTRATION_DATE,@RESETPW_CODE)", dbConnection);

                SqlParameter ORG_USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 50);
                ORG_USR_ID.Value = newID;
                command.Parameters.Add(ORG_USR_ID);

                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 250);
                NAME.Value = newUser.NAME;
                command.Parameters.Add(NAME);

                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 250);
                EMAIL.Value = newUser.EMAIL;
                command.Parameters.Add(EMAIL);

                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
                PASSWORD.Value = newUser.PASSWORD;
                command.Parameters.Add(PASSWORD);                
                
                SqlParameter REGISTRATION_DATE = new SqlParameter("REGISTRATION_DATE", SqlDbType.DateTime);
                REGISTRATION_DATE.Value = newUser.REGISTRATION_DATE;
                command.Parameters.Add(REGISTRATION_DATE);

                SqlParameter RESETPW_CODE = new SqlParameter("RESETPW_CODE", SqlDbType.NVarChar, 250);
                RESETPW_CODE.Value = "88709AC2-2673-4AC3-A7C0-F98C8D368462";
                command.Parameters.Add(RESETPW_CODE);

                //command.Prepare();
                command.ExecuteNonQuery();
              
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertMasterAdmin---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            try
            {
                // insert into the reltionshp table
                SqlCommand command1 = new SqlCommand(null, dbConnection);
            command1.CommandText = "INSERT INTO [dbo].[TABLE_ORG_USR_ROLE] ([USR_ID],[ROLE_ID]) VALUES (@userID,'84b02464-33bc-43f0-a233-08944cbed370')";
            SqlParameter userIdParam = new SqlParameter("userID", SqlDbType.NVarChar, 50);
            userIdParam.Value = newID;
            command1.Parameters.Add(userIdParam);
            command1.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: insertMasterAdmin---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return newID;
        }

        /// <summary>DAL: get organisation information list</summary>
        public static List<OrganizationDetails> getOrgList(SqlConnection dbConnection)
        {
            List<OrganizationDetails> listofOrgDetails = new List<OrganizationDetails>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_DETAILS ORDER BY ORG_NAME ASC", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationDetails orgDetails = new OrganizationDetails();

                    orgDetails.ORG_ID = reader["ORG_ID"].ToString();
                    orgDetails.ORG_NAME = reader["ORG_NAME"].ToString();
                    orgDetails.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    orgDetails.EMAIL = reader["EMAIL"].ToString();
                    orgDetails.LINK = reader["LINK"].ToString();
                   // orgDetails.UEN = reader["UEN"].ToString();
                    orgDetails.ADDRESS = reader["ADDRESS"].ToString();
                    orgDetails.DESCRIPTION = reader["DESCRIPTION"].ToString().ToString();
                    orgDetails.ORG_TYPE = reader["ORG_TYPE"].ToString();

                    listofOrgDetails.Add(orgDetails);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---OrgMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getOrgList---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listofOrgDetails;
        }

        public static List<OrganizationDetails> searchOrg(SqlConnection dbConnection, string connection_string,string name)
        {
            List<OrganizationDetails> listofOrgDetails = new List<OrganizationDetails>();
            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_ORG_DETAILS WHERE CONNECTION_STRING LIKE @CONNECTION_STRING AND ORG_NAME LIKE @NAME ORDER BY ORG_NAME ASC", dbConnection);
                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@NAME", "%" + name + "%");
                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@CONNECTION_STRING", "%" + connection_string + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationDetails orgDetails = new OrganizationDetails();

                    orgDetails.ORG_ID = reader["ORG_ID"].ToString();
                    orgDetails.ORG_NAME = reader["ORG_NAME"].ToString();
                    orgDetails.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    orgDetails.EMAIL = reader["EMAIL"].ToString();
                    orgDetails.LINK = reader["LINK"].ToString();
                    orgDetails.UEN = reader["UEN"].ToString();
                    orgDetails.ADDRESS = reader["ADDRESS"].ToString();
                    orgDetails.DESCRIPTION = reader["DESCRIPTION"].ToString().ToString();
                    orgDetails.ORG_TYPE = reader["ORG_TYPE"].ToString();

                    listofOrgDetails.Add(orgDetails);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TenderingMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getOrgList---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listofOrgDetails;
        }
    }
}