﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.NotificationModels;

namespace VMS.DataLayer.NotificationMgmt
{
    public class NotificationDL
    {
        /*Create notification */
        public static string createNotif(SqlConnection dbConnection, Notification Notification, string tablename)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO "+tablename+ " (NOTI_ID, NOTIFICATION,RECEIVER_ID,TIMESTAMP) OUTPUT INSERTED.NOTI_ID VALUES (NEWID(), @NOTIFICATION,@RECEIVER_ID,@TIMESTAMP)";
            SqlParameter NOTIFICATION = new SqlParameter("NOTIFICATION", SqlDbType.NVarChar, 250);
            NOTIFICATION.Value = Notification.NOTIFICATION;
            command.Parameters.Add(NOTIFICATION);
            SqlParameter RECEIVER_ID = new SqlParameter("RECEIVER_ID", SqlDbType.NVarChar, 100);
            RECEIVER_ID.Value = Notification.RECEIVER_ID;
            command.Parameters.Add(RECEIVER_ID);
            SqlParameter TIMESTAMP = new SqlParameter("TIMESTAMP", SqlDbType.DateTime);
            TIMESTAMP.Value = Notification.TIMESTAMP;
            command.Parameters.Add(TIMESTAMP);

            string id = command.ExecuteScalar().ToString();
            return id;
        }

        /*Update notification to read*/
        public static int updateToRead(SqlConnection dbConnection, List<Notification> NotificationList, string tablename)
        {
            int count = 0;
             foreach (Notification notif in NotificationList)
            {
                try
                {
                    SqlCommand command = new SqlCommand("UPDATE " + tablename + " SET HAS_READ=1 Where NOTI_ID=@NOTI_ID", dbConnection);
                    SqlParameter NOTI_ID = new SqlParameter("NOTI_ID", SqlDbType.NVarChar, 100);
                    NOTI_ID.Value = notif.NOTI_ID;
                    command.Parameters.Add(NOTI_ID);
                    command.ExecuteNonQuery();
                    count = count + 1;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Error:"+e.Message);
                }
            }
            return count;
        }

        /*User get list of notification*/
        public static List<Notification> getNotifList(SqlConnection dbConnection, string tablename, string receiver_id)
        {
            List<Notification> notifiList = new List<Notification>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM " + tablename + " WHERE RECEIVER_ID=@RECEIVER_ID ORDER BY TIMESTAMP DESC", dbConnection);
                SqlParameter RECEIVER_ID = new SqlParameter("RECEIVER_ID", SqlDbType.NVarChar, 100);
                RECEIVER_ID.Value = receiver_id;
                command.Parameters.Add(RECEIVER_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Notification Notification = new Notification();
                    Notification.NOTI_ID = reader["NOTI_ID"].ToString();                    
                    Notification.RECEIVER_ID = reader["RECEIVER_ID"].ToString();
                    Notification.NOTIFICATION = reader["NOTIFICATION"].ToString();
                    Notification.HAS_READ = reader["HAS_READ"].ToString();
                    Notification.TIMESTAMP = Convert.ToDateTime(reader["TIMESTAMP"].ToString());
                    notifiList.Add(Notification);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return notifiList;
        }

        public static int getNumUnreadNotif(SqlConnection dbConnection, string tablename, string receiver_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("SELECT COUNT(*) AS numNotif FROM " + tablename + " WHERE RECEIVER_ID=@RECEIVER_ID AND HAS_READ=0", dbConnection);
                SqlParameter RECEIVER_ID = new SqlParameter("RECEIVER_ID", SqlDbType.NVarChar, 100);
                RECEIVER_ID.Value = receiver_id;
                command.Parameters.Add(RECEIVER_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    count = Convert.ToInt32(reader["numNotif"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return count;
        }


        /*Delete notification*/
        //public static int deleteNoti(SqlConnection dbConnection, string exam_period_id)
        //{
        //    int count = 0;
        //    try
        //    {
        //        SqlCommand command = new SqlCommand("DELETE FROM TABLE_EXAM_PERIOD WHERE EXAM_PERIOD_ID=@EXAM_PERIOD_ID;", dbConnection);
        //        SqlParameter EXAM_PERIOD_ID = new SqlParameter("EXAM_PERIOD_ID", SqlDbType.NVarChar, 100);
        //        EXAM_PERIOD_ID.Value = exam_period_id;
        //        command.Parameters.Add(EXAM_PERIOD_ID);
        //        count = command.ExecuteNonQuery();
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine(e.Message);
        //    }
        //    return count;
        //}
    }
}