﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class EventRoleDL
    {   /*Get all event role*/
        public static List<EventRole> getEvtRoleList(SqlConnection dbConnection, int vocab_id)
        {
            List<EventRole> evtRoleList = new List<EventRole>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM T LEFT JOIN TABLE_EVT_ROLE_SKILL R ON R.TERM_ROLE_ID=T.TERM_ID WHERE VOCAB_ID=@VOCAB_ID AND IS_DELETED=0 ORDER BY TERM_NAME ASC", dbConnection);
                SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
                VOCAB_ID.Value = vocab_id;
                command.Parameters.Add(VOCAB_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EventRole evtRole = new EventRole();
                    evtRole.TERM_SKILL_IDS = reader["TERM_SKILL_IDS"].ToString();
                    evtRole.ROLE_ID = Convert.ToInt32(reader["TERM_ID"]);
                    evtRole.ROLE_NAME = reader["TERM_NAME"].ToString();
                    evtRole.TRAITS = reader["TRAITS"].ToString();
                    evtRole.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);            
                    evtRoleList.Add(evtRole);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return evtRoleList;
        }

        /*Get event role traits*/
        public static EventRole getEvtRole(SqlConnection dataConnection, int term_id)
        {
            EventRole evtRole = new EventRole();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM T INNER JOIN TABLE_EVT_ROLE_SKILL R ON R.TERM_ROLE_ID=T.TERM_ID WHERE TERM_ID=@TERM_ID AND IS_DELETED=0", dataConnection);
                SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.NVarChar, 100);
                TERM_ID.Value = term_id;
                command.Parameters.Add(TERM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    evtRole.TERM_SKILL_IDS = reader["TERM_SKILL_IDS"].ToString();
                    evtRole.ROLE_ID = Convert.ToInt32(reader["TERM_ID"]);
                    evtRole.ROLE_NAME = reader["TERM_NAME"].ToString();
                    evtRole.TRAITS = reader["TRAITS"].ToString();
                    evtRole.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return evtRole;
        }

        /*Update event role traits*/
        public static int updateEvtRole(SqlConnection dbConnection, EventRole evtRole)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_EVT_ROLE_SKILL SET TRAITS=@TRAITS,TERM_SKILL_IDS=@TERM_SKILL_IDS Where TERM_ROLE_ID=@TERM_ROLE_ID", dbConnection);
                SqlParameter TERM_ROLE_ID = new SqlParameter("TERM_ROLE_ID", SqlDbType.Int);
                TERM_ROLE_ID.Value = evtRole.ROLE_ID;
                command.Parameters.Add(TERM_ROLE_ID);
                SqlParameter TRAITS = new SqlParameter("TRAITS", SqlDbType.NVarChar, 100);
                TRAITS.Value = evtRole.TRAITS;
                command.Parameters.Add(TRAITS);
                SqlParameter TERM_SKILL_IDS = new SqlParameter("TERM_SKILL_IDS", SqlDbType.NVarChar, 100);
                TERM_SKILL_IDS.Value = evtRole.TERM_SKILL_IDS;
                command.Parameters.Add(TERM_SKILL_IDS);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Create event role traits & skills */
        public static void createEvtRoleTraits(SqlConnection dbConnection, EventRole evtRole)
        {

            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_EVT_ROLE_SKILL (TERM_ROLE_ID,TRAITS,TERM_SKILL_IDS) VALUES ( @TERM_ROLE_ID,@TRAITS,@TERM_SKILL_IDS)";
            SqlParameter TERM_ROLE_ID = new SqlParameter("TERM_ROLE_ID", SqlDbType.Int);
            TERM_ROLE_ID.Value = evtRole.ROLE_ID;
            command.Parameters.Add(TERM_ROLE_ID);
            SqlParameter TRAITS = new SqlParameter("TRAITS", SqlDbType.NVarChar, 100);
            TRAITS.Value = evtRole.TRAITS;
            command.Parameters.Add(TRAITS);
            SqlParameter TERM_SKILL_IDS = new SqlParameter("TERM_SKILL_IDS", SqlDbType.NVarChar, 100);
            if (evtRole.TERM_SKILL_IDS == "" || evtRole.TERM_SKILL_IDS==null)
            {

                TERM_SKILL_IDS.Value = DBNull.Value;
            }
            else
            {
                TERM_SKILL_IDS.Value = evtRole.TERM_SKILL_IDS;
            }
            command.Parameters.Add(TERM_SKILL_IDS);
            command.ExecuteScalar();
        }

        //organization get event roles
        public static List<EventRole> getEvtRolesID(SqlConnection dbConnection)
        {
            List<EventRole> evtRoleList = new List<EventRole>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_EVT_ROLE_SKILL", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EventRole evtRole = new EventRole();
                    evtRole.ROLE_ID = Convert.ToInt32(reader["TERM_ROLE_ID"]);
                    evtRole.TRAITS = reader["TRAITS"].ToString();
                    evtRole.TERM_SKILL_IDS = reader["TERM_SKILL_IDS"].ToString();
                    evtRoleList.Add(evtRole);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return evtRoleList;
        }

        public static EventRole getEventRoleSkill(SqlConnection databaseConnection, int term_role_id)
        {
            EventRole evtRole = new EventRole();
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "Select * from TABLE_EVT_ROLE_SKILL Where TERM_ROLE_ID = @TERM_ROLE_ID";
            SqlParameter TERM_ROLE_ID = new SqlParameter("TERM_ROLE_ID", SqlDbType.Int);
            TERM_ROLE_ID.Value = term_role_id;
            command.Parameters.Add(TERM_ROLE_ID);

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                evtRole.TERM_SKILL_IDS = reader["TERM_SKILL_IDS"].ToString();
                evtRole.ROLE_ID = Convert.ToInt32(reader["TERM_ROLE_ID"]);
                evtRole.TRAITS = reader["TRAITS"].ToString();
            }
            reader.Close();
            return evtRole;
        }

     
    }
}