﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class TermDL
    {
        /*Get list of term based on the vocab*/
        public static List<Term> getSpecificTermList(SqlConnection dbConnection, int vocab_id)
        {
            List<Term> termList = new List<Term>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM WHERE VOCAB_ID=@VOCAB_ID AND IS_DELETED=0 ORDER BY TERM_NAME ASC;", dbConnection);
                SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
                VOCAB_ID.Value = vocab_id;
                command.Parameters.Add(VOCAB_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Term term = new Term();
                    term.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    term.TERM_NAME = reader["TERM_NAME"].ToString();
                    term.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    term.CREATED_AT= Convert.ToDateTime(reader["CREATED_AT"].ToString());
                    termList.Add(term);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:"+e.Message);
            }
            return termList;
        }

        /*Get term*/
        public static Term getTerm(SqlConnection dataConnection, string term_id)
        {
            Term term = new Term();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM WHERE TERM_ID=@TERM_ID AND IS_DELETED=0", dataConnection);
                SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.NVarChar, 100);
                TERM_ID.Value = term_id;
                command.Parameters.Add(TERM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    term.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    term.TERM_NAME = reader["TERM_NAME"].ToString();
                    term.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    term.CREATED_AT = Convert.ToDateTime(reader["CREATED_AT"].ToString());
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return term;
        }

        public static Boolean getTermByName(SqlConnection dataConnection, string term_name)
        {   
            Boolean existName = false;
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM WHERE UPPER(TERM_NAME)=@TERM_NAME AND IS_DELETED=0", dataConnection);
                SqlParameter TERM_NAME = new SqlParameter("TERM_NAME", SqlDbType.NVarChar, 100);
                TERM_NAME.Value = term_name.ToUpper();
                command.Parameters.Add(TERM_NAME);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    existName = true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return existName;
        }

        /*Create term */
        public static string createTerm(SqlConnection dbConnection, Term term)
        {
            DateTime timeTemp = DateTime.Now;
            string stringDate = timeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
            DateTime created_at = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);

            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_TERM (TERM_NAME,VOCAB_ID,CREATED_AT) OUTPUT INSERTED.TERM_ID VALUES ( @TERM_NAME,@VOCAB_ID,@CREATED_AT)";     
            SqlParameter CREATED_AT = new SqlParameter("CREATED_AT", SqlDbType.DateTime);
            CREATED_AT.Value = created_at;
            command.Parameters.Add(CREATED_AT);
            SqlParameter TERM_NAME = new SqlParameter("TERM_NAME", SqlDbType.NVarChar, 100);
            TERM_NAME.Value = term.TERM_NAME;
            command.Parameters.Add(TERM_NAME);
            SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
            VOCAB_ID.Value = term.VOCAB_ID;
            command.Parameters.Add(VOCAB_ID);

            string id = command.ExecuteScalar().ToString();
            return id;
        }

        /*Update term*/
        public static int updateTerm(SqlConnection dbConnection, Term term)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_TERM SET TERM_NAME=@TERM_NAME Where TERM_ID=@TERM_ID", dbConnection);
                SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.Int);
                TERM_ID.Value = term.TERM_ID;
                command.Parameters.Add(TERM_ID);
                SqlParameter TERM_NAME = new SqlParameter("TERM_NAME", SqlDbType.NVarChar, 100);
                TERM_NAME.Value = term.TERM_NAME;
                command.Parameters.Add(TERM_NAME);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete term*/
        public static int deleteTerm(SqlConnection dbConnection, int term_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_TERM SET IS_DELETED=1 Where TERM_ID=@TERM_ID", dbConnection);
                SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.Int);
                TERM_ID.Value = term_id;
                command.Parameters.Add(TERM_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

    }
}