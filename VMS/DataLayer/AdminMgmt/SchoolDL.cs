﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class SchoolDL
    {
        /*Get list of schools*/
        public static List<School> getSchools(SqlConnection dbConnection, string SCH_LVL)
        {
            List<School> listOfSchools = new List<School>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_SCHL_MASTER S INNER JOIN TABLE_LVL_OF_EDU L ON L.LVL_OF_EDU_ID=S.LVL_OF_EDU_ID WHERE LVL=@SCH_LVL ORDER BY SCHL_NAME ASC;", dbConnection);
                SqlParameter sch_lvl = new SqlParameter("SCH_LVL", SqlDbType.NVarChar, 100);
                sch_lvl.Value = SCH_LVL;
                command.Parameters.Add(sch_lvl);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    School school = new School();
                    school.SCHL_ID = reader["SCHL_ID"].ToString();
                    school.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    school.SCH_LVL = reader["LVL"].ToString();
                    listOfSchools.Add(school);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---SchoolDAL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getSschools---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfSchools;
        }

        public static string getLvlIDByName(SqlConnection dataConnection, string lvl)
        {
            string id = "";
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_LVL_OF_EDU WHERE LVL=@LVL", dataConnection);
                SqlParameter LVL = new SqlParameter("LVL", SqlDbType.NVarChar, 100);
                LVL.Value = lvl;
                command.Parameters.Add(LVL);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    id = reader["LVL_OF_EDU_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return id;
        }

        /*Get school name*/
        public static School getSchByID(SqlConnection dataConnection, string schl_id)
        {
            School school = new School();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_SCHL_MASTER S INNER JOIN TABLE_LVL_OF_EDU L ON L.LVL_OF_EDU_ID=S.LVL_OF_EDU_ID WHERE SCHL_ID = @SCHL_ID;", dataConnection);
                SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);
                SCHL_ID.Value = schl_id;
                command.Parameters.Add(SCHL_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    school.SCHL_ID = reader["SCHL_ID"].ToString();
                    school.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    school.SCH_LVL = reader["LVL"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
            }
            return school;
        }

        public static List<School> searchSchools(SqlConnection dbConnection, string SCH_LVL, string schl_name)
        {
            List<School> listOfSchools = new List<School>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_SCHL_MASTER S INNER JOIN TABLE_LVL_OF_EDU L ON L.LVL_OF_EDU_ID=S.LVL_OF_EDU_ID WHERE LVL=@SCH_LVL AND SCHL_NAME LIKE @SCHL_NAME;", dbConnection);
                SqlParameter sch_lvl = new SqlParameter("SCH_LVL", SqlDbType.NVarChar, 100);
                sch_lvl.Value = SCH_LVL;
                command.Parameters.Add(sch_lvl);
                SqlParameter SCHL_NAME = new SqlParameter("SCHL_NAME", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@SCHL_NAME", "%" + schl_name + "%");


                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    School school = new School();
                    school.SCHL_ID = reader["SCHL_ID"].ToString();
                    school.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    school.SCH_LVL = reader["LVL"].ToString();
                    listOfSchools.Add(school);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:"+e.Message);
            }
            return listOfSchools;
        }

        /*Create School */
        public static string createSchool(SqlConnection dbConnection, School school)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_SCHL_MASTER (SCHL_ID, SCHL_NAME,LVL_OF_EDU_ID) OUTPUT INSERTED.SCHL_ID VALUES (NEWID(), @SCHL_NAME,@LVL_OF_EDU_ID)";
            SqlParameter SCHL_NAME = new SqlParameter("SCHL_NAME", SqlDbType.NVarChar, 100);
            SCHL_NAME.Value = school.SCHL_NAME;
            command.Parameters.Add(SCHL_NAME);
            SqlParameter LVL_OF_EDU_ID = new SqlParameter("LVL_OF_EDU_ID", SqlDbType.NVarChar, 100);
            LVL_OF_EDU_ID.Value = school.LVL_OF_EDU_ID;
            command.Parameters.Add(LVL_OF_EDU_ID);
            
            string schid = command.ExecuteScalar().ToString();
            return schid;
        }

        /*Update School detail*/
        public static int editSchool(SqlConnection dbConnection, School school)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SCHL_MASTER SET SCHL_NAME=@SCHL_NAME, LVL_OF_EDU_ID=@LVL_OF_EDU_ID Where SCHL_ID=@SCHL_ID", dbConnection);
                SqlParameter SCHL_NAME = new SqlParameter("SCHL_NAME", SqlDbType.NVarChar, 100);
                SCHL_NAME.Value = school.SCHL_NAME;
                command.Parameters.Add(SCHL_NAME);
                SqlParameter LVL_OF_EDU_ID = new SqlParameter("LVL_OF_EDU_ID", SqlDbType.NVarChar, 100);
                LVL_OF_EDU_ID.Value = school.LVL_OF_EDU_ID;
                command.Parameters.Add(LVL_OF_EDU_ID);
                SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);
                SCHL_ID.Value = school.SCHL_ID;
                command.Parameters.Add(SCHL_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete School*/
        public static int deleteSchool(SqlConnection dbConnection, string schl_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_SCHL_MASTER WHERE SCHL_ID=@SCHL_ID;", dbConnection);
                SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);
                SCHL_ID.Value = schl_id;
                command.Parameters.Add(SCHL_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

 
        

    }
}