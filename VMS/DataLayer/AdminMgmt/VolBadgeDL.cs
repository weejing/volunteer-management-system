﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class VolBadgeDL
    {

        public static List<Badge> getVolBadges(SqlConnection dbConnection,string usr_id)
        {
            List<Badge> badgeList = new List<Badge>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_USR_BADGE UB INNER JOIN TABLE_BADGE B ON UB.BADGE_ID=B.BADGE_ID LEFT JOIN TABLE_TERM S ON CAUSE_ID=TERM_ID WHERE USR_ID=@USR_ID", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Badge badge = new Badge();
                    if (!reader["MINIMUM_HOURS"].Equals(DBNull.Value))
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.MINIMUM_HOURS = Convert.ToInt32(reader["MINIMUM_HOURS"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    else
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                        badge.CAUSE_NAME = reader["TERM_NAME"].ToString();
                        badge.QTY = Convert.ToInt32(reader["QTY"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    badgeList.Add(badge);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return badgeList;
        }

        public static List<Badge> getVolNewMinHBadges(SqlConnection dbConnection,string usr_id,int volHours)
        {
            List<Badge> badgeList = new List<Badge>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT  * FROM TABLE_BADGE WHERE BADGE_ID NOT IN (SELECT BADGE_ID FROM TABLE_USR_BADGE WHERE USR_ID=@USR_ID) AND STATUS='Published' AND CAUSE_ID IS NULL AND MINIMUM_HOURS <= @VOLHOURS", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                SqlParameter VOLHOURS = new SqlParameter("VOLHOURS", SqlDbType.Int);
                VOLHOURS.Value = volHours;
                command.Parameters.Add(VOLHOURS);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                        Badge badge = new Badge();
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.MINIMUM_HOURS = Convert.ToInt32(reader["MINIMUM_HOURS"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();                   
                        badgeList.Add(badge);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return badgeList;
        }

        public static List<Badge> getVolNewCauseHBadges(SqlConnection dbConnection, string usr_id, List<VolEvtCause> VolcauseCountList)
        {
            List<Badge> badgeList = new List<Badge>();

            try
            {
                SqlCommand command = new SqlCommand(null, dbConnection);
                command.CommandText = "SELECT  * FROM TABLE_BADGE WHERE BADGE_ID NOT IN (SELECT BADGE_ID FROM TABLE_USR_BADGE WHERE USR_ID=@USR_ID) AND STATUS='Published' ";
                String placeHolder = "@CAUSE_ID";
                String QtyPlaceHolder = "@QTY";
                int count = 0;
                foreach (VolEvtCause volCauseCount in VolcauseCountList)
                {
                    if (count == 0)
                        command.CommandText += "AND ((CAUSE_ID=" + placeHolder + count + " AND QTY <="+QtyPlaceHolder+count+")";
                    else
                        command.CommandText += " OR (CAUSE_ID=" + placeHolder + count + " AND QTY <=" + QtyPlaceHolder + count + ")";

                    SqlParameter CAUSE_ID = new SqlParameter(placeHolder + count, SqlDbType.Int);
                    CAUSE_ID.Value = volCauseCount.CAUSE_ID;
                    command.Parameters.Add(CAUSE_ID);
                    SqlParameter QTY = new SqlParameter(QtyPlaceHolder + count, SqlDbType.Int);
                    QTY.Value = volCauseCount.QTY;
                    command.Parameters.Add(QTY);
                    count++;
                }
                command.CommandText += ")";
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                System.Diagnostics.Debug.WriteLine("SQL QUERY:" + command.CommandText);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Badge badge = new Badge();
                    badge.BADGE_ID = reader["BADGE_ID"].ToString();
                    badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    badge.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                    badge.QTY = Convert.ToInt32(reader["QTY"]);
                    badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                    badge.STATUS = reader["STATUS"].ToString();
                    badgeList.Add(badge);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return badgeList;
        }

        public static void createVolBadge(SqlConnection dbConnection, List<Badge> volBadges, string user_id)
        {
            foreach (Badge badge in volBadges)
            {
                try
                {
                    SqlCommand command = new SqlCommand(null, dbConnection);
                    command.CommandText = "INSERT INTO TABLE_USR_BADGE (BADGE_ID,USR_ID) VALUES (@BADGE_ID,@USR_ID)";
                    SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                    BADGE_ID.Value = badge.BADGE_ID;
                    command.Parameters.Add(BADGE_ID);
                    SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                    USR_ID.Value = user_id;
                    command.Parameters.Add(USR_ID);
                    command.ExecuteScalar().ToString();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
                }
            }
        }

        public static int getVolRegEvtCount(SqlConnection dataConnection, String userId)
        {
            int numrows=0;
            try
            {
                SqlCommand command = new SqlCommand("SELECT count(*) AS NUMUNIQUEROWS, dt.USR_ID FROM( select distinct EVT_ID,SESSION_ID,CONNECTION_STRING,USR_ID from TABLE_USR_EVENT UE INNER JOIN TABLE_USR_EVENT_SESSION ES ON UE.USR_EVT_ID=ES.USR_EVT_ID WHERE USR_ID=@USR_ID) dt GROUP BY dt.USR_ID;", dataConnection);
                SqlParameter usr_Id = new SqlParameter("USR_ID", SqlDbType.UniqueIdentifier);
                usr_Id.Value = new Guid(userId);
                command.Parameters.Add(usr_Id);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    numrows = Convert.ToInt32(reader["NUMUNIQUEROWS"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return numrows;
        }

        /*
      *  This method retrieves a list of events ids that correspond to the events joined by a volunteer
      */
        public static List<KeyValuePair<String, List<KeyValuePair<String,String>>>> getVolEvents(SqlConnection dataConnection, String userId, int volRegUniqueEvtCount)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT distinct EVT_ID,SESSION_ID,CONNECTION_STRING from TABLE_USR_EVENT UE INNER JOIN TABLE_USR_EVENT_SESSION ES ON UE.USR_EVT_ID=ES.USR_EVT_ID WHERE USR_ID=@USR_ID ORDER BY CONNECTION_STRING ASC";
            SqlParameter usr_Id = new SqlParameter("usr_id", SqlDbType.UniqueIdentifier);
            usr_Id.Value = new Guid(userId);
            command.Parameters.Add(usr_Id);

            List<KeyValuePair<String, List<KeyValuePair<String, String>>>> categoriseListOfVolEvents = new List<KeyValuePair<string, List<KeyValuePair<string,string>>>>();
            List<KeyValuePair<String,String>> listOfVolEvents = new List<KeyValuePair<String,String>>();

            SqlDataReader dataReader = command.ExecuteReader();
            String connectionString = "";
            int count = 1;
            while (dataReader.Read())
            {
                if (count == 1)
                {
                    connectionString = dataReader["CONNECTION_STRING"] as string;
                }
                if ((count != 1) && (connectionString.Equals(dataReader["CONNECTION_STRING"]) == false))
                {
                    categoriseListOfVolEvents.Add(new KeyValuePair<string, List<KeyValuePair<string, string>>>(connectionString, listOfVolEvents));
                    listOfVolEvents = new List<KeyValuePair<string, string>>();
                    connectionString = dataReader["CONNECTION_STRING"] as string;
                }
                listOfVolEvents.Add(new KeyValuePair<string, string>(dataReader["EVT_ID"].ToString(), dataReader["SESSION_ID"].ToString()));

                if (volRegUniqueEvtCount==count)
                {
                    //last row
                    categoriseListOfVolEvents.Add(new KeyValuePair<string, List<KeyValuePair<string, string>>>(connectionString, listOfVolEvents));
                }
               
                count++;
            }
            dataReader.Close();
            return categoriseListOfVolEvents;
        }

        /*
        * This method gets a list of events joined by a volunteer from a aprticular NPO 
        */
        public static int getCompletedVIA(SqlConnection dataConnection,string usr_id)
        {
            int hours = 0;
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT SUM((DATEDIFF(minute,TIME_IN,TIME_OUT))/60) AS TOTALVIA FROM TABLE_VOL_EVT_ROLE WHERE USR_ID=@USR_ID AND TIME_IN IS NOT NULL AND TIME_OUT IS NOT NULL ";
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["TOTALVIA"].Equals(DBNull.Value))
                    {
                        hours = Convert.ToInt32(reader["TOTALVIA"]);
                    }
                }
                reader.Close();
             }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
            }
            return hours;
        }

        public static List<VolEvtCause> getVolCauseCount(SqlConnection dbConnection, string usr_id)
        {
            List<VolEvtCause> caueCountList = new List<VolEvtCause>();

            try
            {
                SqlCommand command = new SqlCommand("select count(*) AS QTY, dt.CAUSE_ID from(SELECT distinct CAUSE_ID,E.EVT_NAME FROM TABLE_VOL_EVT_ROLE V INNER JOIN TABLE_EVT_SESSION_DAY SD ON V.SESSION_DAY_ID=SD.SESSIONDAY_ID INNER JOIN TABLE_EVT_SESSION S ON S.EVT_SESSION_ID=SD.EVT_SESSION_ID INNER JOIN TABLE_EVT_DETAILS E ON E.EVT_ID=S.EVT_ID INNER JOIN TABLE_EVT_CAUSE EC ON E.EVT_ID=EC.EVT_ID WHERE USR_ID=@USR_ID AND TIME_IN IS NOT NULL AND TIME_OUT IS NOT NULL) dt GROUP BY dt.CAUSE_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VolEvtCause causeCount = new VolEvtCause();
                    causeCount.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                    causeCount.QTY = Convert.ToInt32(reader["QTY"]);
                    caueCountList.Add(causeCount);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
            }
            return caueCountList;
        }

    }
}