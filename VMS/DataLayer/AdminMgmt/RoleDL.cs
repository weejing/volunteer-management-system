﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class RoleDL
    {
        /*Get list of roles within the organization*/
        public static List<Role> getRoleList(SqlConnection dbConnection)
        {
            List<Role> roleList = new List<Role>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_ORG_ROLE WHERE IS_DELETED=0 ORDER BY ROLE_NAME ASC;", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Role role = new Role();
                    role.ROLE_ID = reader["ROLE_ID"].ToString();
                    role.ROLE_NAME = reader["ROLE_NAME"].ToString();
                    role.IS_DELETED = Convert.ToInt32(reader["IS_DELETED"]);
                    roleList.Add(role);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return roleList;
        }

        /*Get role*/
        public static Role getRole(SqlConnection dataConnection, string role_id)
        {
            Role role = new Role();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_ORG_ROLE WHERE ROLE_ID=@ROLE_ID AND IS_DELETED=0", dataConnection);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role_id;
                command.Parameters.Add(ROLE_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    role.ROLE_ID = reader["ROLE_ID"].ToString();
                    role.ROLE_NAME = reader["ROLE_NAME"].ToString();
                    role.IS_DELETED = Convert.ToInt32(reader["IS_DELETED"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return role;
        }

        /*Create term */
        public static string createRole(SqlConnection dbConnection, Role role)
        {

            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_ORG_ROLE (ROLE_ID,ROLE_NAME) OUTPUT INSERTED.ROLE_ID VALUES ( NEWID(),@ROLE_NAME)";
            SqlParameter ROLE_NAME = new SqlParameter("ROLE_NAME", SqlDbType.NVarChar, 100);
            ROLE_NAME.Value = role.ROLE_NAME;
            command.Parameters.Add(ROLE_NAME);

            string id = command.ExecuteScalar().ToString();
            return id;
        }

        /*Update role*/
        public static int updateRole(SqlConnection dbConnection, Role role)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_ORG_ROLE SET ROLE_NAME=@ROLE_NAME Where ROLE_ID=@ROLE_ID", dbConnection);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role.ROLE_ID;
                command.Parameters.Add(ROLE_ID);
                SqlParameter ROLE_NAME = new SqlParameter("ROLE_NAME", SqlDbType.NVarChar, 100);
                ROLE_NAME.Value = role.ROLE_NAME;
                command.Parameters.Add(ROLE_NAME);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete role*/
        public static int deleteRole(SqlConnection dbConnection, string role_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_ORG_ROLE SET IS_DELETED=1 Where ROLE_ID=@ROLE_ID", dbConnection);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role_id;
                command.Parameters.Add(ROLE_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        #region Role <-> Modules

        public static List<Page> getPageAccessRights(SqlConnection dbConnection, string usr_id)
        {
            List<Page> pageList = new List<Page>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT distinct page_name,page_id,icon,url,show_on_menu FROM TABLE_ORG_USR_ROLE UR INNER JOIN TABLE_ROLE_MODULE RM ON RM.ROLE_ID=UR.ROLE_ID INNER JOIN TABLE_PAGE P ON P.MODULE_ID=RM.MODULE_ID WHERE USR_ID=@USR_ID ORDER BY PAGE_NAME ASC", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Page page = new Page();
                    page.NAME = reader["page_name"].ToString();
                    page.ID = reader["page_id"].ToString();
                    page.ICON = reader["icon"].ToString();
                    page.URL = reader["url"].ToString();
                    page.SHOW_ON_MENU = Convert.ToInt32(reader["show_on_menu"]);
                    pageList.Add(page);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return pageList;
        }

        public static List<Page> getBillPage(SqlConnection dbConnection)
        {
            List<Page> pageList = new List<Page>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT distinct page_name,page_id,icon,url,show_on_menu FROM TABLE_ORG_USR_ROLE UR INNER JOIN TABLE_ROLE_MODULE RM ON RM.ROLE_ID=UR.ROLE_ID INNER JOIN TABLE_PAGE P ON P.MODULE_ID=RM.MODULE_ID WHERE PAGE_NAME='Bills'", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Page page = new Page();
                    page.NAME = reader["page_name"].ToString();
                    page.ID = reader["page_id"].ToString();
                    page.ICON = reader["icon"].ToString();
                    page.URL = reader["url"].ToString();
                    page.SHOW_ON_MENU = Convert.ToInt32(reader["show_on_menu"]);
                    pageList.Add(page);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return pageList;
        }
        /*Get role access rights*/
        public static List<Module> getRoleModList(SqlConnection dbConnection,string role_id)
        {
            List<Module> modList = new List<Module>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_ROLE_MODULE WHERE ROLE_ID=@ROLE_ID", dbConnection);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role_id;
                command.Parameters.Add(ROLE_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Module mod = new Module();
                    mod.ID = reader["MODULE_ID"].ToString();
                   // mod.NAME = reader["MODULE_NAME"].ToString();
                    modList.Add(mod);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return modList;
        }

        /*Assign role access rights to the selected mods */
        public static void createRoleMod(SqlConnection dbConnection, Role role)
        {
            List<Module> roleMods = role.roleModList;
            foreach (Module mod in roleMods)
            {
                try
                {
                    SqlCommand command = new SqlCommand(null, dbConnection);
                    command.CommandText = "INSERT INTO TABLE_ROLE_MODULE (ROLE_ID,MODULE_ID) VALUES ( @ROLE_ID,@MODULE_ID)";
                    SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                    ROLE_ID.Value = role.ROLE_ID;
                    command.Parameters.Add(ROLE_ID);
                    SqlParameter MODULE_ID = new SqlParameter("MODULE_ID", SqlDbType.NVarChar, 100);
                    MODULE_ID.Value = mod.ID;
                    command.Parameters.Add(MODULE_ID);

                    command.ExecuteScalar().ToString();
                }
                catch (Exception e)
               {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
                  }
        }
        }

        /*Delete role access rights*/
        public static int deleteRoleMods(SqlConnection dbConnection, string role_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_ROLE_MODULE WHERE ROLE_ID=@ROLE_ID;", dbConnection);
                SqlParameter ROLE_ID = new SqlParameter("ROLE_ID", SqlDbType.NVarChar, 100);
                ROLE_ID.Value = role_id;
                command.Parameters.Add(ROLE_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }
        #endregion
    }
}