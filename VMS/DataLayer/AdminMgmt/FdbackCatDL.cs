﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class FdbackCatDL
    {

        public static List<FdbackCat> getFdbackCatList(SqlConnection dbConnection)
        {
            List<FdbackCat> catList = new List<FdbackCat>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_FDBACK_CAT ORDER BY CATEGORY ASC;", dbConnection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    FdbackCat cat = new FdbackCat();
                    cat.FDBACK_CAT_ID = reader["FDBACK_CAT_ID"].ToString();
                    cat.CATEGORY = reader["CATEGORY"].ToString();
                    catList.Add(cat);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return catList;
        }

        public static FdbackCat getCat(SqlConnection dataConnection, string cat_id)
        {
            FdbackCat cat = new FdbackCat();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_FDBACK_CAT WHERE FDBACK_CAT_ID=@FDBACK_CAT_ID", dataConnection);
                SqlParameter FDBACK_CAT_ID = new SqlParameter("FDBACK_CAT_ID", SqlDbType.NVarChar, 100);
                FDBACK_CAT_ID.Value = cat_id;
                command.Parameters.Add(FDBACK_CAT_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cat.FDBACK_CAT_ID = reader["FDBACK_CAT_ID"].ToString();
                    cat.CATEGORY = reader["CATEGORY"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return cat;
        }


        public static Boolean getCatByName(SqlConnection dataConnection, string category)
        {
            Boolean existName = false;
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_FDBACK_CAT WHERE UPPER(CATEGORY)=@CATEGORY", dataConnection);
                SqlParameter CATEGORY = new SqlParameter("CATEGORY", SqlDbType.NVarChar, 100);
                CATEGORY.Value = category.ToUpper();
                command.Parameters.Add(CATEGORY);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    existName = true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return existName;
        }

        public static string createCat(SqlConnection dbConnection, FdbackCat cat)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_FDBACK_CAT (FDBACK_CAT_ID,CATEGORY) OUTPUT INSERTED.FDBACK_CAT_ID VALUES (NEWID(),@CATEGORY)";
            SqlParameter CATEGORY = new SqlParameter("CATEGORY", SqlDbType.NVarChar, 100);
            CATEGORY.Value = cat.CATEGORY;
            command.Parameters.Add(CATEGORY);

            string id = command.ExecuteScalar().ToString();
            return id;
        }

  
        public static int updateCat(SqlConnection dbConnection, FdbackCat cat)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_FDBACK_CAT SET CATEGORY=@CATEGORY Where FDBACK_CAT_ID=@FDBACK_CAT_ID", dbConnection);
                SqlParameter FDBACK_CAT_ID = new SqlParameter("FDBACK_CAT_ID", SqlDbType.NVarChar, 100);
                FDBACK_CAT_ID.Value = cat.FDBACK_CAT_ID;
                command.Parameters.Add(FDBACK_CAT_ID);
                SqlParameter CATEGORY = new SqlParameter("CATEGORY", SqlDbType.NVarChar, 100);
                CATEGORY.Value = cat.CATEGORY;
                command.Parameters.Add(CATEGORY);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

      
        public static int deleteCat(SqlConnection dbConnection, string cat_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_FDBACK_CAT Where FDBACK_CAT_ID=@FDBACK_CAT_ID", dbConnection);
                SqlParameter FDBACK_CAT_ID = new SqlParameter("FDBACK_CAT_ID", SqlDbType.NVarChar, 100);
                FDBACK_CAT_ID.Value = cat_id;
                command.Parameters.Add(FDBACK_CAT_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }
    }
}