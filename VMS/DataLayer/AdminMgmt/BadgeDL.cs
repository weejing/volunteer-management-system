﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class BadgeDL
    {
        public static List<Badge> getMinHourBadgeList(SqlConnection dbConnection)
        {
            List<Badge> badgeList = new List<Badge>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_BADGE B LEFT JOIN TABLE_TERM S ON CAUSE_ID=TERM_ID ORDER BY MINIMUM_HOURS ASC, TERM_NAME ASC", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Badge badge = new Badge();
                    if (!reader["MINIMUM_HOURS"].Equals(DBNull.Value))
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.MINIMUM_HOURS = Convert.ToInt32(reader["MINIMUM_HOURS"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    else
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                        badge.CAUSE_NAME = reader["TERM_NAME"].ToString();
                        badge.QTY = Convert.ToInt32(reader["QTY"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    badgeList.Add(badge);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return badgeList;
        }

        public static List<Badge> getPublishedBadgeList(SqlConnection dbConnection)
        {
            List<Badge> badgeList = new List<Badge>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_BADGE B LEFT JOIN TABLE_TERM S ON CAUSE_ID=TERM_ID WHERE STATUS='Published' ORDER BY MINIMUM_HOURS ASC, TERM_NAME ASC", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Badge badge = new Badge();
                    if (!reader["MINIMUM_HOURS"].Equals(DBNull.Value))
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.MINIMUM_HOURS = Convert.ToInt32(reader["MINIMUM_HOURS"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    else
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                        badge.CAUSE_NAME = reader["TERM_NAME"].ToString();
                        badge.QTY = Convert.ToInt32(reader["QTY"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    badge.VOL_HAS = 0;
                    badgeList.Add(badge);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return badgeList;
        }


        public static Badge getBadge(SqlConnection dataConnection, string badge_id)
        {
            Badge badge = new Badge();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_BADGE WHERE BADGE_ID=@BADGE_ID", dataConnection);
                SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                BADGE_ID.Value = badge_id;
                command.Parameters.Add(BADGE_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["MINIMUM_HOURS"].Equals(DBNull.Value))
                    {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.MINIMUM_HOURS = Convert.ToInt32(reader["MINIMUM_HOURS"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                    else {
                        badge.BADGE_ID = reader["BADGE_ID"].ToString();
                        badge.DESCRIPTION = reader["DESCRIPTION"].ToString();
                        badge.CAUSE_ID = Convert.ToInt32(reader["CAUSE_ID"]);
                        badge.QTY = Convert.ToInt32(reader["QTY"]);
                        badge.PHOTO_PATH = reader["PHOTO_PATH"].ToString();
                        badge.STATUS = reader["STATUS"].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return badge;
        }

        public static string createBadge(SqlConnection dbConnection, Badge badge)
        {          
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_BADGE (BADGE_ID,PHOTO_PATH,DESCRIPTION,MINIMUM_HOURS,CAUSE_ID,QTY,STATUS) OUTPUT INSERTED.BADGE_ID VALUES ( NEWID(),@PHOTO_PATH,@DESCRIPTION,@MINIMUM_HOURS,@CAUSE_ID,@QTY,@STATUS)";
            SqlParameter PHOTO_PATH = new SqlParameter("PHOTO_PATH", SqlDbType.NVarChar, 100);
            PHOTO_PATH.Value = badge.PHOTO_PATH;
            command.Parameters.Add(PHOTO_PATH);
            SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
            DESCRIPTION.Value = badge.DESCRIPTION;
            command.Parameters.Add(DESCRIPTION);
            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
            STATUS.Value = badge.STATUS;
            command.Parameters.Add(STATUS);

            SqlParameter MINIMUM_HOURS = new SqlParameter("MINIMUM_HOURS", SqlDbType.Int);
            if (badge.MINIMUM_HOURS.ToString().Equals("0")) { MINIMUM_HOURS.Value = DBNull.Value;}
            else
            { MINIMUM_HOURS.Value = badge.MINIMUM_HOURS; }
            command.Parameters.Add(MINIMUM_HOURS);

            SqlParameter CAUSE_ID = new SqlParameter("CAUSE_ID", SqlDbType.Int);
            if (badge.CAUSE_ID.ToString().Equals("0")) { CAUSE_ID.Value = DBNull.Value; }
            else
            { CAUSE_ID.Value = badge.CAUSE_ID; }
            command.Parameters.Add(CAUSE_ID);

            SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int);
            if (badge.QTY.ToString().Equals("0")) { QTY.Value = DBNull.Value; }
            else
            { QTY.Value = badge.QTY; }
            command.Parameters.Add(QTY);

            string id = command.ExecuteScalar().ToString();
            return id;
        }

        public static int updateBadge(SqlConnection dbConnection, Badge badge)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_BADGE SET PHOTO_PATH=@PHOTO_PATH,DESCRIPTION=@DESCRIPTION,STATUS=@STATUS,MINIMUM_HOURS=@MINIMUM_HOURS,CAUSE_ID=@CAUSE_ID,QTY=@QTY Where BADGE_ID=@BADGE_ID", dbConnection);
                SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                BADGE_ID.Value = badge.BADGE_ID;
                command.Parameters.Add(BADGE_ID);
                SqlParameter PHOTO_PATH = new SqlParameter("PHOTO_PATH", SqlDbType.NVarChar, 100);
                PHOTO_PATH.Value = badge.PHOTO_PATH;
                command.Parameters.Add(PHOTO_PATH);
                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
                DESCRIPTION.Value = badge.DESCRIPTION;
                command.Parameters.Add(DESCRIPTION);
                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = badge.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter MINIMUM_HOURS = new SqlParameter("MINIMUM_HOURS", SqlDbType.Int);
                if (badge.MINIMUM_HOURS.ToString().Equals("0")) { MINIMUM_HOURS.Value = DBNull.Value; }
                else
                { MINIMUM_HOURS.Value = badge.MINIMUM_HOURS; }
                command.Parameters.Add(MINIMUM_HOURS);

                SqlParameter CAUSE_ID = new SqlParameter("CAUSE_ID", SqlDbType.Int);
                if (badge.CAUSE_ID.ToString().Equals("0")) { CAUSE_ID.Value = DBNull.Value; }
                else
                { CAUSE_ID.Value = badge.CAUSE_ID; }
                command.Parameters.Add(CAUSE_ID);

                SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int);
                if (badge.QTY.ToString().Equals("0")) { QTY.Value = DBNull.Value; }
                else
                { QTY.Value = badge.QTY; }
                command.Parameters.Add(QTY);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        public static int updateBadgeNoPhoto(SqlConnection dbConnection, Badge badge)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_BADGE SET DESCRIPTION=@DESCRIPTION,STATUS=@STATUS,MINIMUM_HOURS=@MINIMUM_HOURS,CAUSE_ID=@CAUSE_ID,QTY=@QTY Where BADGE_ID=@BADGE_ID", dbConnection);
                SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                BADGE_ID.Value = badge.BADGE_ID;
                command.Parameters.Add(BADGE_ID);              
                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
                DESCRIPTION.Value = badge.DESCRIPTION;
                command.Parameters.Add(DESCRIPTION);
                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = badge.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter MINIMUM_HOURS = new SqlParameter("MINIMUM_HOURS", SqlDbType.Int);
                if (badge.MINIMUM_HOURS.ToString().Equals("0")) { MINIMUM_HOURS.Value = DBNull.Value; }
                else
                { MINIMUM_HOURS.Value = badge.MINIMUM_HOURS; }
                command.Parameters.Add(MINIMUM_HOURS);

                SqlParameter CAUSE_ID = new SqlParameter("CAUSE_ID", SqlDbType.Int);
                if (badge.CAUSE_ID.ToString().Equals("0")) { CAUSE_ID.Value = DBNull.Value; }
                else
                { CAUSE_ID.Value = badge.CAUSE_ID; }
                command.Parameters.Add(CAUSE_ID);

                SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int);
                if (badge.QTY.ToString().Equals("0")) { QTY.Value = DBNull.Value; }
                else
                { QTY.Value = badge.QTY; }
                command.Parameters.Add(QTY);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        public static int updateStatus(SqlConnection dbConnection, Badge badge)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_BADGE SET STATUS='Published' Where BADGE_ID=@BADGE_ID", dbConnection);
                SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                BADGE_ID.Value = badge.BADGE_ID;
                command.Parameters.Add(BADGE_ID);
                
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        public static Boolean deleteBadge(SqlConnection dbConnection, string badge_id)
        {
            Boolean deleted=false;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_BADGE WHERE BADGE_ID=@BADGE_ID;", dbConnection);
                SqlParameter BADGE_ID = new SqlParameter("BADGE_ID", SqlDbType.NVarChar, 100);
                BADGE_ID.Value = badge_id;
                command.Parameters.Add(BADGE_ID);
                command.ExecuteNonQuery();
                deleted = true;
            }
            catch (Exception e)
            {
                deleted = false;
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return deleted;
        }

    }
}