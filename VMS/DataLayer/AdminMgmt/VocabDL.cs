﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class VocabDL
    {        
        public static int getVocabID(SqlConnection databaseConnection, String vocab_name)
        {
            int id=0;
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "Select VOCAB_ID from TABLE_VOCAB Where VOCAB_NAME = @VOCAB_NAME";
            SqlParameter VOCAB_NAME = new SqlParameter("VOCAB_NAME", SqlDbType.NVarChar, 100);
            VOCAB_NAME.Value = vocab_name;
            command.Parameters.Add(VOCAB_NAME);

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                id = Convert.ToInt32(reader[0]);
            }
            reader.Close();
            return id;
        }
        
        public static List<Vocab> getVocabList(SqlConnection dataConnection)
        {
            List<Vocab> vocabList = new List<Vocab>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_VOCAB ORDER BY VOCAB_NAME ASC", dataConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Vocab vocab = new Vocab();
                    vocab.FOR_INTRST_FORM = Convert.ToInt32(reader["FOR_INTRST_FORM"]);
                    vocab.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    vocab.VOCAB_NAME = reader["VOCAB_NAME"].ToString();
                    vocabList.Add(vocab);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return vocabList;
        }

        public static List<Vocab> getVocabForIntFormList(SqlConnection dataConnection)
        {
            List<Vocab> vocabList = new List<Vocab>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_VOCAB WHERE FOR_INTRST_FORM=1 ORDER BY VOCAB_NAME ASC", dataConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Vocab vocab = new Vocab();
                    vocab.FOR_INTRST_FORM = Convert.ToInt32(reader["FOR_INTRST_FORM"]);
                    vocab.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    vocab.VOCAB_NAME = reader["VOCAB_NAME"].ToString();
                    vocabList.Add(vocab);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return vocabList;
        }

    }
}