﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class ExamPeriodDL
    {
        /*Create exam record */
        public static string createExamPeriod(SqlConnection dbConnection, ExamPeriod examPeriod)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_EXAM_PERIOD (EXAM_PERIOD_ID, START_DATE,END_DATE,LVL_OF_EDU_ID) OUTPUT INSERTED.EXAM_PERIOD_ID VALUES (NEWID(), @START_DATE,@END_DATE,@LVL_OF_EDU_ID)";
            SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.DateTime);
            START_DATE.Value = examPeriod.START_DATE;
            command.Parameters.Add(START_DATE);
            SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.DateTime);
            END_DATE.Value = examPeriod.END_DATE;
            command.Parameters.Add(END_DATE);
            SqlParameter LVL_OF_EDU_ID = new SqlParameter("LVL_OF_EDU_ID", SqlDbType.NVarChar, 100);
            LVL_OF_EDU_ID.Value = examPeriod.LVL_OF_EDU_ID;
            command.Parameters.Add(LVL_OF_EDU_ID);
                      
            string id = command.ExecuteScalar().ToString();
            return id;
        }

        /*Update exam detail*/
        public static int updateExamPeriod(SqlConnection dbConnection, ExamPeriod examPeriod)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_EXAM_PERIOD SET START_DATE = @START_DATE, END_DATE=@END_DATE, LVL_OF_EDU_ID=@LVL_OF_EDU_ID Where EXAM_PERIOD_ID=@EXAM_PERIOD_ID", dbConnection);
                SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.DateTime);
                START_DATE.Value = examPeriod.START_DATE;
                command.Parameters.Add(START_DATE);
                SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.DateTime);
                END_DATE.Value = examPeriod.END_DATE;
                command.Parameters.Add(END_DATE);
                SqlParameter LVL_OF_EDU_ID = new SqlParameter("LVL_OF_EDU_ID", SqlDbType.NVarChar, 100);
                LVL_OF_EDU_ID.Value = examPeriod.LVL_OF_EDU_ID;
                command.Parameters.Add(LVL_OF_EDU_ID);
                SqlParameter EXAM_PERIOD_ID = new SqlParameter("EXAM_PERIOD_ID", SqlDbType.NVarChar, 100);
                EXAM_PERIOD_ID.Value = examPeriod.EXAM_PERIOD_ID;
                command.Parameters.Add(EXAM_PERIOD_ID);

                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete exam record*/
        public static int deleteExamPeriod(SqlConnection dbConnection, string exam_period_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_EXAM_PERIOD WHERE EXAM_PERIOD_ID=@EXAM_PERIOD_ID;", dbConnection);
                SqlParameter EXAM_PERIOD_ID = new SqlParameter("EXAM_PERIOD_ID", SqlDbType.NVarChar, 100);
                EXAM_PERIOD_ID.Value = exam_period_id;
                command.Parameters.Add(EXAM_PERIOD_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*get exam detail*/
        public static ExamPeriod getExamPeriod(SqlConnection dbConnection, string exam_period_id)
        {
            ExamPeriod examPeriod = new ExamPeriod();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_EXAM_PERIOD P INNER JOIN TABLE_LVL_OF_EDU L ON P.LVL_OF_EDU_ID=L.LVL_OF_EDU_ID WHERE exam_period_id=@EXAM_PERIOD_ID;", dbConnection);
                SqlParameter EXAM_PERIOD_ID = new SqlParameter("EXAM_PERIOD_ID", SqlDbType.NVarChar, 100);
                EXAM_PERIOD_ID.Value = exam_period_id;
                command.Parameters.Add(EXAM_PERIOD_ID); 

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    examPeriod.EXAM_PERIOD_ID = reader["EXAM_PERIOD_ID"].ToString();
                    examPeriod.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                    examPeriod.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                    examPeriod.SCH_LVL = reader["LVL"].ToString();                  
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return examPeriod;
        }

        /*Get list of exam details*/
        public static List<ExamPeriod> getExamPeriodList(SqlConnection dbConnection)
        {
            List<ExamPeriod> listOfFirms = new List<ExamPeriod>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_EXAM_PERIOD P INNER JOIN TABLE_LVL_OF_EDU L ON P.LVL_OF_EDU_ID=L.LVL_OF_EDU_ID ORDER BY START_DATE DESC;", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExamPeriod examPeriod = new ExamPeriod();
                    examPeriod.EXAM_PERIOD_ID = reader["EXAM_PERIOD_ID"].ToString();
                    examPeriod.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                    examPeriod.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                    examPeriod.SCH_LVL = reader["LVL"].ToString();
                    listOfFirms.Add(examPeriod);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfFirms;
        }
    }
}