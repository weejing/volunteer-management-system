﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class PageDL
    {

        /*Get parent node*/
        public static List<Module> getModuleList(SqlConnection dbConnection)
        {
            List<Module> modList = new List<Module>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_MODULE ORDER BY MODULE_NAME ASC", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Module mod = new Module();
                    mod.NAME = reader["MODULE_NAME"].ToString();
                    mod.ID = reader["MODULE_ID"].ToString();
                    modList.Add(mod);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return modList;
        }

        /*Get list of pages*/
        public static List<Page> getPageList(SqlConnection dbConnection)
        {
            List<Page> pageList = new List<Page>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_PAGE", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Page page = new Page();
                    page.ID = reader["PAGE_ID"].ToString();
                    page.MODULE_ID = reader["MODULE_ID"].ToString();
                    page.NAME = reader["PAGE_NAME"].ToString();
                    page.URL = reader["URL"].ToString();
                    page.SHOW_ON_MENU = Convert.ToInt32(reader["SHOW_ON_MENU"]);
                    page.ICON = reader["ICON"].ToString();
                    pageList.Add(page);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return pageList;
        }

    }
}