﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AdminMgmt
{
    public class TermRelationDL
    {
        /*Get term list of something based on the vocab e.g. vocab_name=skills, this method get usr skills*/
        public static List<TermRelation> getTermRelListByVocab(SqlConnection dbConnection, string vocab_id)
        {
            List<TermRelation> termRelList = new List<TermRelation>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION WHERE VOCAB_ID=@VOCAB_ID;", dbConnection);
                SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.NVarChar, 100);
                VOCAB_ID.Value = vocab_id;
                command.Parameters.Add(VOCAB_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TermRelation termRel = new TermRelation();
                    termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
                    termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
                    termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
                    termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    termRel.VOCAB_ID= Convert.ToInt32(reader["VOCAB_ID"]);
                    termRelList.Add(termRel);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return termRelList;
        }

        public static TermRelation getTermRelByTermID(SqlConnection dbConnection, string term_id)
        {
            TermRelation termRel = null;

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION WHERE TERM_ID=@TERM_ID;", dbConnection);
                SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.NVarChar, 100);
                TERM_ID.Value = term_id;
                command.Parameters.Add(TERM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    termRel=new TermRelation();
                    termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
                    termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
                    termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
                    termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    termRel.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                   
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return termRel;
        }

        public static List<TermRelation> getTermRelListByEntityId(SqlConnection dbConnection, string entity_id, string entity_type)
        {
            List<TermRelation> termRelList = new List<TermRelation>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION WHERE ENTITY_ID=@ENTITY_ID AND ENTITY_TYPE=@ENTITY_TYPE;", dbConnection);
                SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
                ENTITY_ID.Value = entity_id;
                command.Parameters.Add(ENTITY_ID);
                SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
                ENTITY_TYPE.Value = entity_type;
                command.Parameters.Add(ENTITY_TYPE);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TermRelation termRel = new TermRelation();
                    termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
                    termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
                    termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
                    termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    termRel.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    termRelList.Add(termRel);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return termRelList;
        }

        public static List<TermRelation> getTermRelListByEntityIdAndVocab(SqlConnection dbConnection, string entity_id, string entity_type, int vocab_id)
        {
            List<TermRelation> termRelList = new List<TermRelation>();
           
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION R INNER JOIN TABLE_TERM T ON T.TERM_ID=R.TERM_ID WHERE ENTITY_ID=@ENTITY_ID AND ENTITY_TYPE=@ENTITY_TYPE AND R.VOCAB_ID=@VOCAB_ID;", dbConnection);
                SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
                ENTITY_ID.Value = entity_id;
                command.Parameters.Add(ENTITY_ID);
                SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
                ENTITY_TYPE.Value = entity_type;
                command.Parameters.Add(ENTITY_TYPE);
                SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
                VOCAB_ID.Value = vocab_id;
                command.Parameters.Add(VOCAB_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TermRelation termRel = new TermRelation();
                    termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
                    termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
                    termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
                    termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    termRel.TERM_NAME = reader["TERM_NAME"].ToString();
                    termRel.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                    termRelList.Add(termRel);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return termRelList;
        }

        //public static List<TermRelation> getTermRelListByTerm(SqlConnection dbConnection, string entity_type)
        //{
        //    List<TermRelation> termRelList = new List<TermRelation>();

        //    try
        //    {
        //        SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION WHERE ENTITY_TYPE=@ENTITY_TYPE;", dbConnection);
        //        SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
        //        ENTITY_TYPE.Value = entity_type;
        //        command.Parameters.Add(ENTITY_TYPE);

        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            TermRelation termRel = new TermRelation();
        //            termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
        //            termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
        //            termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
        //            termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
        //            termRel.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
        //            termRelList.Add(termRel);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
        //    }
        //    return termRelList;
        //}
        /*Get term relation*/
        public static TermRelation getTermRelation(SqlConnection dataConnection, string term_relation_id)
        {
            TermRelation termRel = new TermRelation();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_TERM_RELATION WHERE term_relation_id=@term_relation_id", dataConnection);
                SqlParameter TERM_RELATION_ID = new SqlParameter("TERM_RELATION_ID", SqlDbType.Int);
                TERM_RELATION_ID.Value = term_relation_id;
                command.Parameters.Add(TERM_RELATION_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    termRel.TERM_RELATION_ID = Convert.ToInt32(reader["TERM_RELATION_ID"]);
                    termRel.ENTITY_ID = reader["ENTITY_ID"].ToString();
                    termRel.ENTITY_TYPE = reader["ENTITY_TYPE"].ToString();
                    termRel.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                    termRel.VOCAB_ID = Convert.ToInt32(reader["VOCAB_ID"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return termRel;
        }

        /*Create term relation */
        public static int createTermRelation(SqlConnection dbConnection, TermRelation termRelation)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_TERM_RELATION (ENTITY_ID, ENTITY_TYPE,TERM_ID,VOCAB_ID) OUTPUT INSERTED.TERM_RELATION_ID VALUES (@ENTITY_ID, @ENTITY_TYPE,@TERM_ID,@VOCAB_ID)";
            SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
            ENTITY_ID.Value = termRelation.ENTITY_ID;
            command.Parameters.Add(ENTITY_ID);
            SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
            ENTITY_TYPE.Value = termRelation.ENTITY_TYPE;
            command.Parameters.Add(ENTITY_TYPE);
            SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.Int);
            TERM_ID.Value = termRelation.TERM_ID;
            command.Parameters.Add(TERM_ID);
            SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
            VOCAB_ID.Value = termRelation.VOCAB_ID;
            command.Parameters.Add(VOCAB_ID);

            int id = Convert.ToInt32(command.ExecuteScalar());
            return id;
        }

        public static void createTermRelList(SqlConnection dbConnection, List<TermRelation> termRelationList)
        {
            foreach (TermRelation termRelation in termRelationList)
            {
                try
                {
                    SqlCommand command = new SqlCommand(null, dbConnection);
                    command.CommandText = "INSERT INTO TABLE_TERM_RELATION (ENTITY_ID, ENTITY_TYPE,TERM_ID,VOCAB_ID) OUTPUT INSERTED.TERM_RELATION_ID VALUES (@ENTITY_ID, @ENTITY_TYPE,@TERM_ID,@VOCAB_ID)";
                    SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
                    ENTITY_ID.Value = termRelation.ENTITY_ID;
                    command.Parameters.Add(ENTITY_ID);
                    SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
                    ENTITY_TYPE.Value = termRelation.ENTITY_TYPE;
                    command.Parameters.Add(ENTITY_TYPE);
                    SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.Int);
                    TERM_ID.Value = termRelation.TERM_ID;
                    command.Parameters.Add(TERM_ID);
                    SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
                    VOCAB_ID.Value = termRelation.VOCAB_ID;
                    command.Parameters.Add(VOCAB_ID);
                    command.ExecuteScalar();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
                }
            }
        }

        public static void createRelListForSameEntity(SqlConnection dbConnection, List<TermRelation> termRelationList,string entity_type,string entity_id)
        {
            foreach (TermRelation termRelation in termRelationList)
            {
                try
                {
                    SqlCommand command = new SqlCommand(null, dbConnection);
                    command.CommandText = "INSERT INTO TABLE_TERM_RELATION (ENTITY_ID, ENTITY_TYPE,TERM_ID,VOCAB_ID) OUTPUT INSERTED.TERM_RELATION_ID VALUES (@ENTITY_ID, @ENTITY_TYPE,@TERM_ID,@VOCAB_ID)";
                    SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
                    ENTITY_ID.Value = entity_id;
                    command.Parameters.Add(ENTITY_ID);
                    SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
                    ENTITY_TYPE.Value = entity_type;
                    command.Parameters.Add(ENTITY_TYPE);
                    SqlParameter TERM_ID = new SqlParameter("TERM_ID", SqlDbType.Int);
                    TERM_ID.Value = termRelation.TERM_ID;
                    command.Parameters.Add(TERM_ID);
                    SqlParameter VOCAB_ID = new SqlParameter("VOCAB_ID", SqlDbType.Int);
                    VOCAB_ID.Value = termRelation.VOCAB_ID;
                    command.Parameters.Add(VOCAB_ID);
                    command.ExecuteScalar();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
                }
            }
        }

        /*Delete relation*/
        public static int deleteTermRelation(SqlConnection dbConnection, string term_relation_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_TERM_RELATION WHERE TERM_RELATION_ID=@TERM_RELATION_ID;", dbConnection);
                SqlParameter TERM_RELATION_ID = new SqlParameter("TERM_RELATION_ID", SqlDbType.Int);
                TERM_RELATION_ID.Value = term_relation_id;
                command.Parameters.Add(TERM_RELATION_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        public static int deleteTermRelByEntityID(SqlConnection dbConnection, string entity_id, string entity_type)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_TERM_RELATION WHERE ENTITY_ID=@ENTITY_ID AND ENTITY_TYPE=@ENTITY_TYPE;", dbConnection);
                SqlParameter ENTITY_ID = new SqlParameter("ENTITY_ID", SqlDbType.NVarChar, 100);
                ENTITY_ID.Value = entity_id;
                command.Parameters.Add(ENTITY_ID);
                SqlParameter ENTITY_TYPE = new SqlParameter("ENTITY_TYPE", SqlDbType.NVarChar, 100);
                ENTITY_TYPE.Value = entity_type;
                command.Parameters.Add(ENTITY_TYPE);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }
    }
}