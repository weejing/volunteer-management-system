﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using VMS.Models.TenderingModels;

namespace VMS.DataLayer.TenderingMgmt
{
    public class TenderingMgmtDL
    {
        public static List<TenderHeader> getTenderHeaderList(SqlConnection dbConnection)
        {
            List<TenderHeader> tenderHeaderList = new List<TenderHeader>();
            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_TENDER_HEADER INNER JOIN TABLE_IVT_PRODUCT ON TABLE_VDR_TENDER_HEADER.CATEGORY = TABLE_IVT_PRODUCT.IVT_PRODUCT_ID INNER JOIN TABLE_ORG_DETAILS ON TABLE_VDR_TENDER_HEADER.CONNECTION_STRING = TABLE_ORG_DETAILS.CONNECTION_STRING ", dbConnection);

               

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderHeader tenderItem = new TenderHeader();

                    tenderItem.TENDER_HEADER_ID = reader["TENDER_HEADER_ID"].ToString();
                    tenderItem.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());         
                    tenderItem.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                    tenderItem.STATUS = reader["STATUS"].ToString();
                    tenderItem.CATEGORY = reader["CATEGORY"].ToString();
                    tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();       
                    tenderItem.ORG_NAME = reader["ORG_NAME"].ToString();
                    tenderItem.NAME = reader["NAME"].ToString();
                    tenderHeaderList.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderHeaderList;
        }

        public static TenderHeader getTenderHeader(SqlConnection dbConnection, TenderHeader headerID)
        {
            TenderHeader tenderHeader = new TenderHeader();
            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_TENDER_HEADER INNER JOIN TABLE_IVT_PRODUCT ON TABLE_VDR_TENDER_HEADER.CATEGORY = TABLE_IVT_PRODUCT.IVT_PRODUCT_ID INNER JOIN TABLE_ORG_DETAILS ON TABLE_VDR_TENDER_HEADER.CONNECTION_STRING = TABLE_ORG_DETAILS.CONNECTION_STRING WHERE TENDER_HEADER_ID=@TENDER_HEADER_ID ", dbConnection);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_HEADER_ID.Value = headerID.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                SqlDataReader reader = command.ExecuteReader();

               
                while (reader.Read())
                {
                    tenderHeader.TENDER_HEADER_ID = reader["TENDER_HEADER_ID"].ToString();
                    tenderHeader.START_DATE = Convert.ToDateTime(reader["START_DATE"].ToString());
                    tenderHeader.END_DATE = Convert.ToDateTime(reader["END_DATE"].ToString());
                    tenderHeader.STATUS = reader["STATUS"].ToString();
                    tenderHeader.CATEGORY = reader["CATEGORY"].ToString();
                    tenderHeader.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    tenderHeader.ORG_NAME = reader["ORG_NAME"].ToString();
                    tenderHeader.NAME = reader["NAME"].ToString();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderHeader;
        }

        public static List<TenderItemDetails> getSaaSTenderItemList(SqlConnection dbConnection, TenderHeader headerID)
        {
            List<TenderItemDetails> tenderItemList = new List<TenderItemDetails>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT  F.FIRM_NAME AS WINNER_SOLO_FIRM_ID,G.FIRM_NAME AS WINNER_GROUP_FIRM_ID,I.TENDER_ITEM_ID,I.TENDER_DESCRIPTION, I.CONNECTION_STRING, I.QTY, I.TENDER_HEADER_ID, I.WINNER_SOLO_FIRM_ID, I.status AS TENDER_STATUS,O.ORG_NAME FROM TABLE_VDR_TENDER_ITEM I INNER JOIN TABLE_ORG_DETAILS O ON I.CONNECTION_STRING = O.CONNECTION_STRING INNER JOIN TABLE_FIRM_DETAILS F ON I.WINNER_SOLO_FIRM_ID = F.USR_ID INNER JOIN TABLE_FIRM_DETAILS G ON I.WINNER_GROUP_FIRM_ID = G.USR_ID  WHERE I.TENDER_HEADER_ID = @TENDER_HEADER_ID", dbConnection);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_HEADER_ID.Value = headerID.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                //SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 100);
                //FIRM_ID.Value = headerID.CATEGORY;
                //command.Parameters.Add(FIRM_ID);


                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderItemDetails tenderItem = new TenderItemDetails();

                    tenderItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    tenderItem.TENDER_DESCRIPTION = reader["TENDER_DESCRIPTION"].ToString();
                    tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    tenderItem.QTY = Int32.Parse(reader["QTY"].ToString());
                    tenderItem.TENDER_STATUS = reader["TENDER_STATUS"].ToString();
                    //tenderItem.BID_STATUS = reader["BID_STATUS"].ToString();
                    tenderItem.TENDER_HEADER_ID = reader["TENDER_HEADER_ID"].ToString();
                    tenderItem.WINNER_GROUP_FIRM_ID = reader["WINNER_GROUP_FIRM_ID"].ToString();
                    tenderItem.WINNER_SOLO_FIRM_ID = reader["WINNER_SOLO_FIRM_ID"].ToString();
                    tenderItem.ORG_NAME = reader["ORG_NAME"].ToString();
                    tenderItemList.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItemList;
        }

        public static List<TenderItemDetails> getTenderItemList(SqlConnection dbConnection, TenderHeader headerID)
        {
            List<TenderItemDetails> tenderItemList = new List<TenderItemDetails>();
            try
            {
                SqlCommand command = new SqlCommand("SELECT O.ORG_NAME,F.FIRM_NAME AS WINNER_SOLO_FIRM_ID,G.FIRM_NAME AS WINNER_GROUP_FIRM_ID,I.TENDER_ITEM_ID,I.TENDER_DESCRIPTION, I.CONNECTION_STRING, I.QTY, I.TENDER_HEADER_ID, I.WINNER_SOLO_FIRM_ID, I.status AS TENDER_STATUS, (CASE WHEN B.status IS NULL THEN 'NA' ELSE B.status END) as BID_STATUS FROM TABLE_VDR_TENDER_ITEM I left JOIN TABLE_VDR_BIDS B ON B.TENDER_ITEM_ID = I.TENDER_ITEM_ID AND B.FIRM_ID = @FIRM_ID INNER JOIN TABLE_ORG_DETAILS O ON I.CONNECTION_STRING = O.CONNECTION_STRING INNER JOIN TABLE_FIRM_DETAILS F ON I.WINNER_SOLO_FIRM_ID = F.USR_ID INNER JOIN TABLE_FIRM_DETAILS G ON I.WINNER_GROUP_FIRM_ID = G.USR_ID  WHERE I.TENDER_HEADER_ID = @TENDER_HEADER_ID", dbConnection);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_HEADER_ID.Value = headerID.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 100);
                FIRM_ID.Value = headerID.CATEGORY;
                command.Parameters.Add(FIRM_ID);


                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderItemDetails tenderItem = new TenderItemDetails();

                    tenderItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    tenderItem.TENDER_DESCRIPTION = reader["TENDER_DESCRIPTION"].ToString();
                    tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    tenderItem.QTY = Int32.Parse(reader["QTY"].ToString());
                    tenderItem.TENDER_STATUS = reader["TENDER_STATUS"].ToString();
                    tenderItem.BID_STATUS = reader["BID_STATUS"].ToString();
                    tenderItem.TENDER_HEADER_ID = reader["TENDER_HEADER_ID"].ToString();
                    tenderItem.WINNER_GROUP_FIRM_ID = reader["WINNER_GROUP_FIRM_ID"].ToString();
                    tenderItem.WINNER_SOLO_FIRM_ID = reader["WINNER_SOLO_FIRM_ID"].ToString();
                    tenderItem.ORG_NAME = reader["ORG_NAME"].ToString();
                    tenderItemList.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItemList;
        }

        public static List<VendorBidsDetails> getTenderLowestBid(SqlConnection dbConnection, TenderItemDetails headerID)
        {
            List<VendorBidsDetails> tenderItemList = new List<VendorBidsDetails>();
            try
            {
                SqlCommand command = new SqlCommand(" Select * from TABLE_VDR_BIDS B INNER JOIN TABLE_FIRM_DETAILS F ON B.FIRM_ID = F.USR_ID WHERE TENDER_ITEM_ID = @TENDER_ITEM_ID ORDER BY BID_PRICE ", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = headerID.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VendorBidsDetails tenderItem = new VendorBidsDetails();

                    tenderItem.FIRM_ID = reader["FIRM_ID"].ToString();
                    tenderItem.FIRM_NAME = reader["FIRM_NAME"].ToString();
                    tenderItem.BID_PRICE = Double.Parse(reader["BID_PRICE"].ToString());                    
                    tenderItemList.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItemList;
        }


        public static List<VendorBidsDetails> getTenderLowestBidSolo(SqlConnection dbConnection, TenderItemDetails headerID)
        {
            List<VendorBidsDetails> tenderItemList = new List<VendorBidsDetails>();
            try
            {
                SqlCommand command = new SqlCommand(" Select * from TABLE_VDR_BIDS B INNER JOIN TABLE_FIRM_DETAILS F ON B.FIRM_ID = F.USR_ID WHERE TENDER_ITEM_ID = @TENDER_ITEM_ID ORDER BY BID_PRICE_SOLO", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = headerID.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VendorBidsDetails tenderItem = new VendorBidsDetails();

                    tenderItem.FIRM_ID = reader["FIRM_ID"].ToString();
                    tenderItem.FIRM_NAME = reader["FIRM_NAME"].ToString();
                    tenderItem.BID_PRICE_SOLO = Double.Parse(reader["BID_PRICE_SOLO"].ToString());
                    tenderItemList.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItemList;
        }

        public static List<TenderCategory> getTenderCategoryList(SqlConnection dbConnection)
        {
            List<TenderCategory> listTenderCat = new List<TenderCategory>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_IVT_PRODUCT", dbConnection);



                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderCategory tenderCatItem = new TenderCategory();

                    tenderCatItem.IVT_PRODUCT_ID = reader["IVT_PRODUCT_ID"].ToString();
                    tenderCatItem.NAME = reader["NAME"].ToString();
                    tenderCatItem.LEVEL1_PARENT = reader["LEVEL1_PARENT"].ToString();
                    tenderCatItem.LEVEL2_PARENT = reader["LEVEL2_PARENT"].ToString();

                    listTenderCat.Add(tenderCatItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return listTenderCat;
        }

        public static List<TenderItemDetails> getTenderListSaaS(SqlConnection dbConnection, TenderItemDetails item)
        {
            List<TenderItemDetails> listOfTender = new List<TenderItemDetails>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_TENDER WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 100);
                CONNECTION_STRING.Value = item.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderItemDetails tenderItem = new TenderItemDetails();

                    //tenderItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    //tenderItem.TENDER_TITLE = reader["TENDER_TITLE"].ToString();
                    //tenderItem.START_DATE = Convert.ToDateTime(reader["START_DATE"]);
                    //tenderItem.END_DATE = Convert.ToDateTime(reader["END_DATE"]);
                    //tenderItem.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    //tenderItem.STATUS = reader["STATUS"].ToString();
                    //tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    //tenderItem.QTY = Int32.Parse(reader["QTY"].ToString());
                    ////tenderItem.WINNER_ORG_ID = reader["WINNER_ORG_ID"].ToString();
                    //tenderItem.CATEGORY = reader["CATEGORY"].ToString();
                    ////tenderItem.WINNER_DATE = Convert.ToDateTime(reader["WINNER_DATE"]);

                    listOfTender.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return listOfTender;
        }

        public static List<BidCombineView> getBidderSaaS(SqlConnection dbConnection, BidCombineView item)
        {
            List<BidCombineView> listOfTender = new List<BidCombineView>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_BIDS INNER JOIN TABLE_FIRM_DETAILS ON TABLE_VDR_BIDS.VENDOR_ID = TABLE_FIRM_DETAILS.USR_ID WHERE TABLE_VDR_BIDS.TENDER_ID=@TENDER_ID", dbConnection);

                SqlParameter TENDER_ID = new SqlParameter("TENDER_ID", SqlDbType.NVarChar, 100);
                TENDER_ID.Value = item.TENDER_ID;
                command.Parameters.Add(TENDER_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BidCombineView tenderItem = new BidCombineView();

                    tenderItem.VDR_BIDS_ID = reader["VDR_BIDS_ID"].ToString();
                    tenderItem.TENDER_ID = reader["TENDER_ID"].ToString();
                    tenderItem.BID_PRICE = Double.Parse(reader["BID_PRICE"].ToString());
                    tenderItem.BID_DATE = Convert.ToDateTime(reader["BID_DATE"]);
                    tenderItem.STATUS = reader["STATUS"].ToString();
                    tenderItem.PRICING_BREAKDOWN = reader["PRICING_BREAKDOWN"].ToString();
                    tenderItem.VENDOR_ID = reader["VENDOR_ID"].ToString();
                    tenderItem.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString();
                    tenderItem.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();
                    tenderItem.DELIVERY_TIME = Convert.ToDateTime(reader["DELIVERY_TIME"]);
                    tenderItem.PRODUCT_BRAND = reader["PRODUCT_BRAND"].ToString();
                    tenderItem.COMPANY_NAME = reader["FIRM_NAME"].ToString();
                    tenderItem.ADDRESS = reader["ADDRESS"].ToString();
                    //tenderItem.CITY = reader["CITY"].ToString();
                    //tenderItem.POSTAL_CODE = reader["POSTAL_CODE"].ToString();
                    //tenderItem.STATE = reader["STATE"].ToString();
                    tenderItem.PHONE = reader["PHONE"].ToString();
                    tenderItem.FAX = reader["FAX"].ToString();
                    tenderItem.COM_DESCRIPTION = reader["FIRM_DESCRIPTION"].ToString();
             
                    listOfTender.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return listOfTender;
        }

        public static BidCombineView getBidItemSaaS(SqlConnection dbConnection, BidCombineView item)
        {
            BidCombineView tenderItem = new BidCombineView();

            try
            {
                
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_BIDS, TABLE_FIRM_DETAILS WHERE TABLE_VDR_BIDS.VENDOR_ID = TABLE_FIRM_DETAILS.USR_ID AND TABLE_VDR_BIDS.VDR_BIDS_ID=@VDR_BIDS_ID", dbConnection);
                SqlParameter VDR_BIDS_ID = new SqlParameter("VDR_BIDS_ID", SqlDbType.UniqueIdentifier);
                VDR_BIDS_ID.Value = Guid.Parse(item.VDR_BIDS_ID);
                command.Parameters.Add(VDR_BIDS_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //tenderItem.VDR_BIDS_ID = reader["VDR_BIDS_ID"].ToString();
                    tenderItem.TENDER_ID = reader["TENDER_ID"].ToString();
                    tenderItem.BID_PRICE = Double.Parse(reader["BID_PRICE"].ToString());
                    tenderItem.BID_DATE = Convert.ToDateTime(reader["BID_DATE"]);
                    tenderItem.STATUS = reader["STATUS"].ToString();
                    tenderItem.PRICING_BREAKDOWN = reader["PRICING_BREAKDOWN"].ToString();
                    tenderItem.VENDOR_ID = reader["VENDOR_ID"].ToString();
                    tenderItem.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString();
                    tenderItem.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();
                    tenderItem.DELIVERY_TIME = Convert.ToDateTime(reader["DELIVERY_TIME"]);
                    tenderItem.PRODUCT_BRAND = reader["PRODUCT_BRAND"].ToString();
                    tenderItem.COMPANY_NAME = reader["FIRM_NAME"].ToString();
                    tenderItem.ADDRESS = reader["ADDRESS"].ToString();
                    //tenderItem.CITY = reader["CITY"].ToString();
                    //tenderItem.POSTAL_CODE = reader["POSTAL_CODE"].ToString();
                    //tenderItem.STATE = reader["STATE"].ToString();
                    tenderItem.PHONE = reader["PHONE"].ToString();
                    tenderItem.FAX = reader["FAX"].ToString();
                    tenderItem.COM_DESCRIPTION = reader["FIRM_DESCRIPTION"].ToString();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItem;
        }

        public static TenderItemDetails getTenderItem(SqlConnection dbConnection, String itemID)
        {
            TenderItemDetails tenderItem = new TenderItemDetails();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_TENDER WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = itemID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //tenderItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    //tenderItem.TENDER_TITLE = reader["TENDER_TITLE"].ToString();
                    //tenderItem.START_DATE = Convert.ToDateTime(reader["START_DATE"]);
                    //tenderItem.END_DATE = Convert.ToDateTime(reader["END_DATE"]);
                    //tenderItem.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    //tenderItem.STATUS = reader["STATUS"].ToString();
                    //tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    //tenderItem.QTY = Int32.Parse(reader["QTY"].ToString());
                    ////tenderItem.WINNER_ORG_ID = reader["WINNER_ORG_ID"].ToString();
                    //tenderItem.CATEGORY = reader["CATEGORY"].ToString();
                    ////tenderItem.WINNER_DATE = Convert.ToDateTime(reader["WINNER_DATE"]);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItem;
        }

        public static TenderItemDetails updateTenderStatusClosed(SqlConnection dbConnection)
        {
            TenderItemDetails tenderItem = new TenderItemDetails();

            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER SET STATUS='CLOSED' WHERE END_DATE=@END_DATE", dbConnection);
                
                DateTime regDatetimeTemp = DateTime.Today.AddDays(-1);
                string stringDate = regDatetimeTemp.ToString("yyyy-MM-dd");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "yyyy-MM-dd", null);

                SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.Date);
                END_DATE.Value = regDate;
                command.Parameters.Add(END_DATE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItem;
        }

        public static TenderItemDetails updateTenderStatusStart(SqlConnection dbConnection)
        {
            TenderItemDetails tenderItem = new TenderItemDetails();

            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER SET STATUS='ONGOING' WHERE START_DATE=@START_DATE", dbConnection);

                DateTime regDatetimeTemp = DateTime.Today;
                string stringDate = regDatetimeTemp.ToString("yyyy-MM-dd");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "yyyy-MM-dd", null);

                SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.Date);
                START_DATE.Value = regDate;
                command.Parameters.Add(START_DATE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return tenderItem;
        }


        public static String createTenderHeader(SqlConnection dbConnection, TenderHeader newTender)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_VDR_TENDER_HEADER VALUES (@TENDER_ITEM_HEADER_ID, @START_DATE, @END_DATE, @STATUS, @CATEGORY,@CONNECTION_STRING)", dbConnection);

                SqlParameter TENDER_ITEM_HEADER_ID = new SqlParameter("TENDER_ITEM_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_HEADER_ID.Value = newID;
                command.Parameters.Add(TENDER_ITEM_HEADER_ID);

                SqlParameter START_DATE = new SqlParameter("START_DATE", SqlDbType.Date);
                START_DATE.Value = newTender.START_DATE;
                command.Parameters.Add(START_DATE);

                SqlParameter END_DATE = new SqlParameter("END_DATE", SqlDbType.Date);
                END_DATE.Value = newTender.END_DATE;
                command.Parameters.Add(END_DATE);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 250);
                STATUS.Value = newTender.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter CATEGORY = new SqlParameter("CATEGORY", SqlDbType.NVarChar, 250);
                CATEGORY.Value = newTender.CATEGORY;
                command.Parameters.Add(CATEGORY);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 250);
                CONNECTION_STRING.Value = newTender.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return newID;
        }

        public static String createTenderItem(SqlConnection dbConnection, TenderItemDetails newTender)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_VDR_TENDER_ITEM VALUES (@TABLE_ITEM_TENDER_ID, @TENDER_DESCRIPTION, @CONNECTION_STRING, @QTY, @TENDER_STATUS,@TENDER_HEADER_ID,@WINNER_GROUP_FIRM_ID,@WINNER_SOLO_FIRM_ID)", dbConnection);

                SqlParameter TABLE_ITEM_TENDER_ID = new SqlParameter("TABLE_ITEM_TENDER_ID", SqlDbType.NVarChar, 100);
                TABLE_ITEM_TENDER_ID.Value = newID;
                command.Parameters.Add(TABLE_ITEM_TENDER_ID);

                SqlParameter TENDER_DESCRIPTION = new SqlParameter("TENDER_DESCRIPTION", SqlDbType.NVarChar, 250);
                TENDER_DESCRIPTION.Value = newTender.TENDER_DESCRIPTION;
                command.Parameters.Add(TENDER_DESCRIPTION);

                SqlParameter CONNECTION_STRING = new SqlParameter("CONNECTION_STRING", SqlDbType.NVarChar, 250);
                CONNECTION_STRING.Value = newTender.CONNECTION_STRING;
                command.Parameters.Add(CONNECTION_STRING);

                SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int);
                QTY.Value = newTender.QTY;
                command.Parameters.Add(QTY);

                SqlParameter TENDER_STATUS = new SqlParameter("TENDER_STATUS", SqlDbType.NVarChar, 250);
                TENDER_STATUS.Value = newTender.TENDER_STATUS;
                command.Parameters.Add(TENDER_STATUS);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 250);
                TENDER_HEADER_ID.Value = newTender.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                SqlParameter WINNER_GROUP_FIRM_ID = new SqlParameter("WINNER_GROUP_FIRM_ID", SqlDbType.NVarChar, 250);
                WINNER_GROUP_FIRM_ID.Value = newTender.WINNER_GROUP_FIRM_ID;
                command.Parameters.Add(WINNER_GROUP_FIRM_ID);

                SqlParameter WINNER_SOLO_FIRM_ID = new SqlParameter("WINNER_SOLO_FIRM_ID", SqlDbType.NVarChar, 250);
                WINNER_SOLO_FIRM_ID.Value = newTender.WINNER_SOLO_FIRM_ID;
                command.Parameters.Add(WINNER_SOLO_FIRM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return newID;
        }

        public static void updateTender(SqlConnection dbConnection, TenderItemDetails newTender)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_ITEM SET TENDER_DESCRIPTION=@TENDER_DESCRIPTION, QTY=@QTY WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = newTender.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlParameter TENDER_DESCRIPTION = new SqlParameter("TENDER_DESCRIPTION", SqlDbType.NVarChar, 100);
                TENDER_DESCRIPTION.Value = newTender.TENDER_DESCRIPTION;
                command.Parameters.Add(TENDER_DESCRIPTION);

                SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int);
                QTY.Value = newTender.QTY;
                command.Parameters.Add(QTY);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateTenderStatus(SqlConnection dbConnection, String tenderID, String status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_ITEM VALUES SET STATUS=@STATUS WHERE VDR_ID=@VDR_ID", dbConnection);

                SqlParameter VDR_ID = new SqlParameter("VDR_ID", SqlDbType.NVarChar, 100);
                VDR_ID.Value = Guid.Parse(tenderID);
                command.Parameters.Add(VDR_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 20);
                STATUS.Value = status;
                command.Parameters.Add(STATUS);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateTenderWinner(SqlConnection dbConnection, String tenderID, String venderID, DateTime winnerDate)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_ITEM VALUES SET WINNER_ORG_ID=@WINNER_ORG_ID, WINNER_DATE=@WINNER_DATE WHERE VDR_ID=@VDR_ID", dbConnection);

                SqlParameter VDR_ID = new SqlParameter("VDR_ID", SqlDbType.NVarChar, 100);
                VDR_ID.Value = Guid.Parse(tenderID);
                command.Parameters.Add(VDR_ID);

                SqlParameter WINNER_ORG_ID = new SqlParameter("WINNER_ORG_ID", SqlDbType.NVarChar, 100);
                WINNER_ORG_ID.Value = venderID;
                command.Parameters.Add(WINNER_ORG_ID);

                SqlParameter WINNER_DATE = new SqlParameter("WINNER_DATE", SqlDbType.DateTime);
                WINNER_DATE.Value = winnerDate;
                command.Parameters.Add(WINNER_DATE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void deleteTender(SqlConnection dbConnection, TenderItemDetails tenderID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE TABLE_VDR_TENDER_ITEM WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = tenderID.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void deleteBid(SqlConnection dbConnection, VendorBidsDetails tenderID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE TABLE_VDR_BIDS WHERE VDR_BIDS_ITEM_ID=@VDR_BIDS_ITEM_ID", dbConnection);

                SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar, 100);
                VDR_BIDS_ITEM_ID.Value = tenderID.VDR_BIDS_ITEM_ID;
                command.Parameters.Add(VDR_BIDS_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static OrganisationDetails getTenderOrganizationInfo(SqlConnection dbConnection, String connectionString)
        {
            OrganisationDetails orgDetails = new OrganisationDetails();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_ITEM WHERE CONNECTION_STRING=@CONNECTION_STRING", dbConnection);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    orgDetails.ORG_ID = reader["ORG_ID"].ToString();
                    orgDetails.ORG_NAME = reader["ORG_NAME"].ToString();
                    orgDetails.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    orgDetails.EMAIL = reader["EMAIL"].ToString();
                    orgDetails.LINK = reader["LINK"].ToString();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return orgDetails;
        }


        public static String vendorSignUp(SqlConnection dbConnection, VendorDetails vendorDetails)
        {
            String newVendorID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_VDR_DETAILS VALUES (@VDR_ID, @VDR_NAME, @VDR_EMAIL, @VDR_TEL, @VDR_WEBSITE, @PASSWORD)", dbConnection);

                SqlParameter VDR_ID = new SqlParameter("VDR_ID", SqlDbType.NVarChar, 100);
                VDR_ID.Value = newVendorID;
                command.Parameters.Add(VDR_ID);

                SqlParameter VDR_NAME = new SqlParameter("VDR_NAME", SqlDbType.NVarChar, 100);
                VDR_NAME.Value = vendorDetails.VDR_NAME;
                command.Parameters.Add(VDR_NAME);

                SqlParameter VDR_EMAIL = new SqlParameter("VDR_EMAIL", SqlDbType.DateTime);
                VDR_EMAIL.Value = vendorDetails.VDR_EMAIL;
                command.Parameters.Add(VDR_EMAIL);

                SqlParameter VDR_TEL = new SqlParameter("VDR_TEL", SqlDbType.DateTime);
                VDR_TEL.Value = vendorDetails.VDR_TEL;
                command.Parameters.Add(VDR_TEL);

                SqlParameter VDR_WEBSITE = new SqlParameter("VDR_WEBSITE", SqlDbType.NVarChar, 250);
                VDR_WEBSITE.Value = vendorDetails.VDR_WEBSITE;
                command.Parameters.Add(VDR_WEBSITE);

                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 20);
                PASSWORD.Value = vendorDetails.PASSWORD;
                command.Parameters.Add(PASSWORD);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return newVendorID;
        }


        public static void updateVendorPassword(SqlConnection dbConnection, String tenderID, String password)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_DETAILS VALUES SET PASSWORD=@PASSWORD, PASSWORD_STATUS='' WHERE VDR_ID=@VDR_ID", dbConnection);

                SqlParameter VDR_ID = new SqlParameter("VDR_ID", SqlDbType.NVarChar, 100);
                VDR_ID.Value = Guid.Parse(tenderID);
                command.Parameters.Add(VDR_ID);

                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 20);
                PASSWORD.Value = password;
                command.Parameters.Add(PASSWORD);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void forgotVendorPassword(SqlConnection dbConnection, String tenderID, String password)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_DETAILS VALUES SET PASSWORD=@PASSWORD, PASSWORD_STATUS='FORGOT' WHERE VDR_ID=@VDR_ID", dbConnection);

                SqlParameter VDR_ID = new SqlParameter("VDR_ID", SqlDbType.NVarChar, 100);
                VDR_ID.Value = Guid.Parse(tenderID);
                command.Parameters.Add(VDR_ID);

                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 50);
                PASSWORD.Value = password;
                command.Parameters.Add(PASSWORD);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateVendorParticulars(SqlConnection dbConnection, VendorDetails orgDetails)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_DETAILS VALUES SET VDR_NAME=@VDR_NAME, VDR_EMAIL=@VDR_EMAIL, VDR_TEL=@VDR_TEL, VDR_WEBSITE=@VDR_WEBSITE", dbConnection);

                SqlParameter PRODUCT_ID = new SqlParameter("VDR_NAME", SqlDbType.NVarChar, 50);
                PRODUCT_ID.Value = orgDetails.VDR_NAME;
                command.Parameters.Add(PRODUCT_ID);

                SqlParameter VDR_EMAIL = new SqlParameter("VDR_EMAIL", SqlDbType.NVarChar, 100);
                VDR_EMAIL.Value = orgDetails.VDR_EMAIL;
                command.Parameters.Add(VDR_EMAIL);

                SqlParameter VDR_TEL = new SqlParameter("VDR_TEL", SqlDbType.NVarChar, 50);
                VDR_TEL.Value = orgDetails.VDR_TEL;
                command.Parameters.Add(VDR_TEL);

                SqlParameter VDR_WEBSITE = new SqlParameter("VDR_WEBSITE", SqlDbType.NVarChar, 50);
                VDR_WEBSITE.Value = orgDetails.VDR_WEBSITE;
                command.Parameters.Add(VDR_WEBSITE);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static String insertVendorBid(SqlConnection dbConnection, VendorBidsDetails newBid)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                SqlCommand command = new SqlCommand("INSERT INTO TABLE_VDR_BIDS VALUES (@VDR_BIDS_ITEM_ID, @TENDER_ITEM_ID, @BID_PRICE, @BID_PRICE_SOLO, @PRODUCT_NAME, @PRODUCT_DESC, @PRODUCT_BRAND,@RATING,@STATUS, @FIRM_ID, @BID_DATE, @VALID_TO,@DELIVERY_TIME,@PACKING_TIME)", dbConnection);

                SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar,250);
                VDR_BIDS_ITEM_ID.Value = newID;
                command.Parameters.Add(VDR_BIDS_ITEM_ID);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 250);
                TENDER_ITEM_ID.Value = newBid.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlParameter BID_PRICE = new SqlParameter("BID_PRICE", SqlDbType.NVarChar, 250);
                BID_PRICE.Value = newBid.BID_PRICE;
                command.Parameters.Add(BID_PRICE);

                SqlParameter BID_PRICE_SOLO = new SqlParameter("BID_PRICE_SOLO", SqlDbType.NVarChar, 250);
                BID_PRICE_SOLO.Value = newBid.BID_PRICE_SOLO;
                command.Parameters.Add(BID_PRICE_SOLO);

                SqlParameter PRODUCT_NAME = new SqlParameter("PRODUCT_NAME", SqlDbType.NVarChar,250);
                PRODUCT_NAME.Value = newBid.PRODUCT_NAME;
                command.Parameters.Add(PRODUCT_NAME);
                
                SqlParameter PRODUCT_DESC = new SqlParameter("PRODUCT_DESC", SqlDbType.NVarChar, 250);
                PRODUCT_DESC.Value = newBid.PRODUCT_DESC;
                command.Parameters.Add(PRODUCT_DESC);

                SqlParameter PRODUCT_BRAND = new SqlParameter("PRODUCT_BRAND", SqlDbType.NVarChar, 250);
                PRODUCT_BRAND.Value = newBid.PRODUCT_BRAND;
                command.Parameters.Add(PRODUCT_BRAND);

                SqlParameter RATING = new SqlParameter("RATING", SqlDbType.NVarChar, 250);
                RATING.Value = newBid.RATING;
                command.Parameters.Add(RATING);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 250);
                STATUS.Value = newBid.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 250);
                FIRM_ID.Value = newBid.FIRM_ID;
                command.Parameters.Add(FIRM_ID);

                DateTime regDatetimeTemp = DateTime.Now;
                string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy HH:mm tt");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy HH:mm tt", null);

                SqlParameter BID_DATE = new SqlParameter("BID_DATE", SqlDbType.DateTime);
                BID_DATE.Value = regDate;
                command.Parameters.Add(BID_DATE);

                SqlParameter VALID_TO = new SqlParameter("VALID_TO", SqlDbType.DateTime);
                VALID_TO.Value = newBid.VALID_TO;
                command.Parameters.Add(VALID_TO);

                SqlParameter DELIVERY_TIME = new SqlParameter("DELIVERY_TIME", SqlDbType.DateTime);
                DELIVERY_TIME.Value = newBid.DELIVERY_TIME;
                command.Parameters.Add(DELIVERY_TIME);

                SqlParameter PACKING_TIME = new SqlParameter("PACKING_TIME", SqlDbType.DateTime);
                PACKING_TIME.Value = newBid.PACKING_TIME;
                command.Parameters.Add(PACKING_TIME);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return newID;
        }

        public static void updateVendorBid(SqlConnection dbConnection, VendorBidsDetails bidsDetails)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_BIDS SET BID_PRICE=@BID_PRICE, BID_PRICE_SOLO=@BID_PRICE_SOLO, PRODUCT_NAME=@PRODUCT_NAME, PRODUCT_DESC=@PRODUCT_DESC, PRODUCT_BRAND=@PRODUCT_BRAND, BID_DATE=@BID_DATE, VALID_TO=@VALID_TO WHERE VDR_BIDS_ITEM_ID=@VDR_BIDS_ITEM_ID", dbConnection);

                SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar, 250);
                VDR_BIDS_ITEM_ID.Value = bidsDetails.VDR_BIDS_ITEM_ID;
                command.Parameters.Add(VDR_BIDS_ITEM_ID);

                SqlParameter BID_PRICE = new SqlParameter("BID_PRICE", SqlDbType.NVarChar, 250);
                BID_PRICE.Value = bidsDetails.BID_PRICE;
                command.Parameters.Add(BID_PRICE);

                SqlParameter BID_PRICE_SOLO = new SqlParameter("BID_PRICE_SOLO", SqlDbType.NVarChar, 250);
                BID_PRICE_SOLO.Value = bidsDetails.BID_PRICE_SOLO;
                command.Parameters.Add(BID_PRICE_SOLO);

                SqlParameter PRODUCT_NAME = new SqlParameter("PRODUCT_NAME", SqlDbType.NVarChar, 250);
                PRODUCT_NAME.Value = bidsDetails.PRODUCT_NAME;
                command.Parameters.Add(PRODUCT_NAME);

                SqlParameter PRODUCT_DESC = new SqlParameter("PRODUCT_DESC", SqlDbType.NVarChar, 250);
                PRODUCT_DESC.Value = bidsDetails.PRODUCT_DESC;
                command.Parameters.Add(PRODUCT_DESC);

                SqlParameter PRODUCT_BRAND = new SqlParameter("PRODUCT_BRAND", SqlDbType.NVarChar, 250);
                PRODUCT_BRAND.Value = bidsDetails.PRODUCT_BRAND;
                command.Parameters.Add(PRODUCT_BRAND);

                DateTime regDatetimeTemp = DateTime.Now;
                string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy HH:mm tt");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy HH:mm tt", null);

                SqlParameter BID_DATE = new SqlParameter("BID_DATE", SqlDbType.DateTime);
                BID_DATE.Value = regDate;
                command.Parameters.Add(BID_DATE);

                SqlParameter VALID_TO = new SqlParameter("VALID_TO", SqlDbType.DateTime);
                VALID_TO.Value = bidsDetails.VALID_TO;
                command.Parameters.Add(VALID_TO);


                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static String updateTenderStatus(SqlConnection dbConnection, TenderItemDetails item)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER SET STATUS=@STATUS WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

               // SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 250);
                //STATUS.Value = item.STATUS;
               // command.Parameters.Add(STATUS);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 250);
                //TENDER_ITEM_ID.Value = item.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return "Success";
        }

        public static String updateTenderStatusStatus(SqlConnection dbConnection)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER SET STATUS=@STATUS", dbConnection);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 250);
                STATUS.Value = "Closed";
                command.Parameters.Add(STATUS);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return "Success";
        }

        public static String updateBidStatus(SqlConnection dbConnection, VendorBidsDetails item)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_BIDS SET STATUS=@STATUS WHERE VDR_BIDS_ITEM_ID=@VDR_BIDS_ITEM_ID", dbConnection);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 250);
                STATUS.Value = item.STATUS;
                command.Parameters.Add(STATUS);

              SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar, 250);
                VDR_BIDS_ITEM_ID.Value = item.VDR_BIDS_ITEM_ID;
              command.Parameters.Add(VDR_BIDS_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return "Success";
        }

        public static String updateBidStatusForPartial(SqlConnection dbConnection, String id)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE B SET B.STATUS='Submitted' FROM TABLE_VDR_BIDS B INNER JOIN TABLE_VDR_TENDER_ITEM I ON I.TENDER_ITEM_ID = B.TENDER_ITEM_ID WHERE I.TENDER_HEADER_ID=@TENDER_HEADER_ID", dbConnection);

                

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 250);
                TENDER_HEADER_ID.Value = id;
                command.Parameters.Add(TENDER_HEADER_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return "Success";
        }

        public static String updateBidStatusBecomePartial(SqlConnection dbConnection, String id)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE B SET B.STATUS='Partial' FROM TABLE_VDR_BIDS B INNER JOIN TABLE_VDR_TENDER_ITEM I ON I.TENDER_ITEM_ID = B.TENDER_ITEM_ID WHERE I.TENDER_HEADER_ID=@TENDER_HEADER_ID", dbConnection);



                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 250);
                TENDER_HEADER_ID.Value = id;
                command.Parameters.Add(TENDER_HEADER_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return "Success";
        }

        public static void updateVendorBidStatus(SqlConnection dbConnection, VendorBidsDetails status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_BIDS SET STATUS=@STATUS WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID AND FIRM_ID=@FIRM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = status.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = status.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 100);
                FIRM_ID.Value = status.FIRM_ID;
                command.Parameters.Add(FIRM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }
        
        public static void updateVendorBidPacked(SqlConnection dbConnection, VendorBidsDetails status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_BIDS SET STATUS=@STATUS,PACKING_TIME=@PACKING_TIME WHERE VDR_BIDS_ITEM_ID=@VDR_BIDS_ITEM_ID", dbConnection);

                SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar, 100);
                VDR_BIDS_ITEM_ID.Value = status.VDR_BIDS_ITEM_ID;
                command.Parameters.Add(VDR_BIDS_ITEM_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = status.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter PACKING_TIME = new SqlParameter("PACKING_TIME", SqlDbType.Date);
                PACKING_TIME.Value = DateTime.Now;
                command.Parameters.Add(PACKING_TIME);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateVendorBidDelivered(SqlConnection dbConnection, VendorBidsDetails status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_BIDS SET STATUS=@STATUS,DELIVERY_TIME=@DELIVERY_TIME WHERE VDR_BIDS_ITEM_ID=@VDR_BIDS_ITEM_ID", dbConnection);

                SqlParameter VDR_BIDS_ITEM_ID = new SqlParameter("VDR_BIDS_ITEM_ID", SqlDbType.NVarChar, 100);
                VDR_BIDS_ITEM_ID.Value = status.VDR_BIDS_ITEM_ID;
                command.Parameters.Add(VDR_BIDS_ITEM_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = status.STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter DELIVERY_TIME = new SqlParameter("DELIVERY_TIME", SqlDbType.Date);
                DELIVERY_TIME.Value = status.DELIVERY_TIME;
                command.Parameters.Add(DELIVERY_TIME);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateTenderItemStatus(SqlConnection dbConnection, TenderItemDetails status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_ITEM SET WINNER_GROUP_FIRM_ID=@WINNER_GROUP_FIRM_ID, WINNER_SOLO_FIRM_ID=@WINNER_SOLO_FIRM_ID, STATUS=@STATUS WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = status.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = status.TENDER_STATUS;
                command.Parameters.Add(STATUS);

                SqlParameter WINNER_GROUP_FIRM_ID = new SqlParameter("WINNER_GROUP_FIRM_ID", SqlDbType.NVarChar, 100);
                WINNER_GROUP_FIRM_ID.Value = status.WINNER_GROUP_FIRM_ID;
                command.Parameters.Add(WINNER_GROUP_FIRM_ID);

                SqlParameter WINNER_SOLO_FIRM_ID = new SqlParameter("WINNER_SOLO_FIRM_ID", SqlDbType.NVarChar, 100);
                WINNER_SOLO_FIRM_ID.Value = status.WINNER_SOLO_FIRM_ID;
                command.Parameters.Add(WINNER_SOLO_FIRM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static void updateTenderHeadStatus(SqlConnection dbConnection, TenderHeader status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_HEADER SET STATUS=@STATUS WHERE TENDER_HEADER_ID=@TENDER_HEADER_ID", dbConnection);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_HEADER_ID.Value = status.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = status.STATUS;
                command.Parameters.Add(STATUS);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }
        public static void updateTenderItemStatusAll(SqlConnection dbConnection, TenderHeader status)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_ITEM SET STATUS=@STATUS WHERE TENDER_HEADER_ID=@TENDER_HEADER_ID", dbConnection);

                SqlParameter TENDER_HEADER_ID = new SqlParameter("TENDER_HEADER_ID", SqlDbType.NVarChar, 100);
                TENDER_HEADER_ID.Value = status.TENDER_HEADER_ID;
                command.Parameters.Add(TENDER_HEADER_ID);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = "Closed";
                command.Parameters.Add(STATUS);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }

        public static List<VendorBidsDetails> getSubmittedBidsList(SqlConnection dbConnection, TenderItemDetails vendorID)
        {
            List<VendorBidsDetails> listOfBids = new List<VendorBidsDetails>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_BIDS B INNER JOIN TABLE_FIRM_DETAILS F ON B.FIRM_ID = F.USR_ID WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = vendorID.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VendorBidsDetails bidItem = new VendorBidsDetails();

                    bidItem.VDR_BIDS_ITEM_ID = reader["VDR_BIDS_ITEM_ID"].ToString();
                    bidItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    bidItem.BID_PRICE = Double.Parse(reader["BID_PRICE"].ToString());
                    bidItem.BID_PRICE_SOLO = Double.Parse(reader["BID_PRICE_SOLO"].ToString());
                    bidItem.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString();
                    bidItem.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();
                    bidItem.PRODUCT_BRAND = reader["PRODUCT_BRAND"].ToString();
                    bidItem.BID_DATE = Convert.ToDateTime(reader["BID_DATE"]);
                    bidItem.STATUS = reader["STATUS"].ToString();
                    bidItem.FIRM_NAME = reader["FIRM_NAME"].ToString();
                    bidItem.FIRM_ID = reader["USR_ID"].ToString();
                    bidItem.VALID_TO = Convert.ToDateTime(reader["VALID_TO"].ToString());
                    bidItem.DELIVERY_TIME = Convert.ToDateTime(reader["DELIVERY_TIME"].ToString());
                    bidItem.PACKING_TIME = Convert.ToDateTime(reader["PACKING_TIME"].ToString());
                    bidItem.ADDRESS = reader["ADDRESS"].ToString();
                    bidItem.PHONE = reader["PHONE"].ToString();
                    bidItem.FAX = reader["FAX"].ToString();
                    bidItem.FIRM_DESCRIPTION = reader["FIRM_DESCRIPTION"].ToString();

                    listOfBids.Add(bidItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return listOfBids;
        }


        public static VendorBidsDetails getSubmittedBid(SqlConnection dbConnection, VendorBidsDetails tenderItemID)
        {
            VendorBidsDetails bidItem = new VendorBidsDetails();
            
            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_BIDS WHERE TENDER_ITEM_ID=@TENDER_ITEM_ID AND FIRM_ID=@FIRM_ID", dbConnection);

                SqlParameter TENDER_ITEM_ID = new SqlParameter("TENDER_ITEM_ID", SqlDbType.NVarChar, 100);
                TENDER_ITEM_ID.Value = tenderItemID.TENDER_ITEM_ID;
                command.Parameters.Add(TENDER_ITEM_ID);

                SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 100);
                FIRM_ID.Value = tenderItemID.FIRM_ID;
                command.Parameters.Add(FIRM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    bidItem.VDR_BIDS_ITEM_ID = reader["VDR_BIDS_ITEM_ID"].ToString();
                    bidItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    bidItem.BID_PRICE = Double.Parse(reader["BID_PRICE"].ToString());
                    bidItem.BID_PRICE_SOLO = Double.Parse(reader["BID_PRICE_SOLO"].ToString());
                    bidItem.PRODUCT_NAME = reader["PRODUCT_NAME"].ToString();
                    bidItem.PRODUCT_DESC = reader["PRODUCT_DESC"].ToString();
                    bidItem.PRODUCT_BRAND = reader["PRODUCT_BRAND"].ToString();
                    bidItem.RATING = Int32.Parse(reader["RATING"].ToString());
                    bidItem.STATUS = reader["STATUS"].ToString();
                    bidItem.FIRM_ID = reader["FIRM_ID"].ToString();
                    bidItem.BID_DATE = DateTime.Parse(reader["BID_DATE"].ToString());
                    bidItem.VALID_TO = DateTime.Parse(reader["VALID_TO"].ToString());
                    bidItem.DELIVERY_TIME = DateTime.Parse(reader["DELIVERY_TIME"].ToString());
                    bidItem.PACKING_TIME = DateTime.Parse(reader["PACKING_TIME"].ToString());
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return bidItem;
        }

        public static BidCombineView getRating(SqlConnection dbConnection, BidCombineView supplier)
        {
            BidCombineView result = new BidCombineView();

            try
            {
                SqlCommand command = new SqlCommand("SELECT AVG(RATING) AS RATING FROM TABLE_VDR_BIDS WHERE TABLE_VDR_BIDS.STATUS = 'Goods Accepted' AND FIRM_ID = @FIRM_ID GROUP BY FIRM_ID", dbConnection);

                SqlParameter FIRM_ID = new SqlParameter("FIRM_ID", SqlDbType.NVarChar, 100);
                FIRM_ID.Value = supplier.VENDOR_ID;
                command.Parameters.Add(FIRM_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.RATING = Double.Parse(reader["RATING"].ToString());                  
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return result;
        }
 
        public static List<TenderItemDetails> getBiddedTenderList(SqlConnection dbConnection, VendorDetails orgDetails)
        {
            List<TenderItemDetails> listOfTender = new List<TenderItemDetails>();

            try
            {
                SqlCommand command = new SqlCommand("Select * from TABLE_VDR_TENDER INNER JOIN TABLE_VDR_BIDS ON TABLE_VDR_TENDER.TENDER_ITEM_ID = TABLE_VDR_BIDS.TENDER_ID WHERE TABLE_VDR_BIDS.VENDOR_ID=@VENDOR_ID", dbConnection);

                SqlParameter VENDOR_ID = new SqlParameter("VENDOR_ID", SqlDbType.NVarChar, 100);
                VENDOR_ID.Value = orgDetails.VDR_ID;
                command.Parameters.Add(VENDOR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TenderItemDetails tenderItem = new TenderItemDetails();

                    //tenderItem.TENDER_ITEM_ID = reader["TENDER_ITEM_ID"].ToString();
                    //tenderItem.TENDER_TITLE = reader["TENDER_TITLE"].ToString();
                    //tenderItem.START_DATE = Convert.ToDateTime(reader["START_DATE"]);
                    //tenderItem.END_DATE = Convert.ToDateTime(reader["END_DATE"]);
                    //tenderItem.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    //tenderItem.STATUS = reader["STATUS"].ToString();
                    //tenderItem.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    //tenderItem.QTY = Int32.Parse(reader["QTY"].ToString());
                    ////tenderItem.WINNER_ORG_ID = reader["WINNER_ORG_ID"].ToString();
                    //tenderItem.CATEGORY = reader["CATEGORY"].ToString();
                    ////tenderItem.WINNER_DATE = Convert.ToDateTime(reader["WINNER_DATE"]);

                    listOfTender.Add(tenderItem);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
            return listOfTender;
        }

        public static void autoUpdateTenderStatusStart(SqlConnection dbConnection)
        {
            List<TenderItemDetails> listOfTender = new List<TenderItemDetails>();
       
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_HEADER SET STATUS = 'Ongoing' WHERE STATUS = 'Not Started' AND START_DATE <= @TODAY", dbConnection);

                DateTime regDatetimeTemp = DateTime.Now;
                string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy", null);

                SqlParameter TODAY = new SqlParameter("TODAY", SqlDbType.Date);
                TODAY.Value = regDate;
                command.Parameters.Add(TODAY);

                SqlDataReader reader = command.ExecuteReader();             
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }
        public static void autoUpdateTenderStatusEnd(SqlConnection dbConnection)
        {
            List<TenderItemDetails> listOfTender = new List<TenderItemDetails>();

            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_VDR_TENDER_HEADER SET STATUS = 'Closed' WHERE STATUS = 'Ongoing' AND END_DATE <= @TODAY", dbConnection);

                //hashed pw with salt
                DateTime regDatetimeTemp = DateTime.Now;
                string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy", null);

                SqlParameter TODAY = new SqlParameter("TODAY", SqlDbType.Date);
                TODAY.Value = regDate;
                command.Parameters.Add(TODAY);

                SqlDataReader reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
            }
        }
    }
}