﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.EventModels;
using System.Data;

namespace VMS.DataLayer.EventMgmt
{
    public class FeedBackMgmtDL
    {
        /*
         * This method creates a feedback and insert it into the database 
         */
        public static void createFeedBackDesign(SqlConnection dataConnection, List<FeedBackDesign> listOfFeedBackDetail)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_FDBACK_DETAILS VALUES ";

            int count = 1;
            foreach(FeedBackDesign feedBack in listOfFeedBackDetail)
            {
                command.CommandText += "(NEWID(), @sessionId"+count+", @type"+count+", @question"+count+", @number"+count+")";

                SqlParameter sessionIdParam = new SqlParameter("sessionId" + count, SqlDbType.UniqueIdentifier);
                sessionIdParam.Value = new Guid(feedBack.sessionId);
                command.Parameters.Add(sessionIdParam);

                SqlParameter typeParam = new SqlParameter("type" + count, SqlDbType.NVarChar, 50);
                typeParam.Value = feedBack.type;
                command.Parameters.Add(typeParam);

                SqlParameter questionParam = new SqlParameter("question" + count, SqlDbType.NVarChar, 50);
                questionParam.Value = feedBack.question;
                command.Parameters.Add(questionParam);

               

                SqlParameter numberParam = new SqlParameter("number" + count, SqlDbType.Int);
                numberParam.Value = count;
                command.Parameters.Add(numberParam);
                
                if(count != listOfFeedBackDetail.Count)
                {
                    command.CommandText += ", ";
                }
                count++;          
            }

            command.ExecuteNonQuery();
        }

        /*
         * This method retireve the feedback form design that was created by the event orgniaser previously 
         */
        public static List<FeedBackDesign> getFeedBackDesign(SqlConnection dataConnection, String sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_FDBACK_DETAILS WHERE EVT_SESSION_ID = @sessionId ORDER BY NUMBER ASC";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionId);
            command.Parameters.Add(sessionIdParam);


            SqlDataReader reader = command.ExecuteReader();
            List<FeedBackDesign> listOfFeedBackDesign = new List<FeedBackDesign>();
            while(reader.Read())
            {
                FeedBackDesign feedbackDesign = new FeedBackDesign();
                feedbackDesign.question = reader["QUESTION"] + "";
                feedbackDesign.number = Convert.ToInt16(reader["NUMBER"]);
                feedbackDesign.type = Convert.ToInt16(reader["TYPE"]);
                feedbackDesign.fdback_detail_id = reader["FDBACK_DETAIL_ID"].ToString();
                listOfFeedBackDesign.Add(feedbackDesign);
            }

            reader.Close();
            return listOfFeedBackDesign;
        }

        /*
         * This method deletes a feedback design in the database.  
         */
        public static void deleteFeedBackDesign(SqlConnection dataConnection, String sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_FDBACK_DETAILS WHERE EVT_SESSION_ID = @sessionId";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionId);
            command.Parameters.Add(sessionIdParam);

            command.ExecuteNonQuery();

        }

        /*
         * This method retrieves a list of feedback id from an event session 
         */
        public static List<String> getFeedBackId(SqlConnection dataConnection, string sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT FDBACK_DETAIL_ID  FROM TABLE_FDBACK_DETAILS WHERE EVT_SESSION_ID = @sessionId ORDER BY NUMBER ASC";
            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionId);
            command.Parameters.Add(sessionIdParam);


            SqlDataReader reader = command.ExecuteReader();
            List<String> listOfFeedBackId = new List<string>();

            while(reader.Read())
            {
                listOfFeedBackId.Add(reader["FDBACK_DETAIL_ID"] + "");
            }
            reader.Close();
            return listOfFeedBackId;

        }

        /*
         * This method inserts the feedback responses into the datbase made by any particular volunteer 
         */
        public static void createFeedbackResponse(SqlConnection dataConnection, FeedBackResponse response, List<String> listOfFeedBackId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_FDBACK VALUES ";

            for(int count =0; count< response.listOfAnswers.Count; count++)
            {
                command.CommandText += "(NEWID(), @usrId"+count+", @fdBackId"+count+", @answer"+count+")";

                SqlParameter userIdParam = new SqlParameter("usrId" + count, SqlDbType.UniqueIdentifier);
                userIdParam.Value = new Guid(response.userId);
                command.Parameters.Add(userIdParam);

                SqlParameter feedbackIdParam = new SqlParameter("fdBackId" + count, SqlDbType.UniqueIdentifier);
                feedbackIdParam.Value = new Guid(listOfFeedBackId[count]);
                command.Parameters.Add(feedbackIdParam);

                SqlParameter answerParam = new SqlParameter("answer" + count, SqlDbType.NVarChar, 150);
                answerParam.Value = response.listOfAnswers[count];
                command.Parameters.Add(answerParam);

                if (response.listOfAnswers.Count - count != 1)
                    command.CommandText += ", ";
            }

            command.ExecuteNonQuery();       
        }

        /*
         * This method retrieves event session with ends dates that are one day before the current date
         *  
         */
        public static List<String> getEventSessionReadyForFeedback(SqlConnection dataConnection, DateTime currentDate)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.EVT_SESSION_ID FROM TABLE_EVT_SESSION t1, TABLE_EVT_SESSION_DAY t2
                                    WHERE t1.EVT_SESSION_ID = t2.EVT_SESSION_ID
                                    AND DATEDIFF(day, t2.EVT_DATE, @currentDate) = 1
                                    AND t2.EVT_DATE in
                                    (
	                                    SELECT TOP 1 t3.EVT_DATE FROM TABLE_EVT_SESSION_DAY t3
	                                    WHERE t1.EVT_SESSION_ID = t3.EVT_SESSION_ID
	                                    ORDER BY t3.EVT_DATE ASC
                                    )";

            SqlParameter dateParam = new SqlParameter("currentDate", SqlDbType.Date);
            dateParam.Value = currentDate;
            command.Parameters.Add(dateParam);

            SqlDataReader reader = command.ExecuteReader();
            List<String> listOfSessionId = new List<string>();
            while(reader.Read())
            {
                listOfSessionId.Add(reader["EVT_SESSION_ID"] + "");
            }
            reader.Close();
            return listOfSessionId;
        }

        /*
         * This method gets the volunteer email 
         */
        public static String getVolEmail(SqlConnection dataConnection, String volId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT EMAIL FROM TABLE_USR_DETAILS WHERE USR_ID  = @usrId ";

            SqlParameter userIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(volId);
            command.Parameters.Add(userIdParam);

            SqlDataReader reader = command.ExecuteReader();
            String email = "";
            while (reader.Read())
            {
                email = reader["EMAIL"] + "";
            }

            reader.Close();
            return email; 
        }


        //shihui STARTTTTTTTTTTTTT
        public static SessionDetails getSessionDetails(SqlConnection dataConnection, string sessionid)
        {
            SessionDetails session = new SessionDetails();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_EVT_SESSION WHERE EVT_SESSION_ID=@EVT_SESSION_ID", dataConnection);
                SqlParameter EVT_SESSION_ID = new SqlParameter("EVT_SESSION_ID", SqlDbType.NVarChar, 100);
                EVT_SESSION_ID.Value = sessionid;
                command.Parameters.Add(EVT_SESSION_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    session.compulsoryDays = Convert.ToInt32(reader["COMPULSORY"]);
                    session.sessionName = reader["SESSION_NAME"].ToString();

                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return session;
        }

        public static List<RatingCount> getRatingCount(SqlConnection dataConnection, string fdback_id)
        {
            List<RatingCount> ratingCountList = new List<RatingCount>();
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT COUNT(*) AS COUNTER,ANSWER FROM TABLE_USR_FDBACK WHERE FDBACK_DETAIL_ID=@FDBACK_DETAIL_ID GROUP BY ANSWER";
            SqlParameter sessionIdParam = new SqlParameter("FDBACK_DETAIL_ID", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(fdback_id);
            command.Parameters.Add(sessionIdParam);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                RatingCount count = new RatingCount();
                count.RATING = Convert.ToInt32(reader["ANSWER"]);
                count.COUNT = Convert.ToInt32(reader["COUNTER"]);
                ratingCountList.Add(count);
            }
            reader.Close();
            return ratingCountList;

        }

        public static string getTextResp(SqlConnection dataConnection, string fdback_id)
        {
            string textResp="";
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT* FROM TABLE_USR_FDBACK WHERE FDBACK_DETAIL_ID=@FDBACK_DETAIL_ID";
            SqlParameter sessionIdParam = new SqlParameter("FDBACK_DETAIL_ID", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(fdback_id);
            command.Parameters.Add(sessionIdParam);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                textResp = textResp + reader["ANSWER"].ToString()+"<br/>";
            }

            reader.Close();
            return textResp;

        }
    }
}