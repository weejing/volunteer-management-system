﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.EventMgmt
{
    public class EventCauseDL
    {
        //shihui's file
        public static List<int> getRatingCount(SqlConnection dataConnection, string evt_id)
        {
            List<int> termList = new List<int>();
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_EVT_CAUSE WHERE EVT_ID=@EVT_ID";
            SqlParameter sessionIdParam = new SqlParameter("EVT_ID", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(evt_id);
            command.Parameters.Add(sessionIdParam);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                termList.Add(Convert.ToInt32(reader["CAUSE_ID"]));
            }
            reader.Close();
            return termList;
        }

        public static List<Term> getCauseName(SqlConnection dataConnection, List<int> causeList)
        {
            List<Term> termList = new List<Term>();
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_TERM";

            int count = 1;
            foreach (int cause_id in causeList)
            {
                if (count == 1)
                    command.CommandText += " WHERE TERM_ID=@TERM_ID" + count;
                else
                    command.CommandText += " OR TERM_ID=@TERM_ID" + count;

                SqlParameter userIdParam = new SqlParameter("TERM_ID" + count, SqlDbType.Int);
                userIdParam.Value = cause_id;
                command.Parameters.Add(userIdParam);

                count++;
            }

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Term term = new Models.AdminModels.Term();
                term.TERM_NAME = reader["TERM_NAME"].ToString();
                term.TERM_ID = Convert.ToInt32(reader["TERM_ID"]);
                termList.Add(term);
            }
            reader.Close();
            return termList;
        }
    }
}