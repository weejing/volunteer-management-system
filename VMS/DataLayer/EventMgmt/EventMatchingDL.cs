﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.Models.EventModels;

namespace VMS.EventMgmt
{
    public class EventMatchingDL
    {
        /*
         * This method retireves the organisation id in which the event is tagged to 
         */
        public static String getOrgId(SqlConnection dataConnection, String connectionString)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT ORG_ID FROM TABLE_ORG_DETAILS WHERE CONNECTION_STRING = @connectionString";

            SqlParameter connectionParam = new SqlParameter("@connectionString", SqlDbType.NVarChar,250 );
            connectionParam.Value = connectionString;
            command.Parameters.Add(connectionParam);

            SqlDataReader reader = command.ExecuteReader();
            String orgId ="";
            while(reader.Read())
            {
                orgId = reader["ORG_ID"].ToString();
            }

            reader.Close();
            return orgId;
        }

         /*
         *  This method returns a list of volunteers that 
         * 1) follow the organisation in which the event is tagged to  
         * 2) Volunteers would have their personality traits skill sets, 
         *    role interst and benficiary interest tagged to them
         */
        public static List<ShortListedVol> retrieveShortListedVolunteers(SqlConnection databaseConnection, string orgId)
        {
            SqlCommand command = new SqlCommand(null, databaseConnection);

            /*
            // statement for sql insert into temp table. table is to make the sql statement more efficient 
            command.CommandText = "SELECT USR_ID into temptable " +
                                    "from TABLE_USR_ORG " +                                    
                                    "WHERE ORG_ID = @orgId;  ";

            SqlParameter orgIdParam = new SqlParameter("orgId", SqlDbType.UniqueIdentifier);
            orgIdParam.Value = new Guid(orgId);
            command.Parameters.Add(orgIdParam);

              AND t1.USR_ID IN
                                    (SELECT * FROM temptable)
            
            */
            // statement for retrieving number of volunteers 
            command.CommandText = @"SELECT t1.USR_ID, t1.EMAIL, t1.PERSONALITY_CHAR, t2.TERM_ID, t2.VOCAB_ID 
                                    FROM TABLE_USR_DETAILS t1, TABLE_TERM_RELATION t2 
                                    WHERE t1.USR_ID = t2.ENTITY_ID                                  
                                    ORDER BY t1.USR_ID ASC, t2.VOCAB_ID ASC, t2.TERM_ID ASC ; ";

                    
            SqlDataReader reader = command.ExecuteReader();
            List<ShortListedVol> listOfShortlisted = new List<ShortListedVol>();
            int volCount = 0;
            while (reader.Read())
            {
                String volId = reader["USR_ID"].ToString();
                if(listOfShortlisted.Count ==0 || listOfShortlisted[listOfShortlisted.Count -1].VOL_ID.Equals(volId) == false)
                {
                    ShortListedVol volunteer = new ShortListedVol();
                    volunteer.VOL_ID = volId;
                    volunteer.email = reader["EMAIL"] + "";
                    volunteer.traits = reader["PERSONALITY_CHAR"].ToString();
                    volunteer.skillSets = new List<int>();
                    volunteer.listOfEventInterest = new List<int>();
                    volunteer.listOfEventRoleInterest = new List<int>();
                    listOfShortlisted.Add(volunteer);
                    volCount++;
                }

                int vocab_id = Convert.ToInt16(reader["VOCAB_ID"]);

                if (vocab_id == 1)
                {
                    listOfShortlisted[volCount - 1].skillSets.Add(Convert.ToInt16(reader["TERM_ID"]));
                    continue;
                }

                if(vocab_id == 2)
                {
                    listOfShortlisted[volCount - 1].listOfEventInterest.Add(Convert.ToInt16(reader["TERM_ID"]));
                    continue;
                }

                if (vocab_id == 3)
                    listOfShortlisted[volCount - 1].listOfEventRoleInterest.Add(Convert.ToInt16(reader["TERM_ID"]));
            }
            reader.Close();
            return listOfShortlisted;
        }

        /*
         * This method retrieve all the dates in which a volunteer is currently tagged to  
         */
        public static void retrieveVolunteerSchedule(SqlConnection dataConnection, ShortListedVol volunteer)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.DATE as DATE from TABLE_USR_SCHED t1, TABLE_USR_EVENT_SESSION t2, TABLE_USR_EVENT t3 
                                  WHERE t1.SCHED_ID = t2.SCHED_ID
                                  AND t2.USR_EVT_ID = t3.USR_EVT_ID
                                  AND t3.USR_ID = @userId
                                  ORDER BY t1.DATE;";

            SqlParameter userParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userParam.Value = new Guid(volunteer.VOL_ID);
            command.Parameters.Add(userParam);

            SqlDataReader reader = command.ExecuteReader();
            volunteer.listOfDates = new List<DateTime>();

            while (reader.Read())
            {
                DateTime date = DateTime.Parse(reader["DATE"] +"");
                volunteer.listOfDates.Add(date);
            }
            reader.Close();
        }


        /*
         * This method inserts an new event invitation into a volunteer event invitation list
        */
        public static void createEventInvitation(SqlConnection dataConnection, ShortListedVol volunteer, String connectionString)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT into TABLE_USR_EVENT_INVIT OUTPUT INSERTED.INVIT_ID VALUES (NEWID(),@eventId, @userId, @connectionString);";

           
            SqlParameter eventParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventParam.Value = new Guid(volunteer.eventId);
            command.Parameters.Add(eventParam);

            SqlParameter userParma = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userParma.Value = new Guid(volunteer.VOL_ID);
            command.Parameters.Add(userParma);

            SqlParameter connectionParam = new SqlParameter("@connectionString", SqlDbType.NVarChar, 250);
            connectionParam.Value = connectionString;
            command.Parameters.Add(connectionParam);


            SqlDataReader reader = command.ExecuteReader();
            String invitId = "";
            if (reader.Read() == true)            
                invitId = reader[0].ToString().ToUpper();
            
            reader.Close();

            foreach (AcceptedSession acceptedSession in volunteer.listOfAcceptedSession)
            {
                createSessionInvite(dataConnection, acceptedSession, invitId);
            }
        }

        /*
         * This method inserts a session invitation that is tag to a specific volunteer and event
         * The reason that the roles and dates can be stored in a string is because the event database resides in  a different database schema 
         * Such a design is done to save database usage as this string will not be compared with any table within the console_VMS
         */
        private static void createSessionInvite(SqlConnection dataConnection, AcceptedSession acceptedSession, String inviteId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_SESSION_INVIT VALUES "+
                                    "(@invitId, @sessionid, @roles, @dates, @percent)";

            SqlParameter invitParam = new SqlParameter("invitId", SqlDbType.UniqueIdentifier);
            invitParam.Value = new Guid(inviteId);
            command.Parameters.Add(invitParam);

            SqlParameter sessionParam = new SqlParameter("sessionid", SqlDbType.UniqueIdentifier);
            sessionParam.Value = new Guid(acceptedSession.sessionId);
            command.Parameters.Add(sessionParam);

            SqlParameter roleParam = new SqlParameter("roles", SqlDbType.NVarChar, 150);
            roleParam.Value = acceptedSession.sessionRoles;
            command.Parameters.Add(roleParam);

            SqlParameter datesParam = new SqlParameter("dates", SqlDbType.NVarChar, 150);
            datesParam.Value = acceptedSession.sessionDates;
            command.Parameters.Add(datesParam);

            SqlParameter percentParam = new SqlParameter("percent", SqlDbType.NVarChar, 150);
            percentParam.Value = acceptedSession.percent;
            command.Parameters.Add(percentParam);

            command.ExecuteNonQuery();
        }

        /*
         * This method creates an event notification  
         */
        public static void createNotificationInvite(SqlConnection dataConnection, String eventName, String eventId, String volId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"INSERT INTO TABLE_USR_NOTIF VALUES
                                    (NEWID(), @notification, @volId, 0, @timeStamp)";

            SqlParameter notificationParam = new SqlParameter("notification", SqlDbType.NVarChar, 250);
            notificationParam.Value = "We would like to invite you to <a href='EventInvite.aspx?eventId="+eventId + "'>"+eventName+"</a>";
            command.Parameters.Add(notificationParam);

            SqlParameter volIdParam = new SqlParameter("volId", SqlDbType.UniqueIdentifier);
            volIdParam.Value = new Guid(volId);
            command.Parameters.Add(volIdParam);

            DateTime currentDate = DateTime.Now;
            SqlParameter timeStampParam = new SqlParameter("timeStamp", SqlDbType.DateTime);
            timeStampParam.Value = currentDate;
            command.Parameters.Add(timeStampParam);


            command.ExecuteNonQuery();
        }

        /*
         * This method deletes an event inviitation in the events where a volunteer rejects it 
         */
        private static void deleteEventInvitiation(SqlConnection dataConnection, String eventId, String volId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"DELETE FROM TABLE_USR_EVENT_INVIT 
                                    WHERE EVT_ID = @eventId
                                    AND USR_ID = @volId";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventId);
            command.Parameters.Add(eventIdParam);

            SqlParameter volIdParam = new SqlParameter("volId", SqlDbType.UniqueIdentifier);
            volIdParam.Value = new Guid(volId);
            command.Parameters.Add(volIdParam);

            command.ExecuteNonQuery();
        }


     }
}