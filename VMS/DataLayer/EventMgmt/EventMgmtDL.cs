﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Models.EventModels;
using System.Data;
using VMS.Models.HrModels;
using VMS.Models.AccountModels;

namespace VMS.EventMgmt
{
    public class EventMgmtDL
    {

        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CREATE EVENT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
        * This method create a new event record in the organisation event table 
        */
        public static void insertEvent(SqlConnection dataConnection, EventDetails eventDetails, String organiserId)
        {                      
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "Insert into TABLE_EVT_DETAILS (EVT_ID, EVT_NAME, STARTDATE, ENDDATE, EVT_DESC, UNIQUE_LINK, EVT_IMAGE) "
                + " VALUES (@eventId, @eventName, @startDate, @endDate, @evtDesc, @link, @image);";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.NVarChar, 50);
            SqlParameter nameParam = new SqlParameter("eventName", SqlDbType.NVarChar, 100);
            SqlParameter startDateParam = new SqlParameter("startDate", SqlDbType.Date);
            SqlParameter endDateParam = new SqlParameter("endDate", SqlDbType.Date);
            SqlParameter descParam = new SqlParameter("evtDesc", SqlDbType.NVarChar, 250);
            SqlParameter linkParam = new SqlParameter("link", SqlDbType.NVarChar, 250);
            SqlParameter imageParam = new SqlParameter("image", SqlDbType.NVarChar, 100);

            eventIdParam.Value = eventDetails.eventId;
            nameParam.Value = eventDetails.eventName;
            startDateParam.Value = eventDetails.startDate.ToShortDateString();
            endDateParam.Value = eventDetails.endDate.ToShortDateString();
            descParam.Value = eventDetails.description;
            linkParam.Value = eventDetails.generateNewLink;
            imageParam.Value = eventDetails.imagePath;

            command.Parameters.Add(eventIdParam);
            command.Parameters.Add(nameParam);
            command.Parameters.Add(startDateParam);
            command.Parameters.Add(endDateParam);
            command.Parameters.Add(descParam);
            command.Parameters.Add(linkParam);
            command.Parameters.Add(imageParam);


            System.Diagnostics.Debug.WriteLine("startDate: " + eventDetails.startDate.ToShortDateString());
            System.Diagnostics.Debug.WriteLine("endDate: " + eventDetails.endDate.ToShortDateString());

            command.ExecuteNonQuery();
            tagEventToOrganiser(dataConnection, organiserId, eventDetails.eventId);
            createEventCause(dataConnection, eventDetails.eventId, eventDetails.listOfEventCause);
        }

        /*
        * This method maps the added event to the event organiser
        */
        public static void tagEventToOrganiser(SqlConnection databaseConnecion, String organiserID, String eventId)
        {
            SqlCommand command = new SqlCommand(null, databaseConnecion);
            command.CommandText = "INSERT INTO TABLE_ORG_EVT_USR (USR_ID, EVT_ID) VALUES (@usr_id, @evt_id);";

            SqlParameter usr_id = new SqlParameter("usr_id", SqlDbType.UniqueIdentifier);
            SqlParameter evt_id = new SqlParameter("evt_id", SqlDbType.UniqueIdentifier);

            usr_id.Value = new Guid(organiserID);
            evt_id.Value = new Guid(eventId);

            command.Parameters.Add(usr_id);
            command.Parameters.Add(evt_id);

            command.ExecuteNonQuery();
        }

        /*
         * This method insert the type of event it is
         */
        private static void createEventCause(SqlConnection dataConnection, String eventId, List<int> listOfEventCause)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_EVT_CAUSE VALUES ";

            int count = 1;
            foreach (int cause in listOfEventCause)
            {
                command.CommandText += "(@eventId" + count + ", @cause" + count + ")";
                SqlParameter idParam = new SqlParameter("eventId" + count, SqlDbType.UniqueIdentifier);
                SqlParameter causeParam = new SqlParameter("cause" + count, SqlDbType.NVarChar, 50);
                causeParam.Value = cause;
                idParam.Value = new Guid(eventId);
                command.Parameters.Add(causeParam);
                command.Parameters.Add(idParam);

                if (count != listOfEventCause.Count)
                    command.CommandText += ", ";
                count++;

            }

            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }


        /*
         *  This method creates a new event session and returns the session id
         */
        public static string insertEventSession(SqlConnection dataConnection, String eventId, SessionDetails sessionDetails)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT into TABLE_EVT_SESSION (EVT_SESSION_ID, EVT_ID, SESSION_NAME, SESSION_DESC, START_TIME, END_TIME, LOCATION, POSTAL, COMPULSORY, SESSION_SIZE) "
                + "OUTPUT INSERTED.EVT_SESSION_ID VALUES ( NEWID(), @eventId, @name, @desc, @start_time, @end_time, @location, @postal ,@compulsory, @sessionSize)";

            SqlParameter evtId = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            SqlParameter name = new SqlParameter("name", SqlDbType.NVarChar, 100);
            SqlParameter desc = new SqlParameter("desc", SqlDbType.NVarChar, 250);
            SqlParameter startTime = new SqlParameter("start_Time", SqlDbType.DateTime);
            SqlParameter endTime = new SqlParameter("end_Time", SqlDbType.DateTime);
            SqlParameter location = new SqlParameter("location", SqlDbType.NVarChar, 100);
            SqlParameter postal = new SqlParameter("postal", SqlDbType.NVarChar, 10);
            SqlParameter compulsory = new SqlParameter("compulsory", SqlDbType.NVarChar, 1);
            SqlParameter sessionSize = new SqlParameter("sessionSize", SqlDbType.Int);

            evtId.Value = new Guid(eventId);
            name.Value = sessionDetails.sessionName;
            desc.Value = sessionDetails.sessionDescription;
            startTime.Value = sessionDetails.startTime.ToShortTimeString();
            endTime.Value = sessionDetails.endTime.ToShortTimeString();
            location.Value = sessionDetails.location;
            postal.Value = sessionDetails.postal;
            sessionSize.Value = sessionDetails.sessionSize;
            compulsory.Value = sessionDetails.compulsoryDays;



            command.Parameters.Add(evtId);
            command.Parameters.Add(name);
            command.Parameters.Add(desc);
            command.Parameters.Add(startTime);
            command.Parameters.Add(endTime);
            command.Parameters.Add(location);
            command.Parameters.Add(postal);
            command.Parameters.Add(compulsory);
            command.Parameters.Add(sessionSize);


            SqlDataReader reader = command.ExecuteReader();
            String eventSessionId = "";

            if (reader.Read() == true)
                eventSessionId = reader[0].ToString();

            reader.Close();

            sessionDetails.sessionId = eventSessionId;

            insertSessionRole(dataConnection, eventSessionId, sessionDetails.listOfSessionRoles);
            insertSessionDays(dataConnection, sessionDetails.listOfSessionDays, eventSessionId);

            return eventSessionId;
        }

        /*
         * This method creates the number of days a session has
         */
        private static void insertSessionDays(SqlConnection dataConnection, List<SessionDays> listOfSessionDays, string sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_EVT_SESSION_DAY VALUES ";

            int count = 1;

            foreach (SessionDays days in listOfSessionDays)
            {
                command.CommandText += "(@sessionDayId" + count + ", @sessionId" + count + ", @evt_date" + count + ", 1 )";
                String newId = Guid.NewGuid().ToString();

                SqlParameter sessionDayIdParam = new SqlParameter("sessionDayId" + count, SqlDbType.NVarChar, 50);
                SqlParameter sessionIdParam = new SqlParameter("sessionId" + count, SqlDbType.UniqueIdentifier);
                SqlParameter evtDateParam = new SqlParameter("evt_date" + count, SqlDbType.Date);

                sessionDayIdParam.Value = newId;
                sessionIdParam.Value = new Guid(sessionId);
                evtDateParam.Value = days.evt_date.ToShortDateString();

                command.Parameters.Add(sessionIdParam);
                command.Parameters.Add(evtDateParam);
                command.Parameters.Add(sessionDayIdParam);

                days.sessionDayId = newId;

                if (count != listOfSessionDays.Count)
                    command.CommandText += ", ";
                count++;
            }

            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }


        /*
         * This method inserts a role of the specifc session which it is tagged to 
         */
        public static void insertSessionRole(SqlConnection dataConnection, string eventSessionId, List<SessionRoles> listOfSessionRoles)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_EVT_SESSION_ROLE (EVT_ROLE_ID, EVT_SESSION_ID, TERM_ROLE_ID, QTY, ROLE_DESC, TRAITS) VALUES ";

            int count = 1;
            foreach (SessionRoles sessionRoleDetails in listOfSessionRoles)
            {
                command.CommandText += "(NEWID(), @sessionId" + count + ", @roleId" + count + ", @qty" + count + ", @desc" + count + ", @trait"+count+")";
                SqlParameter sessionId = new SqlParameter("sessionId" + count, SqlDbType.UniqueIdentifier);
                SqlParameter roleIdParam = new SqlParameter("roleId" + count, SqlDbType.Int);
                SqlParameter qty = new SqlParameter("qty" + count, SqlDbType.Int);
                SqlParameter desc = new SqlParameter("desc" + count, SqlDbType.NVarChar, 50);
                SqlParameter traits = new SqlParameter("trait"+ count, SqlDbType.NVarChar, 10);

                sessionId.Value = new Guid(eventSessionId);
                roleIdParam.Value = sessionRoleDetails.roleId;
                qty.Value = sessionRoleDetails.quantity;
                desc.Value = sessionRoleDetails.roleDesc;
                traits.Value = sessionRoleDetails.personalityTraits;
                

                command.Parameters.Add(sessionId);
                command.Parameters.Add(roleIdParam);
                command.Parameters.Add(qty);
                command.Parameters.Add(desc);
                command.Parameters.Add(traits);


                if (count != listOfSessionRoles.Count)
                    command.CommandText += ", ";
                count++;
            }

            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }

    

        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< delete and edit events >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         * This method deletes an existing event  
         */
        public static void deleteEvent(SqlConnection dataConnection, String eventId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_EVT_DETAILS WHERE EVT_ID = @eventId";

            SqlParameter event_id = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            event_id.Value = new Guid(eventId);
            command.Parameters.Add(event_id);

            command.ExecuteNonQuery();
        }

        /*
         * This method deletes an event session 
         */
        public static void deleteEventSession(SqlConnection dataConnection, String sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_EVT_SESSION WHERE EVT_SESSION_ID = @sessionId ";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionId);
            command.Parameters.Add(sessionIdParam);

            command.ExecuteNonQuery();
        }

    
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< RETRIEVE EVENTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
        * This method retrieves a list of events created by a an event organiser 
        */
        public static List<EventDetails> retrieveOrganiserEvents(SqlConnection dataConnection, String organiserId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * from TABLE_ORG_EVT_USR T1, TABLE_EVT_DETAILS T2 WHERE " +
                                    "T1.EVT_ID = T2.EVT_ID AND T1.USR_ID = @usr_id";

            SqlParameter usr_id = new SqlParameter("usr_id", SqlDbType.UniqueIdentifier);
            usr_id.Value = new Guid(organiserId);
            command.Parameters.Add(usr_id);

            SqlDataReader dataReader = command.ExecuteReader();
            List<EventDetails> listOfEventDetails = new List<EventDetails>();
            while (dataReader.Read())
            {
                EventDetails eventDetails = new EventDetails();
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                System.Diagnostics.Debug.WriteLine("eventId: " + eventDetails.eventId);
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();

                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";

                listOfEventDetails.Add(eventDetails);
            }
            dataReader.Close();

            return listOfEventDetails;
        }

        /*
         * This method retrieve a specific eventDetail
         */
        public static EventDetails getEvent(SqlConnection databaseConnection, String eventId)
        {
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "SELECT * from TABLE_EVT_DETAILS WHERE EVT_ID = @eventId;";

            SqlParameter evt_id = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            evt_id.Value = new Guid(eventId);
            command.Parameters.Add(evt_id);

            SqlDataReader dataReader = command.ExecuteReader();
            EventDetails eventDetails = new EventDetails();
            while (dataReader.Read())
            {
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();

                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";

            }
            dataReader.Close();
            return eventDetails;
        }

        /*
         * This method get a list of session Details in which an event is tagged to  
         */
        public static List<SessionDetails> getListOfSessions(SqlConnection dataConnection, String eventId, Boolean getQuantity, List<String> listOfEventRole)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * from TABLE_EVT_SESSION WHERE EVT_ID = @evtId;";

            SqlParameter evtId = new SqlParameter("evtId", SqlDbType.UniqueIdentifier);
            evtId.Value = new Guid(eventId);
            command.Parameters.Add(evtId);

            SqlDataReader dataReader = command.ExecuteReader();
            List<SessionDetails> listOfSessionDetails = new List<SessionDetails>();
            while (dataReader.Read())
            {
                SessionDetails sessionDetail = new SessionDetails();
                sessionDetail.sessionId = dataReader["EVT_SESSION_ID"].ToString();
                sessionDetail.sessionName = dataReader["SESSION_NAME"].ToString();
                sessionDetail.sessionDescription = dataReader["SESSION_DESC"].ToString();
                sessionDetail.startTime = DateTime.Parse(dataReader["START_TIME"].ToString());
                sessionDetail.endTime = DateTime.Parse(dataReader["END_TIME"].ToString());
                sessionDetail.location = dataReader["LOCATION"].ToString();
                sessionDetail.sessionSize = Int32.Parse(dataReader["SESSION_SIZE"].ToString());

                listOfSessionDetails.Add(sessionDetail);
            }
            dataReader.Close();

            foreach (SessionDetails sessionDetails in listOfSessionDetails)
            {
                getSessionRoles(dataConnection, sessionDetails, listOfEventRole);
                getSessionDays(dataConnection, sessionDetails, getQuantity);
            }

            return listOfSessionDetails;
        }

        /*
         * This method retrieve all the sessionRoles tag to an event 
         */
        private static void getSessionRoles(SqlConnection dataConnection, SessionDetails sessionDetails, List<String> listOfEventRole)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_EVT_SESSION_ROLE T1 " +
                                  "WHERE T1.EVT_SESSION_ID =@sessionId " +
                                  "ORDER BY T1.TERM_ROLE_ID;";

            SqlParameter sessionId = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionId.Value = new Guid(sessionDetails.sessionId);
            command.Parameters.Add(sessionId);

            SqlDataReader dataReader = command.ExecuteReader();
            sessionDetails.listOfSessionRoles = new List<SessionRoles>();
            while (dataReader.Read())
            {
                SessionRoles role = new SessionRoles();
                role.roleId = Convert.ToInt16(dataReader["TERM_ROLE_ID"]);
                role.evt_roleId = dataReader["EVT_ROLE_ID"] + "";
                role.quantity = Int32.Parse(dataReader["QTY"].ToString());
                role.personalityTraits = dataReader["TRAITS"].ToString();
                role.roleDesc = dataReader["ROLE_DESC"] + "";

                for (int count = 0; count < listOfEventRole.Count; count++)
                {
                    String[] array = listOfEventRole[count].Split(',');
                    if (Int16.Parse(array[0]) == role.roleId)
                    {
                        role.roleName = array[1];
                        count = listOfEventRole.Count;
                    }
                }

                sessionDetails.listOfSessionRoles.Add(role);
            }
            dataReader.Close();
        }

        /*
         * This method retrieves all the days in which a particaular event session has 
         */
        private static void getSessionDays(SqlConnection dataConnection, SessionDetails sessionDetails, Boolean getQuantity)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_EVT_SESSION_DAY WHERE EVT_SESSION_ID = @sessionId;";

            SqlParameter sessionid = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionid.Value = new Guid(sessionDetails.sessionId);
            command.Parameters.Add(sessionid);

            SqlDataReader dataReader = command.ExecuteReader();
            sessionDetails.listOfSessionDays = new List<SessionDays>();
            while (dataReader.Read())
            {
                SessionDays sessionDay = new SessionDays();
                String evtDateInString = ((DateTime)dataReader["EVT_DATE"]).ToString("MM/dd/yyyy");
                sessionDay.evt_date = DateTime.ParseExact(evtDateInString, "MM/dd/yyyy", null);
                sessionDay.sessionDayId = dataReader["SESSIONDAY_ID"] + "";
                sessionDetails.listOfSessionDays.Add(sessionDay);
            }
            dataReader.Close();
            if (getQuantity == true)
            {
                foreach (SessionDays sessionDay in sessionDetails.listOfSessionDays)
                {
                    getRoleQuantity(dataConnection, sessionDay, sessionDetails);
                }
            }

        }

        /*
         *  This method retrieves the number of volunteers that attends a specific event session. Volunteer quantity are organised based on their roles
         */
        private static void getRoleQuantity(SqlConnection dataConnection, SessionDays sessionDay, SessionDetails sessionDetails)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.EVT_ROLE_ID, COUNT(USR_ID) as QTY FROM TABLE_VOL_EVT_ROLE t1, 
                                  TABLE_EVT_SESSION_DAY t2
                                  WHERE t1.SESSION_DAY_ID = t2.SESSIONDAY_ID                                    
                                  AND t2.EVT_SESSION_ID = @sessionId AND t2.EVT_DATE = @startDate 
                                  GROUP BY t1.EVT_ROLE_ID 
                                  ORDER BY t1.EVT_ROLE_ID ";

            SqlParameter session_Id = new SqlParameter("sessionID", SqlDbType.UniqueIdentifier);
            session_Id.Value = new Guid(sessionDetails.sessionId);
            command.Parameters.Add(session_Id);

            SqlParameter startDate = new SqlParameter("startDate", SqlDbType.DateTime);
            startDate.Value = sessionDay.evt_date.ToShortDateString();
            command.Parameters.Add(startDate);

            SqlDataReader dataReader = command.ExecuteReader();

            sessionDay.listOfRolesQuantity = new List<Int32>();
            for (int count = 0; count < sessionDetails.listOfSessionRoles.Count; count++)
                sessionDay.listOfRolesQuantity.Add(0);


            while (dataReader.Read())
            {
                String evt_role_id = dataReader["EVT_ROLE_ID"].ToString();
                for (int count = 0; count < sessionDetails.listOfSessionRoles.Count; count++)
                {
                    if (evt_role_id.Equals(sessionDetails.listOfSessionRoles[count].evt_roleId))
                        sessionDay.listOfRolesQuantity[count] = Int32.Parse(dataReader["QTY"].ToString());
                }
            }
            dataReader.Close();
        }


        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< get volunteers details >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         *  This method returns a list of volunteers id that joins this particular session day
         */
        public static List<String> getListofVolId(SqlConnection dataConnection, String sessionDayId, String eventRoleId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT USR_ID  FROM TABLE_VOL_EVT_ROLE " +
                                  "WHERE SESSION_DAY_ID = @sessionDayId AND EVT_ROLE_ID = @startDate ";

            SqlParameter session_Id = new SqlParameter("sessionDayId", SqlDbType.UniqueIdentifier);
            session_Id.Value = new Guid(sessionDayId);
            command.Parameters.Add(session_Id);

            SqlParameter eventRoleIdParam = new SqlParameter("startDate", SqlDbType.UniqueIdentifier);
            eventRoleIdParam.Value = new Guid(eventRoleId);
            command.Parameters.Add(eventRoleIdParam);

            SqlDataReader dataReader = command.ExecuteReader();
            List<String> listofVolId = new List<String>();

            while (dataReader.Read())
            {
                listofVolId.Add(dataReader["USR_ID"] + "");
            }
            dataReader.Close();
            return listofVolId;
        }

        /*
         * This method retrieves a list of volunteer details that joined a partucular session 
         */
        public static List<Volunteer> getJoinedVolunterDetails(SqlConnection dataConnection, List<String> listOfVolunterId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT USR_ID, FULL_NAME, EMAIL, DIETARY FROM TABLE_USR_DETAILS ";

            int count = 1;
            foreach(String volunteerId in listOfVolunterId)
            {
                if (count == 1)
                    command.CommandText += "WHERE USR_ID = @userId" + count;
                else
                    command.CommandText += " OR USR_ID = @userId" + count;

                SqlParameter userIdParam = new SqlParameter("userId" + count, SqlDbType.UniqueIdentifier);
                userIdParam.Value = new Guid(volunteerId);
                command.Parameters.Add(userIdParam);

                count++;
            }

            SqlDataReader reader = command.ExecuteReader();
            List<Volunteer> listOfVolunteer = new List<Volunteer>();
            while(reader.Read())
            {
                Volunteer volunteer = new Volunteer();
                volunteer.VOL_ID = reader["USR_ID"] + "";
                volunteer.EMAIL = reader["EMAIL"] + "";
                volunteer.FULL_NAME = reader["FULL_NAME"] + "";
                volunteer.DIETARY = reader["DIETARY"] + "";
                listOfVolunteer.Add(volunteer);
            }
            reader.Close();
            return listOfVolunteer;
        }

        /*
         * This method gets a lit of volunteer id from a specific event session 
         */
        public static List<String> getVolIdFromSession(SqlConnection dataConnection, String sessionId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT DISTINCT(t3.USR_ID) FROM TABLE_EVT_SESSION t1, TABLE_EVT_SESSION_DAY t2, TABLE_VOL_EVT_ROLE t3 
                                  WHERE t1.EVT_SESSION_ID = t2.EVT_SESSION_ID
                                  AND t2.SESSIONDAY_ID = t3.SESSION_DAY_ID
                                  AND t1.EVT_SESSION_ID = @sessionId";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionId);
            command.Parameters.Add(sessionIdParam);

            SqlDataReader reader = command.ExecuteReader();
            List<String> listOfVolId = new List<String>();
            while(reader.Read())
            {
                listOfVolId.Add(reader["USR_ID"] + "");
            }

            reader.Close();
            return listOfVolId;
        }

        /*
         * This method gets a list of volunteer ids who join a particular event  session
         */
        public static List<String> getVolIdFromEvent(SqlConnection dataConnection, String eventId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT DISTINCT(t3.USR_ID) FROM TABLE_EVT_SESSION t1, TABLE_EVT_SESSION_DAY t2, TABLE_VOL_EVT_ROLE t3, TABLE_EVT_DETAILS t4
                                  WHERE t4.EVT_ID = t1.EVT_ID
                                  AND t1.EVT_SESSION_ID = t2.EVT_SESSION_ID
                                  AND t2.SESSIONDAY_ID = t3.SESSION_DAY_ID
                                  AND t1.EVT_ID = @eventId";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventId);
            command.Parameters.Add(eventIdParam);

            SqlDataReader reader = command.ExecuteReader();
            List<String> listOfVolId = new List<String>();
            while (reader.Read())
            {
                listOfVolId.Add(reader["USR_ID"] + "");
            }

            reader.Close();
            return listOfVolId;
        }

        /*
         * This method gets a list of volunteers form a deleted event session/ event to send an apology message 
         */
         public static List<String> getvolEvent(SqlConnection dataConnection, List<String> listOfVolId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT EMAIL FROM TABLE_USR_DETAILS WHERE ";

            int count = 1;
            foreach(String volId in listOfVolId)
            {
                if (count != 1)
                    command.CommandText += "OR ";

                command.CommandText += "USR_ID  = @usrId"+ count +" ";
                SqlParameter userIdParam = new SqlParameter("usrId" + count, SqlDbType.UniqueIdentifier);
                userIdParam.Value = new Guid(volId);
                command.Parameters.Add(userIdParam);
                count++;
            }

            List<String> listOfEmails = new List<String>();
            SqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                listOfEmails.Add(reader["EMAIL"] + "");
            }

            reader.Close();
            return listOfEmails;
        }



        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< event roles >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         * This method returns a list of event roles from organisation
         */
         public static List<SessionRoles> getListOfEventRolesFromOrg(SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT * FROM TABLE_EVT_ROLE_SKILL ORDER BY TERM_ROLE_ID ASC";

            List<SessionRoles> listOfOrgEventRoles = new List<SessionRoles>();
            SqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                SessionRoles role = new SessionRoles();
                role.roleId = Convert.ToInt16(reader["TERM_ROLE_ID"]);
               
                role.skillSets = reader["TERM_SKILL_IDS"] + "";

                role.concatenateRoleId = role.roleId + "-" + reader["TRAITS"];

                if ((reader["TERM_SKILL_IDS"] + "").Equals("") == false)
                    role.concatenateRoleId += "-" + reader["TERM_SKILL_IDS"];

                listOfOrgEventRoles.Add(role);

            }
            reader.Close();
            return listOfOrgEventRoles;
        }


        /*
         * This method returns a list of event roles from the master copy of an organisation 
         * There is a need to concatenate the traits and skillsets to the role_id as to improve the effcieny of the event matching
         */
        public static List<SessionRoles> getListOfEventRoles(SqlConnection dataConnection, List<SessionRoles> listOfOrgEventRoles)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.TERM_ID, t1.TERM_NAME, t2.TRAITS, t2.TERM_SKILL_IDS FROM  
                                    TABLE_TERM t1, TABLE_EVT_ROLE_SKILL t2
                                    WHERE t1.TERM_ID = t2.TERM_ROLE_ID
                                    ORDER BY t1.TERM_ID ASC";

            SqlDataReader dataReader = command.ExecuteReader();
            List<SessionRoles> listOfEventRoles = new List<SessionRoles>();

            int count = 0;
            while (dataReader.Read())
            {
                SessionRoles role = new SessionRoles();
                role.roleName = dataReader["TERM_NAME"].ToString();

                if(listOfOrgEventRoles.Count != 0 && count < listOfOrgEventRoles.Count && listOfOrgEventRoles[count].roleId == Convert.ToInt16(dataReader["TERM_ID"]))
                {
                    role.concatenateRoleId = listOfOrgEventRoles[count].concatenateRoleId;
                }
                else
                {
                    if ((dataReader["TERM_SKILL_IDS"]+ "").Equals("") == false)
                        role.concatenateRoleId = dataReader["TERM_ID"] + "-" + dataReader["TRAITS"] + "-" + dataReader["TERM_SKILL_IDS"];
                    else
                        role.concatenateRoleId = dataReader["TERM_ID"] + "-" + dataReader["TRAITS"];
                }
              
                listOfEventRoles.Add(role);
                count++;
            }
            dataReader.Close();
            return listOfEventRoles;
        }

        /*
         * This method retrieves a list of event role name with its corresponding ID  
         */
        public static List<String> getListOfEventRoleName(SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.TERM_ID, t1.TERM_NAME FROM  
                                    TABLE_TERM t1, TABLE_EVT_ROLE_SKILL t2
                                    WHERE t1.TERM_ID = t2.TERM_ROLE_ID";

            SqlDataReader dataReader = command.ExecuteReader();
            List<String> listOfEventRoleName = new List<string>();

            while (dataReader.Read())
            {
                listOfEventRoleName.Add(dataReader["TERM_ID"] + "," + dataReader["TERM_NAME"]);
            }
            dataReader.Close();
            return listOfEventRoleName;
        }


      public static SessionDetails getSessionBySessDay(SqlConnection dataConnection, string sessionDayId)
        {
            SessionDetails session = new SessionDetails();
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM TABLE_EVT_SESSION S INNER JOIN TABLE_EVT_SESSION_DAY SD ON S.EVT_SESSION_ID=SD.EVT_SESSION_ID WHERE SESSIONDAY_ID=@SESSIONDAY_ID";
            SqlParameter sessiondayIdParam = new SqlParameter("SESSIONDAY_ID", SqlDbType.UniqueIdentifier);
            sessiondayIdParam.Value = new Guid(sessionDayId);
            command.Parameters.Add(sessiondayIdParam);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                session.sessionName = reader["SESSION_NAME"].ToString();
                SessionDays day = new SessionDays();
                day.evt_date= Convert.ToDateTime(reader["EVT_DATE"].ToString());
                List<SessionDays> dayList = new List<SessionDays>();
                dayList.Add(day);
                session.listOfSessionDays= dayList;
            }
            reader.Close();
            return session;
        }
    }
}