﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Models.EventModels;
using System.Data;

namespace VMS.EventMgmt
{
    public class WeightedSearchDL
    {
        /*
         *  This method retrieves a list of events that the following 2 requirements
         *  (1) This event still have event sessions that are still in its recruitment phrase        
         *  (2) The roles within an event session has not hit its max recuritment
         */
        public static List<EventDetails> retrieveShortListedEvents(SqlConnection dataConnection, DateTime currentDate)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT t1.EVT_ID, t1.EVT_NAME, t1.DESC, t1.CONNECTION_STRING, t1.BENEFICIARY, t2.EVT_SESSION_ID, t4.ROLE_ID, t6.TRAITS, t6.SKILLS, t3.EVT_DATE " + 
                                   "FROM TABLE_EVT_DETAILS t1, TABLE_EVT_SESSION t2, TABLE_EVT_SESSION_DAY t3, TABLE_EVT_SESSION_ROLE t4, TABLE_ORG_EVT_ROW t6 " +
                                   "WHERE t1.EVT_ID = t2.EVT_ID " +
                                   "AND t2.EVT_SESSION_ID = t3.EVT_SESSION_ID " +
                                   "AND t2.EVT_SESSION_ID = t4.EVT_SESSION_ID " +
                                   "AND t4.ROLE_ID = t6.ROLE_ID "+
                                   "AND t3.CLOSE_DATE > @currentDate " + 
                                   "AND t4.QTY > ( " + 
                                   "SELECT COUNT(VOL_ID) FROM TABLE_VOL_EVT_ROLE t5 " +
                                   "WHERE t5.EVT_SESSION_ID = t2.EVT_SESSION_ID " +
                                   "AND t5.ROLE_NAME = t4.ROLE_NAME " +
                                   ");";

            SqlParameter dateParam = new SqlParameter("currentDate", SqlDbType.Date);
            dateParam.Value = currentDate.ToShortDateString();
            command.Parameters.Add(dateParam);

            List<EventDetails> listOfEventDetails = new List<EventDetails>();
            int eventCount = 0;
            int sessionCount=0;
            int roleCount = 0;
            int dayCount = 0;   

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                String evt_id = reader["EVT_ID"].ToString();
                if (listOfEventDetails.Count == 0 || evt_id.Equals(listOfEventDetails[eventCount-1].eventId) == false )
                {
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.eventId = evt_id;
                    eventDetails.eventName = reader["EVT_NAME"].ToString();
                    eventDetails.description = reader["DESC"].ToString();
                    eventDetails.connectionString = reader["CONNECTION_STRING"].ToString();
                    //eventDetails.listOfBeneficiary = reader["BENEFICIARY"].ToString();
                    listOfEventDetails.Add(eventDetails);
                    eventCount++;
                    sessionCount = 0;
                }

                if (listOfEventDetails[eventCount].listOfSession == null)
                    listOfEventDetails[eventCount].listOfSession = new List<SessionDetails>();

                String sessionId = reader["EVT_SESSION_ID"].ToString();
                if (listOfEventDetails[eventCount].listOfSession.Count ==0 || sessionId.Equals(listOfEventDetails[eventCount].listOfSession[sessionCount].sessionId) == false)
                {
                    SessionDetails sessionDetails = new SessionDetails();
                    sessionDetails.sessionId = sessionId;
                    listOfEventDetails[eventCount].listOfSession.Add(sessionDetails);
                    sessionCount++;
                    roleCount = 0;
                    dayCount = 0;                               
                }

                if (listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionRoles == null)
                    listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionRoles = new List<SessionRoles>();

                String roleId = reader["ROLE_ID"].ToString();
                if(listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionRoles.Count ==0 || roleId.Equals(listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionRoles[roleCount -1].roleName) == false)
                {
                    SessionRoles sessionRole = new SessionRoles();
                   // sessionRole.roleId = roleId;
                    sessionRole.personalityTraits = reader["TRAITS"].ToString();
                    sessionRole.skillSets = reader["SKILLS"].ToString();
                    listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionRoles.Add(sessionRole);
                    roleCount++;
                }


                if (listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionDays == null)
                    listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionDays = new List<SessionDays>();

                String sessionDate = reader["EVT_DATE"].ToString(); 
                if(listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionDays.Count == 0 || sessionDate.Equals(listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionDays[dayCount-1].evt_date.ToShortDateString()) == false)
                {
                    SessionDays sessionDay = new SessionDays();
                    sessionDay.evt_date = DateTime.Parse(sessionDate);
                    listOfEventDetails[eventCount].listOfSession[sessionCount].listOfSessionDays.Add(sessionDay);
                    dayCount++;
                }
                                                                          
            }

            reader.Close();
            return listOfEventDetails;
        }

        /*
         * This method retrieve 4 matching details from a volunteer 
         * (1) Traits
         * (2) skills in a list of termIds
         * (3) Benficiary in a list of termids
         * (4) event roles in a list of termIds
         */
        public static void getVolunteerMatchingDetails(SqlConnection dataConnection, string volId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.TERM_ID, t1.VOCAB_ID FROM TABLE_TERM_RELATION t1  
                                    WHERE t1.ENTITY_ID = @volId";

            SqlParameter volIdParam = new SqlParameter("volId", SqlDbType.UniqueIdentifier);
            volIdParam.Value = new Guid(volId);
            command.Parameters.Add(volIdParam);

            SqlDataReader dataReader = command.ExecuteReader();
            while(dataReader.Read())
            {
                
            }            
        }
    }
}