﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using VMS.Models.EventModels;

namespace VMS.EventMgmt
{
    public class CollaboativeFilteringDL
    {
        /*
         *  This method retrieves the number of interests in which a volunteer has         
         */
        public static int getNumberOfInterest(SqlConnection dataConnection, String usrId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT COUNT(*) AS NUM FROM TABLE_TERM_RELATION
                                    WHERE ENTITY_ID = @usrId
                                    AND VOCAB_ID <> 1";

            SqlParameter userIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(usrId);
            command.Parameters.Add(userIdParam);

            SqlDataReader reader = command.ExecuteReader();
            int numOfInterest = 0;
            while(reader.Read())
            {
                numOfInterest = Convert.ToInt16(reader["NUM"]);
            }

            reader.Close();
            return numOfInterest;

        }

        /*
         * This method retrieves a list of volunteer IDs that are similar to current volunteer
         * in terms of interest in event cause and event roles
         * simliarity level is currently set to 70% similiar (100% is more accurate but not enough data)
         * This list of volunteer will at most have 2 more interest then the current volunteer         
         */
        public static List<String> getSimiliarVolunteer(SqlConnection dataConnection, String usrId, int numOfInterest)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.ENTITY_ID, COUNT(*)
                                    FROM TABLE_TERM_RELATION t1
                                    WHERE t1.VOCAB_ID <> 1
                                    AND t1.ENTITY_ID <> @usrId
                                    AND t1.ENTITY_ID IN
                                    (
                                        SELECT t3.ENTITY_ID FROM TABLE_TERM_RELATION t3
										WHERE VOCAB_ID <> 1
										GROUP BY t3.ENTITY_ID 
										HAVING (COUNT(*) - @numOfInterest) <= 3 
                                    )
                                    AND t1.TERM_ID IN 
                                    (
                                        SELECT t2.TERM_ID FROM TABLE_TERM_RELATION t2
                                        WHERE t2.VOCAB_ID <> 1
                                        AND t2.ENTITY_ID = @usrId
                                    )
                                    GROUP BY t1.ENTITY_ID
                                    HAVING (COUNT(*)  * 100 / @numOfInterest) >= 70";

            SqlParameter userIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(usrId);
            command.Parameters.Add(userIdParam);

            SqlParameter numInterestParam = new SqlParameter("numOfInterest", SqlDbType.Int);
            numInterestParam.Value = numOfInterest;
            command.Parameters.Add(numInterestParam);


            List<String> listOfSimiliarVolunteer = new List<String>();
            SqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                listOfSimiliarVolunteer.Add(reader["ENTITY_ID"] + "");
            }
            return listOfSimiliarVolunteer;             
        }

        /*
         * This method gets a list of joined event id 
         */
        public static List<String> getJoinedEventId(SqlConnection dataConnection, String usrId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT DISTINCT(t1.EVT_ID) FROM TABLE_EVT_DETAILS t1, TABLE_EVT_SESSION t2, 
                                    TABLE_EVT_SESSION_DAY t3, TABLE_VOL_EVT_ROLE t4
                                    WHERE t1.EVT_ID = t2.EVT_ID
                                    AND t2.EVT_SESSION_ID = t3.EVT_SESSION_ID
                                    AND t3.SESSIONDAY_ID = t4.SESSION_DAY_ID
                                    AND t4.USR_ID = @usrId";

            SqlParameter userIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(usrId);
            command.Parameters.Add(userIdParam);

            SqlDataReader reader = command.ExecuteReader();
            List<String> listOfJoinEventId = new List<string>();
            while(reader.Read())
            {
                listOfJoinEventId.Add(reader["EVT_ID"] + "");
                System.Diagnostics.Debug.WriteLine("eventId: " + reader["EVT_ID"] + "");
            }
            reader.Close();
            return listOfJoinEventId;
        }

        /*
         * This method gets all event that a volunteer might be interested in 
         * based on the interest of the other volunteers 
         * currently if 50% of volunteer is similiar to one volunteer, then should reccomend this event to the voluntter 
         */
        public static List<EventDetails> getFilterEvents(SqlConnection dataConnection, List<String> listOfSimiliarVol, String usrId, String connectionString, List<String> listOfEvtId, String orgName)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.EVT_ID, t1.EVT_NAME, t1.EVT_DESC, t1.STARTDATE, t1.ENDDATE, t1.EVT_IMAGE, COUNT(DISTINCT t4.USR_ID) 
                                   FROM TABLE_EVT_DETAILS t1, TABLE_EVT_SESSION t2, TABLE_EVT_SESSION_DAY t3, TABLE_VOL_EVT_ROLE t4
                                   WHERE t1.EVT_ID = t2.EVT_ID
                                   AND t2.EVT_SESSION_ID = t3.EVT_SESSION_ID
                                   AND t3.SESSIONDAY_ID = t4.SESSION_DAY_ID
                                   AND t4.USR_ID <> @usrId AND ( ";

            SqlParameter userIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(usrId);
            command.Parameters.Add(userIdParam);

            int count = 1;
            foreach(String volId in listOfSimiliarVol)
            {   
                if(count != 1)
                    command.CommandText += " OR ";

                command.CommandText += "t4.USR_ID = @simUsrId"+count;

                SqlParameter simUsrIdParam = new SqlParameter("simUsrId" + count, SqlDbType.UniqueIdentifier);
                simUsrIdParam.Value = new Guid(volId);
                command.Parameters.Add(simUsrIdParam);

                count++;
            }

            command.CommandText += @") ";

            if(listOfEvtId.Count > 0)
            {
                command.CommandText += " AND (  ";
                int eventCount = 1;
                foreach (String evtId in listOfEvtId)
                {
                    if (eventCount != 1)
                        command.CommandText += " AND ";

                    command.CommandText += " t1.EVT_ID <> @eventId" + eventCount;

                    SqlParameter eventIdParam = new SqlParameter("eventId" + eventCount, SqlDbType.UniqueIdentifier);
                    eventIdParam.Value = new Guid(evtId);
                    command.Parameters.Add(eventIdParam);
                    eventCount++;
                }

                command.CommandText += " ) ";
            }

            command.CommandText +=  @"GROUP BY t1.EVT_ID, t1.EVT_NAME, t1.EVT_DESC, t1.STARTDATE, t1.ENDDATE, t1.EVT_IMAGE 
                                     HAVING COUNT(DISTINCT t4.USR_ID) * 100/ 
                                     (
	                                    SELECT COUNT(DISTINCT t5.USR_ID) 
	                                    FROM TABLE_VOL_EVT_ROLE t5, TABLE_EVT_SESSION_DAY t6, TABLE_EVT_SESSION t7
	                                    WHERE t5.SESSION_DAY_ID = t6.SESSIONDAY_ID
	                                    AND t6.EVT_SESSION_ID = t7.EVT_SESSION_ID
	                                    AND t7.EVT_ID = t1.EVT_ID
                                      )>= 50";

            SqlDataReader dataReader = command.ExecuteReader();
            List<EventDetails> listOfEventDetails = new List<EventDetails>();
            while(dataReader.Read())
            {
                EventDetails eventDetails = new EventDetails();
                
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();
                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";
                eventDetails.connectionString = connectionString;
                eventDetails.orgName = orgName;

                listOfEventDetails.Add(eventDetails);
            }

            return listOfEventDetails;
        }
           
    }
}