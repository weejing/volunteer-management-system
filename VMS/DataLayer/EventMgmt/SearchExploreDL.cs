﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.EventModels;

namespace VMS.EventMgmt
{
    public class SearchExploreDL
    {

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Register volunteer >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
        *  This method inserts a volunteer into the the table at console_vms table
        */
        public static void registerEventInVol(SqlConnection dataConnection, SignUpEvent signUp, String userId)
        {
            String evtUserId = Guid.NewGuid().ToString();

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT into TABLE_USR_EVENT (USR_EVT_ID, USR_ID, EVT_ID, CONNECTION_STRING) VALUES "
                                    + "(@evt_userId, @usr_id, @evt_id, @connectionString)";

            SqlParameter evtUserIdParam = new SqlParameter("evt_userId", SqlDbType.UniqueIdentifier);
            evtUserIdParam.Value = new Guid(evtUserId);
            command.Parameters.Add(evtUserIdParam);

            SqlParameter usr_Id = new SqlParameter("usr_id", SqlDbType.UniqueIdentifier);
            usr_Id.Value = new Guid(userId);
            command.Parameters.Add(usr_Id);

            SqlParameter evt_id = new SqlParameter("evt_id", SqlDbType.UniqueIdentifier);
            evt_id.Value = new Guid(signUp.eventId);
            command.Parameters.Add(evt_id);

            SqlParameter connectionString = new SqlParameter("connectionString", SqlDbType.NVarChar, 250);
            connectionString.Value = signUp.connectionString;
            command.Parameters.Add(connectionString);           

            command.ExecuteNonQuery();
            signUp.evtUserId = evtUserId;
            String[] dates = signUp.eventdateList.Split(',');
            registerEventSessionInVol(dataConnection, dates, signUp);

        }

        /*
        * This method enters into the rleationship table 
        */
        public static void registerEventSessionInVol(SqlConnection dataConnection, String[] dates, SignUpEvent signUp)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_EVENT_SESSION VALUES ";

            List<String> listOfSchedId = new List<string>();

            int count = 1;
            foreach (string date in dates)
            {
                command.CommandText += "(@usrEvtId" + count + ", @sessionId" + count + ", @schedId" + count + " )";

                SqlParameter usrEvtParam = new SqlParameter("usrEvtId" + count, SqlDbType.UniqueIdentifier);
                usrEvtParam.Value = new Guid(signUp.evtUserId);
                command.Parameters.Add(usrEvtParam);

                SqlParameter sessionIdParam = new SqlParameter("sessionId" + count, SqlDbType.UniqueIdentifier);
                sessionIdParam.Value = new Guid(signUp.sessionId);
                command.Parameters.Add(sessionIdParam);

                SqlParameter sechduleParam = new SqlParameter("schedId" + count, SqlDbType.UniqueIdentifier);
                String schedule = Guid.NewGuid().ToString();
                sechduleParam.Value = new Guid(schedule);
                command.Parameters.Add(sechduleParam);
                listOfSchedId.Add(schedule);

                if (count != dates.Length)
                    command.CommandText += ", ";
                count++;
            }

            command.ExecuteNonQuery();
            registerEventDates(dataConnection, dates, listOfSchedId);

        }

        /*
         *  This method checks if a volunteer has joined this specific event before
         */
        public static Boolean checkIfVolJoinBefore(SqlConnection dataConnection, SignUpEvent signUp, String userId)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT USR_EVT_ID, COUNT(EVT_ID) as EXIST FROM TABLE_USR_EVENT 
                                    WHERE EVT_ID = @eventId AND USR_ID =@userId
                                    GROUP BY USR_EVT_ID";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(signUp.eventId);
            command.Parameters.Add(eventIdParam);

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            SqlDataReader reader = command.ExecuteReader();

            int numOfRows = 0;
            while(reader.Read())
            {
                numOfRows = Convert.ToInt16(reader["EXIST"]);
                signUp.evtUserId = reader["USR_EVT_ID"] + "";
            }

            reader.Close();

            if (numOfRows == 1)
                return true;

            return false;
        }


        /*
         * This method inserts all the date in whch a volunteer is tag to. This dates will be mark as dates in which a volunteer is unavlaible  
         */
        public static void registerEventDates(SqlConnection dataConnection, String [] dates, List<String> listOfSched)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_SCHED VALUES ";

            int count = 1;
            foreach(string date in dates)
            {
                command.CommandText += "(@schedId" + count + ", @date" + count + ")";

                SqlParameter schedIdParam = new SqlParameter("schedId" + count, SqlDbType.UniqueIdentifier);
                schedIdParam.Value = new Guid(listOfSched[count -1]);
                command.Parameters.Add(schedIdParam);

                SqlParameter dateParam = new SqlParameter("date" + count, SqlDbType.Date);
                dateParam.Value = date;
                command.Parameters.Add(dateParam);

                if (count != dates.Length)
                    command.CommandText += ", ";
                count++;
            }
            command.ExecuteNonQuery();
        }
      
       
        /*
         * This method get a list session day Ids which correspon to dates in whcih the volunteer want to regsiter for a particular event session
         */
        public static List<String> getListOfSessionDayId(SqlConnection dataConnection, String[] listOfDates, SignUpEvent signUp)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT SESSIONDAY_ID FROM TABLE_EVT_SESSION_DAY WHERE EVT_SESSION_ID = @sessionId AND ";

            SqlParameter sessionIdParam = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(signUp.sessionId);
            command.Parameters.Add(sessionIdParam);

            List<String> listOfSessionDayId = new List<string>();

            int count = 1;
            foreach(String date in listOfDates)
            {
                if (count > 1)
                    command.CommandText += "OR ";

                command.CommandText += "EVT_DATE = @date" + count + " ";
                SqlParameter dateParam = new SqlParameter("date" + count, SqlDbType.Date);
                dateParam.Value = DateTime.Parse(date);
                command.Parameters.Add(dateParam);
                count++;                            
            }

            SqlDataReader dataReader = command.ExecuteReader();
            while(dataReader.Read())
            {
                listOfSessionDayId.Add(dataReader["SESSIONDAY_ID"] + "");
            }
            dataReader.Close();
            return listOfSessionDayId;
        }

        /*
         * This method registers the event at the organisation database
         */
        public static void registerEventInOrg(SqlConnection dataConnection, SignUpEvent signUp, String userId, List<String> listOfSessionDayId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT into TABLE_VOL_EVT_ROLE (VOL_EVT_ROLE_ID, USR_ID, SESSION_DAY_ID, EVT_ROLE_ID) VALUES ";
                                    
            int count = 1;
            foreach(string sessionDayId in listOfSessionDayId)
            {

                command.CommandText += "(NEWID(), @usr_id" + count + ", @session_id" + count + ", @role" + count + ")";

                SqlParameter usr_Id = new SqlParameter("usr_id" + count, SqlDbType.UniqueIdentifier);
                usr_Id.Value = new Guid(userId);
                command.Parameters.Add(usr_Id);


                SqlParameter session_id = new SqlParameter("session_id"+ count, SqlDbType.UniqueIdentifier);
                session_id.Value = new Guid(sessionDayId);
                command.Parameters.Add(session_id);
               

                SqlParameter evt_roleId = new SqlParameter("role" + count, SqlDbType.UniqueIdentifier);
                evt_roleId.Value = new Guid(signUp.evt_roleId);
                command.Parameters.Add(evt_roleId);

                if (count != listOfSessionDayId.Count)
                    command.CommandText += ", ";
                count++;
            }
            command.ExecuteNonQuery();
        }



        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< get event details >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         *  This method gets the request event details clicked by a volunteer 
         */
        public static VolEventDetail getVolEventDetails(SqlConnection databaseConnection , string eventId, List<String> listOfEventRole)
        {
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = @"SELECT * from TABLE_EVT_DETAILS t1, TABLE_EVT_SESSION t2, TABLE_EVT_SESSION_ROLE t3
                                    WHERE t1.EVT_ID = t2.EVT_ID 
                                    AND t2.EVT_SESSION_ID = t3.EVT_SESSION_ID
                                    AND t1.EVT_ID = @eventId
                                    ORDER BY t2.EVT_SESSION_ID ASC, t3.TERM_ROLE_ID ASC ;";

            SqlParameter evt_id = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            evt_id.Value = new Guid(eventId);
            command.Parameters.Add(evt_id);

            SqlDataReader dataReader = command.ExecuteReader();
            VolEventDetail eventDetails = new VolEventDetail();
            int count = 1;
            int sessionCount = 0;
            while (dataReader.Read())
            {
                if(count == 1)
                {
                    eventDetails.eventId = dataReader["EVT_ID"].ToString();
                    eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                    String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                    eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                    System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                    String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                    eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);
                    eventDetails.description = dataReader["EVT_DESC"].ToString();

                    eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";
                    eventDetails.generateNewLink = dataReader["UNIQUE_LINK"] + "";

                    eventDetails.listOfSession = new List<VolSessionDetails>();
                }

                if(eventDetails.listOfSession.Count == 0 || eventDetails.listOfSession[sessionCount-1].sessionId.Equals(dataReader["EVT_SESSION_ID"] + "") == false)
                {
                    VolSessionDetails sessionDetails = new VolSessionDetails();

                    sessionDetails.sessionId = dataReader["EVT_SESSION_ID"] + "";
                    sessionDetails.sessionName = dataReader["SESSION_NAME"] + "";
                    sessionDetails.sessionDescription = dataReader["SESSION_DESC"] + "";
                    sessionDetails.startTime = DateTime.Parse(dataReader["START_TIME"] + "");
                    sessionDetails.endTime = DateTime.Parse(dataReader["END_TIME"] + "");
                    sessionDetails.location = dataReader["LOCATION"] + "";
                    sessionDetails.compulsory = Convert.ToInt16(dataReader["COMPULSORY"]);
                    sessionDetails.listOfEventRoles = new List<SessionRoles>();

                    eventDetails.listOfSession.Add(sessionDetails);
                    sessionCount++;
                    count++;
                }

                SessionRoles role = new SessionRoles();
                role.roleId = Convert.ToInt16(dataReader["TERM_ROLE_ID"]);
                role.roleDesc = dataReader["ROLE_DESC"] + "";
                role.evt_roleId = dataReader["EVT_ROLE_ID"] + "";
                
                for(int roleCount = 0; roleCount < listOfEventRole.Count; roleCount++)
                {
                    String[] roleArray = listOfEventRole[roleCount].Split(',');
                    if(Int16.Parse(roleArray[0]) == role.roleId)
                    {
                        role.roleName = roleArray[1];
                        roleCount = listOfEventRole.Count;
                    }
                }

                eventDetails.listOfSession[sessionCount - 1].listOfEventRoles.Add(role);
                
            }
            dataReader.Close();
            return eventDetails;
        }

        /*
         * This method retrieves all the session days that are joined by a volunteer  
         */
        public static List<String> getJoinedSessions(SqlConnection dataConnection, string userId, string eventId, VolEventDetail eventDetail)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.SESSION_DAY_ID, t2.EVT_DATE, t3.TERM_ROLE_ID, t4.EVT_SESSION_ID FROM  
                                   TABLE_VOL_EVT_ROLE t1, TABLE_EVT_SESSION_DAY t2, 
                                   TABLE_EVT_SESSION_ROLE t3, TABLE_EVT_SESSION t4
                                   WHERE t1.EVT_ROLE_ID = t3.EVT_ROLE_ID
                                   AND t1.SESSION_DAY_ID = t2.SESSIONDAY_ID
                                   AND t2.EVT_SESSION_ID = t4.EVT_SESSION_ID
                                   AND t1.USR_ID = @userId
                                   AND t4.EVT_ID = @eventId
                                   ORDER BY t4.EVT_SESSION_ID ASC, t2.EVT_DATE";

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventId);
            command.Parameters.Add(eventIdParam);

            SqlDataReader dataReder =  command.ExecuteReader();
            List<String> listOfSessionDays = new List<String>();
            int count = 0;
            while(dataReder.Read())
            {
               
                while (eventDetail.listOfSession[count].sessionId.Equals(dataReder["EVT_SESSION_ID"] + "") == false)
                    count++;

                if (eventDetail.listOfSession[count].listOfJoinedDays == null)
                    eventDetail.listOfSession[count].listOfJoinedDays = new List<SessionDays>();                
                
                
                SessionDays day = new SessionDays();
                day.sessionDayId = dataReder["SESSION_DAY_ID"] + "";
                day.evt_date = DateTime.Parse(dataReder["EVT_DATE"] + "");
                System.Diagnostics.Debug.WriteLine(day.evt_date);
                day.role = Convert.ToInt16(dataReder["TERM_ROLE_ID"]);
                
                foreach (SessionRoles role in eventDetail.listOfSession[count].listOfEventRoles)
                {
                    if (role.roleId == day.role)
                        day.roleName = role.roleName;
                }

                listOfSessionDays.Add(day.sessionDayId);
                eventDetail.listOfSession[count].listOfJoinedDays.Add(day);
                
            }
            dataReder.Close();
            return listOfSessionDays;
        }

        /*
         * This method retrieves all sessions that are not join by the volunteer
         * Session days that have alrd passed or have roles that have reached full capcity would not be displayed 
         */

        public static void getNotJoinedSessions(SqlConnection dataConnection, VolEventDetail eventDetails, DateTime currentDate, List<String> listOfSessionDay )
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.EVT_DATE, t2.EVT_SESSION_ID, t3.TERM_ROLE_ID, t1.SESSIONDAY_ID 
                                    FROM TABLE_EVT_SESSION_DAY t1, TABLE_EVT_SESSION t2, TABLE_EVT_SESSION_ROLE t3
                                    WHERE t1.EVT_SESSION_ID = t2.EVT_SESSION_ID
                                    AND t2.EVT_SESSION_ID = t3.EVT_SESSION_ID
                                    AND t2.EVT_ID = @eventId";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventDetails.eventId);
            command.Parameters.Add(eventIdParam);

            int count = 1;
            
            foreach(string day in listOfSessionDay)
            {
                command.CommandText += " AND t1.SESSIONDAY_ID <> @sessionDay" + count;
                SqlParameter sessionDayParam = new SqlParameter("sessionDay" + count, SqlDbType.UniqueIdentifier);
                sessionDayParam.Value = new Guid(day);
                command.Parameters.Add(sessionDayParam);
                count++;
            }

            command.CommandText += @" AND t1.EVT_DATE >= @currentDate
                                    AND t3.QTY > ALL
                                    (
                                        SELECT COUNT(t4.USR_ID) FROM TABLE_VOL_EVT_ROLE t4
                                        WHERE t4.SESSION_DAY_ID = t1.SESSIONDAY_ID
                                        AND t4.EVT_ROLE_ID = t3.EVT_ROLE_ID            
                                    )
                                    ORDER BY t2.EVT_SESSION_ID ASC, t3.TERM_ROLE_ID ASC, t1.EVT_DATE ASC";
            SqlParameter currentDateParam = new SqlParameter("currentDate", SqlDbType.Date);
            currentDateParam.Value = currentDate;
            command.Parameters.Add(currentDateParam);

            SqlDataReader dataReader = command.ExecuteReader();
            count = 0;
            int roleCount = 0;
            Boolean roleExist = false;
            while(dataReader.Read())
            {

                while (eventDetails.listOfSession[count].sessionId.Equals(dataReader["EVT_SESSION_ID"] + "") == false)
                    count++;

                if (eventDetails.listOfSession[count].listOfUnjoinedDays == null)
                {
                    eventDetails.listOfSession[count].listOfUnjoinedDays = new List<SessionDays>();
                    roleCount = 0;
                }


                SessionDays day = new SessionDays();
                day.sessionDayId = dataReader["SESSIONDAY_ID"] as string;
                System.Diagnostics.Debug.WriteLine(dataReader["EVT_DATE"] + "");
                day.evt_date = DateTime.Parse(dataReader["EVT_DATE"] + "");
                day.role = Convert.ToInt16(dataReader["TERM_ROLE_ID"]);

                eventDetails.listOfSession[count].listOfUnjoinedDays.Add(day);
                

                // infinite loop to determine if a session event role is not valid
                Boolean loop = true;
                while(loop)
                {
                    if(day.role > eventDetails.listOfSession[count].listOfEventRoles[roleCount].roleId)
                    {
                        if (roleExist == false)
                        {
                            eventDetails.listOfSession[count].listOfEventRoles.RemoveAt(roleCount);
                        }
                        else
                        {
                            roleCount++;
                            roleExist = false;
                        }
                    }
                    if(day.role == eventDetails.listOfSession[count].listOfEventRoles[roleCount].roleId)
                    {
                        roleExist = true;
                        loop = false;
                    }
                }              
            }
            dataReader.Close();
        }


        /*
        * This method retrieve all the dates in which a volunteer is currently tagged to  
        */
        public static List<DateTime> getVolunteerSchedule(SqlConnection dataConnection, String userId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.DATE as DATE from TABLE_USR_SCHED t1, TABLE_USR_EVENT_SESSION t2, TABLE_USR_EVENT t3 
                                  WHERE t1.SCHED_ID = t2.SCHED_ID
                                  AND t2.USR_EVT_ID = t3.USR_EVT_ID
                                  AND t3.USR_ID = @userId
                                  ORDER BY t1.DATE;";

            SqlParameter userParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userParam.Value = new Guid(userId);
            command.Parameters.Add(userParam);

            SqlDataReader reader = command.ExecuteReader();
            List<DateTime> listOfDates = new List<DateTime>();

            while (reader.Read())
            {
                DateTime date = DateTime.Parse(reader["DATE"] + "");
                listOfDates.Add(date);
            }
            reader.Close();
            return listOfDates;
        }




        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< retrieve/ search event >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         *  This method retrieves a list of events ids that correspond to the events joined by a volunteer
         */
        public static List<KeyValuePair<String, List<String>>> getMyEvents(SqlConnection dataConnection, String userId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT EVT_ID, CONNECTION_STRING from TABLE_USR_EVENT WHERE USR_ID = @usr_id ORDER BY CONNECTION_STRING ASC;";

            SqlParameter usr_Id = new SqlParameter("usr_id", SqlDbType.UniqueIdentifier);
            usr_Id.Value = new Guid(userId);
            command.Parameters.Add(usr_Id);

            List<KeyValuePair<String, List<String>>> categoriseListOfVolEvents = new List<KeyValuePair<string, List<string>>>();
            List<String> listOfVolEvents = new List<String>();

            SqlDataReader dataReader = command.ExecuteReader();
            String connectionString = "";
            int count = 1;
            while (dataReader.Read())
            {
                if (count == 1)
                    connectionString = dataReader["CONNECTION_STRING"] as string;

                if (count != 1 && connectionString.Equals(dataReader["CONNECTION_STRING"]) == false)
                {
                    categoriseListOfVolEvents.Add(new KeyValuePair<string, List<string>>(connectionString, listOfVolEvents));
                    listOfVolEvents = new List<string>();
                    connectionString = dataReader["CONNECTION_STRING"] as string;
                }
                listOfVolEvents.Add(dataReader["EVT_ID"].ToString());

                count++;
            }
            dataReader.Close();
            return categoriseListOfVolEvents;
        }

        /*
         * This method gets a list of events joined by a volunteer from a aprticular NPO 
         */
        public static List<EventDetails> getJoinEventDetails(SqlConnection dataConnection, string connectionString, List<String> listOfEventIds)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * from TABLE_EVT_DETAILS WHERE ";

            int count = 1;
            foreach(String eventId in listOfEventIds)
            {
                if (count == 1)
                    command.CommandText += "EVT_ID = @eventId" + count + " ";
                else
                    command.CommandText += "AND EVT_ID = @eventId" + count + " ";

                SqlParameter eventIdParam = new SqlParameter("eventId" + count, SqlDbType.UniqueIdentifier);
                eventIdParam.Value = new Guid(eventId);
                command.Parameters.Add(eventIdParam);   
            }
            command.CommandText += ";";
            List<EventDetails> listOfEventDetails = new List<EventDetails>();
            SqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                EventDetails eventDetails = new EventDetails();
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                System.Diagnostics.Debug.WriteLine("startDate: " + startDateInString);
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();
                eventDetails.connectionString = connectionString;

                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";


                listOfEventDetails.Add(eventDetails);
            }
            dataReader.Close();
            return listOfEventDetails;

        }


        /*           
         *  This method retireve a list of connection strings for volunteer to query the whole database for events
         */
        public static List<string> retrieveConnectionString(SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT CONNECTION_STRING from TABLE_ORG_DETAILS ORDER BY ORG_ID ASC";

            SqlDataReader dataReader = command.ExecuteReader();
            List<String> listOfConnectionString = new List<string>();

            while (dataReader.Read())
            {
                String connectionString = dataReader["CONNECTION_STRING"].ToString();
                listOfConnectionString.Add(connectionString);
            }

            dataReader.Close();
            return listOfConnectionString;
        }

        /*
         * This method retrieve a list of organiation name 
         */
        public static List<String> getOrgName(SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT ORG_NAME FROM TABLE_ORG_DETAILS ORDER BY ORG_ID ASC";

            SqlDataReader dataReader = command.ExecuteReader();
            List<String> listOfOrgName = new List<string>();

            while(dataReader.Read())
            {
                String orgName = dataReader["ORG_NAME"].ToString();
                listOfOrgName.Add(orgName);
            }
            dataReader.Close();
            return listOfOrgName;
        }


        /*
       * This temporary method retrieves a list of events for a volunteer
       */
        public static List<EventDetails> retrieveEventsList(SqlConnection dataConnection, String connectionString, String orgName)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * from TABLE_EVT_DETAILS";

            List<EventDetails> listOfEventDetails = new List<EventDetails>();

            SqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                EventDetails eventDetails = new EventDetails();
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();
                eventDetails.connectionString = connectionString;
                eventDetails.orgName = orgName;

                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";


                listOfEventDetails.Add(eventDetails);
            }
            dataReader.Close();

            return listOfEventDetails;
        }

        /*
         *  This method search for an event based on the event name entered
         */
        public static List<EventDetails> searchEvents(SqlConnection dataConnection, String connectionString, SearchEvent searchParams, String orgName)
        {
            List<EventDetails> listOfEventDetails = new List<EventDetails>();
            SqlCommand command = new SqlCommand(null, dataConnection);

            if (searchParams.eventCause == null)
                command.CommandText = "SELECT * from TABLE_EVT_DETAILS t1 WHERE ";
            else
                command.CommandText = "SELECT * FROM TABLE_EVT_DETAILS t1, TABLE_EVT_CAUSE t2 WHERE t1.EVT_ID = t2.EVT_ID ";

            int causeCount = 1;
            if (searchParams.eventCause != null)
            {
                foreach(int cause in searchParams.eventCause)
                {
                    if (causeCount == 1)
                        command.CommandText += "AND (";
                    else
                        command.CommandText += "OR ";

                    command.CommandText += " t2.CAUSE_ID = @causeId" + causeCount + " ";
                    SqlParameter causeParam = new SqlParameter("causeId"+causeCount, SqlDbType.Int);
                    causeParam.Value = cause;
                    command.Parameters.Add(causeParam);
                    causeCount++;
                }
                command.CommandText += ") ";            
            }

            if (searchParams.noDate == true)
            {
                if (causeCount > 1)
                    command.CommandText += "AND ";
               
                command.CommandText += "t1.STARTDATE BETWEEN @startDatee AND @endDate";

                SqlParameter startDateParam = new SqlParameter("startDatee", SqlDbType.Date);
                startDateParam.Value = searchParams.startDate;
                command.Parameters.Add(startDateParam);

                SqlParameter endDateParam = new SqlParameter("endDate", SqlDbType.Date);
                endDateParam.Value = searchParams.endDate;
                command.Parameters.Add(endDateParam);
            }
            
                   
            SqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                EventDetails eventDetails = new EventDetails();
                eventDetails.eventId = dataReader["EVT_ID"].ToString();
                System.Diagnostics.Debug.WriteLine("eventId: " + eventDetails.eventId);
                eventDetails.eventName = dataReader["EVT_NAME"].ToString();

                String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                System.Diagnostics.Debug.WriteLine(eventDetails.startDate);

                String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);

                eventDetails.description = dataReader["EVT_DESC"].ToString();
                eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";

                eventDetails.connectionString = connectionString;
                eventDetails.orgName = orgName;

                listOfEventDetails.Add(eventDetails);
            }
            dataReader.Close();

            return listOfEventDetails;
        }

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< retrieve matched event >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*          
         *  This method returns an event in which a volunteer is invited to 
         */
        public static List<AcceptedSession> getInvitedEvent (SqlConnection databaseConnection, String volId, String eventId)
        {
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "SELECT t1.CONNECTION_STRING, t2.SESSION_iD, t2.ROLES, t2.DATES, t2.[PERCENT] FROM " +
                                   "TABLE_USR_EVENT_INVIT t1, " +
                                   "TABLE_USR_SESSION_INVIT t2 " +
                                   "WHERE t1.INVIT_ID = t2.INVIT_ID " +
                                   "AND t1.EVT_ID = @evtId " +
                                   "AND t1.USR_ID = @usrId; ";

            SqlParameter eventIdParam = new SqlParameter("evtId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventId);
            command.Parameters.Add(eventIdParam);

            SqlParameter volIdParam = new SqlParameter("usrId", SqlDbType.UniqueIdentifier);
            volIdParam.Value = new Guid(volId);
            command.Parameters.Add(volIdParam);

            List<AcceptedSession> listOfAcceptedSession = new List<AcceptedSession>();
            SqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                AcceptedSession acceptSession = new AcceptedSession();
                acceptSession.connectionString = reader["CONNECTION_STRING"].ToString();
                acceptSession.sessionId = reader["SESSION_ID"].ToString();
                acceptSession.sessionRoles = reader["ROLES"].ToString();
                acceptSession.sessionDates = reader["DATES"].ToString();
                acceptSession.percent = reader["PERCENT"].ToString();
                listOfAcceptedSession.Add(acceptSession);
            }

            reader.Close();
            return listOfAcceptedSession;
        }

    

        /*
         * This method retrieves all the role names of the roles that a volunter is invited to join 
         */
        public static void getInvitedRoleNames(SqlConnection dataConnection, int[] acceptedRoles, SessionDetails sessionDetail, List<String> listOfEventRole, int [] gainedPercent)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t2.TERM_ROLE_ID, t2.ROLE_DESC, t2.EVT_ROLE_ID FROM TABLE_EVT_SESSION_ROLE t2                                    
                                    WHERE t2.EVT_SESSION_ID = @sessionId AND ( ";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(sessionDetail.sessionId);
            command.Parameters.Add(sessionIdParam);

            int count = 1;
            foreach(int role in acceptedRoles)
            {
                if (count != 1)
                    command.CommandText += "OR ";
                                
                command.CommandText += " t2.TERM_ROLE_ID = @roleId" + count + " ";
                SqlParameter roleParam = new SqlParameter("roleId" + count, SqlDbType.Int);
                roleParam.Value = role;
                command.Parameters.Add(roleParam);
                count++;
            }
            command.CommandText += ")";

            SqlDataReader dataReader = command.ExecuteReader();

            while(dataReader.Read())
            {
                // brute force this first, super ineeficient
                for(int roleCount =0; roleCount < acceptedRoles.Length; roleCount++)
                {
                    if (acceptedRoles[roleCount] == Convert.ToInt16(dataReader["TERM_ROLE_ID"]))
                    {
                        SessionRoles sessionRole = new SessionRoles { roleId = acceptedRoles[roleCount], roleDesc = dataReader["ROLE_DESC"].ToString(), percent = gainedPercent[roleCount], evt_roleId = dataReader["EVT_ROLE_ID"].ToString() };

                        for(int eventRoleCount =0; eventRoleCount < listOfEventRole.Count; eventRoleCount++)
                        {
                            String[] roleArray = listOfEventRole[eventRoleCount].Split(',');
                            if (Int16.Parse(roleArray[0]) == sessionRole.roleId)
                            {
                                sessionRole.roleName = roleArray[1];
                                eventRoleCount = listOfEventRole.Count;
                            }
                        }
                        sessionDetail.listOfSessionRoles.Add(sessionRole);
                    }
                }                           
            }
            dataReader.Close();
        }

        /*
         * This method get the event and event session detailss 
         */
        public static void getInvitedEventDetails(SqlConnection dataConnection, EventDetails eventDetails)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "SELECT * FROM " +
                                   "TABLE_EVT_DETAILS t1, TABLE_EVT_SESSION t2 " +
                                   "WHERE t1.EVT_ID = t2.EVT_ID " +
                                   "AND ( ";

            String placeHolder = "@sessionId";
            int count = 0;
            foreach (SessionDetails sessionDetail in eventDetails.listOfSession)
            {
                if (count == 0)
                    command.CommandText += "t2.EVT_SESSION_ID = " + placeHolder + count;
                else
                    command.CommandText += " OR t2.EVT_SESSION_ID = " + placeHolder + count;

                SqlParameter sessionIdParam = new SqlParameter(placeHolder + count, SqlDbType.UniqueIdentifier);
                sessionIdParam.Value = new Guid(sessionDetail.sessionId);
                command.Parameters.Add(sessionIdParam);
                count++;
            }
            command.CommandText += " );";

            SqlDataReader dataReader = command.ExecuteReader();
            count = 0;
            while(dataReader.Read())
            {
                if(count == 0)
                {
                    eventDetails.eventName = dataReader["EVT_NAME"].ToString();
                    eventDetails.description = dataReader["EVT_DESC"].ToString();
                    eventDetails.startDate = DateTime.Parse(dataReader["STARTDATE"].ToString());
                    eventDetails.endDate = DateTime.Parse(dataReader["ENDDATE"].ToString());
                    eventDetails.imagePath = dataReader["EVT_IMAGE"] + "";
                }

                String sessionId = dataReader["EVT_SESSION_ID"].ToString();

                foreach(SessionDetails sessionDetail in eventDetails.listOfSession)
                {
                    if(sessionDetail.sessionId.Equals(sessionId))
                    {
                        sessionDetail.sessionName = dataReader["SESSION_NAME"].ToString();
                        sessionDetail.sessionDescription = dataReader["SESSION_DESC"].ToString();
                        sessionDetail.startTime = DateTime.Parse(dataReader["START_TIME"].ToString());
                        sessionDetail.endTime = DateTime.Parse(dataReader["END_TIME"].ToString());
                        sessionDetail.location = dataReader["LOCATION"].ToString();
                        sessionDetail.compulsoryDays = Int32.Parse(dataReader["COMPULSORY"].ToString());
                    }
                }               
            }
            dataReader.Close();
        }

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< delete event >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*
         * All volunteers who join this deleted event will have their connection to the event deleted
         * (1) Their shcedule data would be deleted
         * (2) Their event session data would be deleted 
         */
        public static void deleteEvent(SqlConnection dataConnection, String eventId, String connectionString)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_USR_EVENT WHERE EVT_ID = @eventId AND CONNECTION_STRING = @connectionString";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.UniqueIdentifier);
            eventIdParam.Value = new Guid(eventId);
            command.Parameters.Add(eventIdParam);

            SqlParameter connectionParam = new SqlParameter("connectionString", SqlDbType.NVarChar, 150);
            connectionParam.Value = connectionString;
            command.Parameters.Add(connectionParam);

            command.ExecuteNonQuery();

            deleteAllEventNotification(dataConnection, eventId);
        }

        /*
         *  This method would be called when an event is deleted.
         *  All event notifcation tagged to this event would be deleted 
         */
        private static void deleteAllEventNotification(SqlConnection dataConnection, String eventId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_USR_NOTIF WHERE NOTIFICATION LIKE @eventId ";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.NVarChar, 150);
            eventIdParam.Value = "%" + eventId +"%";
            command.Parameters.Add(eventIdParam);

            command.ExecuteNonQuery();
        }

        /*
         *  Delete the event notification from the event  
         */
        public static void deleteEventNotification(SqlConnection dataConnection, String eventId, String userId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "DELETE FROM TABLE_USR_NOTIF WHERE NOTIFICATION LIKE @eventId AND RECEIVER_ID = @userId";

            SqlParameter eventIdParam = new SqlParameter("eventId", SqlDbType.NVarChar, 150);
            eventIdParam.Value = "%" + eventId + "%";
            command.Parameters.Add(eventIdParam);

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            command.ExecuteNonQuery();
        }


        /*
        * This is a method by kf , to be use for SaaS home page, to retrieve list of recent event by organisation
        */
        public static List<EventDetails> retrieveRecentEventsListByOrg(SqlConnection dataConnection)
        {
            List<EventDetails> listOfEventDetails = new List<EventDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT EVT_ID, EVT_NAME, STARTDATE, ENDDATE, EVT_DESC, EVT_IMAGE , UNIQUE_LINK FROM TABLE_EVT_DETAILS WHERE STARTDATE > GETDATE() ORDER BY STARTDATE desc";

                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.eventId = dataReader["EVT_ID"].ToString();
                    eventDetails.eventName = dataReader["EVT_NAME"].ToString();
                    String startDateInString = ((DateTime)dataReader["STARTDATE"]).ToString("MM/dd/yyyy");
                    eventDetails.startDate = DateTime.ParseExact(startDateInString, "MM/dd/yyyy", null);
                    String endDateInString = ((DateTime)dataReader["ENDDATE"]).ToString("MM/dd/yyyy");
                    eventDetails.endDate = DateTime.ParseExact(endDateInString, "MM/dd/yyyy", null);
                    eventDetails.description = dataReader["EVT_DESC"].ToString();
                    eventDetails.imagePath = dataReader["EVT_IMAGE"].ToString();

                    listOfEventDetails.Add(eventDetails);
                }
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfEventDetails;
        }


        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< check in/ check out >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        
         /*
         * This method gets the event session in which a volunteer is suppose to attend today 
         */
        public static CheckIn getTodayEvent(SqlConnection dataConnection, string userId, DateTime currentDate)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.CONNECTION_STRING, t2.SESSION_ID, t3.DATE FROM 
                                    TABLE_USR_EVENT t1, TABLE_USR_EVENT_SESSION t2, TABLE_USR_SCHED t3
                                    WHERE t1.USR_EVT_ID =  t2.USR_EVT_ID
                                    AND t2.SCHED_ID = t3.SCHED_ID
                                    AND t1.USR_ID = @userId
                                    AND t3.DATE = @currentDate";

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            SqlParameter currentDateParam = new SqlParameter("currentDate", SqlDbType.Date);
            currentDateParam.Value = currentDate;
            command.Parameters.Add(currentDateParam);

            SqlDataReader reader = command.ExecuteReader();
            CheckIn checkIn = new CheckIn();
            while(reader.Read())
            {
                checkIn.connectionString = reader["CONNECTION_STRING"] + "";
                String dateString = ((DateTime)reader["DATE"]).ToString("MM/dd/yyyy");
                checkIn.date = DateTime.ParseExact(dateString, "MM/dd/yyyy", null);
                checkIn.sessionId = reader["SESSION_ID"] + "";
            }

            reader.Close();
            return checkIn;
        }

        /*
         * This method gets today event location
         */
        public static void getTodayEventLocation(SqlConnection dataConnection, CheckIn checkIn)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"SELECT t1.POSTAL, t2.SESSIONDAY_ID 
                                    FROM TABLE_EVT_SESSION t1, TABLE_EVT_SESSION_DAY t2
                                    WHERE t1.EVT_SESSION_ID = t2.EVT_SESSION_ID
                                    AND t1.EVT_SESSION_ID = @sessionId
                                    AND t2.EVT_DATE = @date";

            SqlParameter sessionIdParam = new SqlParameter("sessionId", SqlDbType.UniqueIdentifier);
            sessionIdParam.Value = new Guid(checkIn.sessionId);
            System.Diagnostics.Debug.WriteLine("sessionId: " + checkIn.sessionId);
            command.Parameters.Add(sessionIdParam);

            SqlParameter dateParam = new SqlParameter("date", SqlDbType.DateTime);
            dateParam.Value = checkIn.date;
            command.Parameters.Add(dateParam);

            SqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                checkIn.postalCode = Convert.ToInt32(reader["POSTAL"]);
                checkIn.sessionDayId = reader["SESSIONDAY_ID"] + "";
            }
            reader.Close();
        }

        /*
         * Thid method checks in the volunteer 
         */
        public static void checkInVol(SqlConnection dataConnection, CheckIn checkIn, DateTime currentTime, string userId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"UPDATE TABLE_VOL_EVT_ROLE SET TIME_IN = @timeIn 
                                    WHERE USR_ID = @userId
                                    AND SESSION_DAY_ID = @sessionDayId";

            SqlParameter timeInParam = new SqlParameter("timeIn", SqlDbType.DateTime);
            timeInParam.Value = currentTime;
            command.Parameters.Add(timeInParam);

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            SqlParameter sessionDayParam = new SqlParameter("sessionDayId", SqlDbType.UniqueIdentifier);
            sessionDayParam.Value = new Guid(checkIn.sessionDayId);
            command.Parameters.Add(sessionDayParam);

            command.ExecuteNonQuery();
        }

        /*
         * This method checks out the volunteer 
         */
        public static void checkOutVol(SqlConnection dataConnection, CheckIn checkIn, DateTime currentTime, string userId)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = @"UPDATE TABLE_VOL_EVT_ROLE SET TIME_OUT = @timeOut 
                                    WHERE USR_ID = @userId
                                    AND SESSION_DAY_ID = @sessionDayId";

            SqlParameter timeOutParam = new SqlParameter("timeOut", SqlDbType.DateTime);
            timeOutParam.Value = currentTime;
            command.Parameters.Add(timeOutParam);

            SqlParameter userIdParam = new SqlParameter("userId", SqlDbType.UniqueIdentifier);
            userIdParam.Value = new Guid(userId);
            command.Parameters.Add(userIdParam);

            SqlParameter sessionDayParam = new SqlParameter("sessionDayId", SqlDbType.UniqueIdentifier);
            sessionDayParam.Value = new Guid(checkIn.sessionDayId);
            command.Parameters.Add(sessionDayParam);

            command.ExecuteNonQuery();

        }

    }
}