﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.AccountMgmt
{
    public class AccountMgmtDL
    {

       /*
       * This methods gets the salt out of the database for password authentication
       * Returns an empty salt if there is no email match   
       */
        public static String getSalt(SqlConnection databaseConnection, String tableName, String userName)
        {
            String salt = "";
            System.Diagnostics.Debug.WriteLine("userName: " + userName);

            try { 

            if (databaseConnection.State != System.Data.ConnectionState.Open)
            {
                databaseConnection.Open();
            }

            
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandTimeout = 300;
            command.CommandText = "Select registration_date from " + tableName + " Where EMAIL = @user";
            SqlParameter userParam = new SqlParameter("user", SqlDbType.NVarChar, 100);
            userParam.Value = userName;
            command.Parameters.Add(userParam);

            command.CommandTimeout = 300;

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                salt = reader[0].ToString().Trim();     
                System.Diagnostics.Debug.WriteLine("salt:" + salt);
            }
            reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error @ get salt:" + e.Message);
            }
            return salt;
        }

        /*Get salt by resetpw_code to reset password*/
        public static String getSaltByResetCode(SqlConnection databaseConnection,String tablename, String resetpw_code)
        {
            String salt = "";
            try
            {
                SqlCommand command = new SqlCommand(null, databaseConnection);
                command.CommandText = "Select registration_date from " + tablename + " Where RESETPW_CODE = @RESETPW_CODE";
                SqlParameter RESETPW_CODE = new SqlParameter("RESETPW_CODE", SqlDbType.NVarChar, 100);
                RESETPW_CODE.Value = resetpw_code;
                command.Parameters.Add(RESETPW_CODE);

                command.Prepare();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    salt = reader[0].ToString();
                    System.Diagnostics.Debug.WriteLine("salt:" + salt);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error " + e.Message);
            }
            return salt;
        }

        /*
        * This method checks if a username and a password matches
        * 
        */
        public static String getPassword(String userName, String passwordAndSalt, SqlConnection dataConnection, string tableName)
        {
            String usr_ID = "";
            try
            {
                //String hashedPassWord = generateHashPassword(passwordAndSalt);
                SqlCommand command = new SqlCommand(null, dataConnection);

                command.CommandText = "Select USR_ID from " + tableName + " Where Email = @user AND password = @password";
                SqlParameter userParams = new SqlParameter("user", SqlDbType.NVarChar, 100);
                SqlParameter passwordParams = new SqlParameter("password", SqlDbType.NVarChar, 100);

                System.Diagnostics.Debug.WriteLine("userName: " + userName + " password: " + passwordAndSalt);

                userParams.Value = userName;
                passwordParams.Value = passwordAndSalt;
                command.Parameters.Add(userParams);
                command.Parameters.Add(passwordParams);

                command.Prepare();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    usr_ID = reader["USR_ID"].ToString();
                }

                reader.Close();
            }  
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error " + e.Message);
            }

            return usr_ID;
        }

        /*check existing email*/
        public static Boolean getEmailExist(SqlConnection dataConnection,string tablename, string EMAIL)
        {
            SqlCommand command = new SqlCommand("Select * from " + tablename + " Where Email = @EMAIL;", dataConnection);
            SqlParameter EMAILpara = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            EMAILpara.Value = EMAIL;
            command.Parameters.Add(EMAILpara);

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read() == false)
            {
                reader.Close();
                return false;
            }
            else {
                reader.Close();
                return true;
            }
            
        }

        /*
         *  this method returns the user details of a facebook login user
        */
        public static String getFacebookUserData(String fbID, SqlConnection dataConnection)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "Select USR_ID from TABLE_USR_DETAILS where FACEBOOK_ID =@FACEBOOK_ID";
            SqlParameter fbid = new SqlParameter("FACEBOOK_ID", SqlDbType.NVarChar, 100);
            fbid.Value = fbID;
            command.Parameters.Add(fbid);

            SqlDataReader dataReader = command.ExecuteReader();

            String usr_Id = "";
            while (dataReader.Read())
            {
                usr_Id = dataReader["USR_ID"].ToString();              
            }
            dataReader.Close();
            return usr_Id;
        }

        /*volunteer register */
        public static string createAccNoFB(SqlConnection dbConnection, Volunteer volunteer)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_USR_DETAILS (USR_ID, FULL_NAME, SCHL_ID, EMAIL, NRIC, PHONE_NO, DOB, REGISTRATION_DATE, DIETARY, PERSONALITY_CHAR, RELIGION, PASSWORD, IMAGE) OUTPUT INSERTED.USR_ID VALUES (NEWID(), @FULL_NAME, @SCHL_ID, @EMAIL, @NRIC, @PHONE_NO, @DOB, @REGISTRATION_DATE, @DIETARY, @PERSONALITY_CHAR, @RELIGION,@PASSWORD, @IMAGE)";
            SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
            PASSWORD.Value = volunteer.PASSWORD;
            command.Parameters.Add(PASSWORD);
            SqlParameter FULL_NAME = new SqlParameter("FULL_NAME", SqlDbType.NVarChar, 100);
            FULL_NAME.Value = volunteer.FULL_NAME;
            command.Parameters.Add(FULL_NAME);
            SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);

            if (volunteer.SCHL_ID == "")
            {
               
                SCHL_ID.Value = DBNull.Value;
            }
            else
            {
                SCHL_ID.Value = volunteer.SCHL_ID;
            }
            command.Parameters.Add(SCHL_ID);
            SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            EMAIL.Value = volunteer.EMAIL;
            command.Parameters.Add(EMAIL);
            SqlParameter NRIC = new SqlParameter("NRIC", SqlDbType.NVarChar, 100);
            NRIC.Value = volunteer.NRIC;
            command.Parameters.Add(NRIC);
            SqlParameter PHONE_NO = new SqlParameter("PHONE_NO", SqlDbType.NVarChar, 100);
            PHONE_NO.Value = volunteer.PHONE_NO;
            command.Parameters.Add(PHONE_NO);
            SqlParameter DOB = new SqlParameter("DOB", SqlDbType.DateTime);
            DOB.Value = volunteer.DOB;
            command.Parameters.Add(DOB);
            SqlParameter REGISTRATION_DATE = new SqlParameter("REGISTRATION_DATE", SqlDbType.DateTime);
            REGISTRATION_DATE.Value = volunteer.REGISTRATION_DATE;
            command.Parameters.Add(REGISTRATION_DATE);
            SqlParameter DIETARY = new SqlParameter("DIETARY", SqlDbType.NVarChar, 100);
            DIETARY.Value = volunteer.DIETARY;
            command.Parameters.Add(DIETARY);
            SqlParameter PERSONALITY_CHAR = new SqlParameter("PERSONALITY_CHAR", SqlDbType.NVarChar, 20);

            if (volunteer.PERSONALITY_CHAR == "")
            {

                PERSONALITY_CHAR.Value = DBNull.Value;
            }
            else
            {
                PERSONALITY_CHAR.Value = volunteer.PERSONALITY_CHAR;
            }           
                command.Parameters.Add(PERSONALITY_CHAR);
                SqlParameter RELIGION = new SqlParameter("RELIGION", SqlDbType.NVarChar, 100);
                RELIGION.Value = volunteer.RELIGION;
                command.Parameters.Add(RELIGION);

            SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 350);
            IMAGE.Value = "images/users/822eb246edb84e3f8c2a0237331d5255.png";
            command.Parameters.Add(IMAGE);

            string volid = command.ExecuteScalar().ToString();         
            return volid;          
        }

        /*volunteer register */
        public static string createAccFB(SqlConnection dbConnection, Volunteer volunteer)
        {
            string volid = "";
            try
            {
                SqlCommand command = new SqlCommand(null, dbConnection);
                command.CommandText = "INSERT INTO TABLE_USR_DETAILS (USR_ID, FULL_NAME, SCHL_ID, EMAIL, NRIC, PHONE_NO, DOB, REGISTRATION_DATE, DIETARY, PERSONALITY_CHAR, RELIGION, FACEBOOK_ID, IMAGE) OUTPUT INSERTED.USR_ID VALUES (NEWID(), @FULL_NAME, @SCHL_ID, @EMAIL, @NRIC, @PHONE_NO, @DOB, @REGISTRATION_DATE, @DIETARY, @PERSONALITY_CHAR, @RELIGION,@FACEBOOK_ID, @IMAGE)";
                SqlParameter FACEBOOK_ID = new SqlParameter("FACEBOOK_ID", SqlDbType.NVarChar, 100);
                FACEBOOK_ID.Value = volunteer.FACEBOOK_ID;
                command.Parameters.Add(FACEBOOK_ID);
                SqlParameter FULL_NAME = new SqlParameter("FULL_NAME", SqlDbType.NVarChar, 100);
                FULL_NAME.Value = volunteer.FULL_NAME;
                command.Parameters.Add(FULL_NAME);
                SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);

                if (volunteer.SCHL_ID == "")
                {

                    SCHL_ID.Value = DBNull.Value;
                }
                else
                {
                    SCHL_ID.Value = volunteer.SCHL_ID;
                }
                command.Parameters.Add(SCHL_ID);
                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
                EMAIL.Value = volunteer.EMAIL;
                command.Parameters.Add(EMAIL);
                SqlParameter NRIC = new SqlParameter("NRIC", SqlDbType.NVarChar, 100);
                NRIC.Value = volunteer.NRIC;
                command.Parameters.Add(NRIC);
                SqlParameter PHONE_NO = new SqlParameter("PHONE_NO", SqlDbType.NVarChar, 100);
                PHONE_NO.Value = volunteer.PHONE_NO;
                command.Parameters.Add(PHONE_NO);
                SqlParameter DOB = new SqlParameter("DOB", SqlDbType.DateTime);
                DOB.Value = volunteer.DOB;
                command.Parameters.Add(DOB);
                SqlParameter REGISTRATION_DATE = new SqlParameter("REGISTRATION_DATE", SqlDbType.DateTime);
                REGISTRATION_DATE.Value = volunteer.REGISTRATION_DATE;
                command.Parameters.Add(REGISTRATION_DATE);
                SqlParameter DIETARY = new SqlParameter("DIETARY", SqlDbType.NVarChar, 100);
                DIETARY.Value = volunteer.DIETARY;
                command.Parameters.Add(DIETARY);
                SqlParameter PERSONALITY_CHAR = new SqlParameter("PERSONALITY_CHAR", SqlDbType.NVarChar, 20);

                if (volunteer.PERSONALITY_CHAR == "")
                {

                    PERSONALITY_CHAR.Value = DBNull.Value;
                }
                else
                {
                    PERSONALITY_CHAR.Value = volunteer.PERSONALITY_CHAR;
                }
                command.Parameters.Add(PERSONALITY_CHAR);
                SqlParameter RELIGION = new SqlParameter("RELIGION", SqlDbType.NVarChar, 100);
                RELIGION.Value = volunteer.RELIGION;
                command.Parameters.Add(RELIGION);

                SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 350);
                IMAGE.Value = "images/users/822eb246edb84e3f8c2a0237331d5255.png";
                command.Parameters.Add(IMAGE);

                volid = command.ExecuteScalar().ToString();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error " + e.Message);
            }
            return volid;
        }

        /*update account activation code */
        public static string updateActivationCode(SqlConnection dbConnection, string volid)
        {
            string activationCode = Guid.NewGuid().ToString();
            try
            {
              
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET ACTIVATION_CODE = @ACTIVATION_CODE Where USR_ID= @volid", dbConnection);
                SqlParameter ACTIVATION_CODE = new SqlParameter("ACTIVATION_CODE", SqlDbType.NVarChar, 100);
                ACTIVATION_CODE.Value = activationCode;
                command.Parameters.Add(ACTIVATION_CODE);
                SqlParameter volidPara = new SqlParameter("volid", SqlDbType.NVarChar, 100);
                volidPara.Value = volid;
                command.Parameters.Add(volidPara);
                SqlDataReader reader = command.ExecuteReader();              
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return activationCode;
        }

        /*update account activated*/
        public static int updateAccActivated(SqlConnection dbConnection, string activationCode)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET ACTIVATION_CODE = NULL Where ACTIVATION_CODE= @ACTIVATION_CODE", dbConnection);
                SqlParameter ACTIVATION_CODE = new SqlParameter("ACTIVATION_CODE", SqlDbType.NVarChar, 100);
                ACTIVATION_CODE.Value = activationCode;
                command.Parameters.Add(ACTIVATION_CODE);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*get volunteer particulars*/
        public static Volunteer getVolParticular(SqlConnection dbConnection, string usr_id)
        {
            Volunteer volunteer=new Volunteer();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_USR_DETAILS U LEFT JOIN TABLE_SCHL_MASTER S ON U.SCHL_ID=S.SCHL_ID LEFT JOIN TABLE_LVL_OF_EDU L ON L.LVL_OF_EDU_ID=S.LVL_OF_EDU_ID WHERE USR_ID=@USR_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
       
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    volunteer.ACTIVATION_CODE = reader["ACTIVATION_CODE"].ToString();
                    volunteer.DIETARY = reader["DIETARY"].ToString();
                    volunteer.DOB = Convert.ToDateTime(reader["DOB"]);
                    volunteer.EMAIL = reader["EMAIL"].ToString();
                    volunteer.FACEBOOK_ID = reader["FACEBOOK_ID"].ToString();
                    volunteer.FULL_NAME = reader["FULL_NAME"].ToString();
                    volunteer.NRIC = reader["NRIC"].ToString();
                    volunteer.PASSWORD = reader["PASSWORD"].ToString();
                    volunteer.PERSONALITY_CHAR = reader["PERSONALITY_CHAR"].ToString();
                    volunteer.PHONE_NO = reader["PHONE_NO"].ToString();
                    volunteer.VOL_ID = reader["USR_ID"].ToString();
                    volunteer.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    volunteer.RELIGION = reader["RELIGION"].ToString();
                    volunteer.SCHL_ID = reader["SCHL_ID"].ToString();
                    volunteer.VIA_TARGET = reader["VIA_TARGET"].ToString();
                    volunteer.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    volunteer.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    volunteer.SCH_LVL = reader["LVL"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---AccMgmtDAL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getVolParticular---");
                System.Diagnostics.Debug.WriteLine("---Error: " + e.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            
            return volunteer;
        }

        /*update reset password code */
        public static void updateResetPWCode(SqlConnection dbConnection, string tablename, string email, string resetPwCode)
        {          
            try
            {

                SqlCommand command = new SqlCommand("UPDATE " + tablename + " SET RESETPW_CODE = @RESETPW_CODE Where EMAIL= @email", dbConnection);
                SqlParameter RESETPW_CODE = new SqlParameter("RESETPW_CODE", SqlDbType.NVarChar, 100);
                RESETPW_CODE.Value = resetPwCode;
                command.Parameters.Add(RESETPW_CODE);
                SqlParameter emailPara = new SqlParameter("email", SqlDbType.NVarChar, 100);
                emailPara.Value = email;
                command.Parameters.Add(emailPara);
                SqlDataReader reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---AccountMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: updateResetPWCode---");
                System.Diagnostics.Debug.WriteLine("---Error: " + e.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        /*Volunteer reset password */
        public static Boolean updateResetPW(SqlConnection dbConnection, string tablename,string reset_code, string password)
        {
            Boolean changePwSuccess = false;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE "+ tablename + " SET PASSWORD = @PASSWORD , RESETPW_CODE=NULL Where RESETPW_CODE= @RESETPW_CODE", dbConnection);
                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
                PASSWORD.Value = password;
                command.Parameters.Add(PASSWORD);
                SqlParameter RESETPW_CODE = new SqlParameter("RESETPW_CODE", SqlDbType.NVarChar, 100);
                RESETPW_CODE.Value = reset_code;
                command.Parameters.Add(RESETPW_CODE);
                if (command.ExecuteNonQuery() != 0)
                {
                    changePwSuccess = true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---AccountMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: resetPW---");
                System.Diagnostics.Debug.WriteLine("---Error: " + e.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return changePwSuccess;
        }

        /*Volunteer change password */
        public static Boolean updatePW(SqlConnection dbConnection, string tablename, string email, string newpassword, string oldpassword)
        {
            Boolean changePwSuccess = false;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE " + tablename + " SET PASSWORD = @PASSWORD Where EMAIL= @EMAIL AND PASSWORD=@OLDPASSWORD", dbConnection);
                SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
                PASSWORD.Value = newpassword;
                command.Parameters.Add(PASSWORD);
                SqlParameter OLDPASSWORD = new SqlParameter("OLDPASSWORD", SqlDbType.NVarChar, 100);
                OLDPASSWORD.Value = oldpassword;
                command.Parameters.Add(OLDPASSWORD);
                SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
                EMAIL.Value = email;
                command.Parameters.Add(EMAIL);
                if (command.ExecuteNonQuery()!=0) {
                    changePwSuccess = true;
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---AccountMgmtDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: changePW---");
                System.Diagnostics.Debug.WriteLine("---Error: " + e.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return changePwSuccess;
        }

      
        /*Update Account detail*/
        public static int updateAccDetail(SqlConnection dbConnection, Volunteer volunteer)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET FULL_NAME = @FULL_NAME, SCHL_ID=@SCHL_ID,NRIC=@NRIC,PHONE_NO=@PHONE_NO,DOB=@DOB,DIETARY=@DIETARY,PERSONALITY_CHAR=@PERSONALITY_CHAR,RELIGION=@RELIGION Where USR_ID= @USR_ID", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = volunteer.VOL_ID;
                command.Parameters.Add(USR_ID);
                SqlParameter FULL_NAME = new SqlParameter("FULL_NAME", SqlDbType.NVarChar, 100);
                FULL_NAME.Value = volunteer.FULL_NAME;
                command.Parameters.Add(FULL_NAME);
                SqlParameter SCHL_ID = new SqlParameter("SCHL_ID", SqlDbType.NVarChar, 100);
                if (volunteer.SCHL_ID == "")
                {

                    SCHL_ID.Value = DBNull.Value;
                }
                else
                {
                    SCHL_ID.Value = volunteer.SCHL_ID;
                }
                command.Parameters.Add(SCHL_ID);
                SqlParameter NRIC = new SqlParameter("NRIC", SqlDbType.NVarChar, 100);
                NRIC.Value = volunteer.NRIC;
                command.Parameters.Add(NRIC);
                SqlParameter PHONE_NO = new SqlParameter("PHONE_NO", SqlDbType.NVarChar, 100);
                PHONE_NO.Value = volunteer.PHONE_NO;
                command.Parameters.Add(PHONE_NO);
                SqlParameter DOB = new SqlParameter("DOB", SqlDbType.DateTime);
                DOB.Value = volunteer.DOB;
                command.Parameters.Add(DOB);
                SqlParameter DIETARY = new SqlParameter("DIETARY", SqlDbType.NVarChar, 100);
                DIETARY.Value = volunteer.DIETARY;
                command.Parameters.Add(DIETARY);
                SqlParameter PERSONALITY_CHAR = new SqlParameter("PERSONALITY_CHAR", SqlDbType.NVarChar, 20);

                if (volunteer.PERSONALITY_CHAR == "")
                {

                    PERSONALITY_CHAR.Value = DBNull.Value;
                }
                else
                {
                    PERSONALITY_CHAR.Value = volunteer.PERSONALITY_CHAR;
                }
                command.Parameters.Add(PERSONALITY_CHAR);
                SqlParameter RELIGION = new SqlParameter("RELIGION", SqlDbType.NVarChar, 100);
                RELIGION.Value = volunteer.RELIGION;
                command.Parameters.Add(RELIGION);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

       
        /*set VIA goals */
        //public static void setVIAgoal(SqlConnection dbConnection, string volid,string via_target)
        //{       
        //    try
        //    {

        //        SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET VIA_TARGET = @VIA_TARGET Where USR_ID= @volid", dbConnection);
        //        SqlParameter VIA_TARGET = new SqlParameter("VIA_TARGET", SqlDbType.NVarChar, 100);
        //        VIA_TARGET.Value = via_target;
        //        command.Parameters.Add(VIA_TARGET);
        //        SqlParameter volidPara = new SqlParameter("volid", SqlDbType.NVarChar, 100);
        //        volidPara.Value = volid;
        //        command.Parameters.Add(volidPara);
        //        SqlDataReader reader = command.ExecuteReader();
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine("ERROR:"+e.Message);
        //    }
        //}

        /*Get list of volunteers*/
        public static List<Volunteer> getVolunteers(SqlConnection dbConnection)
        {
            List<Volunteer> listOfVols = new List<Volunteer>();

            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_USR_DETAILS U LEFT JOIN TABLE_SCHL_MASTER S ON U.SCHL_ID=S.SCHL_ID ORDER BY FULL_NAME ASC;", dbConnection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Volunteer volunteer = new Volunteer();
                    volunteer.ACTIVATION_CODE = reader["ACTIVATION_CODE"].ToString();
                    volunteer.DIETARY = reader["DIETARY"].ToString();
                    volunteer.DOB = Convert.ToDateTime(reader["DOB"]);
                    volunteer.EMAIL = reader["EMAIL"].ToString();
                    volunteer.FACEBOOK_ID = reader["FACEBOOK_ID"].ToString();
                    volunteer.FULL_NAME = reader["FULL_NAME"].ToString();
                    volunteer.NRIC = reader["NRIC"].ToString();
                    volunteer.PASSWORD = reader["PASSWORD"].ToString();
                    volunteer.PERSONALITY_CHAR = reader["PERSONALITY_CHAR"].ToString();
                    volunteer.PHONE_NO = reader["PHONE_NO"].ToString();
                    volunteer.VOL_ID = reader["USR_ID"].ToString();
                    volunteer.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    volunteer.RELIGION = reader["RELIGION"].ToString();
                    volunteer.SCHL_ID = reader["SCHL_ID"].ToString();
                    volunteer.VIA_TARGET = reader["VIA_TARGET"].ToString();
                    volunteer.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    volunteer.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    listOfVols.Add(volunteer);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfVols;
        }

        /*Search for volunteer*/
        public static List<Volunteer> getVolbyNameSearch(SqlConnection dbConnection, string name)
        {
            List<Volunteer> listOfVols = new List<Volunteer>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_USR_DETAILS U LEFT JOIN TABLE_SCHL_MASTER S ON U.SCHL_ID=S.SCHL_ID WHERE FULL_NAME LIKE @NAME ORDER BY FULL_NAME ASC;", dbConnection);

                SqlParameter NAME = new SqlParameter("NAME", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@NAME", "%" + name + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Volunteer volunteer = new Volunteer();
                    volunteer.ACTIVATION_CODE = reader["ACTIVATION_CODE"].ToString();
                    volunteer.DIETARY = reader["DIETARY"].ToString();
                    volunteer.DOB = Convert.ToDateTime(reader["DOB"]);
                    volunteer.EMAIL = reader["EMAIL"].ToString();
                    volunteer.FACEBOOK_ID = reader["FACEBOOK_ID"].ToString();
                    volunteer.FULL_NAME = reader["FULL_NAME"].ToString();
                    volunteer.NRIC = reader["NRIC"].ToString();
                    volunteer.PASSWORD = reader["PASSWORD"].ToString();
                    volunteer.PERSONALITY_CHAR = reader["PERSONALITY_CHAR"].ToString();
                    volunteer.PHONE_NO = reader["PHONE_NO"].ToString();
                    volunteer.VOL_ID = reader["USR_ID"].ToString();
                    volunteer.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    volunteer.RELIGION = reader["RELIGION"].ToString();
                    volunteer.SCHL_ID = reader["SCHL_ID"].ToString();
                    volunteer.VIA_TARGET = reader["VIA_TARGET"].ToString();
                    volunteer.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    volunteer.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    listOfVols.Add(volunteer);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfVols;
        }

        /*Get Volunteer Past Events*/
        public static List<UserPastEvents> getUserPastEvents(SqlConnection dbConnection, string volID)
        {
            List<UserPastEvents> listOfPastEvents = new List<UserPastEvents>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT EVT_ID,CONNECTION_STRING FROM TABLE_USR_EVENT WHERE USR_ID = @USR_ID;", dbConnection);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@USR_ID", volID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPastEvents pastevents = new UserPastEvents();
                    pastevents.EVT_ID = reader["EVT_ID"].ToString();
                    pastevents.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    listOfPastEvents.Add(pastevents);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfPastEvents;
        }

        /*Get Volunteer Past Events Details*/
        public static List<UserPastEvents> getUserPastEventDetails(SqlConnection dbConnection, string eventID)
        {
            List<UserPastEvents> listOfPastEvents = new List<UserPastEvents>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT EVT_NAME,STARTDATE FROM TABLE_EVT_DETAILS WHERE EVT_ID = @EVT_ID;", dbConnection);

                SqlParameter EVT_ID = new SqlParameter("EVT_ID", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@EVT_ID", eventID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPastEvents pastevents = new UserPastEvents();
                    pastevents.EVT_NAME = reader["EVT_NAME"].ToString();
                    DateTime eventdate = DateTime.Parse(reader["STARTDATE"].ToString());
                    pastevents.STARTDATE = eventdate.Date;
                    listOfPastEvents.Add(pastevents);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfPastEvents;
        }

        //start here
        public static string updateVolVIA(SqlConnection dbConnection, string volid,int hours)
        {
            string activationCode = Guid.NewGuid().ToString();
            try
            {

                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET VIA_HOURS = @VIA_HOURS Where USR_ID= @volid", dbConnection);
                SqlParameter VIA_HOURS = new SqlParameter("VIA_HOURS", SqlDbType.Int);
                VIA_HOURS.Value = hours;
                command.Parameters.Add(VIA_HOURS);
                SqlParameter volidPara = new SqlParameter("volid", SqlDbType.NVarChar, 100);
                volidPara.Value = volid;
                command.Parameters.Add(volidPara);
                SqlDataReader reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error:" + e.Message);
            }
            return activationCode;
        }

        public static int getVolHours(SqlConnection databaseConnection, string volid)
        {
            int hour=0;
            SqlCommand command = new SqlCommand(null, databaseConnection);
            command.CommandText = "Select VIA_HOURS from TABLE_USR_DETAILS Where USR_ID = @volid";
            SqlParameter volidPara = new SqlParameter("volid", SqlDbType.NVarChar, 100);
            volidPara.Value = volid;
            command.Parameters.Add(volidPara);

            command.Prepare();
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                hour = Convert.ToInt32(reader[0]);
            }
            reader.Close();
            return hour;
        }

        public static int getEventVIA(SqlConnection dataConnection, string sessiondayId, string userId)
        {
            int hours = 0;
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT SUM((DATEDIFF(minute,TIME_IN,TIME_OUT))/60) AS TOTALVIA FROM TABLE_VOL_EVT_ROLE WHERE SESSION_DAY_ID=@SESSION_DAY_ID AND USR_ID=@USR_ID";
                SqlParameter SESSION_DAY_ID = new SqlParameter("SESSION_DAY_ID", SqlDbType.NVarChar, 100);
                SESSION_DAY_ID.Value = sessiondayId;
                command.Parameters.Add(SESSION_DAY_ID);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userId;
                command.Parameters.Add(USR_ID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["TOTALVIA"].Equals(DBNull.Value))
                    {
                        hours = Convert.ToInt32(reader["TOTALVIA"]);
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return hours;
        }
    }
}