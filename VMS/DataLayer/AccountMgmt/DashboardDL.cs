﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.DataLayer.AccountMgmt
{
    public class DashboardDL
    {
        public static List<UserPastEvents> getCompletedEvents(SqlConnection dbConnection, string volID)
        {
            List<UserPastEvents> listOfCompletedEvents = new List<UserPastEvents>();
            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_VOL_EVT_ROLE V INNER JOIN TABLE_EVT_SESSION_DAY SD ON SD.SESSIONDAY_ID=V.SESSION_DAY_ID INNER JOIN TABLE_EVT_SESSION S ON S.EVT_SESSION_ID=SD.EVT_SESSION_ID INNER JOIN TABLE_EVT_DETAILS E ON E.EVT_ID=S.EVT_ID WHERE USR_ID=@USR_ID" + " AND TIME_IN IS NOT NULL AND TIME_OUT IS NOT NULL;", dbConnection);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@USR_ID", volID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPastEvents pastevents = new UserPastEvents();
                    pastevents.EVT_ID = reader["EVT_ID"].ToString();
                    pastevents.EVT_DATE = DateTime.Parse(reader["EVT_DATE"].ToString()).Date;
                    pastevents.EVT_SESSION_ID = reader["EVT_SESSION_ID"].ToString();
                    pastevents.EVT_NAME = reader["EVT_NAME"].ToString();
                    pastevents.SESSION_NAME = reader["SESSION_NAME"].ToString();

                    pastevents.TIME_IN = DateTime.Parse(reader["TIME_IN"].ToString());
                    pastevents.TIME_OUT = DateTime.Parse(reader["TIME_OUT"].ToString());
                    listOfCompletedEvents.Add(pastevents);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }

            return listOfCompletedEvents;
        }

        //have upcoming and ongoing
        public static List<UserPastEvents> getUpcomingEvents(SqlConnection dbConnection, string volID)
        {
            List<UserPastEvents> listOfUpcomingEvents = new List<UserPastEvents>();
            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_VOL_EVT_ROLE V INNER JOIN TABLE_EVT_SESSION_DAY SD ON SD.SESSIONDAY_ID=V.SESSION_DAY_ID INNER JOIN TABLE_EVT_SESSION S ON S.EVT_SESSION_ID=SD.EVT_SESSION_ID INNER JOIN TABLE_EVT_DETAILS E ON E.EVT_ID=S.EVT_ID WHERE USR_ID=@USR_ID" + " AND TIME_IN IS NULL AND TIME_OUT IS NULL;", dbConnection);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@USR_ID", volID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPastEvents upcomingevents = new UserPastEvents();
                    upcomingevents.EVT_ID = reader["EVT_ID"].ToString();
                    upcomingevents.EVT_DATE = DateTime.Parse(reader["EVT_DATE"].ToString()).Date;
                    upcomingevents.EVT_SESSION_ID = reader["EVT_SESSION_ID"].ToString();
                    upcomingevents.EVT_NAME = reader["EVT_NAME"].ToString();
                    upcomingevents.SESSION_NAME = reader["SESSION_NAME"].ToString();
                    listOfUpcomingEvents.Add(upcomingevents);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }

            return listOfUpcomingEvents;
        }
        
        //get organisation that user is in
        public static List<string> getNPO(string volID)
        {
            List<string> organisationList = new List<string>();
            Database_Connect database = new Database_Connect("console_vms");
            SqlConnection dbConnection = database.getConnection();
            try
            {
                SqlCommand command = command = new SqlCommand("SELECT DISTINCT CONNECTION_STRING FROM TABLE_USR_EVENT WHERE USR_ID = @USR_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@USR_ID", volID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    organisationList.Add(reader["CONNECTION_STRING"].ToString());
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            
            database.closeDatabase();
            return organisationList;
        }

        //get via hours
        public static int getCompletedVIA(SqlConnection dataConnection, string usr_id)
        {
            int hours = 0;
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT SUM((DATEDIFF(minute,TIME_IN,TIME_OUT))/60) AS TOTALVIA FROM TABLE_VOL_EVT_ROLE WHERE USR_ID=@USR_ID AND TIME_IN IS NOT NULL AND TIME_OUT IS NOT NULL;";
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["TOTALVIA"].Equals(DBNull.Value))
                    {
                        hours = Convert.ToInt32(reader["TOTALVIA"]);
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return hours;
        }
    }
}