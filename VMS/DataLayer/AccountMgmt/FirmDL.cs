﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.AccountModels;

namespace VMS.DataLayer.AccountMgmt
{
    public class FirmDL
    {
        /*Create Account */
        public static string createAccount(SqlConnection dbConnection, Firm firm)
        {
            SqlCommand command = new SqlCommand(null, dbConnection);
            command.CommandText = "INSERT INTO TABLE_FIRM_DETAILS (USR_ID, FIRM_NAME,EMAIL,PASSWORD,REGISTRATION_DATE,ADDRESS,PHONE,FAX,FIRM_DESCRIPTION) OUTPUT INSERTED.USR_ID VALUES (NEWID(), @FIRM_NAME,@EMAIL,@PASSWORD,@REGISTRATION_DATE,@ADDRESS,@PHONE,@FAX,@FIRM_DESCRIPTION)";
            SqlParameter PASSWORD = new SqlParameter("PASSWORD", SqlDbType.NVarChar, 100);
            PASSWORD.Value = firm.PASSWORD;
            command.Parameters.Add(PASSWORD);
            SqlParameter FIRM_NAME = new SqlParameter("FIRM_NAME", SqlDbType.NVarChar, 100);
            FIRM_NAME.Value = firm.FIRM_NAME;
            command.Parameters.Add(FIRM_NAME);
            SqlParameter EMAIL = new SqlParameter("EMAIL", SqlDbType.NVarChar, 100);
            EMAIL.Value = firm.EMAIL;
            command.Parameters.Add(EMAIL);
            SqlParameter PHONE = new SqlParameter("PHONE", SqlDbType.NVarChar, 100);
            PHONE.Value = firm.PHONE;
            command.Parameters.Add(PHONE);
            SqlParameter FAX = new SqlParameter("FAX", SqlDbType.NVarChar, 100);
            FAX.Value = firm.FAX;
            command.Parameters.Add(FAX);
            SqlParameter REGISTRATION_DATE = new SqlParameter("REGISTRATION_DATE", SqlDbType.DateTime);
            REGISTRATION_DATE.Value = firm.REGISTRATION_DATE;
            command.Parameters.Add(REGISTRATION_DATE);
            SqlParameter ADDRESS = new SqlParameter("ADDRESS", SqlDbType.NVarChar, 100);
            ADDRESS.Value = firm.ADDRESS;
            command.Parameters.Add(ADDRESS);
            SqlParameter FIRM_DESCRIPTION = new SqlParameter("FIRM_DESCRIPTION", SqlDbType.NVarChar, 250);
            FIRM_DESCRIPTION.Value = firm.FIRM_DESCRIPTION;
            command.Parameters.Add(FIRM_DESCRIPTION);

            string volid = command.ExecuteScalar().ToString();
            return volid;
        }

        /*Update Account detail*/
        public static int updateAccDetail(SqlConnection dbConnection, Firm firm)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_FIRM_DETAILS SET FIRM_NAME = @FIRM_NAME, PHONE=@PHONE, FAX=@FAX,ADDRESS=@ADDRESS,FIRM_DESCRIPTION=@FIRM_DESCRIPTION Where USR_ID= @USR_ID", dbConnection);
                SqlParameter FIRM_NAME = new SqlParameter("FIRM_NAME", SqlDbType.NVarChar, 100);
                FIRM_NAME.Value = firm.FIRM_NAME;
                command.Parameters.Add(FIRM_NAME);
                SqlParameter PHONE = new SqlParameter("PHONE", SqlDbType.NVarChar, 100);
                PHONE.Value = firm.PHONE;
                command.Parameters.Add(PHONE);
                SqlParameter FAX = new SqlParameter("FAX", SqlDbType.NVarChar, 100);
                FAX.Value = firm.FAX;
                command.Parameters.Add(FAX);
                SqlParameter ADDRESS = new SqlParameter("ADDRESS", SqlDbType.NVarChar, 100);
                ADDRESS.Value = firm.ADDRESS;
                command.Parameters.Add(ADDRESS);
                SqlParameter FIRM_DESCRIPTION = new SqlParameter("FIRM_DESCRIPTION", SqlDbType.NVarChar, 250);
                FIRM_DESCRIPTION.Value = firm.FIRM_DESCRIPTION;
                command.Parameters.Add(FIRM_DESCRIPTION);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = firm.USR_ID;
                command.Parameters.Add(USR_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*Delete Account*/
        public static int deleteAcc(SqlConnection dbConnection, string usr_id)
        {
            int count = 0;
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_FIRM_DETAILS WHERE USR_ID=@USR_ID;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);
                count = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return count;
        }

        /*get employee detail*/
        public static Firm getFirmDetails(SqlConnection dbConnection, string usr_id)
        {
            Firm firm = new Firm();
            try
            {
                SqlCommand command = new SqlCommand("SELECT * FROM TABLE_FIRM_DETAILS WHERE USR_ID=@USR_ID ORDER BY FIRM_NAME ASC;", dbConnection);
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = usr_id;
                command.Parameters.Add(USR_ID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    firm.EMAIL = reader["EMAIL"].ToString();
                    firm.FIRM_NAME = reader["FIRM_NAME"].ToString();
                    firm.USR_ID = reader["USR_ID"].ToString();
                    firm.PASSWORD = reader["PASSWORD"].ToString();
                    firm.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    firm.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    firm.FAX= reader["FAX"].ToString();
                    firm.PHONE= reader["PHONE"].ToString();
                    firm.ADDRESS= reader["ADDRESS"].ToString();
                    firm.FIRM_DESCRIPTION = reader["FIRM_DESCRIPTION"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return firm;
        }

        /*Get list of firm details*/
        public static List<Firm> searchFirms(SqlConnection dbConnection, string firm_name)
        {
            List<Firm> listOfFirms = new List<Firm>();

            try
            {
                SqlCommand command = command = new SqlCommand("SELECT * FROM TABLE_FIRM_DETAILS WHERE FIRM_NAME LIKE @FIRM_NAME;", dbConnection);

                SqlParameter FIRM_NAME = new SqlParameter("FIRM_NAME", SqlDbType.NVarChar, 100);
                command.Parameters.AddWithValue("@FIRM_NAME", "%" + firm_name + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Firm firm = new Firm();
                    firm.EMAIL = reader["EMAIL"].ToString();
                    firm.FIRM_NAME = reader["FIRM_NAME"].ToString();
                    firm.USR_ID = reader["USR_ID"].ToString();
                    firm.PASSWORD = reader["PASSWORD"].ToString();
                    firm.REGISTRATION_DATE = Convert.ToDateTime(reader["REGISTRATION_DATE"].ToString());
                    firm.RESETPW_CODE = reader["RESETPW_CODE"].ToString();
                    firm.FAX = reader["FAX"].ToString();
                    firm.PHONE = reader["PHONE"].ToString();
                    firm.ADDRESS = reader["ADDRESS"].ToString();
                    firm.FIRM_DESCRIPTION = reader["FIRM_DESCRIPTION"].ToString();
                    listOfFirms.Add(firm);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR:" + e.Message);
            }
            return listOfFirms;
        }

    }
}
