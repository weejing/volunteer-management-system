﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace VMS.DataLayer.RelationshipMgmt
{
    public class RelationshipDL
    {
        //get avaliable payment plan
        public static void updateCRMStatus(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string str_sql = @"update TABLE_ORG_CRM 
set CRM_STATUS = 'EXPIRED'
where EXPIRED_DATE <= DATEADD(day, -1, convert(date, GETDATE()))
and CRM_STATUS = 'NOT USED'";

                SqlCommand command = new SqlCommand(str_sql, dbConnection);

                SqlDataReader dataReader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

            }
        }
    }
}