﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.CommunityModels;
using VMS.Models.OrgMgmtModels;

namespace VMS.DataLayer.CommunityMgmt
{
    public class UserPortfolioDL
    {
        /*DL : Get all user ID */
        public static List<String> getUserID(SqlConnection dataConnection)
        {
            List<String> userIDs = new List<String>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ID from TABLE_USR_DETAILS";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userIDs.Add(reader["USR_ID"].ToString());
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserID---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return userIDs;
        }


        /*DL : Get all user ID */
        public static List<UserPortfolio> getListofUsersBySearch(SqlConnection dataConnection, String name)
        {
            List<UserPortfolio> listofUsers = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ID, FULL_NAME, datediff(yy,dob,getdate()) as AGE, SCHL_NAME, IMAGE from TABLE_USR_DETAILS ud LEFT JOIN TABLE_SCHL_MASTER sm ON ud.SCHL_ID = SM.SCHL_ID WHERE FULL_NAME LIKE @0";
                command.Parameters.AddWithValue("@0", "%"+name+"%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio up = new UserPortfolio();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    //up.SCHL_NAME = reader["AGE"].ToString();
                    if (!reader["SCHL_NAME"].ToString().Equals(null))
                    {
                        up.SCHL_NAME = reader["SCHL_NAME"].ToString(); //+ " , " + reader["AGE"].ToString() + " years old"; 
                    }
                    else
                    {
                        up.SCHL_NAME = " "; //reader["AGE"].ToString() + " years old";
                    }
                    up.IMAGE = reader["IMAGE"].ToString();
                    listofUsers.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserID---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofUsers;
        }

        /*DL : Get user full name by ID */
        public static UserPortfolio getUserFullNameByID(SqlConnection dataConnection, String userID)
        {
            UserPortfolio up = new UserPortfolio();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select FULL_NAME from TABLE_USR_DETAILS WHERE USR_ID LIKE @0";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    up.USR_ID = userID;
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFullNameByID---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return up;
        }

        /*DL : Get a user details that are going to be displayed on the user portfolio*/
        public static UserPortfolio getUserData(SqlConnection dataConnection, String userID)
        {
            UserPortfolio userPortfolio = new UserPortfolio();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select FULL_NAME, SCHL_NAME, PERSONALITY_CHAR, DESCRIPTION, IMAGE from TABLE_USR_DETAILS U left join TABLE_SCHL_MASTER S on U.SCHL_ID = S.SCHL_ID where USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userPortfolio.USR_ID = userID;
                    userPortfolio.FULL_NAME = reader["FULL_NAME"].ToString();
                    userPortfolio.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    userPortfolio.PERSONALITY_CHAR = reader["PERSONALITY_CHAR"].ToString();
                    userPortfolio.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    userPortfolio.IMAGE = reader["IMAGE"].ToString(); //.Replace('\\', '_');
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserData---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return userPortfolio;
        }

        /*DL : Get a user interests that are going to be displayed on the user portfolio*/
        public static List<String> getUserInterests(SqlConnection dataConnection, String userID)
        {
            List<String> listOfInterests = new List<String>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select INTEREST from TABLE_USR_INTEREST U, TABLE_INTEREST_MASTER M WHERE U.INTEREST_ID = M.INTEREST_ID AND USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listOfInterests.Add(reader["INTEREST"].ToString());
                }
                reader.Close();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserInterests---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfInterests;
        }

        /*DL : Get a user skills that are going to be displayed on the user portfolio*/
        public static List<String> getUserSkills(SqlConnection dataConnection, String userID)
        {
            List<String> listOfSkills = new List<String>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SKILL from TABLE_USR_SKILL U, TABLE_SKILL_MASTER M WHERE U.SKILL_ID = M.SKILL_ID AND USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    listOfSkills.Add(reader["SKILL"].ToString());
                }
                reader.Close();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserSkills---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfSkills;
        }

        /*DL : Update User Portfolio*/
        public static String updateUserPortfolio(SqlConnection dataConnection, String userID, String description)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET DESCRIPTION=@DESCRIPTION WHERE USR_ID=@USR_ID", dataConnection);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.Text, 9000000);
                DESCRIPTION.Value = description;
                command.Parameters.Add(DESCRIPTION);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL : Get all friends belonging to the user*/
        public static List<UserPortfolio> getUserFriends(SqlConnection dataConnection, String userID)
        {
            List<UserPortfolio> friends = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.USR_ID, FULL_NAME, datediff(yy,dob,getdate()) as AGE, SCHL_NAME, IMAGE from TABLE_USR_DETAILS ud LEFT JOIN TABLE_SCHL_MASTER sm ON ud.SCHL_ID = SM.SCHL_ID WHERE USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS='friend') UNION Select ud.USR_ID, FULL_NAME, datediff(yy, dob, getdate()) as AGE, SCHL_NAME, IMAGE from TABLE_USR_DETAILS ud LEFT JOIN TABLE_SCHL_MASTER sm ON ud.SCHL_ID = SM.SCHL_ID WHERE USR_ID IN (Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS='friend')";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio up = new UserPortfolio();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    up.SCHL_NAME = reader["SCHL_NAME"].ToString();
                    up.IMAGE = reader["IMAGE"].ToString();
                    friends.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return friends;
        }

        /*DL : Get all friends requests belonging to the user*/
        public static List<UserPortfolio> getUserFriendsRequest(SqlConnection dataConnection, String userID)
        {
            List<UserPortfolio> listOfFriendRequests = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS WHERE USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS='pending')";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio up = new UserPortfolio();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    up.IMAGE = reader["IMAGE"].ToString();
                    //up.IMAGE = "data:image/jpeg;base64," + reader["IMAGE"].ToString();
                    listOfFriendRequests.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfFriendRequests;
        }

        /*DL : Check the friend status of 2 users*/
        public static String checkfriends(SqlConnection dataConnection, String userID1, String userID2)
        {
            String status = "not friends";

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select STATUS from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND USR2_ID = @1";
                command.Parameters.AddWithValue("@0", userID1);
                command.Parameters.AddWithValue("@1", userID2);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //String statusInput = reader["STATUS"].ToString();
                    
                    if (!reader["STATUS"].ToString().Equals(null))
                    {
                        status = reader["STATUS"].ToString();
                    } 
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return status;
        }

        /*DL : Check the friend status of 2 users*/
        public static String checkfriendID(SqlConnection dataConnection, String userID1, String userID2)
        {
            String id = "";

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_FRIEND_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND USR2_ID = @1";
                command.Parameters.AddWithValue("@0", userID1);
                command.Parameters.AddWithValue("@1", userID2);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["USR_FRIEND_ID"].ToString().Equals(null))
                    {
                        id = reader["USR_FRIEND_ID"].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return id;
        }

        /*DL : Add a friend for a user*/
        public static String addFriend(SqlConnection dataConnection, String userID1, String userID2)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_FRIEND (USR_FRIEND_ID, USR1_ID, USR2_ID, STATUS, DATE_ADDED) VALUES (NEWID(), @USR1_ID, @USR2_ID, @STATUS, @DATE_ADDED)";
            SqlParameter USR1_ID = new SqlParameter("USR1_ID", SqlDbType.NVarChar, 100);
            USR1_ID.Value = userID1;
            command.Parameters.Add(USR1_ID);
            SqlParameter USR2_ID = new SqlParameter("USR2_ID", SqlDbType.NVarChar, 100);
            USR2_ID.Value = userID2;
            command.Parameters.Add(USR2_ID);
            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);  // status - "pending" , "friend" or "deleted"
            STATUS.Value = "pending";
            command.Parameters.Add(STATUS);
            SqlParameter DATE_ADDED = new SqlParameter("DATE_ADDED", SqlDbType.DateTime);
            DATE_ADDED.Value = System.DateTime.Now;
            command.Parameters.Add(DATE_ADDED);
            command.ExecuteNonQuery();

            return "added friend";
        }


        /*DL : Add a friend for a user when user scan with QR code, and immediately accept  them*/
        public static String addUserFriendWithQR(SqlConnection dataConnection, String userID1, String userID2)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_FRIEND (USR_FRIEND_ID, USR1_ID, USR2_ID, STATUS, DATE_ADDED) VALUES (NEWID(), @USR1_ID, @USR2_ID, @STATUS, @DATE_ADDED)";
            SqlParameter USR1_ID = new SqlParameter("USR1_ID", SqlDbType.NVarChar, 100);
            USR1_ID.Value = userID1;
            command.Parameters.Add(USR1_ID);
            SqlParameter USR2_ID = new SqlParameter("USR2_ID", SqlDbType.NVarChar, 100);
            USR2_ID.Value = userID2;
            command.Parameters.Add(USR2_ID);
            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);  // status - "pending" , "friend" or "deleted"
            STATUS.Value = "friend";
            command.Parameters.Add(STATUS);
            SqlParameter DATE_ADDED = new SqlParameter("DATE_ADDED", SqlDbType.DateTime);
            DATE_ADDED.Value = System.DateTime.Now;
            command.Parameters.Add(DATE_ADDED);
            command.ExecuteNonQuery();

            return "added friend";
        }

        /*DL : Accept a friend status*/
        public static String acceptFriendStatus(SqlConnection dataConnection, String user1ID, String user2ID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_FRIEND SET STATUS=@STATUS, DATE_ADDED=@DATE_ADDED WHERE USR1_ID=@USR1_ID AND USR2_ID=@USR2_ID", dataConnection);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);
                STATUS.Value = "friend";
                command.Parameters.Add(STATUS);

                SqlParameter USR1_ID = new SqlParameter("USR1_ID", SqlDbType.NVarChar, 100);
                USR1_ID.Value = user1ID;
                command.Parameters.Add(USR1_ID);

                SqlParameter USR2_ID = new SqlParameter("USR2_ID", SqlDbType.NVarChar, 100);
                USR2_ID.Value = user2ID;
                command.Parameters.Add(USR2_ID);

                SqlParameter DATE_ADDED = new SqlParameter("DATE_ADDED", SqlDbType.SmallDateTime, 100);
                DATE_ADDED.Value = System.DateTime.Now;
                command.Parameters.Add(DATE_ADDED);

                command.Prepare();
                command.ExecuteNonQuery();

                return "friend accepted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: updateFriendStatus---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL : Remove a friend for a user*/
        public static String deleteFriend(SqlConnection dataConnection, String user1ID, String user2ID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_USR_FRIEND WHERE USR1_ID=@USR1_ID AND USR2_ID=@USR2_ID", dataConnection);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 100);
                STATUS.Value = "deleted";
                command.Parameters.Add(STATUS);

                SqlParameter USR1_ID = new SqlParameter("USR1_ID", SqlDbType.NVarChar, 100);
                USR1_ID.Value = user1ID;
                command.Parameters.Add(USR1_ID);

                SqlParameter USR2_ID = new SqlParameter("USR2_ID", SqlDbType.NVarChar, 100);
                USR2_ID.Value = user2ID;
                command.Parameters.Add(USR2_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDl.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: removeFriend---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL : Get all organisations followed by the user*/
        public static List<OrganizationDetails> getUserOrganisations(SqlConnection dataConnection, String userID)
        {
            List<OrganizationDetails> organisations = new List<OrganizationDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ug.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH from TABLE_USR_ORG ug, TABLE_ORG_DETAILS od WHERE ug.ORG_ID = od.ORG_ID AND USR_ID = @0";

                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationDetails od = new OrganizationDetails();
                    od.ORG_ID = reader["orgID"].ToString();
                    od.ORG_NAME = reader["ORG_NAME"].ToString();
                    od.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    od.IMAGE = "SaaS/" + reader["IMAGE_PATH"].ToString();
                    organisations.Add(od);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserOrganisations---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return organisations;
        }

        /*DL : Check if user follow an organisation*/
        public static Boolean userFollowOrganisation(SqlConnection dataConnection, String userID, String orgID)
        {
            List<OrganizationDetails> organisations = new List<OrganizationDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ORG_ID from TABLE_USR_ORG WHERE USR_ID = @0 AND ORG_ID = @1";
                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", orgID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["USR_ORG_ID"].Equals(null))
                    {
                        return true;
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return false;
        }

        /*DL : Get all users that follow an organisation*/
        public static List<UserPortfolio> getUsersFollowedForOrganisation(SqlConnection dataConnection, String orgID)
        {
            List<UserPortfolio> users = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.USR_ID as userID, FULL_NAME, PERSONALITY_CHAR from TABLE_USR_ORG uo, TABLE_USR_DETAILS ud, TABLE_ORG_DETAILS od WHERE uo.USR_ID = ud.USR_ID AND uo.ORG_ID = od.ORG_ID AND CONNECTION_STRING = @0";
                command.Parameters.AddWithValue("@0", orgID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio user = new UserPortfolio();
                    user.USR_ID = reader["userID"].ToString();
                    user.FULL_NAME = reader["FULL_NAME"].ToString();
                    user.PERSONALITY_CHAR = reader["PERSONALITY_CHAR"].ToString();
                    users.Add(user);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return users;
        }

        /*DL : Follow an organisation from a user*/
        public static String followOrganisation(SqlConnection dataConnection, String userID, String orgID)
        {
            string volid = "";
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "INSERT INTO TABLE_USR_ORG (USR_ORG_ID, USR_ID, ORG_ID, FOLLOW_DATE) VALUES (NEWID(), @USR_ID, @ORG_ID, @FOLLOW_DATE)";
                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);
                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 100);
                ORG_ID.Value = orgID;
                command.Parameters.Add(ORG_ID);
                SqlParameter FOLLOW_DATE = new SqlParameter("FOLLOW_DATE", SqlDbType.SmallDateTime, 100);
                FOLLOW_DATE.Value = System.DateTime.Now;
                command.Parameters.Add(FOLLOW_DATE);
                command.ExecuteNonQuery();
                volid = "success";
            }
            catch (Exception e)
            {
                volid = "failed";
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: followOrganisation---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return volid;

        }


        /*DL : Unfollow an organisation from a user*/
        public static String unfollowOrganisation(SqlConnection dataConnection, String userID, String orgID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_USR_ORG WHERE USR_ID=@USR_ID AND ORG_ID=@ORG_ID", dataConnection);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                SqlParameter ORG_ID = new SqlParameter("ORG_ID", SqlDbType.NVarChar, 100);
                ORG_ID.Value = orgID;
                command.Parameters.Add(ORG_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "deleted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: unfollowOrganisation---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL :  */
        public static List<OrganizationDetails> getAllListofOrganisations(SqlConnection dataConnection)
        {
            List<OrganizationDetails> listofOrganisations = new List<OrganizationDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ORG_ID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH from TABLE_ORG_DETAILS ORDER BY ORG_NAME";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationDetails org = new OrganizationDetails();
                    org.ORG_ID = reader["ORG_ID"].ToString();
                    org.ORG_NAME = reader["ORG_NAME"].ToString();
                    org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    if (reader["IMAGE_PATH"].Equals(null))
                    {
                        org.IMAGE = "../SaaS/images/orgs/2268b167f96843bbbff8f5bfb83e4850.jpg";
                        //org.IMAGE = "/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gNDAK/9sAQwAUDg8SDw0UEhASFxUUGB4yIR4cHB49LC4kMklATEtHQEZFUFpzYlBVbVZFRmSIZW13e4GCgU5gjZeMfZZzfoF8/9sAQwEVFxceGh47ISE7fFNGU3x8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8/8AAEQgBCwGQAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8Adf6ebHa3npKjdNvBFUpRyn++KeKjkO0rnhQ3X0piLS1Ipx2JJ7AZNRqcjI6U6ORklEkbFSnQ+9Ah0UTq7s8bqGORuUipiqMOQDVi21m6MSmQo+fValfVIiP3tpG5JxxQMzIoEZnfbjJwMccCh7VXfGTgcetannaew+aB4/8AdNN8vTyNy3Ekff5hUsZiS2CyO0SvgKN2SO9LpVi8rvvUgAYOeO3FX5oI4m2xTiZZeWIGCBxT1m8i5kOPvoCB6kcf4VI7g+niQrHgjHL9/oKkazZ12MMbejEVbtfLMYL3DB2+ZsrxmrJGRhbiPJHGRigRgNpjPIW4KrwPc0kts0abVLB24GK3/IbaFAjYf71QG1ZTvliI9weFqkBlxxNGiqOQB6VsWEoa2VcjcvBFRvGowF5Y9BTo7dYSSjgMeTnvTYixuIPfOawJmX7RJDFgYY5OOnNbUpkC7U27j71Qa1EIJOTk8k9zSQyoNkeEQZbsB/M0+NNpyTlm6mlW1w5cjDHt6ChVIfABwOp9KbsIRiWO2MjPc+lMKiJemSfzJow6DZHgkevQVDPMbdSzrvY9waTSHccSB88h6dB2FRO+8/Nwvp/j/hUHnhyS3BHI9BU0bwgbhIGNQUPjh3ff4T+76/Wpc7/lj6D+LtTNrsNzjanp/jS7yRgfIg7n+lFhhtVDtUbmPX/69NZApyx3P2HpQXC4C8A/mf8APqaFTcMnAX9P/r0ANVmLIeoB4x0/D1omUuXOPnc5IH17mpguBknaPU9TSN0/uL+poGpWdyWyKxebuw+I87D06jFU3wJlDgHjJ29qkLYG1QRnsOp+tREbj2b2H3R/jQCet2TXMQhshMZQxbBRexFRM3zKRjdtHzHn8qklXzWUyDAVQqj2A9KsRMqQTjCnIUAYzRbUkqqFHPJY+nX86eu5hjgKOw4A/Gn2sCzz7JPlU5A2/T9aSHM0nloeQcDd0ppLqNpoNqgc4PpkcflSF8nI7dzTZAysykbmUZYA9KiyzdQfbFO/Ym3cmDAbu/H4Uqo8jqkeWbsg/wA8UxEIGGP4A1YgkMLho8AjtSs2F7DQkqTL5uBLG2cAdCOlXJzBqUiJLsilC53+vsRVSe4jUl53APr3qo0habzQeCmARTtyoHJyd2QX1ubeRlJztOMg5/Wqkcru23pjv0rdgvT5S210S9u/HAyy/Q9qpSwwQ3LfZ2LoehYYNPcCqIc8nLfXgU8KAMZ49BUrVET1qrCGscDjiqd7KUi478VYkbg1BJtkQq4yKAMyWQu2ScnFLFA8p4GB6mpxbxI3JLfWn+cpIVaANYSj0P5U2SRWKDtu7ipQTjlTmkfO5Pl70CFhWKIERttBOcbulSu6rCduMAUpCspypJ+lJKiGM4XGeOlAEkPEaj0ApuQ8wHZBn8aCkQB4ximxxKF3FiC3JGaAJmboPWo5X3ssY7nJ+lCxl3wGb/61XI9PtyCSzFiMZ3VhUqxg7M1hTcldEVrgneejcAe1RSyyCVHjj37W2Dn171PPbPGu2Jsjp9BUToyxHbjCjI/CqjJSV0TKLi7MvqQqdeg5pEfOZX4BHGewqpuMpx/AOT71YQO+GYHaOg9feqWpJYjLMd7cf3R6VI8pVe5J6D1qB5TGMsppqygNukzuPAGOlWInR2iYblZi3VhyB7VMtx5f8CszdiKrNOABwcnoPWkV1XLOw3HqaQF/7SgXLxJ744pplgcAvEy45+90qmJARvfp/CKCS5ywJ9EFA0Odg7HblY/U9TSiCWSIvHHiMDg+tQypuU+dkD2OMfjTDdHb8rlI+mSfvUmFiw9vLsG2I8jrjis6ayNyzKFZAoO52HLewFaMd1cK3+s2xqOB60+SeW4xk4Ud8cmluBQtLBEt12NvZlDcinSaeFIZlV3/AD/KpLMhY4zjIQFSDwOv/wBarwmhOSYtvvuppAYq2ZRh+7cHJO0En8ac8bLy0mWzwMZx+Faryxj/AFe4A/3uc/SneRG4x5yc9QeCadwMQQsCcorsT6/zpc4PR93qRwK054UgUE7NucDYajNjJKBsdNp5wGBo0AzGnVTnzFJ9Tx+QpplJPcfz/wDrVNc6VJ5g3ny/dufWlWw2jgZPbFTa47kQGRhsKvp6/wCNSrgdOPfv/wDWpWgcA4zntkZ/WmYkQfKu736U+ULkiLn2B/M0uB2GfWqzXBRgJFPJA9qcZSXIJwAcUX7APLFG3BiWByMdqiRnWYOo+ZWB4/rUqvGOM7j6ClLZ6EKPQUctwuQNGXffM5LHqBT/AJvsscS5XYSf5Uya7ihBx87ei81kXWpSuSAfLHoOtVy2DnZszX0cbq867gV2sB1J9R79KrteCdYlt5Ujd2KEPxg9j9DWLO5EEHJy25snr1x/Sq2WqRXL04ueW8stjgkfN1/lV+0ctZIGPzr19axDM+0AHGMnPc1pw6i3nul0fMLoo3t1zj170MenQ0FJHOaaxAAz19qYWTJEbFhz+lRTHEZLNh84CY5+tGgEskoAqq0pZsDmiAByPMDAZ7nHFT7FJ/dNxnsO1NyC2pXKMw5IH1pph9AW/Cr4EKDgDd6nmoLiWO2+eQn94MqpPb6Cpu2BRkt2JzwB3qtGheQMgyN38Vacl1LeWixrpzNtPyzKpBI9D60kavZRSz3FmyxnAUEg7fzpgX1O5AfUUkhA2n/aFRoJFQAunHsad87Y+ePgg9+aq6JLCPkcA49xStyEHqwqHfNuC/JyM04+aWQgx8Hpk0XQE0mGZY/Xk/SnNgDNQDzgzMVQknHDdKWR5AvKpgdfm/8ArUroLEkbCN1Bx83FSxzYb5ulZvnM8jcD5eBhv1q6Y3I3Be2TzXFiKfN7yO3D1Ir3ZE5n+bPaiXgbQBk8VVQM7YVScHmrCJJJLmWPAPQBhz7UYWLSdx4vlTSQ6yj3xqD9wcYHc1fKgKSWIHrVSIsksiBD13dR3/GpN0hOXjbCngcfnXarHCSFOPMfjHQHt/8AXpqAgh2HznhVpnnO+G8tyo+6Mfqaej7Tkqxb/dNAEyRFBudgTjr6VHIpYjKgnsvp7mmi5LfMyOqjp8pp/nIwwVdV9NpyfrQAxY9xPCs/97sPpSBxDIygd/zpReReYI4+XPQEYAqleSEXG4EYPp0zWNV2Wh14WKlNpl+WTemGjGD1FVEtlD78s7dV9hTJblpFU55AxU9rOFgG4gEk4qYO8rGuIp8sOYME8tnI6DqDT2LEcsD7dvxNND4HB6k05XXHJBx6dBXQjzyGB2BcYBwzYyenNStI+MkZ9/8AAVGrqrSZxgNxnoOBTJLheoP4mjZASvIuMsvTnJ6//WqM3WSQPlHcn/PNMQNJz90f3m6/gO1OQIrZRd7d2NLV7DHBsjdkqegJGSf8Kem1fc+poYqR85B9qYzqBwAPrQlbcbfYnyGIdudvODyDUxvcqQY4znvtxVANlW47UnlFsMWxjtnk0egi1FcRgkyxl/Q7sVKZoZJAGEgTHfBqgFIHHHuTUMt2kIILsx9BTt3EaF0lu2zYe+cFcY/Kklit0C5kjyeOvf8AGsqO6llmClQF4I/Oqck9y5J2IB9KE1cLM2Z7PoIgjs3T51P9axNShu7V1WVSm4cAHIP5Uwy3HeRB+IpsokZQXO459au6QrMrRySE/N/KoZozuypJzWnbxWrW03nmRZgP3eOhNUmTB55+oqfaRehXs2tSO5BxCP7sY/XJ/rUUcbSOERdzHoKt3MR8wldpCgD3HGKS1l8iZZNocKSducZqVawDo9JlJzLlB6AZNXTZxB2dkUsT1c5/Spodb8xSq2ZUHKllc4GarXGpmV0WCHEjn595yOcYpXHYtqYkABk79BwKhtruC4ujHMCsQyQUAz+NVrlEe4yU3nbysfyjI9qbawxw3I8ot8w6OMGldWuVyNOzNd9KW4XdaXCSsDkrghsfjVGe1uLV8TIwUf3v6USXWzgyLn0wQR7VPBfzqvlriSPqUbDg/hRckrLKhwACSe1Ld6tBHN5S2atLD8gd8dquxNZT3CiS38mTOQVfjI9Qf8aa+h2txcPOZnYOxYhSMc00BkS6vPPIGMrRt6KOP880t0M2/wAzE9zk96iu7aO3m2xnK+/UUqyuYyhYmJsZQ9MipbNlB2aOim0y6toHkdQQoyR5vb86ym1G1G1kDsSeeuQKyHZ8DLZz71Hz24q7GB0cVzbXFzHDE25nzyzEYP41pDSZmIBMR/7ajP8AOuJz61LFM8Tq6MQVORSsFzsxp0wBBkhHp+9FMfT5NhZ5YmUckCYcVxxlYdzT453Q5DEe1Fhpq+p0Mc9oZTJukXj5jzV2ylhuE2rOI8dpJAvFc/Y3W6dVI+9xipp4IZkaSDKuOqCpVr2Z0OjzK8Hc6gRRDOLuD3/eimyRLgMt7bDBycyCuSjkgUhXD5YY64o+02u0gK2fXdWqijmbN5ppGkl23ERYnYpVQQvp9atW6FB88pkY/wCyAPwrDtbhbdkRoirNzkn7xFXre+QSKWOUbGT7VjUbTsjrw1NTTvua0NuZGCq4GOzY/wAKbdn7FJHHNksw4Cgc/pVe41EQqZgyK68jbg5rJm1xjIGkVpCcMN/OO/HpVQd9zGtT5X6m9bBZYyxZ4wSeGQ5/lT2SP/n5AP8Aun/CtKCUSwxyRn5XUMv0NPLkddw/Gq0Mjmdtut6zFiAowWxwT1rQktormHBmQHqPUVJrN1Na26SwEl94Xpng1pLICOCW+hpNJlRbi7o5WVGgcxkFuwZRwav2bwvAESeJcDB3HBzW2xz8pxz29KwLVbZbhYrpELDIGcYPpUKMYanRUrTrq1tiaWRLfzXaRCowflOc/SlixPHvjMIB5w5ANQX4itmYRRF4gwJjHOfaq2nzR4dJYyH2kKDkYreEVJPucMpOLXYW+Tbne0WNwIEZ3EnHp9KmjFv0MtqHPYsAaRZIlR4gU80KTyOtQfLawNdzpkgcAd/eo92zLV7lywijv3eJYxiHAd8/Ln2xTtQtvsSL8hdW+VWDHr71FoepJCk32lTCj/vFdhhTx2NSDXVvnRYrK4MDPgTFePTNS5WVyuUq3FjdqjPGmfl6CUZH60+3sZvJRnRmYjJ+amu1+5wEKAn7x+UfmaksrjyXk807mUYxuzUe+t0CsxY1WTzFQgbG2tlwOabCnnF1hlTKnBHmjrWfqMYUSys4j8xvlTv1z09OKgsbxYJWkVGY42qgbBYnp/Kru0h2HGV0uJku5CqI23d1A5rTmtYXlRI2YsFyTt4xnjoKyTNFL9pacMkhYN5bZI/OrNjcpczFUTAHU5PAA4H04rOSbdy27OyNVLWNLcEbc5/ukH9asalBH9glXd1wMAYHUVTa3hZA0k7ll9WA5rH1DMl6IFty75DLhsA5HWiNrg0+pMtsgOXCgAck+lVbiMSHMboUD9Aeegq5GDcORewBNmD5bEjOc8+/Ssy4hWSadgY7eNWKqueuPStpTT0IUGlc2dIsIXjke5Qkfw84zis5bRrh5WBCRockjk456Dv0pumrceayQXWxAATnvn2NTRb54pIJWVWV9qlRz9axd0y7NqyJbW2tZpbrznC5U+XubBDetUYIZJLDEcIDZPz4AJ555qtd2c0dxNtDskfJc+lW9Mcrb+az5XJULj9ap6K4JXkKlvNalQGReMgE96gaOKW5Z2uFi7Mdu7n2qa4dZo32FiicsXPGfQYqlGFl2R+XtyeWJ601J2CUYpuws8+GK2zsy4xuIxmq+6dn3bmLevetyOzhdQpQH3q+kEESABET6mpdQfI+pyZeXeBMrNz+Nad1cxu/2a1tYoztCs7rz/8ArraPlqQ7eXgEHmoNanguAoVSLhf+Wm3jHpTTuS42Zi6jMroqiBVYD+EYx+VGnSG3/fJKUbGCDyG/Cllt9w2mXOepx0FOvbKK1eGJJJH3KG3lcKw9qpbA1fYbch5ysjMpduSARVVJTEzIcAHqSMkVeZfs/wAwKsCOKp/Z3uZiE6nk+1JMuTZG9s4OFGR9aaLWU4+UfmKt3bqwBJKsedqmqhc4z0q7oxHfY5c8AfiwpfscijLFBn1am5PBOM09syQqmMbWLD8f/wBVF0KwgtQM7poh/wACNT22nG5YhLiAYHUuF/niqQjY54yRU1vbTSFcROyt/dGTihtAkW5tM+yEPJeQHBHCOGP6GnOCk5eM/K3zA/Wrljp/lsIZBGjy9WmA4X0Hv1NWbeGCQSow3wLlFmVflz7VEndHVQtB3bM65jhkhWWcsNvdavaVplk9w0kkZcKAwVzkHIzms4ShYpIJXAbnGR09Ku2kGoX8EQaX7PCi7Qw6tz6U6msLJ2Mbc0m0dDdWlpcRrviUMvKsBgisz+xoFULExHruB4PtzT4dPubcfu79mx/DImQf1p32oW0qLdvGmTnO7g1zQU4uzdzohDl956WGJo21c4EuePmFNtNOklBE9tBCwbaAYw3bIINX21KDO7zF29qRdUtyQPNX867ErKxyzm5y5mM0bWEltRDIm2WLIIUYAHarUuvWcTqkknLDIxz/ACrl7uC3t7m6BkcQOAw2nO4HnGfrWfC8onD2qMuD8m7kipuHKdbfXsVxFBKUO1pMRg9Tg9f0/Wmt4jgtWMKwSFlwByOayEXV7qWMTRl0VgccL0qUKtpcTzzKpwuEyMsD+NZ3szS11Y3RrEOxGI2s4B29SPrisS7njdXeHBaVhsD4+QDqQKxILqcS+ZC7LJngqcVpTNNpi4BTfKv3x1HqP5Vo2RHR3RbuLqCGJo/MDEr14yfyqvZyxhBFLI7bjlQgBwcce+ayGl3HofyojnaJw68EdKFdbMTs9GjpDq6WmYmizLtBJb+DHP41TfV57xZ4VgVhcLtx3HNUNVkeS7jCqu541+5zn0FWNLgngljmADK69m/nSk7+8xwhb3UX1s7i7tfKNq4WIBXy2CcddvarO5LbS5EiDr5cbYU/L+JFWILqaNHJJd2PEY7VNLtvbR0ZFdip2+xpxpOUbkzrqM+Vo5qx026uF3BjAo5zmprKze4mZbqfzWjcjDknPTn+VaEZtjGAdw3gcFqcGjRvMKY9CKx52b+zRU1BleSO3kQlpXB3DoVHXH61MbZZlSyihTcPmO0/d9yajisJdXu9yqFigOzOSDzzkf571ehs2sLq5dWMjuRwT2p7K4Na2MK90q6RyJm2rgiPjOcdsj+dSabavHJAYAHmddrLzxz1P0rU1eSRo13jYquCSDnsc1TiungLG2YRADG7Gf50OXc2pYdzV0Toxkl8oRLlnHzVDe2AsbpZY5ZvPDgvuPBBPP0ouZiUgaA7ZnGWIGCCDj+lMuJriMK85dmxjLHJIpbGscO57uxJcXDTN9nijOckhh8zk44H0/xqaytlsoWkvIk84nJLEcewzVu3lW10r7Yiq87KZCxHOSeR9OK5y61A3s5lkwWP8IHAq+XQ5ZzV7JbFrUpY5NQiuI2VlQfMMdahhuy8xleQLsB3E4GAT0/PFVJiyrl1IB6VVc+q0+VGfM0zqIo/7UhMYJdH4yp61Xj0d1zB56pCjEcHk10GhtH/AGTbtEioCvIXpmsu4eXzpfLkiX94dqkc4qXHlRUXzsxNUtI7QnEjMD93FWNHEUluHIUuCQd4zge1T3tol1gyHJXv0FR2BxFJGECx44GOhzSb0KUbO5oRCJeAMCobi082QAyHZnNVxeBW2sORUF/fHytioPm71EUynZIW/eG3mTawcLg7RzmqNzqLTNkj2FU3YknNRk1uopGDm2TG4J55q42sSPaQQlExD90kc1mexp2NwPtVWJJPNXzCWGAfToKvJKtpGpMZbeMlgayycmrdjKSDE/Kjke1TKJcZalaaTcRg8URYY4atjWNEltYFuBtYD/WBOi+9U/sQwrQNuBGRnvQ3ZCUbvQqxAs5ABPatGyhjVs3BUjH3aliiQoF2BM9cVYigijbfksQMAAVLlc0ULGPLDi6IjY7G5BbjitbTr15J9kYSOKBduQoy31qPU3ja0Jji2lWBzVG0uyo8tiRGWy2OtJ+9EVuWVjYv7qL7OElAkZ2DbQef/rVFpd0m37GsBJJLbt+Qo64PFULi6aVgdoYL93dzgUyKeaPLwERluuOKIQ5UTJ66GoTEJVhltowrMASa0vOVTtXhRwB6CsCG8kJb7Qu/d3JzV6ctE+NwI6gjoRSqts68JGLbV9TU84lc54rNvhbEvNJGrSEYyeaYtwSMZ4qlfNuIB6YqIq7OmtHkg3YrzTb2CoMKO1MjmIbvUTNjA9KaGOeK6LHkXLnn9ck/Q9DV2xmjMqI4xz8pHrWWoLdVJ+lbVhYRtZrPI20Anr65/wDrVMrJFRTbN+OTCZJrG15VuY4ZlVgok2s2eDV6GYKuxlDCk1GQf2dMgwAyHC7cnNZJ6m0o6GAsMLPhMkeo7VLd25ja3lcl4w+OvP0qTS9eh0/Tnha2Lykko4Axkjqam02WOe0MVw275iMkd88H9a1loYx1LiaZA+1lXbnkrWXqujtxNapgfxKOxrQs7osXEmVkjbD9gT3rQt5DLk8etYptM1aTRkabHFJqcN1KSEt0CHcMAN0/TJ/Kr13aNbMwVQgyTgDjn0qfUBGFjQDDNn8qYtndRSGdJDMRxJEzckemOn0q7c2hUVHe+o/SpLdJmYyFtw+UkYz+FW0hSNH8s8tnmsuOKKBkZsnJynbBPQe9WmnZJV9GrSDaVjKvGHOnEz3jJuJFXMZU4I7cjtV+Dy3skLZC7QVYcEehqvfzLbyiX++hUj1I5H9ajvJ/s1ikasBhQmKOVIjmbsi+dVeGMiNVZyAWY9M4qS4xeWkd1BgMRh1zjmsuKE/2Ybh42IKn5s4x749KzopWTbIGIKg4+uKzv0Z6Kw8ZR9zdFzUpDHaCJlQ7n3K6vksMemOOoqhbyhYwX7/d96s21t9otn38mMZUE9QaokkW9upXDKpJOeuSSP50nqa0pckvZLXcnEnO49KZPmRCyg7QwHTvTN5K4NadqudPj9GlbJH0H/16LG9afJG6K0F4y2ElpgENkA56eorVtrKBEUCFOnXFc88kYupiFztk9etbtvbNLZhzMQMAqcnjj60SuedJJx5+rbDVbN7kJtC4TotY8entclgCF29SfWuijTy4v3j7m7VXAWMMVGCxJoUmjLlTLNlLHpmmW6yKTlB93HU8msO+v4BcyvErnJyMmo9S1E3LrFENkUYA4/iPrWXIeTW1r7mKly7F9dUcsNyKR6Gr/wDaUNwwCxiP/ZHSud5wetAZ0OaTinoCm7mzdFZeNo/3vSq8EkYu1idMqRwTyRUC3Mkm2NVGe7HnAqzp0UMsckriTcXwGzwRUW5VdmnNzOyM+9Ty7llA4qvntWvfwK5XBdiaj/sgDbukOWqlLQlwd9DMJpN5FOlQxSlDzg8H1qM1Zk9BcirVrDKUMyghM7c+9QW9tLcyrHChdj2Arso7Xy9OW2aFAYhwQeGPrQ9hrcutIkkLRuASwwVPeudmsHspGMJfyW5CEfdq74fu7rUrxpHCCOFeSBjJP+TXUbARyAfwqWroE+VnErIMcin+Yu07Tg109zYQSHi2jHqwGD+lY17ohD+ZAe+Sh/pUcrRsqiZmT2jtaOwSWQHoQhIzWSvyjGMHvXounPm0jJ4JHI6c1xOvDZrV17uD+grRIylK7M87s9aA7gYpVbgg0KQSRVEhub8a0Reh7GKNlPmIThs/w+lUwAR9as/ZVbSGlOVljOR6FTSbS3Kje949CaGKWRkBRkDfxMOPz71Y1GxjFnuhJLp82T/EO9UtFJUSuScHAA7VpmfYccEN69KxlpLQ9OEp1qdps53h/rTkRQyjueKuXNmgZngYDPO3t+FM05Q18qSKdx4B7qexrTmTR58qUoSsy8dOuo4/9T6Z5HFbEtvdSWgiRIYkx3Ysf5CrkRbbmReSOSDwT7U4sqpgms73K5bGY8JhCeXIN20bx/Dn2pkM8N07290Ag6EnPPFQ3OoR/aGieOQtvC8AEY/Onx2s07rcW8LsSSCpXpjGKVnudEEnpIgvNISG1KoAw6qynIJ+tY9ndCORY5AVQn5ioy1dZO15FE7PYxsmOVC4/lXPyWqXl3A8G2NmIDJ1IOev0xVx8zGcGtUdDG0E8CHGSQGOMHk1UOo2ltf4Ltl+GXsD9awbyaa3upBufa5+VsbQ6jgY/KoFjlOGMT/MPlODzV2TOe7TudbezJPqsCgEBdoODx1zWrGF8wyDjIA56muatpJJrmISlhKepAAIOK2bWOZyyb2RV4z1Y/nRHqSzK11Gj1aAqzBWIKj05walu5Ss8SFiSASc1a1yJLexEoj3kOoLM2SvPX9P1qneqZZYJUXJIwah6SsdDfNTTtsV72RWeJCxBVg/QngU+4ga+kjSNcnd+Q9TTZ3aIuVMWXTYQTjHX/GtB9XhuYVjaNPNGFQq45P4cirexlHcvzKQqwqMQxr83uB2rkkI8jrXVNNK2mXEk8BhkVGBXOc8cEGuQU8YH4is5Kx6WCd+ZmpoymRZEDBdxwT35H/1qo3NvPA+GXzAuFJ7gDjpViOf7HFHMqqARksByaY91khsk7eN2ev1oSOepVcajlHQomXJAVWJPtV62uJxHBbtFsjLFyTyScfoKkikMp6JHn3GT9BSeeqXG18uSOcdR6U2tCZV51LKTGWVopsry4cncSeM8VY068IUQtkgdBnikuGFnYRQrlvNy5PqP/1VmBvRSah6nTQgpQaZvz3IQ7eN3oDmqF1fbUKJzIeOO1Up5BDEqRja7AZPf8KZBGxYEAFsjGWx3oUepyyl0RAEcHkEY9aaU5ya6K5sp7jqkStnqCT/AEqvPpbrC2CGkHO0DGR6irUzP2ZiF9p4AqOSQuAAMVLNE0bkOrKe4IxUQUZrRGbHpKYo3x95121a0y42IyseAc4zWe5GcelMzg0ON1YIy5Xc6ZZEcgjBxSyAtnkAe3esrTnkdSSrFQcbscfStiJS2BjmsGuV2OtSTVzB1GJhIGxximWNhNf3AjjGB/Ex6AV1H9ky3H3lCL6t/hVq000WO7Epbd1GMCtIt2Oedr6DrCwgsIvLgXlvvMepq3IBt5FZ7ataRzNEZgrKdpyDVtJknXcrBl9Qcg1ZkVPCsaxaUHxjzHLH+X9K2xJ6VgaBJ/xK4kzwjEH+f9a11fNJDe5ZL5FNKhxg9aj3ZpCSCCOo60xChPLJA6VxfiJf+JvMfUAn8q7cNuUVwuvMTrNwD2I/kKBozivOR3pD8rZHSlycUhGc/nQAu4q3tWhFdK+nGBjhs7T9KzSwI2nt0p8LfNjIAbjJ7VMldGkHZmra7EiSNWG4npUl2p8omoLS3TcHnLAbhtdemR/OtS8KG2KlRuI64rJ7noQqOKs0Y8StNIsYzz/KtmwNvAwjfG5OA5H9aqaOisZXPVIyR71bMStnIHNTLQ1cIzumaz8xcfhWRcXcsUxjgjMh45YFsfSlWSW1GFBaI/w+n0pjy6jPMyWkz+SMYAIXH1pxMPYuEtbNEkY1iWNiVaNSeuAg/pThBcEf6TfxKfRpdxH5VWNq+c3d9EuOoMm9vyFEUNhuwstzM2MkIgHFWaXSWlvkv1ZKYmbHkXiyT/3VU7QO5JNUtMe3FxunDBwDtZe1PaSwkcBfNhVRz/EXH6AVSdj9pdoE2jd8q+3pSYNOUWi/rzQPawqp3HdhCB90d/6U2LUFC+TGZG8vgbfT1qOGxlvJC9wGUKMKimtW00SGAq4VWOPmZzk01NRVjzZRu9DKt7hzerKNzMWJAzg9PetizvpRMwdSA/vk596syWsLD52C4/u8UnnQQAiNRnuaXtLIPZtsi12dv7LkUIuGwDnnvWFcNJd6GGVTmBvmbPUdP6iruu3y/Y2i3KJGIIXv1qp4fu1iE0cg3A8kHuDQrtczNfdtyIxvM3JtwPrjmtHRogLq3lIzmZQPzFRXNrELpli+6TwAentVgyi0WKRB/qmDAfjWrdzCKtLU67WGCaTcFTxtx+ZFcZ0Nbl/rlvf6aYYxIsrEZVl7detYT4BAzhvSpmnc9HByjGDu+pbdGfSm5Bw5AHfBFU1b5fYipPtBSB4l4Y9/T/OKgI2qADnjFEUcddp1HYvQYht1OBuI54rMllc3rbWIHfFaBZTEo3Be2ay4/nuJGqkYmtqlwZQCh4RPl9MZ4/SspLqRmA+X64q0qfuGX1BqtZRgyln6DipSVjVVJp+67XLYy7B5MZIwB6CnngZHamM5aTNKzcc1JSOjtJxLbxvnORz9adMSfugc9TXMw3klrkJ9084NaumagLolTwy9RWbi0Umh2o6e9wA6/eA/OsSazljDMVOFBJrtFwVqrcWwkVgRwwwaak0TKKZwh9afCAZF3DK9xU15ZSWkhVsMucBhSwxAR89TzXRfQiFKTlZo6HQ7Y6oGLTKscRx5SDGPcDpz610sdtBaRkqFQAcsf8a4awvJNPuVnh6jhl7MPSrOp6vLqL4OUhHSMH+frUq25vKjKUrdDZv/ABFBDlLRfOf+8eFH+Nc9d6jd3bZllOByFXgCoM00mi5vGjCBKZDJAsKjfNI20ZHQV0kMS2lrHAnRB+Z71zVncC3u45GAIB5rp5PmXsatM4a0OWWnUy9E1ISPJCy4z8ykDj3rejfJrm7cmDa3yDb2Fa8dwFiEyAuhGQB1rOMgnBrc1u1KduwlyFA6kmsRtbeQbYohGPUnJqlJLJNKryOXAOSCaHNCVNnQmdmOUJ29sDFcx4ltil0twR/rlwfYj/62K6JZ4woIbP0rM8QSLPYEY5QhhzzT5kLlZywPFIDn8KDmp7aMMGBqm7IqnTdSXKivsLjIFNGUPNaVvGo25UdSDUs1pAwOAVPsajnR0rBzaumMhmRkgUFiVbJz25rauQrRlu+3iufWHyt2H6HK5rUeUm2z7VD3LkmrJ7kVg21Wx3rRSSsm1bEeatxSZPXp1rOS1O+MfdLrsGTkA+1Z0kSPKUYlBnr1x+FWN2ZAabcgnkdQciktB26EAGnR/ee4mI/ugKD+eTVzRpIpIZPlCFJOFYkjB6fj71XSSUnbbWETZ6N5ZY/XJoe3v2kLzzxxkjBLSqMD6CuiLsedWg5aX/ElCYvmTcrDaNnH3R2H5Ypl8xs5UdIlYuOpPek8vyoHnR0Mdvj5wCPMYkZHPoP5Uy4uPtti8i/8snGPpj/9dQ1rczleK5bl61vV2Y6A85Paop7+csEgkKrn8TWUsuAQT8rU3zimcMcYwKXKYuZsF5dmWkbPQ5Ap1iyI7GbfI38IwT+grJgllnkVZZ2Cep5xXRWv2aNf3ZBz1+cGqhDW5nKWhzfiB/M1QHYV/dqMGohE0UiHoduau6pGt3r4jHClQMrz0FE1jLEM7w6rz6VrIUWishJuBu6mpbkBgqYzyMjvUIBLqD+HNTlHmVUQhZOxqVuDIIjtuDngA1PcgG5iPbFRTQyQzKJWUuRn5aV8tcIo5OMVqQMWNpbhxzjOaW8GGGBjIrSSCQIyPEWzjBBwB71U1G2eIKwHyDqxPSoKuVVbMYz1qGJVBdnOMkke+O1SR/MnHP0q7BZxxP5oL4YfdPIoQMhYnJbHBqtER5ZIXG3j65rTNpuYyKMjBO2sxSEjVW4J5NS9C4gnzuRyakIK9MitDQbOO6kmeQEqoAGDitG50u1UZBdfxqHJI1SucxI3HNTaMkhvg6cKoO8npim6hGqTCKMlia0rO38mFUx8x5b60SlZGtKk6krPobH9oRRKAgMje3A/OqlxeTzDB+RfRf8AGm7MDJprdKyud8KMI62KskXm4jI4Y1nyxNBK0TjBU1pSNtOR1HNVNWmWW4WZR95Ru4xz3rSIqujuVs8UhYAZqMvxgUA7jk9BVmLnfRDuep60HpSZyaCaBXGnj6V1mmkzadAxUt8uOvpxXJE1tad5q6arKXTk4YdOtUjkr7Ge0hPetHRLjG62f7rcqfQ+lY7tsXJohuHRldDypzUJdTorSjL3XubV5atbSeYB+7Y/lTVAX5ieK1CRcQKSN0Uigj6VQ/s2RjhZf3eehHNJx7HNGempC9+w+VASalXTrm7jZp32AqdqjuauW9jFDyBub1NW1ypx2qlElzfQ4dwVO09Qas2yBo8nIb1BqxrFmY71tvAY7h9DUMY2KKTeh14elafN0LMC7V5OeeBTnf5sdcUyJlGSSB6U0N196jqenGy2EcBhyKVJgImi5xjjJ70hqNPlly3QGgxrRTJLU5izVi3b5iD9armL7LePCG3R43IfY0/JRgRTkgoS5qaLmcMKmlHy59KrId7jHerbcrisjRlRQZFK/axBCOoJbDfgKaRpsHLyTzY67VCj9ajVplEwt32uqk5wDwOvX2qgGdkZS4IYYbjJNbxV0cGIq+zk0ia/1V7oeTHGkUC/cUDp/wDr7mrGkvGT5IH7uRNrA/3qzjHGoPr71JprhLyAMePM5q5LQ89SbldjpLd4p3jZuFJANL5SqATyferupqFui6tlZO3uKqP0PtUIc9yeHBY47VbVc1StCMN0yavowVenPakySjasX1tjuJEe4DP5f1NaU2fLc8kAGsaGC5F79oRRguWByOQTW9byrN8rxyLn1HFbXRFjDVt0oJGOauW0YluAu5lx3U81HqMMNrKiQ797nOCeAKfYyA3A4PAOeKTtfQFe2o7VwFuojzgr/U03TVWXUNzchFz/AIU3WJC7xErtGDg5qPSCBdnL4ypAyfvHir6COhY5GF6Vjaxb3M7RxRISh5yB3rZiiZjVtYsdaQHH3lvcWaRblTlduVXA4/rW7ZQCa3jkdcblBxWjLZwz486JXx0yOlSCMKoVQABwAO1KwXOf1S7MMn2aDaBj5z3+grGIweeasX0wa7lZuCXOR+NVcsxxtY9gy1MkaRZ0/h2MLZZA++xP9Kdqs4hQ5NXLCAWdhEjfeVfm+veua165MkhAPGcVha7OhOyuV7JDLPJdvwiHjPrWjDIxG7O3PQVTgH+iQxL0wWb3J/8ArYq0g+UUT3PSw8OWmr9SdpD1aonc+g/Oms341Bu2nYT8p6H0qUjoFkJwSao3XIqzJIcbT1qpcn5a0RhXfuMbbW73U6xRj5j+g9akuoPs1zJCDkKetbHh6NUt3mIG5zgE+grP1hCuoyEjG7B/StLaHmwleVimKaTS000jZvQFG+RVzjcQM109rcRQRrEj/IowFPNc9p8Xn3YB6AE1srZHPQEVcUcdV3ZgkZG1q3tM0myurMNhyf4snBBrJChTuUgn3FXtJnWO4kmkk2Kq42/3s1CN60Wlc3khWC1EEZLBBgE8nFMXA4rCk1xpLpWUFYQcH1xWurjAwcg85pnMWhxTyMrkVCpyB602a/trRczSc/3F5Y1RJW1i28y1EwHMfX6Vg1ZvNTlu2IDFISeIwf5+tVs1E1bU9HCTvFx7CZwc1IDxUZqW1j86URlwmehIrM7OZR3Co3wDk1r/ANmoq5aRmPsMVQvbXYQ6D5e4pXREq0ZaIqq+bpMtk7Mdasnlaoji9Q+vFXhwfarYYbRSXZlvT0aYlUwCvr6Vfa0kH93H1rNsJPJvEPZvlNdD1rJmdepOErLYxNPAW5nLjJCsp9OTj+Wa59mZWZTnIODW9fxyWl6zxkhZOR9e9YuoI0VyWI4k+b/Gtqb6HPio80VUREWogKiZC4GM85qIEnrThyea1ODY3ZIhPHuHOCzAg/59KoOe/rWtptsy2iq56j8qybtTFKydlJFS1YblzO5PaDK5q4oZyI1+8xwKpWJzEe3NaOmkG8XzCBjp9ajqPoXLC0mgO2TlfTHT8a0RGvoKkC07aBWpmY2s6dDLBJPsJlROMGs/Q7V7iWaSTIQYAHaunZQwwRkGmpEka4RQo9AKYGNqWmllE0Y3mMY8v1+nvVC005ZLsbjCI3PEZclg30Bz611irTtoLZwM+tAEcERjiVWO5gOTjrUgXFOxRQA3FNZTg4OD2NSUh6UAcBqlq9nevE7F/wCIMeN2e9TaFD9o1SMHlUyx/D/6+K6PWdPhu0jeRSHU43A9vSmaPYRWrSSRrjPGc5rOcraGkFfUuXcmyI81xd47XF2QnIzgDNdDr155UJUH5m4rm7Yf6ShTkjJ9e1RBdTaT2iaMxCybFYMowAwGAeKs/dWq09w1xcb3ChjgfKMD8qsS5Bx39qh7nsx0STIiahkORT3Iz3H1qCQ5wKaKk7IQtuOTUFx0WpqaqCS6gQ9CwB/OqRy1n7jOj06HyLWKJh/Dn8aq3sYN2QwDDaOtXTII1Jc4AGc1mmUyyNK3f+VVN2VjzaeruRPZwn+HH0NZ15HHFgJnJ960ZXOCTwKyLh98hI6Cpjc0kwhIXJIP4VcivJIv9XLIvsTxVW3bPy5xirBxj39qs53qQhsd6aDnIz1phpMkcikdrnfcfFGRuVjgEZXnrV+21YW0SxvGZCvAOeoqgx3oB6HNIImfGBnHpVLU5Jx5WaE2tXEqkRAQg9xyfzqjncck8nrSvA8ShiMA8UzOK0RiyZRxTsntUKvgdKlQ596ma0N8LJqpZdRdzHsKcpZWDA8g5GKKM1znr27m1BeLPD6OOo9KRxuXBrHSVo3DIcEVqQ3cc6ccOOq1LRyThyPTYx7qLybpD2DZq13pb+PzKkWMlc561V9DbDPWRFkqwI6g5Fb1pcLcxBxwehHoaw2UKMNxUtldfZZwScxtw3+NJ6mlenzxut0bV3GJYSCASOR9a5nViGhjIH3WIz611ZwyZHII4rndYQIrrt+UjIPvRHRnHF81OUGYQqSPk1HUiEbR1zXUjzmbWn6o6qsTgOQBtA6n2qHVk238no2G/Sr2lzw+XHHs2SEY6daZr0BQxT44I2n+lSwRn2p25B6VajYbtwOPeqEcmMg8A1bVwF5IqGUdhG25FPqAaUmqtnJvtYj/ALIqwOtakDwM07GKQcCnD1NAABSgUU6gBKWiigBMUh4pTQfWgCne/cUdzSH/AEe25445qSaJZZASThaz9XnlkjiiQHKnjtk+tYSs2bwTSKEka3JZ5gGBPANMs0trS8WQxoqkEEnJH5VbZl/iUDHtWfdOGkG0cAfnUp2N6UPaTsy1dxq17bv5aRsWIZUGAcHgj6girjRxscsgP1FVLRzcSRgyb/Ly3I6ZGMf59KuvwaiTuzWpeLSfQgktrcjmMViXJQ3DiMYUcCta8m8uFiOvQfWsPoc1SNKPM9W9BaWCNpL2IL1HzflSE1Z09T5jyAdAAK0juPEO0GXdQm/condzz9KiX7tQTS+dd+yDbUzttSlN3ZxQVolW6lVThug5x6+1ZfU1d1BNscbN95iTj2qmpxVpWREpdR8QO7djgcE1Yzx1pyR40/f3LVBz60MzQ+7ge3kAcD5lDDFVzXQavb+dah1HzR8/h3rn8FjxzRY3U7q5LFG7cjpWvaQhYl457k1VhddoC1bhkG7ZkbsZxVpWMZSctBbyDzbZlT7w5FYeexroww7VianD5VyWA+V+R9e9NGbK+amiziq+alhcZwTjNN6ocZOMk0T5pM1NJEyYKqCpHHNRHd/dxXNZnsKrBq9xufY0hYggjg+1PEbscZx9acYAoyxyPaqUWYzrwXUfBJJOSpGSBnNKhyHDEjaeOe1Njfyi23AGMVEiySTZGQg6n1p8utjmjWcXzRJyrdc7h60mKrGR4WIRsDNL9qY9Rk/WlyPodsMZTfxaGxp+orb/ALm4bEf8LHt7U69jOorsiI5JA9Rx3HpWDJNnAKjrW9oEzNZOWCttYjOOaOW2pyVZRnP3Gcw6MjsjDDKcEehrbttFZ7eOVZAGZQ2GHHNQ67ZiOX7TH92Q/MPRv/r1tRT5tYRF02Dn8K0TujkkmnYopYzwlg8saK3XZkt+tUdXvjcSCEcRRevc1tdeTyagTToPNMrJkk556U7BcyYLVYovtN0CV6qnTJpkEsks4UgEseMDpWrf2ct0U8rGF7GrGm6Sts3mSnfJ9OBSYGpaL5duiZJwOpqwuSaYq8DAqYDFMQ4U7rSKpqQACluAgGKXNFHSqEFFIaOQM0ALUcrbV6En0FPzSGkNaFfBjUljksQfp2xWdeHn3BrSmG51FZ10SADkH73/AKFWElqdMXdFF5AByCW9KoTv84Kg8dqtSEbumKjtYfOuN3VFOTR0KpT5JpmlZQrbwAAfM3LUshJOc08cCqV/c+Sm1fvt09ves0rl6ykVL+cSMEXnaeT71TNGfXrQSKtHfGKjGww9Kt2z+TavIe5wv1qnKxCHHXFCSyzRokjfKvQAYq46anHiJbRLdumBuNWYozK29h8g6e9Vw22H5+FHUj0qyuoWoi+WZOB0PFOKu7s5akraIz9ZH7+PB6r0/GqG33xVm8nW5nDqflAwOKr5q2Zl+250+VD65H6VTJwOtXLYqtsqnvmqEwwStJok66GBpFHmAhcdDUzx28MeBbxEDHG0VOAiA7jk9hWOJJXcShgYnf7v9Kpoa7muLS3IybeI/wDABS/YrT/n1hz/ALgp0G6VF2E9Mk/yqYIckhuB1yOPelZhch+xWxHMCAewpraXYuMvbKfzq4EbCkgcjJB7UJlnAPBPf0osw5iidE00jm1X8z/jTDoGmE/8e+Po7f41ogMRnHfHFKoPzZGCue9GoN9ygdEsdm3Y+PTeab/YFiDwsg/4FWmQys2QOBkHP+eaVgQpKkZBA2kdfxoswuZn/CP2WM5lH/AqG8P2cg5ebA9GH+FajbsnI6dvalYlCFYcccj1PTNGotDIPhmy675+P9of4U5PDlqgIEkxyc8sP8K2QpIyO3b3oJAXcTgAZot1C5zd74btVid1ednwSqrgkn6Yrn5dJv0chLSdl9fLNegxqSTI3U9B6CpQuaYXPNP7M1DPNlc5/wCuTf4Vp6RbX1vHMr2dwASCB5R5ruguKcKTV1YcZWdzgNTjvZoliFncAZyT5bY/lU1i8ojSCS2mjZRgEocGu6ooSsEpczuzkxE+PuNn6VKkb9wcfSuooqrknPxRAGrAQVsYHoKTav8AdH5UXEZaJjvxUoUVf2J/dX8qTyo/7i/lSGUxS1aMSf3RR5Mf939adxFWirJgjPY/mab9mT1b86B2K9GanNsOzt+OKqxCSVdygZBII96VwsOFQXl3DZQmW4Yhc4GBkk1a8mTH3f1FVNS09r22MTRt1yCMcH86ASuZcviO0OFjilZmO3LAKB+NQ3sxDKq/KFJJ56gnNUpvDF/zsjJHoWFTxaXqqrtmt/MGMZ3DNZy12NknEWSMqvB3FulWreIRQqox6k+9RJY36gB7duOAetElvfrCyrbycjjis2m9DSK6kd7qUdtlEw8np2H1rGeZ5H3v8xPU0+TTL1W5tpcn/ZpgsL0f8u8v/fJrRRSIU5xd0BcDqDSbx7042t2PvW0n/fNBtrj/AJ4SD/gJp8qNfrNXsQXD/uyBxmiGQAetJLa3LH/UyYH+zUX2e4TnypB/wE07IxlOcnzSLs9wBbMB1biodPshdlmd9qJjOO9QNHOww0b/APfNbGI0s9kQ2MwGdo5zQlYiV5MzpkVHIT7oJA+lQnk4FTSq7H50YH1A4qEqynkUgsy3JIkSbVO4gY4qiSc5NPbmo/rT3Bqx3qROD8rjGP4lzWfdWTxsVTBRueeMfSthB8opky7k9xVvUhOxV02VVi2ZxtHTOMfWtHc4GPLJxyNrCs62gX7QQ33XO7HqR2P+e1agNMQ0O7D7pX605nXachmx2CHml6gihC2D3A70mxoTzmKjEbfTHP8AhQsjHpA3uSRx+Gar3N0DJ5IdkOMnA7fWqLalMsh8jCJnpjOfxqHUSZrGjKWxtZYDIHfv/P3oDqCCNzt7A4qjb6uj4EylD6jkVfSRZF3IwYeoNUpJkShKO4glUlt5289CDmnggkcZHXHUk+9KKdmnckDyMDj1Peoj+9k2D7ife9z6Us0hTai/6x+FH9foKkijEcYReg7nv70h7IcBThSCnUCClFJS0AFFFFAC0UlFAC0UUUAFFFFABRSUtABRRSUAFVbX5Li5j/2w4/Ef/Wq1VU/JqS/9NI/1B/8Ar0MqPVFqkoooJENFFFMBDTGFPNNNAEDoDURjq0ajIoArGOozHVsgGmFaAKpWmlTVorTCtAEIBp1O20EUwImFRNGrdVH5VYIpjLQBTe3Q/wAC/lVWS1jPWND+ArTIqJ0BoC5dzxUTOD0NPzUbxB/Y+opCGMp6qcEcg1aWVTGH9R0qkxmiyN2RSLOR1AqJuVvd3GkaCSFmA2kZq4FCpt/OskXAI7g+1WUvCqgP83uetZUva2aqFadDO1aEvOo9jyOvsBVCCC4AzJtiXtv6n8OtbNyxllR0XgD8c1mzRPOCzIxCk4wamTd3FHTTbJVgRuFk+Y9iuBUBuWt3xGxVgeSO1WrWynSUSXKEIwyCD+hFR2+lSTXcjTDbEHJH+1z/ACpQUlKzZSqqz5joUYMAR0Ip5ZUUsxAUDJJpkahVAHQVE/8ApM/lD/VR8ye57D+tdRxpXYtsjSO1zIMM4wgP8K//AF+tWqSigG7i0uaSigQ7NFJRQAtLSUUALRSUUALRSUEgdTQAtFJuGM5qvBeRzyzRqRmJtvXrwDn9aAsyzRTdwpQc0CFopKKBhVS9OyW2l9JNp/HirdVdSUmxkI6phx+BzQ9iofEizRSKwZQw6EZozTJCg0GkNACZpCaDTTQAE000ppDQAw0007r0ppoAQ000ppKYDaYxAqQ0yQiNDI/CjnJoAYSKaTTtyzN5sWHBTgZ75prx7AD0z29KAGMaYxpWphpiHxNnHcVYGMcVmWjN5hGeK0Y+lSD0FdcqQaqvHhSRV09DVG9YiHgkZ9K4qsqkayUXoy1awQjI3dqczZNVrNiY2BPFSEnP412WsTe5Y346VE0WTlTt5ySBmjtUUrsCMEiplFSKjJx2LzTtMwLdBwAO1XICCgrO/uN3Zcn61etvuVyUbqq0xy2JLicxoFjGZXO1B/WpIIhDEEBz3JPc9zVaD5tQnLclFUL7Z61crtQnorDqKSimSOpabS0gFpabS0CFopKBQA6ikooGLUcsaSY3549Din000AVLmPeUjViiY+Y+lU7e0tvtF2oypV12sDyPlFaMwGKy4f8Aj9u/95f/AEGkzSLdmaMV3aR/uy7Bhwd2aspNC/3JUPtmudu+Lw47gZpi8qCalysSo3Oox6GjB9K5kSyR/cdl+hqxb39zvAMxI98GmpXBxsbtMkXzI3Q9GBFLE7PFljk0oqyblSxk3WEeeoG0/hxUckxDkKeBSWpIWcDoJGx7VEaS2Kn8TH+c470ee/rUJ6UhNAibz39aPNf+8fzqDvTxQA/z3Hc/nTftT0xqSIAtzQItwtNIMrxjHGccVIytk8fj2qvDn95yeORz0pqTSO+1nJFMCaSRVQAbAcfMWPSoxJHlcyZHfjFUpSWlcnnmmECgC5PdKBhEBycZ3dPeqF3c3E+EAGwfrTj0pn8VROPMrXGtCxFOi2yLIDvHoKa10gOdrk+5pr9qqXRwv41a0Vg3JnvlyeKWKczHKr8vrWb1PNasKhYV2jFNMR//2Q==";
                    }
                    else
                    { 
                        org.IMAGE = reader["IMAGE_PATH"].ToString();
                    }
                    listofOrganisations.Add(org);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getAllListofOrganisations---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofOrganisations;
        }


        /*DL :  */
        public static List<OrganizationDetails> getListofOrganisationsBySearch(SqlConnection dataConnection, String name)
        {
            List<OrganizationDetails> listofOrganisations = new List<OrganizationDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ORG_ID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH from TABLE_ORG_DETAILS WHERE ORG_NAME LIKE @0 ORDER BY ORG_NAME";
                command.Parameters.AddWithValue("@0", "%" + name + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationDetails org = new OrganizationDetails();
                    org.ORG_ID = reader["ORG_ID"].ToString();
                    org.ORG_NAME = reader["ORG_NAME"].ToString();
                    org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    org.IMAGE = reader["IMAGE_PATH"].ToString();
                    listofOrganisations.Add(org);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getListofOrganisationsBySearch---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofOrganisations;
        }

        public static List<OrganizationCommunityDetails> getListofOrganisationsWithFollow(SqlConnection dataConnection, String userID)
        {
            List<OrganizationCommunityDetails> listofOrganisations = new List<OrganizationCommunityDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select 'unfollow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od, TABLE_USR_ORG uo where od.ORG_ID = uo.ORG_ID AND USR_ID = @0 Union (Select 'follow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od full join TABLE_USR_ORG uo on od.ORG_ID = uo.ORG_ID) except(Select 'follow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od, TABLE_USR_ORG uo where od.ORG_ID = uo.ORG_ID AND USR_ID = @0)";

                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationCommunityDetails org = new OrganizationCommunityDetails();
                    if (!reader["orgID"].ToString().Equals(null))
                    {
                        org.UNFOLLOW_STATUS = reader["UnfollowStatus"].ToString();
                        org.ORG_ID = reader["orgID"].ToString();
                        org.ORG_NAME = reader["ORG_NAME"].ToString();
                        org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                        org.IMAGE = reader["IMAGE_PATH"].ToString();
                        listofOrganisations.Add(org);
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofOrganisations;
        }


        public static List<OrganizationCommunityDetails> getListofOrganisationsWithFollowBySearch(SqlConnection dataConnection, String userID, String name)
        {
            List<OrganizationCommunityDetails> listofOrganisations = new List<OrganizationCommunityDetails>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select 'unfollow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od, TABLE_USR_ORG uo where od.ORG_ID = uo.ORG_ID AND USR_ID = @0 AND ORG_NAME LIKE @1 Union (Select 'follow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od full join TABLE_USR_ORG uo on od.ORG_ID = uo.ORG_ID WHERE ORG_NAME LIKE @1) except(Select 'follow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od, TABLE_USR_ORG uo where od.ORG_ID = uo.ORG_ID AND USR_ID = @0)";

                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", "%" + name + "%");

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    OrganizationCommunityDetails org = new OrganizationCommunityDetails();
                    if (!reader["orgID"].ToString().Equals(null))
                    {
                        org.UNFOLLOW_STATUS = reader["UnfollowStatus"].ToString();
                        org.ORG_ID = reader["orgID"].ToString();
                        org.ORG_NAME = reader["ORG_NAME"].ToString();
                        org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                        org.IMAGE = reader["IMAGE_PATH"].ToString();
                        listofOrganisations.Add(org);
                    }
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofOrganisations;
        }

        public static OrganizationCommunityDetails getOrganisationDetailsWithFollow(SqlConnection dataConnection, String userID, String orgID)
        {
            OrganizationCommunityDetails org = new OrganizationCommunityDetails();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select 'unfollow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING, IMAGE_PATH From TABLE_ORG_DETAILS od, TABLE_USR_ORG uo where od.ORG_ID = uo.ORG_ID AND USR_ID = @0 AND uo.ORG_ID = @1";

                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", orgID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!reader["UnfollowStatus"].Equals(null))
                    {
                        org.UNFOLLOW_STATUS = reader["UnfollowStatus"].ToString();
                        org.ORG_ID = reader["orgID"].ToString();
                        org.ORG_NAME = reader["ORG_NAME"].ToString();
                        org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                        org.IMAGE = reader["IMAGE_PATH"].ToString();
                        reader.Close();
                        return org;
                    }
                    else
                    {
                        org.UNFOLLOW_STATUS = "follow";
                        reader.Close();
                        return org;
                    }
                }
                org.UNFOLLOW_STATUS = "follow";
                reader.Close();
                return org;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return org;
        }

        public static OrganizationCommunityDetails getOrganisationDetailsWithFollow2(SqlConnection dataConnection, String orgID)
        {
            OrganizationCommunityDetails org = new OrganizationCommunityDetails();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select 'follow' As UnfollowStatus, od.ORG_ID as orgID, ORG_NAME, CONNECTION_STRING From TABLE_ORG_DETAILS od WHERE od.ORG_ID = @0";

                command.Parameters.AddWithValue("@0", orgID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    org.UNFOLLOW_STATUS = reader["UnfollowStatus"].ToString();
                    org.ORG_ID = reader["orgID"].ToString();
                    org.ORG_NAME = reader["ORG_NAME"].ToString();
                    org.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    return org;
                }
                reader.Close();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return org;
        }

        /*DL : Get all comments (words of encouragement) posted to the user*/
        public static List<UserComments> getCommentsForUser(SqlConnection dataConnection, String toUserID)
        {
            List<UserComments> listOfComments = new List<UserComments>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_COMMENT_ID, D1.FULL_NAME fnto, D2.FULL_NAME fnform, D2.IMAGE image, TO_USR_ID, FROM_USR_ID, COMMENT, COMMENT_DATE from TABLE_USR_COMMENT C, TABLE_USR_DETAILS D1, TABLE_USR_DETAILS D2 WHERE C.TO_USR_ID = D1.USR_ID AND C.FROM_USR_ID = D2.USR_ID AND C.TO_USR_ID = @0";

                command.Parameters.AddWithValue("@0", toUserID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserComments comment = new UserComments();
                    comment.USR_COMMENT_ID = reader["USR_COMMENT_ID"].ToString();
                    comment.FROM_USR_ID = reader["FROM_USR_ID"].ToString();
                    comment.TO_USR_ID = reader["TO_USR_ID"].ToString();
                    comment.FROM_USR_NAME = reader["fnform"].ToString();
                    comment.TO_USR_NAME = reader["fnto"].ToString();
                    comment.COMMENT = reader["COMMENT"].ToString();
                    comment.COMMENT_DATE = reader["COMMENT_DATE"].ToString();
                    comment.FROM_USR_IMAGE = reader["image"].ToString();
                    listOfComments.Add(comment);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getCommentsForUser---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfComments;
        }

        /*DL : Get all comments (words of encouragement) posted written by the user*/
        public static List<UserComments> getCommentsByUser(SqlConnection dataConnection, String toUserID)
        {
            List<UserComments> listOfComments = new List<UserComments>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_COMMENT_ID, D1.FULL_NAME fnto, D2.FULL_NAME fnform, TO_USR_ID, FROM_USR_ID, COMMENT, COMMENT_DATE from TABLE_USR_COMMENT C, TABLE_USR_DETAILS D1, TABLE_USR_DETAILS D2 WHERE C.TO_USR_ID = D1.USR_ID AND C.FROM_USR_ID = D2.USR_ID AND C.FROM_USR_ID = @0";

                command.Parameters.AddWithValue("@0", toUserID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserComments comment = new UserComments();
                    comment.USR_COMMENT_ID = reader["USR_COMMENT_ID"].ToString();
                    comment.FROM_USR_ID = reader["FROM_USR_ID"].ToString();
                    comment.TO_USR_ID = reader["TO_USR_ID"].ToString();
                    comment.FROM_USR_NAME = reader["fnform"].ToString();
                    comment.TO_USR_NAME = reader["fnto"].ToString();
                    comment.COMMENT = reader["COMMENT"].ToString();
                    comment.COMMENT_DATE = reader["COMMENT_DATE"].ToString();
                    listOfComments.Add(comment);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfComments;
        }


        /*DL : Get last comment date and time by user to another user*/
        public static String getCommentDatetimeByUserForUser(SqlConnection dataConnection, String fromUserID, String toUserID)
        {
            List<UserComments> listOfComments = new List<UserComments>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select COMMENT_DATE from TABLE_USR_COMMENT C, TABLE_USR_DETAILS D1, TABLE_USR_DETAILS D2 WHERE C.TO_USR_ID = D1.USR_ID AND C.FROM_USR_ID = D2.USR_ID AND C.FROM_USR_ID = @0 AND C.TO_USR_ID = @1";

                command.Parameters.AddWithValue("@0", fromUserID);
                command.Parameters.AddWithValue("@1", toUserID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    String commentDatetime = reader["COMMENT_DATE"].ToString();
                    reader.Close();
                    return commentDatetime;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return System.DateTime.Now.AddDays(-7).ToString();
        }

        /*DL : Get activity ID from a comment*/
        public static String getActivityIDfromComment(SqlConnection dataConnection, String commentID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT USR_ACTIVITY_ID FROM TABLE_USR_COMMENT WHERE USR_COMMENT_ID=@0";

                command.Parameters.AddWithValue("@0", commentID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return reader["USR_ACTIVITY_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "";
        }

        /*DL : Add a comment (words of encouragement) to another user*/
        public static String addComments(SqlConnection dataConnection, String toUserID, String fromUserID, String comment)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "INSERT INTO TABLE_USR_COMMENT (USR_COMMENT_ID, TO_USR_ID, FROM_USR_ID, COMMENT, COMMENT_DATE) VALUES (NEWID(), @TO_USR_ID, @FROM_USR_ID, @COMMENT, @COMMENT_DATE)";
                SqlParameter TO_USR_ID = new SqlParameter("TO_USR_ID", SqlDbType.NVarChar, 100);
                TO_USR_ID.Value = toUserID;
                command.Parameters.Add(TO_USR_ID);
                SqlParameter FROM_USR_ID = new SqlParameter("FROM_USR_ID", SqlDbType.NVarChar, 100);
                FROM_USR_ID.Value = fromUserID;
                command.Parameters.Add(FROM_USR_ID);
                SqlParameter COMMENT = new SqlParameter("COMMENT", SqlDbType.NVarChar, 100);
                COMMENT.Value = comment;
                command.Parameters.Add(COMMENT);
                SqlParameter COMMENT_DATE = new SqlParameter("COMMENT_DATE", SqlDbType.SmallDateTime, 100);
                COMMENT_DATE.Value = System.DateTime.Now;
                command.Parameters.Add(COMMENT_DATE);

                command.ExecuteNonQuery();

                return "added";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: ---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
                return "failed";
            }
        }

        /*DL : Remove a comment (words of encouragement) form a user*/
        public static String removeComment(SqlConnection dataConnection, String commentID)
        {
            SqlCommand command = new SqlCommand("DELETE FROM TABLE_USR_COMMENT WHERE USR_COMMENT_ID=@USR_COMMENT_ID", dataConnection);

            /*
            SqlParameter TO_USR_ID = new SqlParameter("TO_USR_ID", SqlDbType.NVarChar, 100);
            TO_USR_ID.Value = toUserID;
            command.Parameters.Add(TO_USR_ID);
            SqlParameter FROM_USR_ID = new SqlParameter("FROM_USR_ID", SqlDbType.NVarChar, 100);
            FROM_USR_ID.Value = fromUserID;
            command.Parameters.Add(FROM_USR_ID);
            */
            SqlParameter USR_COMMENT_ID = new SqlParameter("USR_COMMENT_ID", SqlDbType.NVarChar, 100);
            USR_COMMENT_ID.Value = commentID;
            command.Parameters.Add(USR_COMMENT_ID);

            command.ExecuteNonQuery();

            return "deleted";
        }


        /* NO LONGER FUNCTION
        public static List<UserAcitvities> getUserActivities(SqlConnection dataConnection, String userID)
        {
            List<UserAcitvities> listOfUsersDetails = new List<UserAcitvities>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ACTIVITY_ID, ud1.FULL_NAME fullname, ACTIVITY_TYPE, TARGET_USR_ID, ud2.FULL_NAME targetuserfullname, DATE_TIME from TABLE_USR_ACTIVITY ua, TABLE_USR_DETAILS ud1, TABLE_USR_DETAILS ud2 WHERE ua.USR_ID=ud1.USR_ID AND ud2.USR_ID= ua.TARGET_USR_ID AND ua.USR_ID = @0 ORDER BY DATE_TIME desc";

                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserAcitvities activity = new UserAcitvities();

                    activity.USR_ID = userID;
                    activity.USR_ACTIVITY_ID = reader["USR_ACTIVITY_ID"].ToString();
                    activity.ACTIVITY_TYPE = reader["ACTIVITY_TYPE"].ToString();
                    activity.TARGET_USR_ID = reader["TARGET_USR_ID"].ToString();
                    activity.TARGET_USR_NAME = reader["targetuserfullname"].ToString();
                    activity.USR_FULL_NAME = reader["fullname"].ToString();

                    if (activity.ACTIVITY_TYPE.Equals("JOIN EVENT"))
                    {
                        activity.MESSAGE_DISPLAY = activity.USR_FULL_NAME + " has joined a new event - " + activity.EVENT_NAME;
                    }
                    else if (activity.ACTIVITY_TYPE.Equals("ADDFRIEND"))
                    {
                        activity.MESSAGE_DISPLAY = activity.USR_FULL_NAME + " is friends with " + activity.TARGET_USR_NAME;
                    }
                    else if (activity.ACTIVITY_TYPE.Equals("ADDCOMMENT"))
                    {
                        activity.MESSAGE_DISPLAY = activity.USR_FULL_NAME + " has commented on " + activity.TARGET_USR_NAME + " portfolio";
                    }

                    activity.DATETIME = reader["DATE_TIME"].ToString();

                    listOfUsersDetails.Add(activity);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserActivities---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfUsersDetails;
        }
*/
        /*DL : Upload image to user*/
        public static String uploadImage(SqlConnection dataConnection, String imagePath, String userID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_USR_DETAILS SET IMAGE=@IMAGE WHERE USR_ID=@USR_ID", dataConnection);

                SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.VarChar, 1048576);
                IMAGE.Value = imagePath;
                command.Parameters.Add(IMAGE);

                SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                USR_ID.Value = userID;
                command.Parameters.Add(USR_ID);

                command.Prepare();
                command.ExecuteNonQuery();

                return "image stored";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }
    }
}
 