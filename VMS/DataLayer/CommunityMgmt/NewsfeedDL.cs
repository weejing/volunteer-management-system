﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Models.CommunityModels;
using VMS.Models.EventModels;

namespace VMS.DataLayer.CommunityMgmt
{
    public class NewsfeedDL
    {

        /*DL : */
        public static String addUserActivity(SqlConnection dataConnection, String userID, String activityType, String targetUserID, String targetUserName, String eventID, String eventName)
        {
            // ALL FIELDS

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_ACTIVITY (USR_ACTIVITY_ID, USR_ID, ACTIVITY_TYPE, TARGET_USR_ID, TARGET_USR_NAME, DATE_TIME) VALUES (NEWID(), @USR_ID, @ACTIVITY_TYPE, @TARGET_USR_ID, @TARGET_USR_NAME, @DATE_TIME)";
            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);
            SqlParameter ACTIVITY_TYPE = new SqlParameter("ACTIVITY_TYPE", SqlDbType.NVarChar, 100);
            ACTIVITY_TYPE.Value = activityType;
            command.Parameters.Add(ACTIVITY_TYPE);
            SqlParameter TARGET_USR_ID = new SqlParameter("TARGET_USR_ID", SqlDbType.NVarChar, 100);
            TARGET_USR_ID.Value = targetUserID;
            command.Parameters.Add(TARGET_USR_ID);
            SqlParameter TARGET_USR_NAME = new SqlParameter("TARGET_USR_NAME", SqlDbType.NVarChar, 100);
            TARGET_USR_NAME.Value = targetUserName;
            command.Parameters.Add(TARGET_USR_NAME);
            SqlParameter EVENT_ID = new SqlParameter("EVENT_ID", SqlDbType.NVarChar, 100);
            EVENT_ID.Value = eventID;
            command.Parameters.Add(EVENT_ID);
            SqlParameter EVENT_NAME = new SqlParameter("EVENT_NAME", SqlDbType.NVarChar, 100);
            EVENT_NAME.Value = eventName;
            command.Parameters.Add(EVENT_NAME);
            SqlParameter DATETIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime);
            DATETIME.Value = System.DateTime.Now;
            command.Parameters.Add(DATETIME);
            command.ExecuteNonQuery();

            return "added";
        }

        /*DL : */
        public static String addUserActivityUser(SqlConnection dataConnection, String userID, String activityType, String targetUserID)
        {
            // USER INTERACTION FIELDS

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_ACTIVITY (USR_ACTIVITY_ID, USR_ID, ACTIVITY_TYPE, TARGET_USR_ID, DATE_TIME) VALUES (NEWID(), @USR_ID, @ACTIVITY_TYPE, @TARGET_USR_ID, @DATE_TIME)";
            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);
            SqlParameter ACTIVITY_TYPE = new SqlParameter("ACTIVITY_TYPE", SqlDbType.NVarChar, 100);
            ACTIVITY_TYPE.Value = activityType;
            command.Parameters.Add(ACTIVITY_TYPE);
            SqlParameter TARGET_USR_ID = new SqlParameter("TARGET_USR_ID", SqlDbType.NVarChar, 100);
            TARGET_USR_ID.Value = targetUserID;
            command.Parameters.Add(TARGET_USR_ID);
            SqlParameter DATETIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime);
            DATETIME.Value = System.DateTime.Now;
            command.Parameters.Add(DATETIME);
            command.ExecuteNonQuery();

            return "added";
        }


        /*DL : */
        public static String addUserActivityEvent(SqlConnection dataConnection, String userID, String activityType, String eventID, String eventName)
        {
            // EVENT INTERACTION FIELDS

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_USR_ACTIVITY (USR_ACTIVITY_ID, USR_ID, ACTIVITY_TYPE, EVENT_ID, EVENT_NAME,  DATE_TIME) VALUES (NEWID(), @USR_ID, @ACTIVITY_TYPE, @EVENT_ID, @EVENT_NAME @DATE_TIME)";
            SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
            USR_ID.Value = userID;
            command.Parameters.Add(USR_ID);
            SqlParameter ACTIVITY_TYPE = new SqlParameter("ACTIVITY_TYPE", SqlDbType.NVarChar, 100);
            ACTIVITY_TYPE.Value = activityType;
            command.Parameters.Add(ACTIVITY_TYPE);
            SqlParameter TARGET_USR_ID = new SqlParameter("TARGET_USR_ID", SqlDbType.NVarChar, 100);
            //TARGET_USR_ID.Value = "00000000-0000-0000-0000-fcb4f48fd786";
            command.Parameters.Add(TARGET_USR_ID);
            SqlParameter EVENT_ID = new SqlParameter("EVENT_ID", SqlDbType.NVarChar, 100);
            EVENT_ID.Value = eventID;
            command.Parameters.Add(EVENT_ID);
            SqlParameter EVENT_NAME = new SqlParameter("EVENT_NAME", SqlDbType.NVarChar, 100);
            EVENT_NAME.Value = eventName;
            command.Parameters.Add(EVENT_NAME);
            SqlParameter DATETIME = new SqlParameter("DATE_TIME", SqlDbType.DateTime);
            DATETIME.Value = System.DateTime.Now;
            command.Parameters.Add(DATETIME);
            command.ExecuteNonQuery();

            return "added";
        }

        /*DL : */
        public static String selectActivityID(SqlConnection dataConnection, String userID, String activityType, String targetUserID)
        {
            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "SELECT Top 1 USR_ACTIVITY_ID FROM TABLE_USR_ACTIVITY WHERE USR_ID = @0 AND ACTIVITY_TYPE = @1 AND TARGET_USR_ID = @2 ORDER BY DATE_TIME desc";
                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", activityType);
                command.Parameters.AddWithValue("@2", targetUserID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return reader["USR_ACTIVITY_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriendsActivities---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "failed to retrieve id";
        }

        /*DL : Update*/
        public static String updateActivityIDtoUserFriend(SqlConnection dataConnection, String userID, String activityType, String targetUserID)
        {
            try
            {
                if (activityType.Equals("ADDFRIEND"))
                {
                    SqlCommand command = new SqlCommand("UPDATE TABLE_USR_FRIEND SET USR_ACTIVITY_ID = (SELECT Top 1 USR_ACTIVITY_ID FROM TABLE_USR_ACTIVITY WHERE USR_ID = @0 AND ACTIVITY_TYPE = @1 AND TARGET_USR_ID = @2 ORDER BY DATE_TIME desc) WHERE USR1_ID = @3 AND USR2_ID = @4", dataConnection);

                    SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                    USR_ID.Value = userID;
                    command.Parameters.Add(USR_ID);

                    SqlParameter ACTIVITY_TYPE = new SqlParameter("ACTIVITY_TYPE", SqlDbType.NVarChar, 50);
                    ACTIVITY_TYPE.Value = activityType;
                    command.Parameters.Add(ACTIVITY_TYPE);

                    SqlParameter TARGET_USR_ID = new SqlParameter("TARGET_USR_ID", SqlDbType.NVarChar, 100);
                    TARGET_USR_ID.Value = targetUserID;
                    command.Parameters.Add(TARGET_USR_ID);

                    SqlParameter USR1_ID = new SqlParameter("USR1_ID", SqlDbType.NVarChar, 100);
                    USR1_ID.Value = userID;
                    command.Parameters.Add(USR1_ID);

                    SqlParameter USR2_ID = new SqlParameter("USR2_ID", SqlDbType.NVarChar, 100);
                    USR2_ID.Value = targetUserID;
                    command.Parameters.Add(USR2_ID);

                    command.Prepare();
                    command.ExecuteNonQuery();
                }
                else if(activityType.Equals("ADDCOMMENT"))
                {
                    SqlCommand command = new SqlCommand("UPDATE TABLE_USR_COMMENT SET USR_ACTIVITY_ID = (SELECT Top 1 USR_ACTIVITY_ID FROM TABLE_USR_ACTIVITY WHERE USR_ID = @0 AND ACTIVITY_TYPE = @1 AND TARGET_USR_ID = @2 ORDER BY DATE_TIME desc) WHERE FROM_USR_ID = @3 AND TO_USR_ID = @4", dataConnection);

                    SqlParameter USR_ID = new SqlParameter("USR_ID", SqlDbType.NVarChar, 100);
                    USR_ID.Value = userID;
                    command.Parameters.Add(USR_ID);

                    SqlParameter ACTIVITY_TYPE = new SqlParameter("ACTIVITY_TYPE", SqlDbType.NVarChar, 50);
                    ACTIVITY_TYPE.Value = activityType;
                    command.Parameters.Add(ACTIVITY_TYPE);

                    SqlParameter TARGET_USR_ID = new SqlParameter("TARGET_USR_ID", SqlDbType.NVarChar, 100);
                    TARGET_USR_ID.Value = targetUserID;
                    command.Parameters.Add(TARGET_USR_ID);

                    SqlParameter USR1_ID = new SqlParameter("FROM_USR_ID", SqlDbType.NVarChar, 100);
                    USR1_ID.Value = userID;
                    command.Parameters.Add(USR1_ID);

                    SqlParameter USR2_ID = new SqlParameter("TO_USR_ID", SqlDbType.NVarChar, 100);
                    USR2_ID.Value = targetUserID;
                    command.Parameters.Add(USR2_ID);

                    command.Prepare();
                    command.ExecuteNonQuery();
                }
                
                return "updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL : Delete*/
        public static String removeActivity(SqlConnection dataConnection, String activityID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_USR_ACTIVITY WHERE USR_ACTIVITY_ID = @0", dataConnection);

                SqlParameter USR_ACTIVITY_ID = new SqlParameter("USR_ACTIVITY_ID", SqlDbType.NVarChar, 100);
                USR_ACTIVITY_ID.Value = activityID;
                command.Parameters.Add(USR_ACTIVITY_ID);
                
                command.Prepare();
                command.ExecuteNonQuery();
                return "deleted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        /*DL : Get users with top VIA hours  */
        public static List<UserPortfolio> getTopUsersVIA(SqlConnection dataConnection, int size)
        {
            List<UserPortfolio> listofUsers = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                //command.CommandText = "Select TOP 5 FULL_NAME, uv.USR_ID as id, ud.IMAGE, SUM(VIA_HOURS) as totalVIA from TABLE_USR_VIA uv, TABLE_USR_DETAILS ud WHERE uv.USR_ID = ud.USR_ID GROUP BY uv.USR_ID, FULL_NAME ORDER BY totalVIA desc";
                command.CommandText = "Select TOP " + size + " FULL_NAME, USR_ID, IMAGE, VIA_HOURS from TABLE_USR_DETAILS WHERE VIA_HOURS > 0 ORDER BY VIA_HOURS desc";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio up = new UserPortfolio();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.VIA_HOURS = reader["VIA_HOURS"].ToString();
                    up.IMAGE =  reader["IMAGE"].ToString();
                    //up.IMAGE = "data:image/jpeg;base64," + reader["IMAGE"].ToString();
                    listofUsers.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserID---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listofUsers;
        }

        /*DL : Get activities of friends (friends they added)*/
        public static List<UserNewsfeed> getFriendsActivitiesFriendsAdded(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud1.FULL_NAME, ud1.USR_ID, ud1.IMAGE, ud2.FULL_NAME TARGET_FULL_NAME, ud2.USR_ID TARGET_USR_ID, ud2.IMAGE T_IMAGE, DATE_ADDED from TABLE_USR_FRIEND uf, TABLE_USR_DETAILS ud1, TABLE_USR_DETAILS ud2 WHERE uf.USR1_ID = ud1.USR_ID AND uf.USR2_ID = ud2.USR_ID AND uf.STATUS = 'friend' AND uf.USR1_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend' UNION Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend') ORDER BY DATE_ADDED desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["USR_ID"].ToString();
                    activity.TARGET_NAME = reader["TARGET_FULL_NAME"].ToString();
                    activity.TARGET_ID = reader["TARGET_USR_ID"].ToString();
                    activity.ACTIVITY_DATETIME = reader["DATE_ADDED"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = reader["T_IMAGE"].ToString();
                    activity.ACTIVITY_TYPE = "ADDFRIEND";
                    activity.MESSAGE_DISPLAY = " is friends with ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }

        /*DL : Get activities of friends (comment they post)*/
        public static List<UserNewsfeed> getFriendsActivitiesComments(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud1.FULL_NAME, ud1.USR_ID FIRST_USR_ID, ud1.IMAGE, ud2.FULL_NAME TARGET_FULL_NAME, ud2.USR_ID TARGET_USR_ID, ud2.IMAGE T_IMAGE, uc.COMMENT, COMMENT_DATE from TABLE_USR_COMMENT uc, TABLE_USR_DETAILS ud1, TABLE_USR_DETAILS ud2 WHERE uc.FROM_USR_ID=ud1.USR_ID AND uc.TO_USR_ID = ud2.USR_ID AND uc.FROM_USR_ID IN  (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend' UNION Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend') ORDER BY COMMENT_DATE desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["FIRST_USR_ID"].ToString();
                    activity.TARGET_NAME = reader["TARGET_FULL_NAME"].ToString();
                    activity.TARGET_ID = reader["TARGET_USR_ID"].ToString();
                    activity.COMMENTED_MESSAGE = reader["COMMENT"].ToString();
                    activity.ACTIVITY_DATETIME = reader["COMMENT_DATE"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = reader["T_IMAGE"].ToString();
                    activity.ACTIVITY_TYPE = "ADDCOMMENT";
                    activity.MESSAGE_DISPLAY = " has commented on ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }



        /*DL : Get activities of friends (organsiations they followed)*/
        public static List<UserNewsfeed> getFriendsActivitiesOrgFollows(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.FULL_NAME, ud.USR_ID, ud.IMAGE, od.ORG_NAME, od.ORG_ID, uo.FOLLOW_DATE, od.IMAGE_PATH from TABLE_USR_ORG uo, TABLE_USR_DETAILS ud, TABLE_ORG_DETAILS od WHERE uo.ORG_ID=od.ORG_ID AND uo.USR_ID = ud.USR_ID AND ud.USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend' UNION Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend') ORDER BY FOLLOW_DATE desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["USR_ID"].ToString();
                    activity.TARGET_NAME = reader["ORG_NAME"].ToString();
                    activity.TARGET_ID = reader["USR_ID"].ToString();
                    activity.ACTIVITY_DATETIME = reader["FOLLOW_DATE"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = "SaaS/" + reader["IMAGE_PATH"].ToString();
                    activity.ACTIVITY_TYPE = "FOLLOWORGANISATION";
                    activity.MESSAGE_DISPLAY = " has followed an organisation : ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }


        /*DL : Get activities of friends (events joined)*/
        public static List<UserEvent> getFriendsActivitiesEventsJoined(SqlConnection dataConnection, String userID)
        {
            List<UserEvent> listOfEventsInfo = new List<UserEvent>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.FULL_NAME, ud.USR_ID, ud.IMAGE, ev.EVT_ID, ev.CONNECTION_STRING from TABLE_USR_DETAILS ud, TABLE_USR_EVENT ev WHERE ev.USR_ID = ud.USR_ID AND ud.USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend' UNION Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend')";
                //command.CommandText = "Select ud.FULL_NAME, ud.USR_ID, ud.IMAGE, ev.EVT_ID from TABLE_USR_DETAILS ud, TABLE_USR_EVENT ev WHERE ev.USR_ID = ud.USR_ID AND ud.USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend' UNION Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend')";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserEvent eventInfo = new UserEvent();
                    eventInfo.USR_ID = reader["USR_ID"].ToString();
                    eventInfo.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    eventInfo.USR_IMAGE = reader["IMAGE"].ToString();
                    eventInfo.EVENT_ID = reader["EVT_ID"].ToString();
                    eventInfo.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    listOfEventsInfo.Add(eventInfo);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfEventsInfo;
        }


        /*DL : Get activities of friends (events joined)*/
        public static EventDetails getActivitiesEventsJoinedDetails(SqlConnection dataConnection, String eventID)
        {
            EventDetails eventInfo = new EventDetails();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select EVT_NAME from TABLE_EVT_DETAILS WHERE EVT_ID = @0";
                command.Parameters.AddWithValue("@0", eventID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    eventInfo.eventName = reader["EVT_NAME"].ToString();
                    reader.Close();
                    return eventInfo;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return eventInfo;
        }


        /*DL : Get activities of friends (friends they added)*/
        public static List<UserNewsfeed> getUserActivitiesFriendsAdded(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud1.FULL_NAME, ud1.USR_ID, ud1.IMAGE, ud2.FULL_NAME TARGET_FULL_NAME, ud2.USR_ID TARGET_USR_ID, ud2.IMAGE T_IMAGE, DATE_ADDED from TABLE_USR_FRIEND uf, TABLE_USR_DETAILS ud1, TABLE_USR_DETAILS ud2 WHERE uf.USR1_ID = ud1.USR_ID AND uf.USR2_ID = ud2.USR_ID AND uf.STATUS = 'friend' AND uf.USR1_ID = @0 ORDER BY DATE_ADDED desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["USR_ID"].ToString();
                    activity.TARGET_NAME = reader["TARGET_FULL_NAME"].ToString();
                    activity.TARGET_ID = reader["TARGET_USR_ID"].ToString();
                    activity.ACTIVITY_DATETIME = reader["DATE_ADDED"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = reader["T_IMAGE"].ToString();
                    activity.ACTIVITY_TYPE = "ADDFRIEND";
                    activity.MESSAGE_DISPLAY = " is friends with ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }

        /*DL : Get activities of friends (comment they post)*/
        public static List<UserNewsfeed> getUserActivitiesComments(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud1.FULL_NAME, ud1.USR_ID, ud1.IMAGE, ud2.FULL_NAME TARGET_FULL_NAME, ud2.USR_ID TARGET_USR_ID, ud2.IMAGE T_IMAGE, uc.COMMENT, COMMENT_DATE from TABLE_USR_COMMENT uc, TABLE_USR_DETAILS ud1, TABLE_USR_DETAILS ud2 WHERE uc.FROM_USR_ID=ud1.USR_ID AND uc.TO_USR_ID = ud2.USR_ID AND uc.FROM_USR_ID = @0 ORDER BY COMMENT_DATE desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["USR_ID"].ToString();
                    activity.TARGET_NAME = reader["TARGET_FULL_NAME"].ToString();
                    activity.TARGET_ID = reader["TARGET_USR_ID"].ToString();
                    activity.COMMENTED_MESSAGE = reader["COMMENT"].ToString();
                    activity.ACTIVITY_DATETIME = reader["COMMENT_DATE"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = reader["T_IMAGE"].ToString();
                    activity.ACTIVITY_TYPE = "ADDCOMMENT";
                    activity.MESSAGE_DISPLAY = " has commented on ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }



        /*DL : Get activities of friends (organsiations they followed)*/
        public static List<UserNewsfeed> getUserActivitiesOrgFollows(SqlConnection dataConnection, String userID)
        {
            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.FULL_NAME, ud.USR_ID, ud.IMAGE, od.ORG_NAME, od.ORG_ID, uo.FOLLOW_DATE, od.IMAGE_PATH from TABLE_USR_ORG uo, TABLE_USR_DETAILS ud, TABLE_ORG_DETAILS od WHERE uo.ORG_ID=od.ORG_ID AND uo.USR_ID = ud.USR_ID AND ud.USR_ID = @0 ORDER BY FOLLOW_DATE desc";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserNewsfeed activity = new UserNewsfeed();

                    activity.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    activity.USR_ID = reader["USR_ID"].ToString();
                    activity.TARGET_NAME = reader["ORG_NAME"].ToString();
                    activity.TARGET_ID = reader["USR_ID"].ToString();
                    activity.ACTIVITY_DATETIME = reader["FOLLOW_DATE"].ToString();
                    activity.USR_IMAGE = reader["IMAGE"].ToString();
                    activity.TARGET_IMAGE = "SaaS/" + reader["IMAGE_PATH"].ToString();
                    activity.ACTIVITY_TYPE = "FOLLOWORGANISATION";
                    activity.MESSAGE_DISPLAY = " has followed an organisation : ";

                    listOfActivities.Add(activity);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfActivities;
        }


        /*DL : Get activities of friends (events joined)*/
        public static List<UserEvent> getUserActivitiesEventsJoined(SqlConnection dataConnection, String userID)
        {
            List<UserEvent> listOfEventsInfo = new List<UserEvent>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select ud.FULL_NAME, ud.USR_ID, ud.IMAGE, ev.EVT_ID, ev.CONNECTION_STRING from TABLE_USR_DETAILS ud, TABLE_USR_EVENT ev WHERE ev.USR_ID = ud.USR_ID AND ud.USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserEvent eventInfo = new UserEvent();
                    eventInfo.USR_ID = reader["USR_ID"].ToString();
                    eventInfo.USR_FULL_NAME = reader["FULL_NAME"].ToString();
                    eventInfo.USR_IMAGE = reader["IMAGE"].ToString();
                    eventInfo.EVENT_ID = reader["EVT_ID"].ToString();
                    eventInfo.CONNECTION_STRING = reader["CONNECTION_STRING"].ToString();
                    listOfEventsInfo.Add(eventInfo);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return listOfEventsInfo;
        }


        /*DL : Get list of user's friend of friend */
        public static List<UserPortfolio> getUserFriendsOfFriends(SqlConnection dataConnection, String userID, String friendID)
        {
            List<UserPortfolio> friends = new List<UserPortfolio>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS WHERE USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @1 AND STATUS = 'friend' AND USR1_ID <> @0) UNION Select USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS WHERE USR_ID IN (Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @1 AND STATUS = 'friend' AND USR2_ID <> @0) EXCEPT (Select USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS WHERE USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID = @0 AND STATUS = 'friend') UNION Select USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS WHERE USR_ID IN (Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID = @0 AND STATUS = 'friend'))";
                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", friendID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolio up = new UserPortfolio();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    up.IMAGE = reader["IMAGE"].ToString();
                    friends.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return friends;
        }


        /*DL : Get user facebook ID , if user don't have facebook ID, it will return empty string*/
        public static String getUserFacebookID(SqlConnection dataConnection, String userID)
        {
            String userFacebookID = "";

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select FACEBOOK_ID from TABLE_USR_DETAILS WHERE USR_ID = @0";
                command.Parameters.AddWithValue("@0", userID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userFacebookID = reader["FACEBOOK_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return userFacebookID;
        }

        /*DL : Get list of user's friend of friend */
        public static List<UserPortfolioRecommendationModel> getUserFacebookFriendsRecommend(SqlConnection dataConnection, String userID, String friendFacebookID)
        {
            List<UserPortfolioRecommendationModel> friendsRecommend = new List<UserPortfolioRecommendationModel>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "(Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE FACEBOOK_ID = @1 AND USR_ID IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR2_ID <> @0) UNION Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE FACEBOOK_ID = @1 AND USR_ID IN (Select USR2_ID from TABLE_USR_FRIEND WHERE USR1_ID <> @0) EXCEPT Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE USR_ID = @0) UNION (Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE FACEBOOK_ID = @1 AND USR_ID NOT IN (Select USR1_ID from TABLE_USR_FRIEND WHERE USR1_ID <> @0) UNION Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE FACEBOOK_ID = @1 AND USR_ID NOT IN (Select USR2_ID from TABLE_USR_FRIEND WHERE USR2_ID <> @0) EXCEPT Select ud.USR_ID, FULL_NAME, IMAGE from TABLE_USR_DETAILS ud WHERE USR_ID = @0)";
                command.Parameters.AddWithValue("@0", userID);
                command.Parameters.AddWithValue("@1", friendFacebookID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserPortfolioRecommendationModel up = new UserPortfolioRecommendationModel();
                    up.USR_ID = reader["USR_ID"].ToString();
                    up.FULL_NAME = reader["FULL_NAME"].ToString();
                    up.IMAGE = reader["IMAGE"].ToString();
                    friendsRecommend.Add(up);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---PortfolioDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getUserFriends---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return friendsRecommend;
        }

    }
}