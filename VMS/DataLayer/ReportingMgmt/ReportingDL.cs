﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.Models.ReportingModels;

namespace VMS.DataLayer.ReportingMgmt
{
    public class ReportingDL
    {
        //general method
        public static List<string> getNPOConnect(SqlConnection dbConnection)
        {
            try
            {
                //calling sql statements
                string sql_str = @"select * from TABLE_ORG_DETAILS";
                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                List<string> ans = new List<string>();
                while (dataReader.Read())
                {
                    string data = dataReader["CONNECTION_STRING"].ToString();
                    ans.Add(data);
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {

            }
            return null;
        }

        //volunteer reporting
        //get volunteer sign up rate
        public static List<ReportVolunteers> getVolNumDataDL(SqlConnection dbConnection, string dateStr, string orgID)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select CONVERT(CHAR(10),REGISTRATION_DATE,103) AS DATE, COUNT(CONVERT(CHAR(10),REGISTRATION_DATE,103)) AS VOL_REG
from TABLE_USR_DETAILS 
where REGISTRATION_DATE >= @dateStr
group by CONVERT(CHAR(10),REGISTRATION_DATE,103)
order by CONVERT(CHAR(10),REGISTRATION_DATE,103) asc";

                if (orgID != null)
                {
                    sql_str = @"select CONVERT(CHAR(10),FOLLOW_DATE,103) AS DATE, COUNT(CONVERT(CHAR(10),FOLLOW_DATE,103)) AS VOL_REG
from TABLE_USR_ORG 
where ORG_ID = @orgID and FOLLOW_DATE >= @dateStr
group by CONVERT(CHAR(10),FOLLOW_DATE,103)
order by CONVERT(CHAR(10),FOLLOW_DATE,103) asc";
                }
                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("dateStr", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = dateStr;
                command.Parameters.Add(sql_dateStr);

                if (orgID != null)
                {
                    SqlParameter sql_orgID = new SqlParameter("orgID", SqlDbType.NVarChar, 50);
                    sql_orgID.Value = orgID;
                    command.Parameters.Add(sql_orgID);
                }

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportVolunteers> ans = new List<ReportVolunteers>();
                while (dataReader.Read())
                {
                    ReportVolunteers data = new ReportVolunteers();
                    data.VOLUNTEER_DATE = dataReader["DATE"].ToString();
                    data.VOLUNTEER_COUNT = dataReader["VOL_REG"].ToString();
                    ans.Add(data);
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //breakdown of interest
        public static List<ReportVolunteers> getVolInterestDataDL(SqlConnection dbConnection, string orgID)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select v.VOCAB_NAME, t.TERM_NAME, COUNT(t.TERM_NAME) as TERM_COUNT
from TABLE_TERM_RELATION r
left join TABLE_TERM t on r.TERM_ID = t.TERM_ID and r.VOCAB_ID = t.VOCAB_ID
left join TABLE_VOCAB v on r.VOCAB_ID = v.VOCAB_ID
group by t.TERM_NAME,  v.VOCAB_NAME
order by v.VOCAB_NAME";

                if (orgID != null)
                {
                    sql_str = @"select v.VOCAB_NAME, t.TERM_NAME, COUNT(t.TERM_NAME) as TERM_COUNT
from TABLE_TERM_RELATION r
left join TABLE_TERM t on r.TERM_ID = t.TERM_ID and r.VOCAB_ID = t.VOCAB_ID
left join TABLE_VOCAB v on r.VOCAB_ID = v.VOCAB_ID
left join TABLE_USR_ORG o on r.ENTITY_ID = o.USR_ID
where ORG_ID = @orgID
group by t.TERM_NAME,  v.VOCAB_NAME
order by v.VOCAB_NAME";
                }
                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                if (orgID != null)
                {
                    SqlParameter sql_orgID = new SqlParameter("orgID", SqlDbType.NVarChar, 50);
                    sql_orgID.Value = orgID;
                    command.Parameters.Add(sql_orgID);
                }

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportVolunteers> ans = new List<ReportVolunteers>();
                while (dataReader.Read())
                {
                    ReportVolunteers data = new ReportVolunteers();
                    data.VOLUNTEER_INTEREST_CAT = dataReader["VOCAB_NAME"].ToString();
                    data.VOLUNTEER_INTEREST = dataReader["TERM_NAME"].ToString();
                    data.VOLUNTEER_COUNT = dataReader["TERM_COUNT"].ToString();
                    ans.Add(data);
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }

        //list of interest
        public static List<ItemSummary> getInterestListDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select * 
from TABLE_TERM t
left join TABLE_VOCAB v on t.VOCAB_ID = v.VOCAB_ID
order by v.VOCAB_NAME, t.TERM_NAME";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ItemSummary> ans = new List<ItemSummary>();
                while (dataReader.Read())
                {
                    ItemSummary data = new ItemSummary();
                    data.ITEM_NAME = dataReader["VOCAB_NAME"].ToString();
                    data.ITEM_DESC = dataReader["TERM_NAME"].ToString();
                    ans.Add(data);
                }
                dataReader.Close();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //MBTI
        public static AVolunteerReport getVolunteerDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                //string sql_str = "select * from TABLE_USR_DETAILS";
                string sql_str = "select COUNT(USR_ID) AS OUTPUT , PERSONALITY_CHAR from TABLE_USR_DETAILS GROUP BY PERSONALITY_CHAR ";
                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                SqlDataReader dataReader = command.ExecuteReader();
                AVolunteerReport item = new AVolunteerReport();
                List<ItemSummary> sub_item = null;
                while (dataReader.Read())
                {
                    ItemSummary temp = new ItemSummary();
                    temp.ITEM_NAME = dataReader["PERSONALITY_CHAR"].ToString();
                    temp.ITEM_AMOUNT = dataReader["OUTPUT"].ToString();
                    sub_item.Add(temp);

                    //  item.TOTAL_VOLUNTEER_NUMBER = dataReader["PERSONALITY_CHAR"].ToString();

                    //temp_payment.ORG_PAYMENT_ID = dataReader["ORG_PAYMENT_ID"].ToString();
                    //temp_payment.ORG_ID = dataReader["ORG_ID"].ToString();
                    //temp_payment.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    //temp_payment.AMT = dataReader["AMT"].ToString();
                    //temp_payment.PAYMENT_TIME = dataReader["PAYMENT_TIME"].ToString();
                    //temp_payment.PLAN_TYPE = dataReader["PLAN_NAME"].ToString();
                }
                item.VOLUNTEER_SUMMARY_MBTI = sub_item;
                dataReader.Close();
                return item;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }

        //transaction
        public static List<ReportTransaction> getTransactionDataDL(SqlConnection dbConnection, string dateStr)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select CAST(PAYMENT_TIME AS DATE) AS TDate , COUNT(PAYMENT_TIME) AS PAYMENT_COUNT, COUNT (PLAN_TYPE) AS PLAN_TYPE_COUNT, COUNT(CRM_ID) AS CRM_COUNT
from TABLE_ORG_PAYMENT
where PAYMENT_TIME >= @dateStr
group by CAST(PAYMENT_TIME AS DATE)";
                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("dateStr", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = dateStr;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportTransaction> list = new List<ReportTransaction>();
                while (dataReader.Read())
                {
                    ReportTransaction item = new ReportTransaction();
                    item.TDate = dataReader["TDate"].ToString();
                    item.PAYMENT_COUNT = dataReader["PAYMENT_COUNT"].ToString();
                    item.PLAN_TYPE_COUNT = dataReader["PLAN_TYPE_COUNT"].ToString();
                    item.CRM_COUNT = dataReader["CRM_COUNT"].ToString();

                    DateTime date = new DateTime();
                    DateTime.TryParse(item.TDate, out date);
                    item.TDate = date.ToString("MM/dd/yyyy");
                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //npo
        public static ReportNPO getOrgNumDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select count(o1.ORG_STATUS) as ACTIVE, count(o2.ORG_STATUS) as INACTIVE
from TABLE_ORG_DETAILS o
left join TABLE_ORG_DETAILS o1 on o.ORG_ID = o1.ORG_ID and o1.ORG_STATUS = 'ACTIVE'
left join TABLE_ORG_DETAILS o2 on o.ORG_ID = o2.ORG_ID and o2.ORG_STATUS = 'INACTIVE'";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                ReportNPO item = new ReportNPO();
                while (dataReader.Read())
                {
                    item.ORG_STATUS_ACTIVE = dataReader["ACTIVE"].ToString();
                    item.ORG_STATUS_INACTIVE = dataReader["INACTIVE"].ToString();
                }
                dataReader.Close();
                return item;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<ReportNPO> getOrgTypeDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select * from TABLE_ORG_DETAILS order by ORG_TYPE";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportNPO> list = new List<ReportNPO>();
                while (dataReader.Read())
                {
                    ReportNPO item = new ReportNPO();
                    item.ORG_ID = dataReader["ORG_ID"].ToString();
                    item.CONNECTION_STRING = dataReader["CONNECTION_STRING"].ToString();
                    item.ORG_TYPE = dataReader["ORG_TYPE"].ToString();
                    item.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static void getOrgVolNumDataDL(SqlConnection dbConnection, ReportNPO item)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                string orgID = item.ORG_ID;
                //calling sql statements
                string sql_str = @"select count(ORG_ID) as VOLUNTEER_NO from TABLE_USR_ORG where ORG_ID = @orgID";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("orgID", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = orgID;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    item.VOLUNTEER_NO = dataReader["VOLUNTEER_NO"].ToString();
                }
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        //event
        public static string getEventObjDL(SqlConnection dbConnection, ReportEvent item)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                string date = item.DATE.ToString("yyyy'-'MM'-'dd");
                //ToString("dd'/'MM");Date.ToString();
                //calling sql statements
                string sql_str = @"select count(EVT_DATE) AS EVENT_CREATED from TABLE_EVT_SESSION_DAY where EVT_DATE = @date";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = date;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    int num = 0;
                    string itemNum = dataReader["EVENT_CREATED"].ToString();
                    Int32.TryParse(itemNum, out num);

                    item.EVENT_CREATED += num;
                }
                dataReader.Close();
                return "success";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "error";
        }
        public static List<ReportNPO> getEventDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select * from TABLE_ORG_DETAILS order by ORG_TYPE";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportNPO> list = new List<ReportNPO>();
                while (dataReader.Read())
                {
                    ReportNPO item = new ReportNPO();
                    item.ORG_ID = dataReader["ORG_ID"].ToString();
                    item.CONNECTION_STRING = dataReader["CONNECTION_STRING"].ToString();
                    item.ORG_TYPE = dataReader["ORG_TYPE"].ToString();
                    item.ORG_NAME = dataReader["ORG_NAME"].ToString();
                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static void getEventTurnUpDataDL(SqlConnection dbConnection, List<ReportEvent> list, string date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select s.EVT_SESSION_ID, v.TIME_OUT, e.SESSION_NAME 
from TABLE_VOL_EVT_ROLE v
left join TABLE_EVT_SESSION_DAY s on v.SESSION_DAY_ID = s.SESSIONDAY_ID
left join TABLE_EVT_SESSION e on s.EVT_SESSION_ID = e.EVT_SESSION_ID
where TIME_IN >= @date
order by s.EVT_SESSION_ID, TIME_IN desc";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = date;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                bool run = false;
                if (list.Count != 0)
                {
                    run = true;
                }
                ReportEvent item = null;
                string prevName = "";
                while (dataReader.Read())
                {
                    string sessionName = dataReader["SESSION_NAME"].ToString();

                    if (!sessionName.Equals(prevName))
                    {
                        if (item != null)
                        {
                            if (run)
                            {
                                sortItem(list, item);
                            }
                            else
                            {
                                item.TIME_IN++;
                                list.Add(item);
                            }
                        }
                        prevName = sessionName;
                        item = new ReportEvent();
                    }
                    item.TIME_IN++;
                    item.SESSION_NAME = sessionName;

                    string strDate = dataReader["TIME_OUT"].ToString().Split(' ')[0];
                    DateTime date1 = new DateTime();
                    DateTime.TryParse(strDate, out date1);
                    item.DATE = date1;
                }
                if (item != null)
                {
                    list.Add(item);
                }

                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        public static void sortItem(List<ReportEvent> list, ReportEvent item)
        {
            try
            {
                bool insert = true;
                DateTime date = item.DATE;
                for (int i = 0; i < list.Count; i++)
                {
                    if (date < list[i].DATE)
                    {
                        list.Insert(i, item);
                        insert = false;
                    }
                }
                if (insert)
                {
                    list.Add(item);
                }
            }
            catch (Exception e)
            {

            }
        }
        //npo-event reporting
        public static List<ReportEvent> getGeneralEventDataDL(SqlConnection dbConnection, string dateStr)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select e.START_DATE, e.END_DATE, l.LVL 
from TABLE_EXAM_PERIOD e
left join TABLE_LVL_OF_EDU l on l.LVL_OF_EDU_ID = e.LVL_OF_EDU_ID
where e.START_DATE > @dateStr or e.END_DATE > @dateStr";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("dateStr", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = dateStr;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportEvent> list = new List<ReportEvent>();
                while (dataReader.Read())
                {
                    ReportEvent item = new ReportEvent();
                    item.START_DATE = dataReader["START_DATE"].ToString();
                    item.END_DATE = dataReader["END_DATE"].ToString();

                    string level = dataReader["LVL"].ToString();
                    item.EVENT_NAME = level + " school Examination Period";

                    item.EXAM = 1;

                    DateTime date = new DateTime();
                    DateTime.TryParse(item.START_DATE, out date);
                    if (date.Date < DateTime.Now.Date)
                    {
                        item.EXCEED = 1;
                    }

                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<ReportEvent> getNPOEventDataDL(SqlConnection dbConnection, List<ReportEvent> list, List<ItemSummary> cause)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select d.EVT_ID, d.EVT_NAME, d.STARTDATE, d.ENDDATE, c.CAUSE_ID
from TABLE_EVT_DETAILS d
left join TABLE_EVT_CAUSE c on d.EVT_ID = c.EVT_ID
where d.STARTDATE > GETDATE() or d.ENDDATE > GETDATE()
order by c.CAUSE_ID";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    
                    ReportEvent item = new ReportEvent();
                    item.EVENT_ID = dataReader["EVT_ID"].ToString();

                    ReportEvent exist = list.Find(x => x.EVENT_ID == item.EVENT_ID);
                    if (exist == null)
                    {
                        item.START_DATE = dataReader["STARTDATE"].ToString();
                        item.END_DATE = dataReader["ENDDATE"].ToString();
                        item.EVENT_NAME = dataReader["EVT_NAME"].ToString();

                        string causeIDStr = dataReader["CAUSE_ID"].ToString();
                        int causeID = 0;
                        Int32.TryParse(causeIDStr, out causeID);
                        ItemSummary temp = cause.Find(x => x.ITEM_NUM == causeID);
                        item.EVENT_DESC = temp.ITEM_NAME;

                        list.Add(item);
                    }
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<ItemSummary> getEventCauseList()
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                //calling sql statements
                string sql_str = @"select * from TABLE_TERM
where VOCAB_ID = '2'
order by TERM_ID";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                List<ItemSummary> list = new List<ItemSummary>();
                while (dataReader.Read())
                {
                    ItemSummary item = new ItemSummary();
                    int numInt = 0;
                    string num = dataReader["TERM_ID"].ToString();
                    Int32.TryParse(num, out numInt);

                    item.ITEM_NUM = numInt;
                    item.ITEM_NAME = dataReader["TERM_NAME"].ToString();

                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static List<ReportEvent> getEventTurnOut1DL(SqlConnection dbConnection, string dateStr)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select e.EVT_ID, e.EVT_NAME, e.STARTDATE, e.ENDDATE, count(v.USR_ID) as REG, count(v.TIME_OUT) as TURN_UP
from TABLE_EVT_DETAILS e
left join TABLE_EVT_SESSION s on e.EVT_ID = s.EVT_ID
left join TABLE_EVT_SESSION_DAY d on s.EVT_SESSION_ID = d.EVT_SESSION_ID
left join TABLE_VOL_EVT_ROLE v on d.SESSIONDAY_ID = v.SESSION_DAY_ID
where e.STARTDATE >= @dateStr or e.ENDDATE >= @dateStr
group by e.EVT_ID, e.EVT_NAME, e.STARTDATE, e.ENDDATE";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("dateStr", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = dateStr;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportEvent> list = new List<ReportEvent>();
                while (dataReader.Read())
                {
                    ReportEvent item = new ReportEvent();
                    item.EVENT_ID = dataReader["EVT_ID"].ToString();
                    item.EVENT_NAME = dataReader["EVT_NAME"].ToString();
                    item.START_DATE = dataReader["STARTDATE"].ToString();
                    item.END_DATE = dataReader["ENDDATE"].ToString();

                    string registerStr = dataReader["REG"].ToString();
                    string turnStr = dataReader["TURN_UP"].ToString();
                    int register = 0;
                    int turn = 0;
                    Int32.TryParse(registerStr, out register);
                    Int32.TryParse(turnStr, out turn);
                    item.REG = register;
                    item.TURN_UP = turn;

                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        //VENDOR
        public static List<ReportVendor> getVendorDataDL(SqlConnection dbConnection)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select * from TABLE_FIRM_DETAILS order by REGISTRATION_DATE desc";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportVendor> list = new List<ReportVendor>();
                while (dataReader.Read())
                {
                    ReportVendor item = new ReportVendor();
                    item.USR_ID = dataReader["USR_ID"].ToString();
                    item.REGISTRATION_DATE = dataReader["REGISTRATION_DATE"].ToString();
                    item.FIRM_NAME = dataReader["FIRM_NAME"].ToString();
                    item.SPONSOR_COUNT = "0";
                    item.VENDOR_RATING = "-";

                    DateTime date = new DateTime();
                    DateTime.TryParse(item.REGISTRATION_DATE, out date);
                    item.REGISTRATION_DATE = date.ToString("dd'/'MM");
                    item.REG_DATE = date;

                    list.Add(item);
                }
                dataReader.Close();
                return list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }
        }
        public static void getVendorSponsoredDL(SqlConnection dbConnection, List<ReportVendor> list)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select count(f.FIRM_NAME) as SPONSOR_COUNT, f.FIRM_NAME
from TABLE_SPR_RECORD i
left join TABLE_FIRM_DETAILS f on i.SPONSOR_USR_ID = f.USR_ID
group by f.FIRM_NAME";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();


                while (dataReader.Read())
                {
                    string name = dataReader["FIRM_NAME"].ToString();
                    ReportVendor item = list.Find(x => x.FIRM_NAME.Equals(name));
                    item.SPONSOR_COUNT = dataReader["SPONSOR_COUNT"].ToString();
                    item.FIRM_NAME = name;
                }
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }
        public static void getVendorRatingDL(SqlConnection dbConnection, List<ReportVendor> list)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"select FIRM_ID, avg(RATING) as VENDOR_RATING
from TABLE_VDR_BIDS
group by FIRM_ID";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);
                SqlDataReader dataReader = command.ExecuteReader();


                while (dataReader.Read())
                {
                    string name = dataReader["FIRM_ID"].ToString();
                    ReportVendor item = list.Find(x => x.USR_ID.Equals(name));
                    item.VENDOR_RATING = dataReader["VENDOR_RATING"].ToString();
                }
                dataReader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
        }

        //overview data
        public static List<ReportOverview> getOverviewDataDL_vendor(SqlConnection dbConnection, string date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"SELECT count(f.USR_ID) as VENDOR_COUNT, CONVERT(CHAR(8),f.REGISTRATION_DATE,10) as DATE
FROM TABLE_FIRM_DETAILS f 
where f.REGISTRATION_DATE >= @date
group by CONVERT(CHAR(8),f.REGISTRATION_DATE,10)
order by CONVERT(CHAR(8),f.REGISTRATION_DATE,10)";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = date;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportOverview> list1 = new List<ReportOverview>();
                while (dataReader.Read())
                {
                    ReportOverview item = new ReportOverview();

                    item.NUM = dataReader["VENDOR_COUNT"].ToString();

                    item.DATE = dataReader["DATE"].ToString();

                    list1.Add(item);
                }
                dataReader.Close();
                return list1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return null;
        }
        public static List<ReportOverview> getOverviewDataDL_npo(SqlConnection dbConnection, string date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"SELECT count(o.ORG_ID) as ORG_COUNT, CONVERT(CHAR(8),o.DATE_REG,10) as DATE
FROM TABLE_ORG_DETAILS o 
where o.DATE_REG >= @date
group by CONVERT(CHAR(8),o.DATE_REG,10)
order by CONVERT(CHAR(8),o.DATE_REG,10)";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = date;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportOverview> list1 = new List<ReportOverview>();
                while (dataReader.Read())
                {
                    ReportOverview item = new ReportOverview();
                    item.NUM = dataReader["ORG_COUNT"].ToString();

                    item.DATE = dataReader["DATE"].ToString();

                    list1.Add(item);
                }
                dataReader.Close();
                return list1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return null;
        }
        public static List<ReportOverview> getOverviewDataDL_volunteer(SqlConnection dbConnection, string date)
        {
            String newID = Guid.NewGuid().ToString();

            try
            {
                //calling sql statements
                string sql_str = @"SELECT count(v.USR_ID) as VOL_COUNT, CONVERT(CHAR(8),v.REGISTRATION_DATE,10) as DATE
FROM TABLE_USR_DETAILS v 
where v.REGISTRATION_DATE >= @date
group by CONVERT(CHAR(8),v.REGISTRATION_DATE,10)
order by CONVERT(CHAR(8),v.REGISTRATION_DATE,10)";

                SqlCommand command = new SqlCommand(sql_str, dbConnection);

                //insert into sql
                SqlParameter sql_dateStr = new SqlParameter("date", SqlDbType.NVarChar, 50);
                sql_dateStr.Value = date;
                command.Parameters.Add(sql_dateStr);

                SqlDataReader dataReader = command.ExecuteReader();

                List<ReportOverview> list1 = new List<ReportOverview>();
                while (dataReader.Read())
                {
                    ReportOverview item = new ReportOverview();
                    item.NUM = dataReader["VOL_COUNT"].ToString();

                    item.DATE = dataReader["DATE"].ToString();

                    list1.Add(item);
                }
                dataReader.Close();
                return list1;
            }
            catch (Exception e)
            {
            }
            return null;
        }
    }
}