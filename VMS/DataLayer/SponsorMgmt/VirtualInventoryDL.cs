﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;

namespace VMS.DataLayer.SponsorMgmt
{
    public class VirtualInventoryDL
    {
        static String imageHost = "http://localhost:50348/";

        // Create item
        /*DL : */
        public static String addItem(SqlConnection dataConnection, String itemName, int qtyAva, String description, String categoryID, String sponsorID, String imagePath)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_ITEM (SPR_ITEM_ID, ITEM_NAME, QTY_AVAILABLE, QTY_ON_BIDDING, ITEM_DESCRIPTION, SPR_ITEM_CATEGORY_ID, SPONSOR_USR_ID, IMAGE) VALUES (NEWID(), @ITEM_NAME, @QTY_AVAILABLE, 0, @ITEM_DESCRIPTION, @SPR_ITEM_CATEGORY_ID, @SPONSOR_USR_ID, @IMAGE)";
            SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
            ITEM_NAME.Value = itemName;
            command.Parameters.Add(ITEM_NAME);
            SqlParameter QTY_AVAILABLE = new SqlParameter("QTY_AVAILABLE", SqlDbType.Int, 5);
            QTY_AVAILABLE.Value = qtyAva;
            command.Parameters.Add(QTY_AVAILABLE);
            SqlParameter ITEM_DESCRIPTION = new SqlParameter("ITEM_DESCRIPTION", SqlDbType.NVarChar, 100);
            ITEM_DESCRIPTION.Value = description;
            command.Parameters.Add(ITEM_DESCRIPTION);
            SqlParameter SPR_ITEM_CATEGORY_ID = new SqlParameter("SPR_ITEM_CATEGORY_ID", SqlDbType.NVarChar, 100);
            SPR_ITEM_CATEGORY_ID.Value = categoryID;
            command.Parameters.Add(SPR_ITEM_CATEGORY_ID);
            SqlParameter SPONSOR_USR_ID = new SqlParameter("SPONSOR_USR_ID", SqlDbType.NVarChar, 100);
            SPONSOR_USR_ID.Value = sponsorID;
            command.Parameters.Add(SPONSOR_USR_ID);
            SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 3999);
            IMAGE.Value = imagePath;       
            command.Parameters.Add(IMAGE);

            command.ExecuteNonQuery();
            return "added new item";
        }

        // Update item 
        /*DL : */
        public static String updateItem(SqlConnection dataConnection, String itemID, String itemName, String description)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_ITEM SET ITEM_NAME=@ITEM_NAME AND DESCRIPTION=@DESCRIPTION AND WHERE ITEM_ID=@ITEM_ID", dataConnection);

                SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
                ITEM_NAME.Value = itemName;
                command.Parameters.Add(ITEM_NAME);

                SqlParameter DESCRIPTION = new SqlParameter("DESCRIPTION", SqlDbType.NVarChar, 100);
                DESCRIPTION.Value = description;
                command.Parameters.Add(DESCRIPTION);

                SqlParameter ITEM_ID = new SqlParameter("ITEM_ID", SqlDbType.NVarChar, 100);
                ITEM_ID.Value = itemID;
                command.Parameters.Add(ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "false";
        }

        // Update item quantity
        /*DL : */
        public static String updateQuantity(SqlConnection dataConnection, String itemID, int qtyAva, int qtyBid)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_ITEM SET QTY_AVAILABLE=@QTY_AVAILABLE, QTY_ON_BIDDING=@QTY_ON_BIDDING WHERE SPR_ITEM_ID=@SPR_ITEM_ID", dataConnection);

                SqlParameter QTY_AVAILABLE = new SqlParameter("QTY_AVAILABLE", SqlDbType.NVarChar, 50);
                QTY_AVAILABLE.Value = qtyAva;
                command.Parameters.Add(QTY_AVAILABLE);

                SqlParameter QTY_ON_BIDDING = new SqlParameter("QTY_ON_BIDDING", SqlDbType.NVarChar, 50);
                QTY_ON_BIDDING.Value = qtyBid;
                command.Parameters.Add(QTY_ON_BIDDING);

                SqlParameter SPR_ITEM_ID = new SqlParameter("SPR_ITEM_ID", SqlDbType.NVarChar, 100);
                SPR_ITEM_ID.Value = itemID;
                command.Parameters.Add(SPR_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();

                return "quantity updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "failed to update quantity";
        }


        // Update item quantity
        /*DL : */
        public static String updateQuantityWithSponsorDetails(SqlConnection dataConnection, int qtyAva, String itemName, String itemCatID, String sponsorID, String itemDesc)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_ITEM SET QTY_AVAILABLE=@QTY_AVAILABLE, QTY_ON_BIDDING=0 WHERE ITEM_NAME = @ITEM_NAME AND SPR_ITEM_CATEGORY_ID = @SPR_ITEM_CATEGORY_ID AND SPONSOR_USR_ID = @SPONSOR_USR_ID AND ITEM_DESCRIPTION = @ITEM_DESCRIPTION", dataConnection);

                SqlParameter QTY_AVAILABLE = new SqlParameter("QTY_AVAILABLE", SqlDbType.NVarChar, 50);
                QTY_AVAILABLE.Value = qtyAva;
                command.Parameters.Add(QTY_AVAILABLE);

                SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
                ITEM_NAME.Value = itemName;
                command.Parameters.Add(ITEM_NAME);

                SqlParameter SPR_ITEM_CATEGORY_ID = new SqlParameter("SPR_ITEM_CATEGORY_ID", SqlDbType.NVarChar, 100);
                SPR_ITEM_CATEGORY_ID.Value = itemCatID;
                command.Parameters.Add(SPR_ITEM_CATEGORY_ID);

                SqlParameter SPONSOR_USR_ID = new SqlParameter("SPONSOR_USR_ID", SqlDbType.NVarChar, 100);
                SPONSOR_USR_ID.Value = sponsorID;
                command.Parameters.Add(SPONSOR_USR_ID);

                SqlParameter ITEM_DESCRIPTION = new SqlParameter("ITEM_DESCRIPTION", SqlDbType.NVarChar, 500);
                ITEM_DESCRIPTION.Value = itemDesc;
                command.Parameters.Add(ITEM_DESCRIPTION);

                command.Prepare();
                command.ExecuteNonQuery();

                return "quantity updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "failed to update quantity";
        }


        public static String deleteItemFromInventory(SqlConnection dataConnection, String sprItemID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_SPR_ITEM WHERE SPR_ITEM_ID=@SPR_ITEM_ID", dataConnection);

                SqlParameter SPR_ITEM_ID = new SqlParameter("SPR_ITEM_ID", SqlDbType.NVarChar, 100);
                SPR_ITEM_ID.Value = sprItemID;
                command.Parameters.Add(SPR_ITEM_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "item deleted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---UserPortfolioDl.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: removeFriend---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "failed to delete item";
        }


        // Retrieve all items
        /*DL : */
        public static List<VirtualItemSponsor> getAllItems(SqlConnection dataConnection)
        {
            List<VirtualItemSponsor> listOfItems = new List<VirtualItemSponsor>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_ID, ITEM_NAME, QTY_AVAILABLE, QTY_ON_BIDDING, ITEM_DESCRIPTION, CATEGORY_NAME, IMAGE from TABLE_SPR_ITEM i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VirtualItemSponsor items = new VirtualItemSponsor();
                    items.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    items.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    items.QTY_AVAILABLE = (int)reader["QTY_AVAILABLE"];
                    items.QTY_ON_BIDDING = (int)reader["QTY_ON_BIDDING"];
                    items.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    items.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    items.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(items);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retreive items by category
        /*DL : */
        public static List<VirtualItemSponsor> getItemsByCategory(SqlConnection dataConnection, String categoryID)
        {
            List<VirtualItemSponsor> listOfItems = new List<VirtualItemSponsor>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_ID, ITEM_NAME, QTY_AVAILABLE, QTY_ON_BIDDING, ITEM_DESCRIPTION, CATEGORY_NAME, IMAGE from TABLE_SPR_ITEM i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND  i.SPR_ITEM_CATEGORY_ID=@0";
                command.Parameters.AddWithValue("@0", categoryID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VirtualItemSponsor items = new VirtualItemSponsor();
                    items.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    items.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    items.QTY_AVAILABLE = (int)reader["QTY_AVAILABLE"];
                    items.QTY_ON_BIDDING = (int)reader["QTY_ON_BIDDING"];
                    items.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    items.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    items.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(items);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retreive items by search
        /*DL : */
        public static List<VirtualItemSponsor> getItemsBySearch(SqlConnection dataConnection, String itemName, String categoryID)
        {
            List<VirtualItemSponsor> listOfItems = new List<VirtualItemSponsor>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);

                String searchQuery = "Select SPR_ITEM_ID, ITEM_NAME, QTY_AVAILABLE, QTY_ON_BIDDING, ITEM_DESCRIPTION, CATEGORY_NAME, i.IMAGE from TABLE_SPR_ITEM i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID";

                if (!itemName.Equals("_"))
                {
                    searchQuery = searchQuery + " AND i.ITEM_NAME like '%" + itemName + "%'";
                }
                if (!categoryID.Equals("_"))
                {
                    searchQuery = searchQuery + " AND  i.SPR_ITEM_CATEGORY_ID=@0";
                    command.Parameters.AddWithValue("@0", categoryID);
                }

                command.CommandText = searchQuery;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VirtualItemSponsor items = new VirtualItemSponsor();
                    items.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    items.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    items.QTY_AVAILABLE = (int)reader["QTY_AVAILABLE"];
                    items.QTY_ON_BIDDING = (int)reader["QTY_ON_BIDDING"];
                    items.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    items.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    items.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfItems.Add(items);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfItems;
        }


        // Retrieve individual item
        /*DL : */
        public static VirtualItemSponsor getIndidivudalItem(SqlConnection dataConnection, String itemID)
        {
            VirtualItemSponsor item = new VirtualItemSponsor();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_ID, ITEM_NAME, QTY_AVAILABLE, QTY_ON_BIDDING, ITEM_DESCRIPTION, CATEGORY_NAME, i.IMAGE from TABLE_SPR_ITEM i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND SPR_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", itemID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    item.SPR_ITEM_ID = reader["SPR_ITEM_ID"].ToString();
                    item.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    item.QTY_AVAILABLE = (int)reader["QTY_AVAILABLE"];
                    item.QTY_ON_BIDDING = (int)reader["QTY_ON_BIDDING"];
                    item.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    item.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    item.IMAGE = imageHost + reader["IMAGE"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return item;
        }

        // Retrieve individual item quantity
        /*DL : */
        public static int getItemQuantity(SqlConnection dataConnection, String itemID)
        {
            VirtualItemSponsor item = new VirtualItemSponsor();
            int quantityAvaliable = 0;

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select QTY_AVAILABLE from TABLE_SPR_ITEM WHERE SPR_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", itemID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    item.QTY_AVAILABLE = (int)reader["QTY_AVAILABLE"];
                    quantityAvaliable = item.QTY_AVAILABLE;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return quantityAvaliable;
        }


        // Retrieve individual item quantity placed on bidding
        /*DL : */
        public static int getItemQuantityOnBidding(SqlConnection dataConnection, String itemID)
        {
            VirtualItemSponsor item = new VirtualItemSponsor();
            int QTY_ON_BIDDING = 0;

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select QTY_ON_BIDDING from TABLE_SPR_ITEM WHERE SPR_ITEM_ID = @0";
                command.Parameters.AddWithValue("@0", itemID);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    item.QTY_ON_BIDDING = (int)reader["QTY_ON_BIDDING"];
                    QTY_ON_BIDDING = item.QTY_ON_BIDDING;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return QTY_ON_BIDDING;
        }

        public static Boolean checkItemNameExist(SqlConnection dataConnection, String sponsorVendorID, String itemName)
        {
            //VirtualItemSponsor record = new VirtualItemSponsor();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_ID from TABLE_SPR_ITEM WHERE SPONSOR_USR_ID = @0 and ITEM_NAME = @1";
                command.Parameters.AddWithValue("@0", sponsorVendorID);
                command.Parameters.AddWithValue("@1", itemName);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    reader.Close();
                    return true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return false;
        }

        public static String getItemID(SqlConnection dataConnection, String sponsorVendorID, String itemName)
        {
            String itemID = "";

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_ID from TABLE_SPR_ITEM WHERE SPONSOR_USR_ID = @0 and ITEM_NAME = @1";
                command.Parameters.AddWithValue("@0", sponsorVendorID);
                command.Parameters.AddWithValue("@1", itemName);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    itemID = reader["SPR_ITEM_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return itemID;
        }

    }
}