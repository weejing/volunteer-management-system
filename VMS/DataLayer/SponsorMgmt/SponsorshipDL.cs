﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;

namespace VMS.DataLayer.SponsorMgmt
{
    public class SponsorshipDL
    {
        static String imageHost = "http://localhost:50348/";

        // 4) Submit sponsorship request (from vendor to SGServe)
        /*DL : */
        public static String addSponsorshipRequest(SqlConnection dataConnection, String itemID, int qty, String sponsorshipDesc, String collectionDesc, String sponsorID)
        {
            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_RECORD (SPR_RECORD_ID, SPR_ITEM_RECORD_ID, QTY, SPONSORSHIP_DESCRIPTION, COLLECTION_DESCRIPTION, SPONSOR_USR_ID, STATUS, REQUEST_DATE) VALUES (NEWID(), @SPR_ITEM_RECORD_ID, @QTY, @SPONSORSHIP_DESCRIPTION, @COLLECTION_DESCRIPTION, @SPONSOR_USR_ID, @STATUS, @REQUEST_DATE)";
            /*
            if (itemID.Trim().Equals("null"))
            {
                itemID = "2bffed67-7284-4474-bb09-bae6a512169a";
            }
            */
            SqlParameter ITEM_ID = new SqlParameter("SPR_ITEM_RECORD_ID", SqlDbType.NVarChar, 100);
            ITEM_ID.Value = itemID;
            command.Parameters.Add(ITEM_ID);
            SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int, 5);
            QTY.Value = qty;
            command.Parameters.Add(QTY);
            SqlParameter SPONSORSHIP_DESCRIPTION = new SqlParameter("SPONSORSHIP_DESCRIPTION", SqlDbType.NVarChar, 100);
            SPONSORSHIP_DESCRIPTION.Value = sponsorshipDesc;
            command.Parameters.Add(SPONSORSHIP_DESCRIPTION);
            SqlParameter COLLECTION_DESCRIPTION = new SqlParameter("COLLECTION_DESCRIPTION", SqlDbType.NVarChar, 100);
            COLLECTION_DESCRIPTION.Value = collectionDesc;
            command.Parameters.Add(COLLECTION_DESCRIPTION);
            SqlParameter SPONSOR_USR_ID = new SqlParameter("SPONSOR_USR_ID", SqlDbType.NVarChar, 100);
            SPONSOR_USR_ID.Value = sponsorID;
            command.Parameters.Add(SPONSOR_USR_ID);
            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);  // status - "pending" , "accepted" or "deleted"
            STATUS.Value = "pending";
            command.Parameters.Add(STATUS);
            SqlParameter REQUEST_DATE = new SqlParameter("REQUEST_DATE", SqlDbType.SmallDateTime, 100);
            REQUEST_DATE.Value = System.DateTime.Now;
            command.Parameters.Add(REQUEST_DATE);
            command.ExecuteNonQuery();

            return "added new sponsorship request";
        }


        /*DL :
        public static String addSponsorshipRequestNewItem(SqlConnection dataConnection, String itemName, int qty, String description, String sponsorID)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_ITEM_RECORD (SPR_RECORD_ID, ITEM_NAME, QTY, SPONSORSHIP_DESCRIPTION, SPONSOR_USR_ID, STATUS, REQUEST_DATE) VALUES (NEWID(), @ITEM_NAME, @QTY, @SPONSORSHIP_DESCRIPTION, @SPONSOR_USR_ID, @STATUS, @REQUEST_DATE)";
            /*
            SqlParameter ITEM_ID = new SqlParameter("ITEM_ID", SqlDbType.NVarChar, 100);
            ITEM_ID.Value = "2bffed67-7284-4474-bb09-bae6a512169a";
            command.Parameters.Add(ITEM_ID);
            
            SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
            ITEM_NAME.Value = itemName;
            command.Parameters.Add(ITEM_NAME);
            SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int, 5);
            QTY.Value = qty;
            command.Parameters.Add(QTY);
            SqlParameter SPONSORSHIP_DESCRIPTION = new SqlParameter("SPONSORSHIP_DESCRIPTION", SqlDbType.NVarChar, 100);
            SPONSORSHIP_DESCRIPTION.Value = description;
            command.Parameters.Add(SPONSORSHIP_DESCRIPTION);
            SqlParameter SPONSOR_USR_ID = new SqlParameter("SPONSOR_USR_ID", SqlDbType.NVarChar, 100);
            SPONSOR_USR_ID.Value = sponsorID;
            command.Parameters.Add(SPONSOR_USR_ID);
            SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);  // status - "pending" , "accepted" or "deleted"
            STATUS.Value = "pending";
            command.Parameters.Add(STATUS);
            SqlParameter REQUEST_DATE = new SqlParameter("REQUEST_DATE", SqlDbType.SmallDateTime, 100);
            REQUEST_DATE.Value = System.DateTime.Now;
            command.Parameters.Add(REQUEST_DATE);
            //SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 3999);
            //IMAGE.Value = "/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gNzAK/9sAQwAKBwcIBwYKCAgICwoKCw4YEA4NDQ4dFRYRGCMfJSQiHyIhJis3LyYpNCkhIjBBMTQ5Oz4+PiUuRElDPEg3PT47/9sAQwEKCwsODQ4cEBAcOygiKDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7/8AAEQgBLAEsAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8AyaKKK6TIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigYUUUUCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKBhRRRQIKKKKACiiigAooooAKKKKACiirVlpl5qDYtoGcDq3RR+NANpblWiurs/CESYa9uS5/uRcD8zWzbabYWePs9nEpH8TDc35mixjKvFbHBw2F5c/6m1mkHqqEirkfhrV5ORZkD/adR/Wu68xvWk3N6mnYyeIfRHFjwnqx/5ZIP+2gpG8K6uvS3VvpIv+Ndrk+po3N6n86LC+sSOBl0PVIfv2M3/ARu/lVJ43ibbIjI3owwa9MEjjvSSeXMuyaFJF9GUEUWKWI7o8yoru7rw1pV3kojW7nvGcD8ulYN/wCE7+1Be3IuUH93hvy/wpG0asZGFRSsrIxV1KsOCCMEUlBoFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAwooooEFFFFAwooooEFFFFABUkMEtzKsUMbSO3RVFT6dptxqdx5UIwo++56KK7bTtMt9Mh2QLlj9+Q/eb/PpRYyqVVD1MrTPC0UIEt+RK/XylPyj6nvXQKqogRFCovRVGAPwooqjilOUtwooooICiiigAooooAKKKKACnK7L0NNooGQX2mWOqpi5iAk7SLww/H/GuQ1bw7daWTIP31v/AM9FH3fqO1dtT1fA2sNyngg0rGsKrieYUV1eueF1ZWu9NX3aEf8Asv8AhXKEYODSO2MlJXQUUUUFBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQMKKKKBBVvTtPm1K6EMXA6u/ZRVeKKSeZIYl3SSHCj1rvNK02PTLNYVwXPMj/wB400jKrU5FpuTWdnDY2ywQLhR1Pcn1NT0UUzgbvqwooooEFFIzBVLMQAOSTW9penxpClxIuZHG4Aj7o/xpSlyq5pTpupKyMhbS5ddywSEeu2omVkYqylSOxGK3bjX9LtbjyJbxA4OCACQD7kDAqxcWsF/ADlTkZSRef/1io52t0bewhK6hK7RzNFPmheCVopBhlNEMTTzpCn3nPH+NaHNZ3sNVGdtqKWJ7AZqV7S4jXc8EgHqVroIYLfT7cnKoqjLyNx+ZqC213TLyfyILtGkJwFIK5+mRzWfO3qkdXsIRspys2YFFbmp6ajxtPCoV1GWA6MKw6uMlJGFSm4OzCiiimZjkcoePyrB8ReH1uUa/sk/ejmSMD7/uPf8AnW5TkcofbvQXCbi7o8xorpPFOii3f+0LZf3Tn94o/hPr9DXN1J6EZKSugooooKCiiigAooooAKKKKACiiigAooooAKKKKBhSE4GTS1LYWjajqMVqM7WOXI7KOtBLdldnR+FNM2RHUZl+eQYiB7L6/jXSU2NFjjVEACqMADtTqo82UnJ3YUUUUEhQSAMk4AoJwMmqVxMX+VeF/nTSuTKXKipqd20qlE+4P1r0NJFuLVZImwsiZU/UcV5vOuQa6TwjrAeL+zJ2xImTCT/Evp9R/L6U60LwTXQeCrWrOMvtHOXNs0UjxyLh1JDA+tbvg/U2SVtMlbKkF4c9vUf1/OtTXNCF+DcW4AnA5HZ//r1x26bTNQjmKMkkDglSMH3FWpKtC3UylCeErKfT9DtdehUWpuwOYh82O4qPw3at9lN/MP3k/wBwf3U7fn1/KtVlivLUqfmimT8wRRI0dnaMwG2OFOAPQDpXHze7ynsOkva+06W/Hv8Acc14pvmmlFijfu05f3b0/CucjtpJrqKKAHzXcBcdj61oMs15cHapklkYnAHJJrpdF0NdP/0ifDXDDAx0Qeg9/eurnVKNjyvZSxVXm6foak7BLeRm6BST+VcpWxrN6MfZYzk9XI/lWPWFNWR24malKy6BRRRWhyhRRRQA7Yk0TwSqGjkBBB7157qunvpmoSWzZKjlG/vL2NegVleKbD7bpgukXMttycd17/40mdFGdnY4iiiikdoUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA1zgV03g2yxDLesOZDtT6D/wCv/KuVlYngV6PpVqLPTYIMYKIAfr3/AFpo58RK0bFuiiimcQUdKKhkfdwOlNITdhk0hbgdKquyr95gv1NSyuEUsa57UZnlnBJPsPSprVHSpOaWxtgcMsZio0G7Xv8Agrms7Rn+Nfzqqx2OJI5NrqcqynkGswE+poyfU1wLNJL7P4n0kuFKb/5ev7l/mehaH4phulW3v3WKcDAkJwr/AOBrRvLbSdXAilkhlc/dKSDePpivLMn1NbHhMn/hJrPnu3/oJrJYxymuWNjqnkkadCXPPmsuqR6PZ2ws7OK2V2dYl2qzdcdqS8theWzQFyitjJHXFT0V13d7nicq5eXoULZdL07Mcc0Ebjhi0g3fjVW/1+2TdFbTxs3QvuGB9PWuE8Qk/wDCQX3J/wBcazsn1NcTxjUndXPehksZ0lyztddkdmbmEkkzoSe5YUn2iD/ntH/30K43J9TRk+pqv7Qf8pl/q3D/AJ+P7kdqro4yjKw9Qc0tY/h4kwzZPcf1rYr0KNT2kFLufM43DrDV5Uk72CiiitTkCnIFbdG4BVxgg96bQDg5oGjzzUbQ2OoT2x/5ZtgZ7jt+lVq6TxnbBbyC7UcSptP1H/1j+lc3UnpQfNFMKKKKCgooooAKKKKACiiigAooooGFB4FFNc8UCH6fF9o1a2iPIMgJ+g5NelKMIB7V5/4aTzNdjP8AcVj+mP616DVHFXfvWCiimsc0HONds8dqichRk09iAMmq0hLHJq0jOTILhi3JrDvhh9x6Dqa25BkVm36L5TDGc9aVeCqUZRf9W1NctxEsPjqdRK+tvv0/UzhLHj/WL+dL5sf99fzpqwx4+4KXyY/7gr5Y/WtBfNj/AL6/nWv4UniTxJaM0qKAW5LD+6ax/Jj/ALgrX8K20L+JLRXjUglsgj/ZNXT+NGGJt7Cd+z/I9M+22n/P1D/38FH220/5+of+/gpn9m2X/PtH+VH9m2X/AD7R/lXr+8fFfu/P8DzDxBNE2v3pWRCDMcEMKz/Nj/vr+dX9ft4U169VY1AExwMVn+TH/cFePP4mfb0Leyj6L8kL5sf99fzo8xD0dfzpPJj/ALgpyQx5+4Kk10Oh8PIwtpHIIViMH1rXqGzAFjAAMDy1/lU1fRUYKFNRR+YY6s62JnNq2v5afoFFFFanGFFFFAGT4qh83QhJjmGQHPseP6iuJr0HWE83Qr1PSPd+XP8ASvPqlndQd4hRRRQbhRRRQAUUUUAFFFFABRRRQMKY9PqN6BGp4TGdab2hP8xXeVwXhRtutH3iYfqDXe1RwV/jENNY4FOJxULHJpowbGOcmomqU1G1WjFkL9KzL/8A1bVpv0rMv/8AVtTn/Dl6P8h4f/eaf+KP5ozl6UtIvSlr5M/YArY8Jf8AIzWf1b/0E1j1seEv+Rms/q3/AKCaun8a9TnxX8Cfo/yPTqKKK9k+HPKvEP8AyMF9/wBdjWdWj4h/5GC+/wCuxrOrxZ/Ez7qh/Cj6L8kFOTrTacnWpNWdfaf8eUH/AFzX+VTVDaf8eUH/AFzX+VTV9LD4Ufldb+LL1f5sKKKKoyCiiigCG+GdLvQe8D/yNec16LqDbdJvW/6YP/I151SZ24f4WFFFFI6AooooAKKKKACiiigAooooGFRvUlMegRa8PyeVrsGejbl/MGvQwcoD7V5fbzfZryGf/nm4b8jXpkLh4wQc1aOLEK0kxzHNMNPNNNM5GMNRNUpqJqpEMhfpWZf/AOratN+lZl//AKtqc/4cvR/kGH/3mn/ij+aM5elLSL0pa+TP2AK2PCX/ACM1n9W/9BNY9bHhL/kZrP6t/wCgmrp/GvU58V/An6P8j06iiivZPhzyrxD/AMjBff8AXY1nVo+If+Rgvv8Arsazq8WfxM+6ofwo+i/JBTk602nJ1qTVnX2n/HlB/wBc1/lU1Q2n/HlB/wBc1/lU1fSw+FH5XW/iy9X+bCiiiqMgooooAoa9J5Wg3bd2AUfiQK4Kuv8AF0/l6bBADzLLu/AD/wCuK5CpZ30FaAUUUUGwUUUUAFFFFABRRRQAUUUUDCmtTqRqBFeQV3nh27+1aXCScsq7G+o4/lXCuK2/Cd75N3JaMeJPmT/eHX9P5VSOevG8L9jtTTDTgQygjvTTVHAMNRNUpqJqpGbIX6VmX/8Aq2rTfpWZf/6tqc/4cvR/kGH/AN5p/wCKP5ozl6UtIvSlr5M/YArY8Jf8jNZ/Vv8A0E1j1seEv+Rms/q3/oJq6fxr1OfFfwJ+j/I9Oooor2T4c8q8Q/8AIwX3/XY1nVo+If8AkYL7/rsazq8WfxM+6ofwo+i/JBTk602nJ1qTVnX2n/HlB/1zX+VTVDaf8eUH/XNf5VNX0sPhR+V1v4svV/mwoooqjIKKKjubhLO2luZPuxLu+voPzoGlfQ5HxVdefq3kqcrboE/Hqf5/pWLT5ZXmmeWQ5d2LMfUmmVJ6cVypIKKKKBhRRRQAUUUUAFFFFABRRRQMKDRRQIjYVHHK9vOk0Zw6MGBqZhULrTQmeh6Zex3tpHMh+VxnHoe4q2a4bw5qn2K6+zStiKU/KT/C3/167dXDrnv3qzzKkOSVhDUTVKaiaqRgyF+lZl//AKtq036VTlh8w1duaLj3MozdOpGolezT+5pmOvSlrR+wr/dFL9hX0rx/7Ln/ADL8T7b/AFsof8+pfejNrY8Jf8jNZ/Vv/QTUP2FfSnwQPbTLNA7RyL0ZTgiqjlk4yT5l+JlV4oo1KcoKlLVNbo9Rorzn7fqn/QRuf+/po+36p/0Ebn/v6a7vqz7nz/8AaUf5H+BQ8Q/8jBff9djWdWpLbNPK0srF3Y5ZmOSTTfsK+lcEssm23zL8T6GnxTRhBR9lLRLqjNpyda0PsK+lX7DSY1YTSqDj7qn+tS8skteZfiax4poydlSl96NG0BFnCDwRGv8AKpaKK9FKySPlpy5pOXdsKKKKZAVzPizUc7NPjbph5cfoP6/lW3qV/Hptm874LdEX+83YVwMsrzzPLKxZ3O5ie5pM6aELvmYyiiikdgUUUUAFFFFABRRRQAUUUUAFFFFAwooooEBFRsKkpCKAKzrXVeHtaM6C1nb98g+Un+Mf41zTLUYLRuHRirKcgjqDVJmVSmpxselhgy5FRtWHouui7AimIWcDkdn9x71uKwcZFaI8qcXF2ZGRmk2D0qbbRtqrmfKQ7B6UbB6VNto20XDlIdg9KNg9Km20baLhykOwelGwelTbaNtFw5SHYPSjYPSpttTRQ/xMPoKTlYahcjt7UcO4+gq3RRWbdzeMVFaBRRRSGFMmmjt4WllYIiDJJ7UTTxW8TSzOERRkk1xWta1JqcuxMpbqflXu3uaDWnTc2Ravqj6pd+YcrEnEaeg9frVCiipO9JJWQUUUUDCiiigAooooAKKKKACiiigAooooGFFFFAgooooAaRUbLU1NIoArkFWDKSCDkEdq6HSfEWCsN421ugl7H6/41hstRslUnYyqU41FZno8UqSAHI56ehqXbXn1hq93pxCo3mRd426fh6V1OneIrS7whfy5D/BIcfke9Xe5wToSh5o2NtG2hZUbvj60/FBlYZto20/FGKLhYZto20/FPVO5pXCw2OPualoopGiVgooqC5vbazjMlxMkajuTSGT1T1HVbXTIt075c/djXlmrA1Hxc0mYtOTA/wCerj+Q/wAa593klkMsrtJI3VmOSaLm8KDesi7qWrXOpy5lO2MH5YweB/iao0UVJ2JJKyCiiigYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAhFNK0+igCEpUZSrJFNK0XESWuq39lgRTsUH8D/MP/rVsWvi9lwLi2I9WiP8AQ/41glKaUqrmcqUJbo7SDxTp0v3pth9HUj9elXY9YsZPu3MJ+kgrz3y6TyqdzJ4aPRnpS3tsefNT/voUralZp964jH1cD+teaeVQIqVxfVvM9Cl8Q6XD967iP+627+Waz7jxlZJkQRySntgYH6/4Vx4i9qeIxRctYePU17rxXqNzlYVSBT6fMf14/SspzLcSeZPI8jHuxzSgAUtK5tGEY7IAAOlFFFIoKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKADFJilooATaKNtLRQAm0UuBRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH//2Q==";
            //command.Parameters.Add(IMAGE);

            command.ExecuteNonQuery();

            return "added new sponsorship request";
        }
        */

        // A2) Update (accept / reject) sponsorship request (from SGServe to vendor)
        // once accept, create or update virtual inventory item [SPR_ITEM]
        /*DL : */
        public static String updateSponsorship(SqlConnection dataConnection, String status, String sponsorRecordID)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_RECORD SET STATUS=@STATUS WHERE SPR_RECORD_ID=@SPR_RECORD_ID", dataConnection);

                SqlParameter STATUS = new SqlParameter("STATUS", SqlDbType.NVarChar, 50);
                STATUS.Value = status;
                command.Parameters.Add(STATUS);

                SqlParameter SPR_RECORD_ID = new SqlParameter("SPR_RECORD_ID", SqlDbType.NVarChar, 100);
                SPR_RECORD_ID.Value = sponsorRecordID;
                command.Parameters.Add(SPR_RECORD_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "status updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "status failed to update";
        }

        // 1) Retrieve all sponsored item requests
        public static List<SponsorshipRecord> getAllSponsoredRequest(SqlConnection dataConnection)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE, i.IMAGE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID order by REQUEST_DATE desc";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();   
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        // A1) Retreive all pending request (for admin)
        /*DL :  */
        public static List<SponsorshipRecord> getAllPendingRequest(SqlConnection dataConnection)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE, i.IMAGE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.STATUS = 'pending' order by REQUEST_DATE desc";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        


        // A1) Retrieve all sponsored item requests of a vendor
        public static List<SponsorshipRecord> getSponsoredRequestByVendorID(SqlConnection dataConnection, String sponsorVendorID)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE, i.IMAGE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.SPONSOR_USR_ID = @0 order by REQUEST_DATE desc";
                command.Parameters.AddWithValue("@0", sponsorVendorID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        // A1.2) Retrieve all sponsored item requests of a vendor
        public static List<SponsorshipRecord> getSponsoredRequestByVendorIDByStatus(SqlConnection dataConnection, String sponsorVendorID, String status)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE, i.IMAGE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.SPONSOR_USR_ID = @0 AND r.STATUS = @1 order by REQUEST_DATE desc";
                command.Parameters.AddWithValue("@0", sponsorVendorID);
                command.Parameters.AddWithValue("@1", status);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        // 2) Retrieve all sponsored item requests of a category
        /*
        public static List<SponsorshipRecord> getSponsoredRequestByCategory(SqlConnection dataConnection)
        {
        }
        */

        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        public static SponsorshipRecord getSponsoredRequestByRecordID(SqlConnection dataConnection, String requestID)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, COLLECTION_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE, i.IMAGE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.SPR_RECORD_ID = @0 order by REQUEST_DATE desc";
                command.Parameters.AddWithValue("@0", requestID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.COLLECTION_DESCRIPTION = reader["COLLECTION_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    reader.Close();
                    return record;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return record;
        }

        public static String getItemIDbyRequestID(SqlConnection dataConnection, String requestID)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_RECORD_ID from TABLE_SPR_RECORD WHERE SPR_RECORD_ID = @0";
                command.Parameters.AddWithValue("@0", requestID);

                String id = "";
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    id = reader["SPR_ITEM_RECORD_ID"].ToString();
                    reader.Close();
                    return id;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "faailed to get item id";
        }


        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        public static SponsorshipRecord getSponsoredRequestByItem(SqlConnection dataConnection, String itemID)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, i.IMAGE, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, COLLECTION_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.SPR_ITEM_RECORD_ID = @0";
                command.Parameters.AddWithValue("@0", itemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.COLLECTION_DESCRIPTION = reader["COLLECTION_DESCRIPTION"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return record;
        }

        public static SponsorshipRecord getSponsoredRequestByItemR(SqlConnection dataConnection, String sprRecordID)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, i.IMAGE, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, COLLECTION_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND SPR_RECORD_ID = @0";
                command.Parameters.AddWithValue("@0", sprRecordID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.COLLECTION_DESCRIPTION = reader["COLLECTION_DESCRIPTION"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return record;
        }


        public static SponsorshipRecord getSponsorshipItemDetails(SqlConnection dataConnection, String itemID)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_RECORD_ID, ITEM_NAME, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME, i.IMAGE from TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND i.SPR_ITEM_RECORD_ID = @0";
                command.Parameters.AddWithValue("@0", itemID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    record.ITEM_ID = reader["SPR_ITEM_RECORD_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return record;
        }


        public static Boolean checkItemNameExist(SqlConnection dataConnection, String sponsorVendorID, String itemName)
        {
            SponsorshipRecord record = new SponsorshipRecord();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_RECORD_ID from TABLE_SPR_ITEM_RECORD r WHERE SPONSOR_USR_ID = @0 and ITEM_NAME = @1";
                command.Parameters.AddWithValue("@0", sponsorVendorID);
                command.Parameters.AddWithValue("@1", itemName);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    reader.Close();
                    return true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return false;
        }

        public static String getItemID(SqlConnection dataConnection, String sponsorVendorID, String itemName)
        {
            String itemID = "";

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_RECORD_ID from TABLE_SPR_ITEM_RECORD r WHERE SPONSOR_USR_ID = @0 and ITEM_NAME = @1";
                command.Parameters.AddWithValue("@0", sponsorVendorID);
                command.Parameters.AddWithValue("@1", itemName);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    itemID = reader["SPR_ITEM_RECORD_ID"].ToString();
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return itemID;
        }


        public static List<SponsorshipRecord> getItemNameList(SqlConnection dataConnection, String sponsorVendorID)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select distinct SPR_ITEM_RECORD_ID, ITEM_NAME from TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND i.SPONSOR_USR_ID = @0";
                command.Parameters.AddWithValue("@0", sponsorVendorID);

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.ITEM_ID = reader["SPR_ITEM_RECORD_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        public static List<SponsorItemCategory> getItemCategoryList(SqlConnection dataConnection)
        {
            List<SponsorItemCategory> listOfRecords = new List<SponsorItemCategory>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_CATEGORY_ID, CATEGORY_NAME from TABLE_SPR_ITEM_CATEGORY c WHERE CATEGORY_NAME <>'ALL' ORDER BY CATEGORY_NAME";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorItemCategory record = new SponsorItemCategory();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        public static List<SponsorItemCategory> getItemCategoryListForSearch(SqlConnection dataConnection)
        {
            List<SponsorItemCategory> listOfRecords = new List<SponsorItemCategory>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                command.CommandText = "Select SPR_ITEM_CATEGORY_ID, CATEGORY_NAME from TABLE_SPR_ITEM_CATEGORY c ORDER BY CATEGORY_NAME";

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorItemCategory record = new SponsorItemCategory();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


        // Add new Item
        public static String createItemToRecord(SqlConnection dataConnection, String itemName, String description, String categoryID, String sponsorID, String imagePath)
        {

            SqlCommand command = new SqlCommand(null, dataConnection);
            command.CommandText = "INSERT INTO TABLE_SPR_ITEM_RECORD (SPR_ITEM_RECORD_ID, ITEM_NAME, ITEM_DESCRIPTION, SPR_ITEM_CATEGORY_ID, SPONSOR_USR_ID, IMAGE) VALUES (NEWID(), @ITEM_NAME, @ITEM_DESCRIPTION, @SPR_ITEM_CATEGORY_ID, @SPONSOR_USR_ID, @IMAGE)";
            SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
            ITEM_NAME.Value = itemName;
            command.Parameters.Add(ITEM_NAME);
            SqlParameter ITEM_DESCRIPTION = new SqlParameter("ITEM_DESCRIPTION", SqlDbType.NVarChar, 499);
            ITEM_DESCRIPTION.Value = description;
            command.Parameters.Add(ITEM_DESCRIPTION);
            SqlParameter SPR_ITEM_CATEGORY_ID = new SqlParameter("SPR_ITEM_CATEGORY_ID", SqlDbType.NVarChar, 100);
            SPR_ITEM_CATEGORY_ID.Value = categoryID;
            command.Parameters.Add(SPR_ITEM_CATEGORY_ID);
            SqlParameter SPONSOR_USR_ID = new SqlParameter("SPONSOR_USR_ID", SqlDbType.NVarChar, 100);
            SPONSOR_USR_ID.Value = sponsorID;
            command.Parameters.Add(SPONSOR_USR_ID);
            SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 300);
            if (!imagePath.Equals("_"))
            {
                IMAGE.Value = imagePath;
            }
            else if(categoryID.Equals("3E6E06E1-761E-4D3D-AB85-7A29FD40FBF5"))
            {
                IMAGE.Value = "images/bidItem/18267NRKTDW147834438592435.jpg";
            }
            else
            {
                IMAGE.Value = "images/bidItem/O4YIM70147834434234155.jpg";
            }
            command.Parameters.Add(IMAGE);
            command.ExecuteNonQuery();

            return "added new item";
        }

        // 7) Delete sposorship request (for vendor)
        // once accepted, cannot vendor cannot delete striaght from system, need to personally email to request
        // 7) Delete sposorship request (for SGServe admin)
        /*DL : */
        public static String deleteSponsorshipRequest(SqlConnection dataConnection, String sponsorRecordID)
        {
            try
            {
                SqlCommand command = new SqlCommand("DELETE FROM TABLE_SPR_RECORD WHERE SPR_RECORD_ID=@SPR_RECORD_ID", dataConnection);

                SqlParameter SPR_RECORD_ID = new SqlParameter("SPR_RECORD_ID", SqlDbType.NVarChar, 100);
                SPR_RECORD_ID.Value = sponsorRecordID;
                command.Parameters.Add(SPR_RECORD_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "deleted";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }
            return "failed to delete";
        }


        // Update sponsorship request
        public static String updateSponsorshipRequest(SqlConnection dataConnection, String sponsorRecordID, String sponsorItemRecordID, int quantity, String sponsorshipDesc, String collectionDesc)
        {
            try
            {
                SqlCommand command = new SqlCommand("UPDATE TABLE_SPR_RECORD SET SPR_ITEM_RECORD_ID=@SPR_ITEM_RECORD_ID, QTY=@QTY, SPONSORSHIP_DESCRIPTION=@SPONSORSHIP_DESCRIPTION, COLLECTION_DESCRIPTION=@COLLECTION_DESCRIPTION WHERE SPR_RECORD_ID=@SPR_RECORD_ID", dataConnection);

                SqlParameter SPR_RECORD_ID = new SqlParameter("SPR_RECORD_ID", SqlDbType.NVarChar, 100);
                SPR_RECORD_ID.Value = sponsorRecordID;
                command.Parameters.Add(SPR_RECORD_ID);

                SqlParameter SPR_ITEM_RECORD_ID = new SqlParameter("SPR_ITEM_RECORD_ID", SqlDbType.NVarChar, 100);
                SPR_ITEM_RECORD_ID.Value = sponsorItemRecordID;
                command.Parameters.Add(SPR_ITEM_RECORD_ID);

                SqlParameter QTY = new SqlParameter("QTY", SqlDbType.Int, 4);
                QTY.Value = quantity;
                command.Parameters.Add(QTY);

                SqlParameter SPONSORSHIP_DESCRIPTION = new SqlParameter("SPONSORSHIP_DESCRIPTION", SqlDbType.NVarChar, 250);
                SPONSORSHIP_DESCRIPTION.Value = sponsorshipDesc;
                command.Parameters.Add(SPONSORSHIP_DESCRIPTION);

                SqlParameter COLLECTION_DESCRIPTION = new SqlParameter("COLLECTION_DESCRIPTION", SqlDbType.NVarChar, 250);
                COLLECTION_DESCRIPTION.Value = collectionDesc;
                command.Parameters.Add(COLLECTION_DESCRIPTION);

                command.Prepare();
                command.ExecuteNonQuery();
                return "request updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "request failed to update";
        }

        // Update sponsorship request item
        public static String updateSponsorshipItem(SqlConnection dataConnection, String sponsorItemRecordID, String itemName, String itemDesc, String itemCatID, String imagePath)
        {
            try
            {
                String commandText = "UPDATE TABLE_SPR_ITEM_RECORD SET IMAGE=@IMAGE ";
                SqlCommand command = new SqlCommand(commandText, dataConnection);
                Boolean next = false;

                SqlParameter IMAGE = new SqlParameter("IMAGE", SqlDbType.NVarChar, 300);
                if (!imagePath.Equals("_"))
                {
                    IMAGE.Value = imagePath;
                }
                else if (itemCatID.Equals("3E6E06E1-761E-4D3D-AB85-7A29FD40FBF5"))
                {
                    IMAGE.Value = "images/bidItem/18267NRKTDW147834438592435.jpg";
                }
                else
                {
                    IMAGE.Value = "images/bidItem/O4YIM70147834434234155.jpg";
                }
                command.Parameters.Add(IMAGE);

                if (!itemName.Equals("_"))
                {
                    commandText = commandText + " , ITEM_NAME= @ITEM_NAME ";
                    next = true;

                    SqlParameter ITEM_NAME = new SqlParameter("ITEM_NAME", SqlDbType.NVarChar, 100);
                    ITEM_NAME.Value = itemName;
                    command.Parameters.Add(ITEM_NAME);
                }

                if (!itemDesc.Equals("_"))
                {
                    commandText = commandText + " , ITEM_DESCRIPTION= @ITEM_DESCRIPTION ";

                    SqlParameter ITEM_DESCRIPTION = new SqlParameter("ITEM_DESCRIPTION", SqlDbType.NVarChar, 499);
                    ITEM_DESCRIPTION.Value = itemDesc;
                    command.Parameters.Add(ITEM_DESCRIPTION);

                    next = true;
                }

                if (!itemCatID.Equals("_"))
                {
                    commandText = commandText + " , SPR_ITEM_CATEGORY_ID= @SPR_ITEM_CATEGORY_ID ";

                    SqlParameter SPR_ITEM_CATEGORY_ID = new SqlParameter("SPR_ITEM_CATEGORY_ID", SqlDbType.NVarChar, 100);
                    SPR_ITEM_CATEGORY_ID.Value = itemCatID;
                    command.Parameters.Add(SPR_ITEM_CATEGORY_ID);
                }

                commandText = commandText + " WHERE SPR_ITEM_RECORD_ID=@SPR_ITEM_RECORD_ID";
                command.CommandText = commandText;

                SqlParameter SPR_ITEM_RECORD_ID = new SqlParameter("SPR_ITEM_RECORD_ID", SqlDbType.NVarChar, 100);
                SPR_ITEM_RECORD_ID.Value = sponsorItemRecordID;
                command.Parameters.Add(SPR_ITEM_RECORD_ID);

                command.Prepare();
                command.ExecuteNonQuery();
                return "item updated";
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return "item failed to update";
        }


        public static List<SponsorshipRecord> getSponsoredRequestBySearch(SqlConnection dataConnection, String sponsorName, String itemName, String itemCatID)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                String searchQuery = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, i.IMAGE, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID";
                if (!sponsorName.Equals("_"))
                {
                    searchQuery = searchQuery + " AND f.FIRM_DESCRIPTION like '%" + sponsorName + "%'";
                }
                if (!itemName.Equals("_"))
                {
                    searchQuery = searchQuery + " AND i.ITEM_NAME like '%" + itemName + "%'";
                }
                if (!itemCatID.Equals("_"))
                {
                    searchQuery = searchQuery + " AND c.SPR_ITEM_CATEGORY_ID = @0";
                    command.Parameters.AddWithValue("@0", itemCatID);
                }

                searchQuery = searchQuery + " order by REQUEST_DATE desc";

                command.CommandText = searchQuery;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }

        public static List<SponsorshipRecord> getSponsoredRequestBySearchByVendor(SqlConnection dataConnection, String itemName, String itemCatID, String sponsorVendorID)
        {
            List<SponsorshipRecord> listOfRecords = new List<SponsorshipRecord>();

            try
            {
                SqlCommand command = new SqlCommand(null, dataConnection);
                String searchQuery = "Select SPR_RECORD_ID, i.SPR_ITEM_RECORD_ID as ITEM_ID, i.ITEM_NAME, i.IMAGE, r.QTY, ITEM_DESCRIPTION, c.SPR_ITEM_CATEGORY_ID, c.CATEGORY_NAME ,f.USR_ID as SPONSOR_USR_ID, f.FIRM_DESCRIPTION as SPONSOR_NAME, SPONSORSHIP_DESCRIPTION, STATUS, o.USR_ID as ACTION_ADMIN_ID, o.NAME as ACTION_ADMIN_NAME, REQUEST_DATE, ADMIN_ACTION_DATE from TABLE_FIRM_DETAILS f, TABLE_ORG_USR o, TABLE_SPR_RECORD r, TABLE_SPR_ITEM_RECORD i, TABLE_SPR_ITEM_CATEGORY c WHERE i.SPR_ITEM_RECORD_ID = r.SPR_ITEM_RECORD_ID AND i.SPR_ITEM_CATEGORY_ID = c.SPR_ITEM_CATEGORY_ID AND r.SPONSOR_USR_ID = f.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID AND r.ACTION_ADMIN_ID = o.USR_ID";
                if (!itemName.Equals("_"))
                {
                    searchQuery = searchQuery + " AND i.ITEM_NAME like '%" + itemName + "%'";
                }
                if (!itemCatID.Equals("_"))
                {
                    searchQuery = searchQuery + " AND c.SPR_ITEM_CATEGORY_ID = @0";
                    command.Parameters.AddWithValue("@0", itemCatID);
                }

                searchQuery = searchQuery + " AND r.SPONSOR_USR_ID = @1 order by REQUEST_DATE desc";
                command.Parameters.AddWithValue("@1", sponsorVendorID);

                command.CommandText = searchQuery;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SponsorshipRecord record = new SponsorshipRecord();
                    record.SPONSOR_RECORD_ID = reader["SPR_RECORD_ID"].ToString();
                    record.ITEM_ID = reader["ITEM_ID"].ToString();
                    record.ITEM_NAME = reader["ITEM_NAME"].ToString();
                    record.QTY = (int)reader["QTY"];
                    record.ITEM_DESCRIPTION = reader["ITEM_DESCRIPTION"].ToString();
                    record.SPR_ITEM_CATEGORY_ID = reader["SPR_ITEM_CATEGORY_ID"].ToString();
                    record.ITEM_CATEGORY_NAME = reader["CATEGORY_NAME"].ToString();
                    record.SPONSOR_USR_ID = reader["SPONSOR_USR_ID"].ToString();
                    record.SPONSOR_NAME = reader["SPONSOR_NAME"].ToString();
                    record.SPONSORSHIP_DESCRIPTION = reader["SPONSORSHIP_DESCRIPTION"].ToString();
                    record.STATUS = reader["STATUS"].ToString();
                    record.ACTION_ADMIN_ID = reader["ACTION_ADMIN_ID"].ToString();
                    record.ACTION_ADMIN_NAME = reader["ACTION_ADMIN_NAME"].ToString();
                    record.REQUEST_DATE = reader["REQUEST_DATE"].ToString();
                    record.ADMIN_ACTION_DATE = reader["ADMIN_ACTION_DATE"].ToString();
                    record.IMAGE = imageHost + reader["IMAGE"].ToString();
                    listOfRecords.Add(record);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");
            }

            return listOfRecords;
        }


    }
}