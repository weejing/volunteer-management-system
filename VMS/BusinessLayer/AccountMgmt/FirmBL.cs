﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using VMS.AccountMgmt;
using VMS.Database;
using VMS.DataLayer.AccountMgmt;
using VMS.Models.AccountModels;

namespace VMS.BusinessLayer.AccountMgmt
{
    public class FirmBL
    {
        /*Create Account */
        public static string createAccount(Firm firm)
        {
            string usr_id=null;
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            //check email
            if (checkEmailExist(firm.EMAIL) != true)
            {
                //hashed password
                DateTime createDatetimeTemp = DateTime.Now;
                string stringDate = createDatetimeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
                DateTime regDate = new DateTime();
                regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);
                firm.REGISTRATION_DATE = regDate;
                firm.PASSWORD = AuthenticateUserBL.generateHashPassword(firm.PASSWORD + regDate);
                usr_id = FirmDL.createAccount(dbConnection, firm);
                string resetPwCode = Guid.NewGuid().ToString();
            }
            database.closeDatabase();
            return usr_id;
        }

        /*Update Account detail*/
        public static int updateAccDetail(Firm firm)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count=FirmDL.updateAccDetail(dbConnection, firm);
            database.closeDatabase();
            return count;
        }

        /*Delete Account*/
        public static int deleteAcc(string connection_string, string USR_ID)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int count= FirmDL.deleteAcc(dbConnection, USR_ID);
            database.closeDatabase();
            return count;
        }

        /*get employee detail*/
        public static Firm getFirmDetails(string connection_string, string USR_ID)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            Firm firm = FirmDL.getFirmDetails(dbConnection, USR_ID);
            database.closeDatabase();
            return firm;
        }
       
        /*Search for firms*/
        public static List<Firm> searchFirms(string connection_string, string firm_name)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Firm> firmList = FirmDL.searchFirms(dbConnection, firm_name);
            database.closeDatabase();
            return firmList;
        }

        /*Get all firms*/
        public static List<Firm> getAllFirms(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Firm> firmList = FirmDL.searchFirms(dbConnection, "");
            database.closeDatabase();
            return firmList;
        }

        /*Check existing employee email*/
        public static Boolean checkEmailExist(string EMAIL)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            //use the common method in AccountMgmtDL to cehck
            Boolean emailExist= AccountMgmtDL.getEmailExist(dbConnection, "TABLE_FIRM_DETAILS", EMAIL);
            database.closeDatabase();
            return emailExist;
        }

        /*Request to reset password*/
        public static Boolean reqResetPw(string url, string email)
        {
            Boolean reqSuccess = false;
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            if (checkEmailExist(email) == true)
            {
                //update resetpw_code
                string resetPwCode = Guid.NewGuid().ToString();
                AccountMgmtDL.updateResetPWCode(dbConnection, "TABLE_FIRM_DETAILS", email, resetPwCode);
                SendResetPwEmail(url, resetPwCode, email);
                reqSuccess = true;
            }
            database.closeDatabase();
            return reqSuccess;
        }
      
        /*Employee reset password */
        public static Boolean resetPW(string reset_code, string newpassword)
        {   //validate reset_code is it GUID
            Guid guidOutput;
            bool isGuid = Guid.TryParse(reset_code, out guidOutput);
            Boolean resetPwSuccess = false;
            if (isGuid == true)
            {
                Database_Connect database = new Database_Connect("CONSOLE_VMS");
                SqlConnection dbConnection = database.getConnection();
                string salt = AccountMgmtDL.getSaltByResetCode(dbConnection, "TABLE_FIRM_DETAILS", reset_code);
                if (salt != "")
                {
                    string newHashedPw = AuthenticateUserBL.generateHashPassword(newpassword + salt);
                    AccountMgmtDL.updateResetPW(dbConnection, "TABLE_FIRM_DETAILS", reset_code, newHashedPw);
                    resetPwSuccess = true;
                }
                database.closeDatabase();
            }
            return resetPwSuccess;
        }

        /*Employee change password*/
        public static Boolean changePw(String email, string newpassword, string oldpassword)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String salt = AccountMgmtDL.getSalt(dbConnection, "TABLE_FIRM_DETAILS", email);
            String hashedNewPassWord = AuthenticateUserBL.generateHashPassword(newpassword + salt);
            String hashedOldPassWord = AuthenticateUserBL.generateHashPassword(oldpassword + salt);
            Boolean pwChanged=AccountMgmtDL.updatePW(dbConnection, "TABLE_FIRM_DETAILS", email, hashedNewPassWord, hashedOldPassWord);
            database.closeDatabase();
            return pwChanged;

        }


        private static void SendResetPwEmail(string url, string activationCode, string email)
        {
            using (MailMessage mm = new MailMessage("admin@sgserve.com", email))
            {
                mm.Subject = "SGServe Vendor - Reset Password";
                string body = "Hello, ";
                body += "<br /><br />Please click the following link to reset password.";
                body += "<br />Click <a href = " + url + "?ResetPwCode=" + activationCode + ">here</a> to reset password.";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.zoho.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("admin@sgserve.com", "sgserve1991_iAM");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }
    }
}