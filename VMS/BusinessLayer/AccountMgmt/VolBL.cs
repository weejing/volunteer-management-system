﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using VMS.AccountMgmt;
using VMS.BusinessLayer.AdminMgmt;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AccountMgmt
{
    public class VolBL
    {
        /*Volunteer register without facebook*/
        public static string createAccNoFB(Volunteer volunteer)
        {
            string volid="";
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //hashed pw with salt
            DateTime regDatetimeTemp = DateTime.Now;
            string stringDate= regDatetimeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
            DateTime regDate = new DateTime();
            regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);
            volunteer.REGISTRATION_DATE = regDate;
            volunteer.PASSWORD = AuthenticateUserBL.generateHashPassword(volunteer.PASSWORD + regDate);
            volid= AccountMgmtDL.createAccNoFB(dbConnection, volunteer);
            TermRelationBL.createRelListForSameEntity(volunteer.volListofInts, "TABLE_USR_DETAILS", volid);

            volunteer.ACTIVATION_CODE = AccountMgmtDL.updateActivationCode(dbConnection, volid);
            SendActivationEmail(volunteer.VOL_ID, volunteer.ACTIVATION_CODE, volunteer.FULL_NAME, volunteer.EMAIL,volunteer.URL);
            database.closeDatabase();
            return volid;        

        }

        /*Volunteer register with facebook*/
        public static string createAccFB(Volunteer volunteer)
        {

            string volid = "";
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //hashed pw with salt
            DateTime regDatetimeTemp = DateTime.Now;
            string stringDate = regDatetimeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
            DateTime regDate = new DateTime();
            regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);
            volunteer.REGISTRATION_DATE = regDate;
            volid = AccountMgmtDL.createAccFB(dbConnection, volunteer);
            TermRelationBL.createRelListForSameEntity(volunteer.volListofInts, "TABLE_USR_DETAILS", volid);
            database.closeDatabase();
            return volid;

        }

        private static void SendActivationEmail(string userId, string activationCode, string full_name, string email,string url)
        {
            using (MailMessage mm = new MailMessage("admin@sgserve.com", email))
            {
                mm.Subject = "SGServe -Account Activation";
                string body = "Hello " + full_name.Trim() + ",";
                body += "<br /><br />Please click the following link to activate your account.";
                body += "<br />Click <a href='" + url + "/AccActivation.aspx?ActivationCode=" + activationCode + "'>here</a> to activate your account.";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.zoho.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("admin@sgserve.com", "sgserve1991_iAM");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        private static void SendResetPwEmail(string activationCode,string email, string url)
        {
            using (MailMessage mm = new MailMessage("admin@sgserve.com", email))
            {
                mm.Subject = "SGServe - Reset Password";
                string body = "Hello, ";
                body += "<br /><br />Please click the following link to reset password.";
                body += "<br />Click <a href='" + url + "/ChangePassword.aspx?ResetPwCode=" + activationCode + "'>here</a> to reset password.";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.zoho.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("admin@sgserve.com", "sgserve1991_iAM");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        public static Boolean checkEmailExist(string EMAIL)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            Boolean emailExist= AccountMgmtDL.getEmailExist(dbConnection, "TABLE_USR_DETAILS", EMAIL);
            database.closeDatabase();
            return emailExist;
        }

        /*Update account activation*/
        public static int updateAccActivated(string ACTIVATION_CODE)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = AccountMgmtDL.updateAccActivated(dbConnection, ACTIVATION_CODE);
            database.closeDatabase();
            return count;
        }

        /*get volunteer particulars*/
        public static Volunteer getVolParticular(string usr_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            Volunteer vol = AccountMgmtDL.getVolParticular(dbConnection, usr_id);
            database.closeDatabase();
            return vol;
        }

        /*Volunter request to reset password*/
        public static Boolean reqResetPw(string email,string URL)
        {
            Boolean reqSuccess=false;
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            if (checkEmailExist(email) == true)
            {
                //update resetpw_code
               string resetPwCode = Guid.NewGuid().ToString();
               AccountMgmtDL.updateResetPWCode(dbConnection, "TABLE_USR_DETAILS", email, resetPwCode);
               SendResetPwEmail(resetPwCode, email,URL);
                reqSuccess= true;
            }
            database.closeDatabase();
            return reqSuccess;
        }

        /*Volunteer reset password */
        public static Boolean resetPW(string reset_code, string newpassword)
        {   //validate reset_code is it GUID
            Guid guidOutput;
            bool isGuid = Guid.TryParse(reset_code, out guidOutput);
            Boolean resetPwSuccess = false;
            if (isGuid == true)
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                string salt = AccountMgmtDL.getSaltByResetCode(dbConnection,"TABLE_USR_DETAILS", reset_code);
                if (salt != "")
                {                  
                    string newHashedPw = AuthenticateUserBL.generateHashPassword(newpassword + salt);
                    AccountMgmtDL.updateResetPW(dbConnection, "TABLE_USR_DETAILS", reset_code, newHashedPw);
                    resetPwSuccess = true;
                }
                database.closeDatabase();
            }        

            return resetPwSuccess;
        }

        /*Volunter change password*/
        public static Boolean changePw(String email, string newpassword, string oldpassword)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String salt = AccountMgmtDL.getSalt(dbConnection, "TABLE_USR_DETAILS", email);
            String hashedNewPassWord = AuthenticateUserBL.generateHashPassword(newpassword + salt);
            String hashedOldPassWord = AuthenticateUserBL.generateHashPassword(oldpassword + salt);
            Boolean pwChanged = AccountMgmtDL.updatePW(dbConnection, "TABLE_USR_DETAILS", email, hashedNewPassWord, hashedOldPassWord);
            database.closeDatabase();
            return pwChanged;
          
        }

        /*Update Account detail*/
        public static int updateAccDetail(Volunteer volunteer)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = AccountMgmtDL.updateAccDetail(dbConnection, volunteer);
            database.closeDatabase();
            return count;
        }


        /*Update volunteer interest form records - Interests, Skills, Preferred Days*/
        public static int updateInterestForm(Volunteer volunteer)
        {
            int success = 0;
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            //delete old record
            TermRelationBL.deleteTermRelByEntityID(volunteer.VOL_ID, "TABLE_USR_DETAILS");
            TermRelationBL.createRelListForSameEntity(volunteer.volListofInts, "TABLE_USR_DETAILS", volunteer.VOL_ID);
            database.closeDatabase();
            return 1;
        }

        /*set VIA goals */
        //public static void setVIAgoal(string volid, string via_target)
        //{
        //    Database_Connect database = new Database_Connect("Console_VMS");
        //    SqlConnection dbConnection = database.getConnection();
        //    AccountMgmtDL.setVIAgoal(dbConnection, volid,  via_target);
        //}

        /*Get list of volunteers*/
        public static List<Volunteer> getVolunteers(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Volunteer> volList = AccountMgmtDL.getVolunteers(dbConnection);
            database.closeDatabase();
            return volList;
        }

        /*get volunteer particulars*/
        public static List<TermRelation> getVolInterestForm(string usr_id, string vocab_name)
        {
            int vocab_id= TermBL.getVocabID(vocab_name);
            return TermRelationBL.getTermRelListByEntityIdAndVocab(usr_id, "TABLE_USR_DETAILS", vocab_id);
        }

        /*Search for volunteers*/
        public static List<Volunteer> searchVol(string name)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<Volunteer> volList= AccountMgmtDL.getVolbyNameSearch(dbConnection, name);
            database.closeDatabase();
            return volList;
        }

        /*Get Volunteer Past Events*/
        public static List<UserPastEvents> getUserPastEvents(string connection, string volID)
        {
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
            return AccountMgmtDL.getUserPastEvents(dbConnection, volID);
        }

        /*Get Volunteer Past Events Details*/
        public static List<UserPastEvents> getUserPastEventDetails(string connection, string eventID)
        {
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
            return AccountMgmtDL.getUserPastEventDetails(dbConnection, eventID);
        }
        //start
        public static void updateVolHours(string connection, string volid,int eventhour)
        {
            int total = 0;
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
           int currentHour= AccountMgmtDL.getVolHours(dbConnection,volid);
            total= currentHour + eventhour;
            AccountMgmtDL.updateVolVIA(dbConnection, volid,total);
        }

        }
}