﻿using System;
using System.Collections.Generic;
using VMS.Database;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using VMS.Models.AccountModels;
using System.Globalization;
using VMS.DataLayer.OrgAccountMgmt;

namespace VMS.AccountMgmt
{
    public class AuthenticateUserBL
    {
        /*
         * Normal user logins
         * This methods performs an authentication check on to determine if the username and the passwords are a legitimate match 
         * This method can only be called when there is a need to autheticate non-facebook login 
         */
        public static String login(String userName, String password, String tableName)
        {
            Database_Connect databaseConfig = new Database_Connect("Console_VMS");
            SqlConnection databaseConnection = databaseConfig.getConnection();

            String salt = AccountMgmtDL.getSalt(databaseConnection, tableName, userName);
            System.Diagnostics.Debug.WriteLine("salt at login:" + salt);
            if (salt.Equals(""))
                return "";
            //DateTime regDate = DateTime.ParseExact(salt, "MM/dd/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture);
            String hashedPassWord = generateHashPassword(password+ salt);
            return AccountMgmtDL.getPassword(userName, hashedPassWord, databaseConnection, tableName);
        }
        
                
        /*
         * Facebook login Authetication
         * This method mitigate the risk of malicious users levraging on facebook login to gain a JWT. This procedure is checked against facebook access token which can only be generate by the facebook app secret key  
         *  Upon retrieving, if user id is a match to the VMS database, it means that current user would be authorise to have a jWT
         */
        public static String facebookAuthentication(String fbID , String accessToken)
        {
            Uri targetUserUri = new Uri("https://graph.facebook.com/"+fbID+"?fields=first_name,last_name,gender,locale,link,birthday&access_token=" + accessToken);
            HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);
            String jsonResponse = string.Empty;

            try
            {
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                jsonResponse = userInfo.ReadToEnd();
            }
            catch (Exception e)
            {
                // this means that either access token fails or facebook id does not match
                return "";
            }

            System.Diagnostics.Debug.WriteLine("jsonResponse: " + jsonResponse);
            Database_Connect databaseConfig = new Database_Connect("Console_VMS");
            SqlConnection databaseConnection = databaseConfig.getConnection();
            
            string volid=AccountMgmtDL.getFacebookUserData(fbID, databaseConnection);
            if (volid == "")
            {
                return "";
            }
            else {
                return volid;
            }
            
        }


        /*
         * This method helps to generate the hashPassWord for a specific user
         */
        public static String generateHashPassword(String passwordAndSalt)
        {
            SHA256 shaAlgo = new SHA256CryptoServiceProvider();
            byte[] DataBytes = Encoding.UTF8.GetBytes(passwordAndSalt);
            byte[] hashedPassword = shaAlgo.ComputeHash(DataBytes);
            return Convert.ToBase64String(hashedPassword);
        }
        
        /*
         * This method authenticates an organiser employee towards their SAAS databse table 
         * 
         */
        public static String orgUserLogin(String userName, String password, string tableName, String connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection databaseConnection = databaseConfig.getConnection();

            String salt = AccountMgmtDL.getSalt(databaseConnection, tableName, userName);
            System.Diagnostics.Debug.WriteLine("salt at ologin: " + salt);
            String hashedPassword = generateHashPassword(password + salt);
            System.Diagnostics.Debug.WriteLine("hashed pasword: " + hashedPassword);
            return AccountMgmtDL.getPassword(userName, hashedPassword, databaseConnection, tableName);

        }
        
        
        /*
         * This method authenticates an vendor user login 
         */     
         public static String vendorLogin(String userName, String password)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            String salt = AccountMgmtDL.getSalt(dataConnection, "TABLE_FIRM_DETAILS", userName);
            String hashedPassword = generateHashPassword(password + salt);
            return AccountMgmtDL.getPassword(userName, hashedPassword, dataConnection, "TABLE_FIRM_DETAILS");                  
        }



        /*
         * This method authenticates an vendor user login 
         */
        public static String oneThirdLogin(String userName, String password)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            String salt = AccountMgmtDL.getSalt(dataConnection, "TABLE_ORG_USR", userName);
            String hashedPassword = generateHashPassword(password + salt);
            return OrgAccDL.checkPassword(userName, hashedPassword, dataConnection, "TABLE_ORG_USR");
        }
    }
}