﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using VMS.AccountMgmt;
using VMS.BusinessLayer.AdminMgmt;
using VMS.DataLayer.AccountMgmt;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;
namespace VMS.BusinessLayer.AccountMgmt
{
    public class DashboardBL
    {        
        public static List<UserPastEvents> getCompletedEvents(string connection, string volID)
        {
            List<UserPastEvents> listOfCompletedEvents = new List<UserPastEvents>();
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
            listOfCompletedEvents = DashboardDL.getCompletedEvents(dbConnection, volID);
            database.closeDatabase();
            return listOfCompletedEvents;
        }

        //get upcoming and ongoing
        public static List<UserPastEvents> getUpcomingEvents(string connection, string volID)
        {
            List<UserPastEvents> listOfUpcomingEvents = new List<UserPastEvents>();
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
            listOfUpcomingEvents = DashboardDL.getUpcomingEvents(dbConnection, volID);
            database.closeDatabase();
            return listOfUpcomingEvents;
        }
        
        public static List<String> getNPO(string volID)
        {
            List<string> organisationList = new List<string>();
            organisationList = DashboardDL.getNPO(volID);
            return organisationList;
        }

        //get via hours
        public static int getCompletedVIA(string connection, string volID)
        {
            int hours = 0;
            Database_Connect database = new Database_Connect(connection);
            SqlConnection dbConnection = database.getConnection();
            hours = DashboardDL.getCompletedVIA(dbConnection, volID);
            database.closeDatabase();
            return hours;
        }
    }
}