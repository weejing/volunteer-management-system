﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.Models.OrgMgmtModels;
using VMS.DataLayer.OrgMgmt;
using VMS.BusinessLayer.AdminMgmt;
using VMS.AccountMgmt;
using System.Net.Mail;

namespace VMS.BusinessLayer.OrgMgmt
{
    public class OrgMgmtBL
    {
        /// <summary>BBL: insert new organisation</summary>
        public static String insertOrganization(OrganizationDetails newOrg)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            if (newOrg.LINK == null)
            {
                newOrg.LINK = "";
            }
            return OrgMgmtDL.insertOrganization(dbConnection, newOrg);
        }

        /// <summary>BBL: launch new organisation database instance</summary>
        public static String insertOrgInstance(OrgUserDetails newOrg)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            //dbConnection.ConnectionTimeout = 0;
            System.Diagnostics.Debug.WriteLine("connectionString: " + newOrg.CONNECTION_STRING);
            OrgMgmtDL.insertOrgInstance(dbConnection, newOrg.CONNECTION_STRING);
            database.closeDatabase();

            Database_Connect database2 = new Database_Connect(newOrg.CONNECTION_STRING);
            SqlConnection dbConnection2 = database2.getConnection();

            OrgMgmtDL.feedNewTable(dbConnection2);
            
            database2.closeDatabase();

            insertMasterAdmin(newOrg);
            //insertMasterEventRoles(connectionString.CONNECTION_STRING);
            sendEmail(newOrg);
            return "Success";
        }

        // send email
        public static void sendEmail(OrgUserDetails newOrg)
        {
            string your_id = "admin@sgserve.com";
            string your_password = "sgserve1991_iAM";
            string recepient = newOrg.EMAIL;
            string subject = "Your Account has been created!";
            string body = @"Dear " + newOrg.NAME + "," + System.Environment.NewLine +
                System.Environment.NewLine +
                "Your account at SGServe has been created!" + System.Environment.NewLine +
                "Your organisation's login link is " +newOrg.USR_ID +"/"+ newOrg.CONNECTION_STRING + "/ologin" + System.Environment.NewLine +
                System.Environment.NewLine +
                "Regards," + System.Environment.NewLine +
                "The SGServe Team";

            try
            {
                SmtpClient client = new SmtpClient
                {
                    Host = "smtp.zoho.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(your_id, your_password),
                    Timeout = 10000,
                };
                MailMessage mm = new MailMessage(your_id, recepient, subject, body);
                client.Send(mm);
                Console.WriteLine("Email Sent");
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not end email\n\n" + e.ToString());
            }
        }

        // this method inserts a list of event roles into organisation event role table
        public static void insertMasterEventRoles(String connectionString)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<EventMasterRole> listOfEventMasterRoles = OrgMgmtDL.getEventRoleMasterList(dbConnection);
            database.closeDatabase();

            database = new Database_Connect(connectionString);
            dbConnection = database.getConnection();
            //OrgMgmtDL.importEventRoleTable(dbConnection, listOfEventMasterRoles);
            database.closeDatabase();
        }

        /// <summary>BBL: insert organisation master admin</summary>
        public static String insertMasterAdmin(OrgUserDetails newUser)
        {
            Database_Connect database = new Database_Connect(newUser.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();

            DateTime regDateTime = DateTime.Now;
            string stringDate = stringDate = regDateTime.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
            DateTime regDate = new DateTime();
            regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);
            newUser.REGISTRATION_DATE = regDate;
            System.Diagnostics.Debug.WriteLine("before hashed PS: " + newUser.PASSWORD);
            newUser.PASSWORD = AuthenticateUserBL.generateHashPassword(newUser.PASSWORD + regDate);

            System.Diagnostics.Debug.WriteLine("hashed PS: " + newUser.PASSWORD);

            //Database_Connect database2 = new Database_Connect("Console_VMS");
            //SqlConnection dbConnection2 = database2.getConnection();
            //OrgMgmtDL.updateOrganizationStatus(dbConnection2, newUser.CONNECTION_STRING);
            //database2.closeDatabase();

            String result = OrgMgmtDL.insertMasterAdmin(dbConnection, newUser);
            database.closeDatabase();
            return result;
        }

        /// <summary>BBL: get organisation information</summary>
        public static OrganizationDetails getOrgInfo(OrganizationDetails org)
        {
            //console datatbase string
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            return OrgMgmtDL.getOrgInfo(dbConnection, org);
        }

        
        public static OrganizationDetails getOrgInfo2(OrganizationDetails org)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            return OrgMgmtDL.getOrgInfo2(dbConnection, org);
        }

        /// <summary>BBL: get organisation information list</summary>
        public static List<OrganizationDetails> getOrgList()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            return OrgMgmtDL.getOrgList(dbConnection);
        }

        /// <summary>BBL: update organisation information</summary>
        public static String updateOrganization(OrganizationDetails newOrg)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            if (newOrg.LINK == null)
            {
                newOrg.LINK = "";
            }

            return OrgMgmtDL.updateOrganization(dbConnection, newOrg);
        }

        /*search for organization*/
        public static List<OrganizationDetails> searchOrg(string connection_string, string name)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            return OrgMgmtDL.searchOrg(dbConnection,connection_string, name);
        }
    }
}