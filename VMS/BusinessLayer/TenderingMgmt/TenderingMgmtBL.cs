﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.BusinessLayer.AuditMgmt;
using VMS.Database;
using VMS.DataLayer.TenderingMgmt;
using VMS.Models.TenderingModels;

namespace VMS.BusinessLayer.TenderingMgmt
{
    public class TenderingMgmtBL
    {
        public static List<TenderHeader> getTenderHeaderList()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<TenderHeader> tenderHeaderList = TenderingMgmtDL.getTenderHeaderList(dbConnection);
            database.closeDatabase();
            return tenderHeaderList;
        }
        public static TenderHeader getTenderHeader(TenderHeader headerID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderHeader tenderHeader = TenderingMgmtDL.getTenderHeader(dbConnection, headerID);
            database.closeDatabase();
            return tenderHeader;
        }
        public static List<TenderItemDetails> getTenderItemList(TenderHeader headerID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnectionGet = database.getConnection();

            //update all partial bids
            List<TenderItemDetails> bids = TenderingMgmtDL.getTenderItemList(dbConnectionGet, headerID);
            int bidCount = bids.Count;
            int delivery = 0;
            int accepted = 0;
            int partial = 0;
            int submitted = 0;
            for (int i = 0; i < bidCount; i++)
            {
                if (bids[i].BID_STATUS.ToString() == "Delivery Confirmed" )
                {
                    delivery++;
                }
                if (bids[i].BID_STATUS.ToString() == "Goods Accepted")
                {
                    accepted++;
                }
                if (bids[i].BID_STATUS.ToString() == "Partial")
                {
                    partial++;
                }
                if (bids[i].BID_STATUS.ToString() == "Submitted")
                {
                    submitted++;
                }
            }
            database.closeDatabase();
            if (delivery > 0 || accepted > 0)
            {
                
            }
            else {
                if (partial + submitted == bidCount)
                {
                    Database_Connect databaseUpd = new Database_Connect("Console_VMS");
                    SqlConnection dbConnectionUpd = databaseUpd.getConnection();
                    TenderingMgmtDL.updateBidStatusForPartial(dbConnectionUpd, headerID.TENDER_HEADER_ID);
                    databaseUpd.closeDatabase();
                }
                else if (partial + submitted < bidCount)
                {
                    Database_Connect databaseUpd = new Database_Connect("Console_VMS");
                    SqlConnection dbConnectionUpd = databaseUpd.getConnection();
                    TenderingMgmtDL.updateBidStatusBecomePartial(dbConnectionUpd, headerID.TENDER_HEADER_ID);
                    databaseUpd.closeDatabase();
                }
            }
            Database_Connect database2 = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database2.getConnection();
            List<TenderItemDetails> tenderItemList = TenderingMgmtDL.getTenderItemList(dbConnection, headerID);
            database2.closeDatabase();
            return tenderItemList;
        }

        public static List<TenderItemDetails> getSaaSTenderItemList(TenderHeader headerID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TenderItemDetails> items = TenderingMgmtDL.getSaaSTenderItemList(dbConnection, headerID);
            database.closeDatabase();
            return items;
        }

        public static List<VendorBidsDetails> getTenderLowestBid(TenderItemDetails headerID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VendorBidsDetails> items = TenderingMgmtDL.getTenderLowestBid(dbConnection, headerID);
            database.closeDatabase();
            return items;
        }

        public static List<VendorBidsDetails> getTenderLowestBidSolo(TenderItemDetails headerID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VendorBidsDetails> items = TenderingMgmtDL.getTenderLowestBidSolo(dbConnection, headerID);
            database.closeDatabase();
            return items;
        }

        public static List<TenderCategory> getTenderCategoryList()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TenderCategory> items = TenderingMgmtDL.getTenderCategoryList(dbConnection);
            database.closeDatabase();
            return items;
        }

        public static List<TenderItemDetails> getTenderListSaaS(TenderItemDetails item)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TenderItemDetails> items = TenderingMgmtDL.getTenderListSaaS(dbConnection, item);
            database.closeDatabase();
            return items;
        }

        public static List<BidCombineView> getBidderSaaS(BidCombineView item)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidCombineView> items = TenderingMgmtDL.getBidderSaaS(dbConnection, item);
            database.closeDatabase();
            return items;
        }

        public static List<TenderItemDetails> getBiddedTenderList(VendorDetails orgDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TenderItemDetails> items = TenderingMgmtDL.getBiddedTenderList(dbConnection, orgDetails);
            database.closeDatabase();
            return items;
        }

        public static BidCombineView getBidItemSaaS(BidCombineView itemID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            BidCombineView item = TenderingMgmtDL.getBidItemSaaS(dbConnection, itemID);
            database.closeDatabase();
            return item;
        }

        public static String updateTenderStatusStatus()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String item = TenderingMgmtDL.updateTenderStatusStatus(dbConnection);
            database.closeDatabase();
            return item;
        }

        public static String updateTenderStatus(TenderItemDetails itemID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String item = TenderingMgmtDL.updateTenderStatus(dbConnection, itemID);
            database.closeDatabase();
            return item;
        }

        public static String updateBidStatus(VendorBidsDetails itemID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String item = TenderingMgmtDL.updateBidStatus(dbConnection, itemID);
            database.closeDatabase();
            return item;
        }

        public static TenderItemDetails getTenderItem(String itemID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TenderItemDetails item = TenderingMgmtDL.getTenderItem(dbConnection, itemID);
            database.closeDatabase();
            return item;
        }

        public static String createTenderHeader(TenderHeader newTender)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String item = TenderingMgmtDL.createTenderHeader(dbConnection, newTender);
            database.closeDatabase();

            AuditMgmtBL.createAuditObj(4, 11, "Create New Tender Header", "TenderingMgmtBL.createTenderHeader", "", newTender.NAME, newTender.CONNECTION_STRING);

            return item;
        }

        public static String createTenderItem(TenderItemDetails newTender)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            String item = TenderingMgmtDL.createTenderItem(dbConnection, newTender);
            database.closeDatabase();

            AuditMgmtBL.createAuditObj(4, 11, "Create New Tender Item", "TenderingMgmtBL.createTenderItem", "", newTender.ORG_NAME, newTender.CONNECTION_STRING);

            return item;
        }

        public static void updateTender(TenderItemDetails newTender)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateTender(dbConnection, newTender);
            database.closeDatabase();

            AuditMgmtBL.createAuditObj(4, 11, "Update Tender Item", "TenderingMgmtBL.updateTender", "", newTender.ORG_NAME, newTender.CONNECTION_STRING);
        }

        public static BidCombineView getRating(BidCombineView supplier)
        {
            BidCombineView result = new BidCombineView();
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            result = TenderingMgmtDL.getRating(dbConnection, supplier);
            database.closeDatabase();
            return result;
        }

        public static void updateTenderStatusStart()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateTenderStatusStart(dbConnection);
            database.closeDatabase();
        }

        public static void updateTenderStatusClosed()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateTenderStatusClosed(dbConnection);
            database.closeDatabase();
        }

        public static void updateTenderWinner(String tenderID, String venderID, DateTime winnerDate)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateTenderWinner(dbConnection, tenderID, venderID, winnerDate);
            database.closeDatabase();
        }

        public static void deleteTender(TenderItemDetails tenderID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.deleteTender(dbConnection, tenderID);
            database.closeDatabase();

            AuditMgmtBL.createAuditObj(4, 11, "Delete Tender Item", "TenderingMgmtBL.deleteTender", "", tenderID.ORG_NAME, tenderID.CONNECTION_STRING);
        }

        public static void deleteBid(VendorBidsDetails tenderID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.deleteBid(dbConnection, tenderID);
            database.closeDatabase();
        }

        public static OrganisationDetails getTenderOrganizationInfo(String connectionString)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            OrganisationDetails item = TenderingMgmtDL.getTenderOrganizationInfo(dbConnection, connectionString);
            database.closeDatabase();
            return item;
        }

        public static void vendorSignUp(VendorDetails vendorDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.vendorSignUp(dbConnection, vendorDetails);
            database.closeDatabase();
        }


        public static void updateVendorPassword(String tenderID, String password)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorPassword(dbConnection, tenderID, password);
            database.closeDatabase();
        }


        public static void forgotVendorPassword(String tenderID, String password)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.forgotVendorPassword(dbConnection, tenderID, password);
            database.closeDatabase();
        }


        public static void updateVendorParticulars(VendorDetails orgDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorParticulars(dbConnection, orgDetails);
            database.closeDatabase();
        }

        public static void insertVendorBid(VendorBidsDetails newBid)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.insertVendorBid(dbConnection, newBid);
            database.closeDatabase();
        }


        public static void updateVendorBid(VendorBidsDetails bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorBid(dbConnection, bidsDetails);
            database.closeDatabase();
        }


        public static void updateVendorBidStatus(VendorBidsDetails bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorBidStatus(dbConnection, bidsDetails);
            database.closeDatabase();
        }

        public static void updateTenderItemStatus(TenderItemDetails bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateTenderItemStatus(dbConnection, bidsDetails);
            database.closeDatabase();

            
        }

        public static void updateTenderHeadStatus(TenderHeader bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            if (bidsDetails.STATUS == "Closed")
            {
                Database_Connect database2 = new Database_Connect("Console_VMS");
                SqlConnection dbConnection2 = database2.getConnection();
                TenderingMgmtDL.updateTenderItemStatusAll(dbConnection2, bidsDetails);
                database2.closeDatabase();
            }

            TenderingMgmtDL.updateTenderHeadStatus(dbConnection, bidsDetails);
            database.closeDatabase();
        }

        public static void updateVendorBidPacked(VendorBidsDetails bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorBidPacked(dbConnection, bidsDetails);
            database.closeDatabase();
        }

        public static void updateVendorBidDelivered(VendorBidsDetails bidsDetails)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            TenderingMgmtDL.updateVendorBidDelivered(dbConnection, bidsDetails);
            database.closeDatabase();
        }

        public static List<VendorBidsDetails> getSubmittedBidsList(TenderItemDetails vendorID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VendorBidsDetails> bidItem = TenderingMgmtDL.getSubmittedBidsList(dbConnection, vendorID);
            database.closeDatabase();
            return bidItem;
        }

        public static VendorBidsDetails getSubmittedBid(VendorBidsDetails tenderItemID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            VendorBidsDetails item = TenderingMgmtDL.getSubmittedBid(dbConnection, tenderItemID);
            database.closeDatabase();
            return item;
        }

        public static void autoUpdateTenderStatusStart()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TenderingMgmtDL.autoUpdateTenderStatusStart(dbConnection);
            database.closeDatabase();
        }

        public static void autoUpdateTenderStatusEnd()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TenderingMgmtDL.autoUpdateTenderStatusEnd(dbConnection);
            database.closeDatabase();
        }
    }
}