﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.NotificationMgmt;
using VMS.Models.NotificationModels;

namespace VMS.BusinessLayer.NotificationMgmt
{
    public class NotificationBL
    {
        public static string createNotif(Notification notification) {
            Database_Connect database = new Database_Connect(notification.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            string id = NotificationDL.createNotif(dbConnection, notification, notification.TABLE_NAME);
            database.closeDatabase();
            return id;
        }

        public static int updateToRead(List<Notification> NotificationList) {
            string connection_string="";
            string tablename = "";
            foreach (Notification notif in NotificationList)
            {
                connection_string = notif.CONNECTION_STRING;
                tablename = notif.TABLE_NAME;
                break;
            }
                Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int count = NotificationDL.updateToRead(dbConnection, NotificationList, tablename);
            database.closeDatabase();
            return count;
        }

        public static List<Notification> getNotifList(string connection_string, string tablename, string receiver_id) {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Notification> notiList= NotificationDL.getNotifList(dbConnection, tablename, receiver_id);
            database.closeDatabase();
            return notiList;
        }

        public static int getNumUnreadNotif(string connection_string, string tablename, string receiver_id)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int count = NotificationDL.getNumUnreadNotif(dbConnection, tablename, receiver_id);
            database.closeDatabase();
            return count;
        }
        }
}