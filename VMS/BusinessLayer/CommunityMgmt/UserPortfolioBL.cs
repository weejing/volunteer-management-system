﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Database;
using VMS.DataLayer.CommunityMgmt;
using VMS.Models.CommunityModels;
using VMS.Models.OrgMgmtModels;

namespace VMS.BusinessLayer.CommunityMgmt
{
    public class UserPortfolioBL
    {
        public static List<String> getUserIDs()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<String> listOfUserIDs = UserPortfolioDL.getUserID(dbConnection);
            database.closeDatabase();
            return listOfUserIDs;
        }

        public static List<UserPortfolio> getListofUsersBySearch(String name)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserPortfolio> listofUsers = UserPortfolioDL.getListofUsersBySearch(dbConnection, name);
            database.closeDatabase();
            return listofUsers;
        }

        public static UserPortfolio getUserDetails(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            UserPortfolio portfolio = UserPortfolioDL.getUserData(dbConnection, userID);
            database.closeDatabase();
            return portfolio;
        }

        public static List<String> getUserInterests(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<String> listOfUserInterests = UserPortfolioDL.getUserInterests(dbConnection, userID);
            database.closeDatabase();
            return listOfUserInterests;
        }

        public static List<String> getUserSkills(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<String> listOfUserSkills =  UserPortfolioDL.getUserSkills(dbConnection, userID);
            database.closeDatabase();
            return listOfUserSkills;
        }

        public static UserPortfolio getUserFullNameByID(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            UserPortfolio portfolio = UserPortfolioDL.getUserFullNameByID(dbConnection, userID);
            database.closeDatabase();
            return portfolio;
        }

        public static String updateUserPortfolio(String userID, String description)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.updateUserPortfolio(dbConnection, userID, description);
            database.closeDatabase();
            return result;
        }

        public static List<UserPortfolio> getUserFriends(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserPortfolio> listOfUserPortfolio = UserPortfolioDL.getUserFriends(dbConnection, userID);
            database.closeDatabase();
            return listOfUserPortfolio;
        }

        public static List<UserPortfolio> getUserFriendsRequest(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserPortfolio> listOfUserPortfolio = UserPortfolioDL.getUserFriendsRequest(dbConnection, userID);
            database.closeDatabase();
            return listOfUserPortfolio;
        }

        public static String checkfriends(String userID1, String userID2)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.checkfriends(dbConnection, userID1, userID2);
            database.closeDatabase();
            return result;
        }

        public static String checkfriendID(String userID1, String userID2)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.checkfriendID(dbConnection, userID1, userID2);
            database.closeDatabase();
            return result;
        }

        public static String addUserFriend(String userID, String friendID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.addFriend(dbConnection, userID, friendID);
            database.closeDatabase();
            return result;
        }

        public static String addUserFriendWithQR(String userID, String friendID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.addUserFriendWithQR(dbConnection, userID, friendID);
            database.closeDatabase();
            return result;
        }

        public static String acceptFriendStatus(String user1ID, String user2ID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.acceptFriendStatus(dbConnection, user1ID, user2ID);
            database.closeDatabase();
            return result;
        }

        public static String deleteFriend(String user1ID, String user2ID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.deleteFriend(dbConnection, user1ID, user2ID);
            database.closeDatabase();
            return result;
        }

        public static String followOrganisation(String userID, String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.followOrganisation(dbConnection, userID, orgID);
            database.closeDatabase();
            return result;
        }

        public static String unfollowOrganisation(String userID, String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.unfollowOrganisation(dbConnection, userID, orgID);
            database.closeDatabase();
            return result;
        }

        public static List<OrganizationDetails> getUserOrganisations(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<OrganizationDetails> listOfOrgs = UserPortfolioDL.getUserOrganisations(dbConnection, userID);
            database.closeDatabase();
            return listOfOrgs;
        }

        public static List<UserPortfolio> getUsersFollowedForOrganisation(String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserPortfolio> listOfUsers = UserPortfolioDL.getUsersFollowedForOrganisation(dbConnection, orgID);
            database.closeDatabase();
            return listOfUsers;
        }

        public static List<OrganizationDetails> getAllListofOrganisations()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<OrganizationDetails> listOfOrgs = UserPortfolioDL.getAllListofOrganisations(dbConnection);
            database.closeDatabase();
            return listOfOrgs;
        }

        public static List<OrganizationDetails> getListofOrganisationsBySearch(String name)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<OrganizationDetails> listOfOrgs = UserPortfolioDL.getListofOrganisationsBySearch(dbConnection, name);
            database.closeDatabase();
            return listOfOrgs;
        }

        public static List<UserComments> getCommentsForUser(String toUserID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserComments> listOfComments = UserPortfolioDL.getCommentsForUser(dbConnection, toUserID);
            database.closeDatabase();
            return listOfComments;
        }

        public static List<UserComments> getCommentsByUser(String fromUserID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserComments> listOfComments = UserPortfolioDL.getCommentsByUser(dbConnection, fromUserID);
            database.closeDatabase();
            return listOfComments;
        }

        public static String getCommentDatetimeByUserForUser(String fromUserID, String toUserID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String commentDatetime = UserPortfolioDL.getCommentDatetimeByUserForUser(dbConnection, fromUserID, toUserID);
            database.closeDatabase();
            return commentDatetime;
        }

        public static String getActivityIDfromComment(String commentID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.getActivityIDfromComment(dbConnection, commentID);
            database.closeDatabase();
            return result;
        }

        public static String addComments(String toUserID, String fromUserID, String comment)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.addComments(dbConnection, toUserID, fromUserID, comment);
            database.closeDatabase();
            return result;
        }

        public static String removeComment(String commentID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.removeComment(dbConnection, commentID);
            database.closeDatabase();
            return result;
        }

        /* NO LONGER FUNCTION
        public static List<UserAcitvities> getUserActivities(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            return UserPortfolioDL.getUserActivities(dbConnection, userID);
        }
        */

        public static List<OrganizationCommunityDetails> getListofOrganisationsWithFollow(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<OrganizationCommunityDetails> listOfOrgs = UserPortfolioDL.getListofOrganisationsWithFollow(dbConnection, userID);
            database.closeDatabase();
            return listOfOrgs;
        }

        public static List<OrganizationCommunityDetails> getListofOrganisationsWithFollowBySearch(String userID, String name)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<OrganizationCommunityDetails> listOfOrgs = UserPortfolioDL.getListofOrganisationsWithFollowBySearch(dbConnection, userID, name);
            database.closeDatabase();
            return listOfOrgs;
        }

        public static OrganizationCommunityDetails getOrganisationDetailsWithFollow(String userID, String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            OrganizationCommunityDetails org = UserPortfolioDL.getOrganisationDetailsWithFollow(dbConnection, userID, orgID);
            database.closeDatabase();
            return org;
        }

        public static OrganizationCommunityDetails getOrganisationDetailsWithFollow2(String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            OrganizationCommunityDetails org = UserPortfolioDL.getOrganisationDetailsWithFollow2(dbConnection, orgID);
            database.closeDatabase();
            return org;
        }

        public static Boolean userFollowOrganisation(String userID, String orgID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            Boolean result = UserPortfolioDL.userFollowOrganisation(dbConnection, userID, orgID);
            database.closeDatabase();
            return result;
        }

        public static String uploadImage(String userID, String imagePath)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = UserPortfolioDL.uploadImage(dbConnection, imagePath, userID);
            database.closeDatabase();
            return result;
        }

    }
}