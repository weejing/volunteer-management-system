﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Database;
using VMS.DataLayer.CommunityMgmt;
using VMS.Models.CommunityModels;
using VMS.Models.EventModels;
using VMS.Models.OrgMgmtModels;
using Facebook;
using Newtonsoft.Json;

namespace VMS.BusinessLayer.CommunityMgmt
{
    public class NewsfeedBL
    {
        public static String addUserActivity(String userID, String activityType, String targetUserID, String targetUserName, String eventID, String eventName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = NewsfeedDL.addUserActivity(dbConnection, userID, activityType, targetUserID, targetUserName, eventID, eventName);
            database.closeDatabase();
            return result;
        }

        public static String addUserActivityUser(String userID, String activityType, String targetUserID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = NewsfeedDL.addUserActivityUser(dbConnection, userID, activityType, targetUserID);
            database.closeDatabase();
            return result;
        }

        public static String addUserActivityEvent(String userID, String activityType, String eventID, String eventName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = NewsfeedDL.addUserActivityEvent(dbConnection, userID, activityType, eventID, eventName);
            database.closeDatabase();
            return result;
        }

        public static String updateActivityIDtoUserFriend(String userID, String activityType, String targetUserID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = NewsfeedDL.updateActivityIDtoUserFriend(dbConnection, userID, activityType, targetUserID);
            database.closeDatabase();
            return result;
        }

        public static String removeActivity(String activityID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = NewsfeedDL.removeActivity(dbConnection, activityID);
            database.closeDatabase();
            return result;
        }

        public static List<UserNewsfeed> getUserActivities(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();
            List<UserNewsfeed> listOfActivitiesFriends = NewsfeedDL.getUserActivitiesFriendsAdded(dbConnection, userID);
            List<UserNewsfeed> listOfActivitiesComments = NewsfeedDL.getUserActivitiesComments(dbConnection, userID);
            List<UserNewsfeed> listOfActivitiesOrgFollows = NewsfeedDL.getUserActivitiesOrgFollows(dbConnection, userID);
            List<UserEvent> listOfUserEvents = NewsfeedDL.getUserActivitiesEventsJoined(dbConnection, userID);
            database.closeDatabase();
            List<UserNewsfeed> listOfActivitiesEventsJoined = new List<UserNewsfeed>();

            Database_Connect database2 = new Database_Connect("CONSOLE_VMS");
            foreach (UserEvent userEvent in listOfUserEvents)
            {
                UserNewsfeed newFeed = new UserNewsfeed();
                newFeed.USR_ID = userEvent.USR_ID;
                newFeed.USR_FULL_NAME = userEvent.USR_FULL_NAME;
                newFeed.TARGET_USR_ID = userEvent.EVENT_ID;
                newFeed.USR_IMAGE = userEvent.USR_IMAGE;
                newFeed.TARGET_IMAGE = userEvent.USR_IMAGE;
                newFeed.EVENT_ID = "EventDetail.aspx?e=" + userEvent.EVENT_ID + "&o=" + userEvent.CONNECTION_STRING;
                newFeed.ACTIVITY_TYPE = "JOINEVENT";
                newFeed.ACTIVITY_DATETIME = System.DateTime.Now.ToString();
                newFeed.MESSAGE_DISPLAY = " has joined an event : ";
                newFeed.USR_IMAGE = userEvent.USR_IMAGE;

                database2 = new Database_Connect(userEvent.CONNECTION_STRING);
                SqlConnection dbConnection2 = database2.getConnection();

                EventDetails eventInfo = new EventDetails();
                eventInfo = NewsfeedDL.getActivitiesEventsJoinedDetails(dbConnection2, userEvent.EVENT_ID);
                newFeed.TARGET_NAME = eventInfo.eventName;

                listOfActivitiesEventsJoined.Add(newFeed);
            }
            database2.closeDatabase();


            foreach (var listResult in listOfActivitiesFriends)
            {
                listResult.EVENT_ID = "UserPortfolio.aspx?id=" + listResult.TARGET_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesComments)
            {
                listResult.EVENT_ID = "UserPortfolio.aspx?id=" + listResult.TARGET_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesOrgFollows)
            {
                listResult.EVENT_ID = "UserOrgFollows.aspx?id=" + listResult.TARGET_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesEventsJoined)
            {
                listOfActivities.Add(listResult);
            }

            // sort by date
            List<UserNewsfeed> listOfSortedActivities = listOfActivities.OrderByDescending(o => o.ACTIVITY_DATETIME).ToList();

            return listOfActivities;
        }


        public static List<UserNewsfeed> getUserFriendsActivities(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<UserNewsfeed> listOfActivities = new List<UserNewsfeed>();
            List<UserNewsfeed> listOfActivitiesFriends = NewsfeedDL.getFriendsActivitiesFriendsAdded(dbConnection, userID);
            List<UserNewsfeed> listOfActivitiesComments = NewsfeedDL.getFriendsActivitiesComments(dbConnection, userID);
            List<UserNewsfeed> listOfActivitiesOrgFollows = NewsfeedDL.getFriendsActivitiesOrgFollows(dbConnection, userID);
            List<UserEvent> listOfUserEvents = NewsfeedDL.getFriendsActivitiesEventsJoined(dbConnection, userID);
            database.closeDatabase();
            List<UserNewsfeed> listOfActivitiesEventsJoined = new List<UserNewsfeed>();

            Database_Connect database2 = new Database_Connect("CONSOLE_VMS");
            foreach (UserEvent userEvent in listOfUserEvents)
            {
                UserNewsfeed newFeed = new UserNewsfeed();
                newFeed.USR_ID = userEvent.USR_ID;
                newFeed.USR_FULL_NAME = userEvent.USR_FULL_NAME;
                newFeed.TARGET_USR_ID = userEvent.EVENT_ID;
                newFeed.USR_IMAGE = userEvent.USR_IMAGE;
                newFeed.TARGET_IMAGE = userEvent.USR_IMAGE;
                newFeed.EVENT_ID = "EventDetail.aspx?e=" + userEvent.EVENT_ID + "&o=" + userEvent.CONNECTION_STRING;
                newFeed.ACTIVITY_TYPE = "JOINEVENT";
                newFeed.ACTIVITY_DATETIME = System.DateTime.Now.ToString();
                newFeed.MESSAGE_DISPLAY = " has joined an event : ";
                newFeed.USR_IMAGE = userEvent.USR_IMAGE;

                database2 = new Database_Connect(userEvent.CONNECTION_STRING);
                SqlConnection dbConnection2 = database2.getConnection();

                EventDetails eventInfo = new EventDetails();
                eventInfo = NewsfeedDL.getActivitiesEventsJoinedDetails(dbConnection2, userEvent.EVENT_ID);
                newFeed.TARGET_NAME = eventInfo.eventName;

                listOfActivitiesEventsJoined.Add(newFeed);
            }
            database2.closeDatabase();

            foreach (var listResult in listOfActivitiesFriends)
            {
                listResult.EVENT_ID = "UserPortfolio.aspx?id=" + listResult.TARGET_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesComments)
            {
                listResult.EVENT_ID = "UserPortfolio.aspx?id=" + listResult.TARGET_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesOrgFollows)
            {
                listResult.EVENT_ID = "UserOrgFollows.aspx?id=" + listResult.USR_ID;
                listOfActivities.Add(listResult);
            }

            foreach (var listResult in listOfActivitiesEventsJoined)
            {
                listOfActivities.Add(listResult);
            }

            // sort by date
            List<UserNewsfeed> listOfSortedActivities = listOfActivities.OrderByDescending(o => o.ACTIVITY_DATETIME).ToList();

            return listOfSortedActivities;
        }

        public static List<UserPortfolioRecommendationModel> getListOfRecommendedUsersByNetwork(String userID)
        {
            List<UserPortfolioRecommendationModel> listOfRecommendedUsers = new List<UserPortfolioRecommendationModel>();
            List<UserPortfolioRecommendationModel> listOfRecommendedUsersGrouped = new List<UserPortfolioRecommendationModel>();
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<UserPortfolio> listOfFriends = UserPortfolioDL.getUserFriends(dbConnection, userID);

            foreach (UserPortfolio friend in listOfFriends)
            {
                List<UserPortfolio> tempList = NewsfeedDL.getUserFriendsOfFriends(dbConnection, userID, friend.USR_ID);
                foreach (UserPortfolio user in tempList)
                {
                    UserPortfolioRecommendationModel userR = new UserPortfolioRecommendationModel();
                    userR.USR_ID = user.USR_ID;
                    userR.FULL_NAME = user.FULL_NAME;
                    userR.IMAGE = user.IMAGE;
                    listOfRecommendedUsers.Add(userR);
                }
            }

            var newGroup = listOfRecommendedUsers.GroupBy(u => u.USR_ID).Select(grp => grp.ToList()).ToList();
            foreach (var userGroped in newGroup)
            {
                userGroped[0].RECOMMENDATION_FROM = "Recommendation : You have mutual friends on SGServe.";
                userGroped[0].RECOMMENDATION_PRIORITY = userGroped.Count();
                listOfRecommendedUsersGrouped.Add(userGroped[0]);
            }

            database.closeDatabase();
            return listOfRecommendedUsersGrouped;
        }

        public static List<UserPortfolioRecommendationModel> getListOfRecommendedUsersByFacebook(String userID, String facebookID, String accessToken)
        {
            List<UserPortfolioRecommendationModel> listOfRecommendedUsers = new List<UserPortfolioRecommendationModel>();
            List<UserPortfolioRecommendationModel> listOfRecommendedUsersGrouped = new List<UserPortfolioRecommendationModel>();
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            FBFriends friends = new FBFriends();
            var client = new FacebookClient(accessToken);
            dynamic fbresult = client.Get("me/friends");
            var data = fbresult["data"].ToString();
            friends.friendsListing = JsonConvert.DeserializeObject<List<FBFriend>>(data);

            foreach (FBFriend friend in friends.friendsListing)
            {
                List<UserPortfolioRecommendationModel> list = NewsfeedDL.getUserFacebookFriendsRecommend(dbConnection, userID, friend.id);
                if (list.Count > 0)
                {
                    list[0].RECOMMENDATION_FROM = "Recommendation : Both of you are friends on Facebook.";
                    listOfRecommendedUsersGrouped.Add(list[0]);
                }
            }
            
            database.closeDatabase();
            return listOfRecommendedUsersGrouped;
        }


        public static List<UserPortfolio> getTopUsersVIA(int size)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<UserPortfolio> listOfUsers = NewsfeedDL.getTopUsersVIA(dbConnection, size);
            database.closeDatabase();
            return listOfUsers;
        }


        public static String getUserFacebookID(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String userFacebookID = NewsfeedDL.getUserFacebookID(dbConnection, userID);
            database.closeDatabase();
            return userFacebookID;
        }

    }
}