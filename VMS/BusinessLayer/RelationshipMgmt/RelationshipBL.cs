﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.RelationshipMgmt;

namespace VMS.BusinessLayer.RelationshipMgmt
{
    public class RelationshipBL
    {
        //to add in nottifications later on
        // <summary>BBL: update discount stautus </summary>
        public static void discountAction()
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                //update crm discount status to expired
                RelationshipDL.updateCRMStatus(dbConnection);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: displayRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

            }

        }
    }
}