﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using VMS.Database;
using VMS.Models.EventModels;
using VMS.Models.HrModels;
using VMS.Models.AccountModels;
using System.Net.Mail;
using System.Net;
using VMS.BusinessLayer.EventMgmt;

namespace VMS.EventMgmt
{
    public class EventMgmtBL
    {
        public static void createEvent(EventDetails eventDetails, String organiserID, String connectionString)
        {
            eventDetails.eventId = Guid.NewGuid().ToString();
            string newlygeneratedlink = generateUniqueLink(eventDetails.eventId, connectionString);
            eventDetails.generateNewLink = newlygeneratedlink;

            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection databaseConnection = databaseConfig.getConnection();            
            EventMgmtDL.insertEvent(databaseConnection, eventDetails, organiserID);
            
            foreach(SessionDetails session in eventDetails.listOfSession)
            {
                EventMgmtDL.insertEventSession(databaseConnection, eventDetails.eventId, session);
            }
            databaseConfig.closeDatabase();

            //create thread to perform event matching
           Thread eventMatching = new Thread(delegate ()
            {
                matchVolunteers(eventDetails, connectionString);
            });
            eventMatching.Start();

        }

        /*
         *  This methods retrieves the sessionRole traits and skillsets first before sending to the event matching
         */
        private static void matchVolunteers(EventDetails eventDetails, String connectionString)
        {
            EventMatchingBL.matchVolunteers(eventDetails, connectionString);
        }




        /*
        * This method gnerates a unique link which is tag to an event
        */
        public static String generateUniqueLink(String eventId, String connectionString)
        {
            //generate unique url
            string urlpagename = "http://sgserveportal.azurewebsites.net/EventDetail?e=" + eventId +"&o="+connectionString;
            GenerateUniqueURL generateURL = new GenerateUniqueURL();
            string newlygeneratedlink = generateURL.getShortenURL(urlpagename);
            return newlygeneratedlink;
        }
        
        /*
         * This method deletes an entire event detail 
         */
        public static void deleteEventDetails(String eventId, String connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<String> listOfVolId = EventMgmtDL.getVolIdFromEvent(dataConnection, eventId);
            EventMgmtDL.deleteEvent(dataConnection, eventId);
            databaseConfig.closeDatabase();

            databaseConfig = new Database_Connect("CONSOLE_VMS");
            dataConnection = databaseConfig.getConnection();

            SearchExploreDL.deleteEvent(dataConnection, eventId, connectionString);
           
            if (listOfVolId.Count != 0)
            {
                List<String> listOfEmail = EventMgmtDL.getvolEvent(dataConnection, listOfVolId);
                foreach (string email in listOfEmail)
                {
                    sendApologyEmail(email, connectionString);
                }
            }
               
            databaseConfig.closeDatabase();                                  
        }

        /*
         * This method sends an apology email to volunteers who have an event deleted 
         */
        private static void sendApologyEmail(String email , String connectionString)
        {
            using (MailMessage message = new MailMessage("sgserve8mc@gmail.com", email))
            {
                message.Subject = connectionString + " - Event cancel";
                string body = "Hello ,";
                body += "<br /><br />We would like to sincerly apologise.";
                message.Body = body;
                message.IsBodyHtml = true;


                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("sgserve8mc@gmail.com", "8mcdjjshwj");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(message);
            }
        }


        /*
         *  This method retrieve a list of events that are created or managed by an organiser
         */
        public static List<EventDetails> retrieveOrganiserEvents(String organiserId, String connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<EventDetails> listOfEventDetails = EventMgmtDL.retrieveOrganiserEvents(dataConnection, organiserId);

            databaseConfig.closeDatabase();
            return listOfEventDetails;
        }

        /*
         *  This method returns a full event details for an organiser
         */
        public static EventDetails retrieveOrgEventSessionDetails(String connectionString, String eventId, Boolean getQuantity)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();           
            List<String> listOfEventRole = EventMgmtDL.getListOfEventRoleName(dataConnection);
            databaseConfig.closeDatabase();


            databaseConfig = new Database_Connect(connectionString);
            dataConnection = databaseConfig.getConnection();

            EventDetails eventDetails = EventMgmtDL.getEvent(dataConnection,eventId);
            //here
            eventDetails.listofCauseName = EventCauseBL.getEvtCauseNames(connectionString, eventId);
            eventDetails.listOfSession = EventMgmtDL.getListOfSessions(dataConnection, eventId, getQuantity, listOfEventRole);
            databaseConfig.closeDatabase();
            
            return eventDetails;
        }

        /*
         * Get the volunteer details of all volunteers that joined an organiser evnet for a specifc role 
         */
        public static List<Volunteer> getListOfVolunteers(String connectionString, String sessionDayId, String evtRoleId)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();
            List<String> listOfVolunteerId = EventMgmtDL.getListofVolId(dataConnection, sessionDayId, evtRoleId);
            databaseConfig.closeDatabase();

            databaseConfig = new Database_Connect("CONSOLE_VMS");
            dataConnection = databaseConfig.getConnection();
            List<Volunteer> listOfVolunteer = new List<Volunteer>();
            if (listOfVolunteerId.Count !=0) {
                listOfVolunteer = EventMgmtDL.getJoinedVolunterDetails(dataConnection, listOfVolunteerId);
            }
            
            databaseConfig.closeDatabase();
            return listOfVolunteer;
        }

        
        /*
         * This method returns a list of event roles for a particular orgnisation 
         */
        public static List<SessionRoles> getMasterListOfEventRoles(String connectionString)
        {
            Database_Connect databaseConifg = new Database_Connect(connectionString);
            SqlConnection dbConnection = databaseConifg.getConnection();

            List<SessionRoles> listOfOrgEventRoles = EventMgmtDL.getListOfEventRolesFromOrg(dbConnection);
            databaseConifg.closeDatabase();


            databaseConifg = new Database_Connect("CONSOLE_VMS");
            dbConnection = databaseConifg.getConnection();

            List<SessionRoles> listOfEventRoles = EventMgmtDL.getListOfEventRoles(dbConnection, listOfOrgEventRoles);
            System.Diagnostics.Debug.WriteLine("numOfEvnetROles: " + listOfEventRoles.Count);     
            databaseConifg.closeDatabase();

            return listOfEventRoles;

        }

        public static SessionDetails getSessionBySessDay(string connectionString,String sessionDayId) {
            Database_Connect databaseConifg = new Database_Connect(connectionString);
            SqlConnection dbConnection = databaseConifg.getConnection();
            SessionDetails session = EventMgmtDL.getSessionBySessDay(dbConnection, sessionDayId);
            databaseConifg.closeDatabase();
            return session;
        }
    }
}