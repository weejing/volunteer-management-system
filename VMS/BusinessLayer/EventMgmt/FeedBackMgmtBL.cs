﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.EventModels;
using VMS.Database;
using System.Data.SqlClient;
using VMS.DataLayer.EventMgmt;
using System.Net.Mail;
using System.Net;

namespace VMS.EventMgmt
{
    public class FeedBackMgmtBL
    {
        
        /*
         * This method creates a feedback and insert it into the database 
         */
        public static void createFeedBackDesign(List<FeedBackDesign> listOfFeedBackDetails, String connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

            FeedBackMgmtDL.deleteFeedBackDesign(dataConnection, listOfFeedBackDetails[0].sessionId);
            FeedBackMgmtDL.createFeedBackDesign(dataConnection, listOfFeedBackDetails);
            databaseConfig.closeDatabase();
        }

        /*
         *  This method returns all the feedback design that was previously created by an event organsier
         */
        public static List<FeedBackDesign> getFeedBackDesign(String connectionString, String sessionId)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<FeedBackDesign> listOfFeedBackDesign = FeedBackMgmtDL.getFeedBackDesign(dataConnection, sessionId);
            databaseConfig.closeDatabase();

            return listOfFeedBackDesign;
        }

        /*
         * This method deletes a feedback design saved in the database 
         */
        public static void deleteFeedBack(String connectionString, String sessionId)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

            FeedBackMgmtDL.deleteFeedBackDesign(dataConnection, sessionId);
            databaseConfig.closeDatabase();
        }

        /*
         * This method get the feedback response of an event 
         */
        public static void getFeedBackResponse(String connectionString, String sessionId)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();

        }


        /*
         * This method inserts the response to an event session feedback 
         */
        public static void createFeedBackResponse(FeedBackResponse response)
        {
            Database_Connect databaseConifg = new Database_Connect(response.connectionString);
            SqlConnection dataConnection = databaseConifg.getConnection();

            List<String> listOfFeedBackId = FeedBackMgmtDL.getFeedBackId(dataConnection, response.sessionId);
            FeedBackMgmtDL.createFeedbackResponse(dataConnection, response, listOfFeedBackId);
            
            databaseConifg.closeDatabase();
        }

        /*
         * This method is a time triggered event that checks if an event session is over 
         */
        public static void sendFeedBack()
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<String> listOfConnectionString = SearchExploreDL.retrieveConnectionString(dataConnection);
            databaseConfig.closeDatabase();
            DateTime currentDate = DateTime.Now;
            foreach (String connectionString in listOfConnectionString)
            {
                databaseConfig = new Database_Connect(connectionString);
                dataConnection = databaseConfig.getConnection();

                List<String> listOfSessionId = FeedBackMgmtDL.getEventSessionReadyForFeedback(dataConnection, currentDate);
                databaseConfig.closeDatabase();
                if (listOfSessionId.Count == 0)                
                    continue;

                foreach(String sessionId in listOfSessionId)
                {
                    databaseConfig = new Database_Connect(connectionString);
                    dataConnection = databaseConfig.getConnection();

                    List<String> listOfVolId = EventMgmtDL.getVolIdFromSession(dataConnection, sessionId);
                    databaseConfig.closeDatabase();

                    foreach(String volId in listOfVolId)
                    {
                        databaseConfig = new Database_Connect("CONSOLE_VMS");
                        dataConnection = databaseConfig.getConnection();

                        String email = FeedBackMgmtDL.getVolEmail(dataConnection, volId);
                        sendFeedBackEmail(volId, email, sessionId, connectionString);
                    }
                }
            }
        }

        /*
         * This method sends the link for the feedback to its volunteer 
         */
        private static void sendFeedBackEmail(String volId, String email, String sessionId, String connectionString)
        {
            using (MailMessage message = new MailMessage("sgserve8mc@gmail.com", email))
            {
                String url = "http://localhost:53197/EventFeedBack?v=" + volId + "&s=" + sessionId + "&o=" + connectionString;
                message.Subject = connectionString + " - Event feedback";
                string body = "Hello ,";
                body += "<br /><br />We would like to kindly request for your participation in our event feedback.";
                body += "<br />Please click <a href = " + url + ">here</a> to access the feedback form";
                body += "<br /><br />Thanks";
                message.Body = body;
                message.IsBodyHtml = true;


                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("sgserve8mc@gmail.com", "8mcdjjshwj");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(message);
            }
        }

        //shihui start
        public static SessionDetails getSessionDetails(String connectionString, string sessionid)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();
            SessionDetails session= FeedBackMgmtDL.getSessionDetails(dataConnection, sessionid);
            databaseConfig.closeDatabase();
            return session;
        }

        public static List<FeedBackDesign> getFdbackOverallResp(String connectionString, string sessionid)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();
            //get feedback list of questions
            //for loop each question and store the ans
            List<FeedBackDesign> overallRespList = getFeedBackDesign(connectionString, sessionid);
            foreach (FeedBackDesign ques in overallRespList)
            {
                if (ques.type==1) {
                    //text
                   ques.listOfTextAns= FeedBackMgmtDL.getTextResp(dataConnection, ques.fdback_detail_id);
                } else {
                    //rating
                   List<RatingCount> count= FeedBackMgmtDL.getRatingCount(dataConnection, ques.fdback_detail_id);
                    ques.ratingCountList = count;
                }
            }

             databaseConfig.closeDatabase();
            return overallRespList;
        }

    }       
}