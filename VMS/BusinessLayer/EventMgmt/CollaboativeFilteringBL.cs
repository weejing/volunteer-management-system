﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using VMS.Database;
using VMS.Models.EventModels;

namespace VMS.EventMgmt
{
    public class CollaboativeFilteringBL
    {
        /*
         *  This method perfrom a collaborative filtering matching on volunteers
         *  It will first determine how similiar is a volunteer against its other peers 
         *  Upon the completion of that simliarity check, 
         */
        public static List<List<EventDetails>> collaboativeFiltering(String usrId)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<String> listOfOrganisation = SearchExploreDL.retrieveConnectionString(dataConnection);
            List<String> listOfOrgName = SearchExploreDL.getOrgName(dataConnection);

            int numOfInterest = CollaboativeFilteringDL.getNumberOfInterest(dataConnection, usrId);
            List<String> listOfSimiliarVolId = CollaboativeFilteringDL.getSimiliarVolunteer(dataConnection, usrId, numOfInterest);
            databaseConfig.closeDatabase();

            List<List<EventDetails>> listOfFilterdEvents = new List<List<EventDetails>>();
            List<Thread> listOfThreads = new List<Thread>();

            Semaphore mutex = new Semaphore(1, 1);
            int count = 0;
            foreach (String connectionString in listOfOrganisation)
            {
                Thread thread = new Thread(delegate () {
                    collaboativeFilteringWorker(connectionString, listOfSimiliarVolId, usrId, mutex, listOfFilterdEvents, listOfOrgName[count]);
                });
                thread.Start();
                listOfThreads.Add(thread);
                System.Diagnostics.Debug.WriteLine("count: " + count);
                count++;
            }

            foreach (Thread thread in listOfThreads)
                thread.Join();

            return listOfFilterdEvents;
                               
        }

        /*
         * This worker thread will access one of the the organisation database and determine which event requires collabrative filtering 
         */
        public static void collaboativeFilteringWorker(String connectionString, List<String> listOfVolId, string usrId, Semaphore mutex, List<List<EventDetails>> listOfFilterdEvents, String orgName)
        {
           
            try
            {
                Database_Connect databaseConfig = new Database_Connect(connectionString);
                SqlConnection dataConnection = databaseConfig.getConnection();

                List<String> listOfJoinedEventId = CollaboativeFilteringDL.getJoinedEventId(dataConnection, usrId);
                List<EventDetails> listOfEventDetails = CollaboativeFilteringDL.getFilterEvents(dataConnection, listOfVolId, usrId, connectionString, listOfJoinedEventId, orgName);
                databaseConfig.closeDatabase();

                if (listOfEventDetails.Count >= 1)
                {
                    mutex.WaitOne();
                    listOfFilterdEvents.Add(listOfEventDetails);
                    mutex.Release();
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("its calling here?");
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
            }
                            
        }

    }
}