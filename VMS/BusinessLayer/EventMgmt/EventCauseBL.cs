﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.EventMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.EventMgmt
{
    public class EventCauseBL
    {
        public static List<Term> getEvtCauseNames(String connectionString, string evt_id)
        {
            //get cause_id from npo database
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection dataConnection = databaseConfig.getConnection();
            List<int> termList = EventCauseDL.getRatingCount(dataConnection, evt_id);
            databaseConfig.closeDatabase();

            //get cause name from console_vms
            Database_Connect databaseConfig2 = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection2 = databaseConfig2.getConnection();
            List<Term> causeNameList= EventCauseDL.getCauseName(dataConnection2, termList);

            databaseConfig2.closeDatabase();
            return causeNameList;
        }


        }
}