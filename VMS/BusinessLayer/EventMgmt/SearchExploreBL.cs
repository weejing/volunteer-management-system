﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using VMS.AccountMgmt;
using VMS.BusinessLayer.AdminMgmt;
using VMS.BusinessLayer.EventMgmt;
using VMS.BusinessLayer.VolBiddingMgmt;
using VMS.Database;
using VMS.Models.AdminModels;
using VMS.Models.EventModels;

namespace VMS.EventMgmt
{
    public class SearchExploreBL
    {
        /*
        *  This method allows a volunteer to voluntary register for an event
        */
        public static void volRegisterEvent(SignUpEvent signUp, string userId)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();
            System.Diagnostics.Debug.WriteLine("connectionString: " + signUp.connectionString);
            Boolean exist = SearchExploreDL.checkIfVolJoinBefore(dataConnection, signUp, userId);

            String[] dates = signUp.eventdateList.Split(',');


            if (exist == false)
                SearchExploreDL.registerEventInVol(dataConnection, signUp, userId);
            else            
                SearchExploreDL.registerEventSessionInVol(dataConnection,dates , signUp);
            
            databaseConfig.closeDatabase();

            databaseConfig = new Database_Connect(signUp.connectionString);
            dataConnection = databaseConfig.getConnection();
            List<String> listOfSessionDayId = SearchExploreDL.getListOfSessionDayId(dataConnection,dates,signUp);
            SearchExploreDL.registerEventInOrg(dataConnection, signUp, userId, listOfSessionDayId);
            databaseConfig.closeDatabase();
        }

        /*
         * This method would be called when volunteer register for an invited event 
         */
        public static void volRegisterInvitedEvent(SignUpEvent signUp, string userId)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            SearchExploreDL.registerEventInVol(dataConnection, signUp, userId);
            SearchExploreDL.deleteEventNotification(dataConnection, signUp.eventId, userId);

            databaseConfig.closeDatabase();

            String[] dates = signUp.eventdateList.Split(',');

            databaseConfig = new Database_Connect(signUp.connectionString);
            dataConnection = databaseConfig.getConnection();
            List<String> listOfSessionDayId = SearchExploreDL.getListOfSessionDayId(dataConnection, dates, signUp);
            SearchExploreDL.registerEventInOrg(dataConnection, signUp, userId, listOfSessionDayId);
            databaseConfig.closeDatabase();
        }


        /*
         * This method retrieves details of a selected event when view by a user who does not have an existing account in the VMS     
         * (1) Event sessions that have passed, or have full capcity count will not be displayed         
         */
        public static VolEventDetail getEventDetailsForPublic(string eventId, string connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();
            List<String> listOfEventRoles = EventMgmtDL.getListOfEventRoleName(dataConnection);
            databaseConfig.closeDatabase();

            databaseConfig = new Database_Connect(connectionString);
            dataConnection = databaseConfig.getConnection();
            VolEventDetail eventDetail = SearchExploreDL.getVolEventDetails(dataConnection, eventId, listOfEventRoles);
            //shihui added here
            eventDetail.listofCauseName = EventCauseBL.getEvtCauseNames(connectionString, eventId);
            List<String> listOfSessionId = new List<string>();
            SearchExploreDL.getNotJoinedSessions(dataConnection, eventDetail, DateTime.Now, listOfSessionId);

            return eventDetail;
        }


        /*
         * This method retrieves details of a selected events for a volunteer 
         * It also includes the following
         * (1) Event sessions that are joined by a volunteer 
         * (2) Event sessions that have passed, or have full capcity count will not be displayed
         * (3) Event session in which a volunteer is unable to attend would not be displayed          
         */
        public static VolEventDetail retrieveEventDetailForVol(string userid, string eventId, string connectionString)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();
            List<String> listOfEventRoles = EventMgmtDL.getListOfEventRoleName(dataConnection);
            List<DateTime> listOfDates = SearchExploreDL.getVolunteerSchedule(dataConnection, userid);
            databaseConfig.closeDatabase();

            databaseConfig = new Database_Connect(connectionString);
            dataConnection = databaseConfig.getConnection();
            VolEventDetail eventDetail = SearchExploreDL.getVolEventDetails(dataConnection, eventId, listOfEventRoles);
            //shihui added here
            eventDetail.listofCauseName = EventCauseBL.getEvtCauseNames(connectionString, eventId);
            List<String> listOfSessionId = SearchExploreDL.getJoinedSessions(dataConnection, userid, eventId, eventDetail);
            SearchExploreDL.getNotJoinedSessions(dataConnection, eventDetail, DateTime.Now, listOfSessionId);                                   
            databaseConfig.closeDatabase();

            foreach(VolSessionDetails session in eventDetail.listOfSession)
            {
                int count = 0;
                if(session.listOfUnjoinedDays != null)
                {
                    foreach(SessionDays day in session.listOfUnjoinedDays.ToList())
                    {
                       Boolean free = EventMatchingBL.checkSpecificDate(day,listOfDates);

                        if (free == false)
                            session.listOfUnjoinedDays.Remove(day);
                        count++;                 
                    }
                }              
            }
            return eventDetail;
        }


        /*
        *  This method retireve a list of events that are created or managed by an organiser
        *  and display it to the public view in a list form      
        */
        public static List<List<EventDetails>> retrieveEventsForPublic()
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<string> listOfConnectionString = SearchExploreDL.retrieveConnectionString(dataConnection);
            List<String> listOfOrgName = SearchExploreDL.getOrgName(dataConnection);
            databaseConfig.closeDatabase();


            List<List<EventDetails>> listofEventDetails = new List<List<EventDetails>>();
            List<Thread> listOfThreads = new List<Thread>();
            Semaphore mutex = new Semaphore(1, 1);

            int count = 0;
            foreach (string connectionString in listOfConnectionString)
            {
                Thread thread = new Thread(delegate ()
                {
                    getListOfEventsWorkerThread(connectionString, listofEventDetails, mutex, listOfOrgName[count]);
                });
                thread.Start();
                listOfThreads.Add(thread);
                System.Diagnostics.Debug.WriteLine("count:" + count);
                count++;
            }
            foreach (Thread thread in listOfThreads)
                thread.Join();

            return listofEventDetails;
        }

        public static void getListOfEventsWorkerThread(String connectionString, List<List<EventDetails>> categorisedEventDetails, Semaphore mutex, String orgName)
        {
            try
            {
                Database_Connect databaseConfig = new Database_Connect(connectionString);
                SqlConnection dataConnection = databaseConfig.getConnection();

                List<EventDetails> listOfEvents = SearchExploreDL.retrieveEventsList(dataConnection, connectionString, orgName);
                databaseConfig.closeDatabase();

                if (listOfEvents.Count > 0)
                {
                    // write to shared List
                    mutex.WaitOne();
                    categorisedEventDetails.Add(listOfEvents);
                    mutex.Release();
                }               
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(connectionString + "does not exist");
            }
        }

      
        /*
         *  this method gets a list of event joined by the user
         */
        public static List<List<EventDetails>> retrieveListOfJoinEvents(String userId)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<KeyValuePair<String, List<String>>> listOfMyEvents = SearchExploreDL.getMyEvents(dataConnection, userId);
            databaseConfig.closeDatabase();

            List<List<EventDetails>> listofEventDetails = new List<List<EventDetails>>();
            List<Thread> listOfThreads = new List<Thread>();
            Semaphore mutex = new Semaphore(1, 1);
            foreach (KeyValuePair<String, List<String>> myevent in listOfMyEvents)
            {
                Thread thread = new Thread(delegate () 
                {
                    getListOfJoinEventsWorkerThread(myevent.Key, myevent.Value, listofEventDetails, mutex);
                });
                thread.Start();
                listOfThreads.Add(thread);
            }
            foreach (Thread thread in listOfThreads)
                thread.Join();
            return listofEventDetails;
        }

        private static void getListOfJoinEventsWorkerThread(string connectionString, List<String> listOfEventId, List<List<EventDetails>> categorisedEventDetails, Semaphore mutex)
        {
            try
            {
                Database_Connect databaseConifg = new Database_Connect(connectionString);
                SqlConnection dataConnection = databaseConifg.getConnection();

                List<EventDetails> listOfEventDetails = SearchExploreDL.getJoinEventDetails(dataConnection, connectionString, listOfEventId);
                databaseConifg.closeDatabase();

                if (listOfEventDetails.Count >= 1)
                {
                    // write to shared List
                    mutex.WaitOne();
                    categorisedEventDetails.Add(listOfEventDetails);
                    mutex.Release();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(connectionString + "does not exist");
            }
        }

        /*
         *  This method returns a list of event based on the searched parameter
         */
        public static List<List<EventDetails>> searchEvents(SearchEvent searchParams)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<string> listOfConnectionString = SearchExploreDL.retrieveConnectionString(dataConnection);
            List<String> listOfOrgName = SearchExploreDL.getOrgName(dataConnection);
            databaseConfig.closeDatabase();

            List<List<EventDetails>> listOfSearchedEvents = new List<List<EventDetails>>();
            List<Thread> listOfThreads = new List<Thread>();

            Semaphore mutex = new Semaphore(1, 1);

            int count = 0;
            foreach (string connectionString in listOfConnectionString)
            {
                Thread thread = new Thread(delegate ()
               {
                   searchEventWokerThread(connectionString, searchParams, mutex, listOfSearchedEvents, listOfOrgName[count]);
               });
                thread.Start();
                listOfThreads.Add(thread);
                count++;
            }

            foreach (Thread thread in listOfThreads)
                thread.Join();
                 
            return listOfSearchedEvents;
        }

        /*
         * This worker thread focus on searching the one database for the required event results
         */
        private static void searchEventWokerThread(String connectionString, SearchEvent searchParams, Semaphore mutex, List<List<EventDetails>> listOfSearchEvents, String orgName)
        {            
            try
            {
                Database_Connect databaseConfig = new Database_Connect(connectionString);
                SqlConnection dataConnection = databaseConfig.getConnection();
                List<EventDetails> listOfEventDetails = SearchExploreDL.searchEvents(dataConnection, connectionString, searchParams, orgName);
                databaseConfig.closeDatabase();

                if (listOfEventDetails.Count >= 1)
                {
                    mutex.WaitOne();
                    listOfSearchEvents.Add(listOfEventDetails);
                    mutex.Release();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(connectionString + "does not exist");
            }           
        }

     
        /*
         *  This method returns an event in which a volunteer is invited to 
         */
        public static EventDetails getInvitedEvent(String eventId, String volId)
        {
            Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConfig.getConnection();

            List<AcceptedSession> listOfAcceptedSession = SearchExploreDL.getInvitedEvent(dataConnection, volId, eventId);
            List<String> listOfEventRoles = EventMgmtDL.getListOfEventRoleName(dataConnection);
            databaseConfig.closeDatabase();

            EventDetails eventDetails = new EventDetails { listOfSession = new List<SessionDetails>()};
            eventDetails.connectionString = listOfAcceptedSession[0].connectionString;
            eventDetails.eventId = eventId;

            databaseConfig = new Database_Connect(eventDetails.connectionString);
            dataConnection = databaseConfig.getConnection();

            foreach (AcceptedSession acceptedSession in listOfAcceptedSession)
            {
                SessionDetails sessionDetail = new SessionDetails { sessionId = acceptedSession.sessionId};

                sessionDetail.listOfSessionDays = new List<SessionDays>();
                String[] dates = acceptedSession.sessionDates.Split(',');

                int dateCount = 1;
                foreach(String date in dates)
                {  
                    if(dateCount != dates.Length)
                    {
                        System.Diagnostics.Debug.WriteLine("date: " + date);
                        SessionDays day = new SessionDays { evt_date = DateTime.ParseExact(date, "d/MM/yyyy", null) };
                        sessionDetail.listOfSessionDays.Add(day);                       
                    }                                                          
                    dateCount++;                                       
                }

                String sessionRole = acceptedSession.sessionRoles.Substring(0, acceptedSession.sessionRoles.Length - 1);
                String roleWeight = acceptedSession.percent.Substring(0, acceptedSession.percent.Length - 1);

                System.Diagnostics.Debug.WriteLine("sessionRole: " + sessionRole);

                int[] roles = Array.ConvertAll(sessionRole.Split(','), int.Parse);
                String[] tempArray = roleWeight.Split(',');
                tempArray = tempArray.Take(tempArray.Count() - 1).ToArray();
                int[] weights = Array.ConvertAll(tempArray, int.Parse);

                sessionDetail.listOfSessionRoles = new List<SessionRoles>();
                SearchExploreDL.getInvitedRoleNames(dataConnection, roles, sessionDetail, listOfEventRoles, weights);
                eventDetails.listOfSession.Add(sessionDetail);
            }

           
            SearchExploreDL.getInvitedEventDetails(dataConnection, eventDetails);
            databaseConfig.closeDatabase();
            return eventDetails;                  
        }


        public static void getFeedbackResponse()
        {

        }




        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CheckIn /Check out >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        /*
         * This method retrieves a event sesison in which a  volunteer is suppose to attend. 
         * This return value would be use for verfication checking to ensure that a volunteer is indded checking in 
         */
        public static CheckIn getTodayEvent(String userId)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            CheckIn checkIn = SearchExploreDL.getTodayEvent(dbConnection, userId, DateTime.Now);
            database.closeDatabase();

            database = new Database_Connect(checkIn.connectionString);
            dbConnection = database.getConnection();

            SearchExploreDL.getTodayEventLocation(dbConnection, checkIn);

            return checkIn;
        }

        /*
         *  This method checks in the volunteer at the current time
         */
        public static void checkInVol(CheckIn checkIn, string userId)
        {
            Database_Connect database = new Database_Connect(checkIn.connectionString);
            SqlConnection dbConnection = database.getConnection();

            SearchExploreDL.checkInVol(dbConnection, checkIn, DateTime.Now, userId);
            database.closeDatabase();
        }

      /*
         *  This method checks out a volunteer at the current time
         */
        public static List<Badge> checkOutVol(String userId)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            //get today event details
            CheckIn checkIn = SearchExploreDL.getTodayEvent(dbConnection, userId, DateTime.Now);
            database.closeDatabase();

            System.Diagnostics.Debug.WriteLine(checkIn.sessionDayId);
            //check out code
            Database_Connect database2 = new Database_Connect(checkIn.connectionString);
            SqlConnection dbConnection2 = database2.getConnection();
            SearchExploreDL.getTodayEventLocation(dbConnection2, checkIn);
            SearchExploreDL.checkOutVol(dbConnection2, checkIn, DateTime.Now.AddHours(6), userId);
            database2.closeDatabase();

            //get hour done
            Database_Connect database3 = new Database_Connect(checkIn.connectionString);
            SqlConnection dbConnection3 = database3.getConnection();
            int eventhour = AccountMgmtDL.getEventVIA(dbConnection3, checkIn.sessionDayId, userId);
            database3.closeDatabase();

            //get badges
            List<Badge> badges = VolBadgeBL.issueBadges(userId);

            //update volunteers VIA hours
            BiddingTransactionBL.updateVolVIAhours(eventhour, userId);

            //add points for complet event
            BiddingTransactionBL.addPointsForCompleteEvent(userId, eventhour, checkIn.sessionId);

            return badges;
        }




        /*
          * This is a method by kf , to  be use for SaaS home page, to retrieve list of recent event by organisation
          */
        public static List<EventDetails> retrieveRecentEventsListByOrg(String connectionName)
        {
            Database_Connect database = new Database_Connect(connectionName);
            SqlConnection dbConnection = database.getConnection();
            List<EventDetails> listOfEvents = SearchExploreDL.retrieveRecentEventsListByOrg(dbConnection);
            database.closeDatabase();
            return listOfEvents;
        }



    }
}