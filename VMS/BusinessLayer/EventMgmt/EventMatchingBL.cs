﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Models.EventModels;
using VMS.Database;
using System.Data.SqlClient;
using System.Threading;
using System.Net.Mail;
using System.Net;

namespace VMS.EventMgmt
{
    public class EventMatchingBL
    {
        /*
         * This method is triggered upon the succesful creation of an event. 
         * This method will actively search for volunteers who are a perfect match for the event.
         */
        public static void matchVolunteers(EventDetails eventDetails, String connectionString)
        {
            Database_Connect databseConfig = new Database_Connect("CONSOLE_VMS");
            SqlConnection databaseConnection = databseConfig.getConnection();

            String orgId = EventMatchingDL.getOrgId(databaseConnection, connectionString);

            // first get a list of volunteers who follows the organisation and have the interest in benficiary
            List<ShortListedVol> listOfVolunteers = EventMatchingDL.retrieveShortListedVolunteers(databaseConnection, orgId);

            if (listOfVolunteers.Count == 0)
            {
                // send a prompt towards the event organiser 
                System.Diagnostics.Debug.WriteLine("there is no volunters");
            }
            volDateMainThread(listOfVolunteers);

            String matched = "<hr> <p>Matched volunteers </p> <br/>";
            String stats = "<u> Event matching conditions</u>";
            stats += "<br/> traits: " + eventDetails.listOfWeights[0];
            stats += " cause: " + eventDetails.listOfWeights[1];
            stats += " role: " + eventDetails.listOfWeights[2];
            stats += " skill: " + eventDetails.listOfWeights[3];
            stats += "<br/> passing: " + eventDetails.passingWeight;


            foreach(SessionDetails sessionDetail in eventDetails.listOfSession)
            {
                stats += "<br/> <br/> <u>"+sessionDetail.sessionName+" </u> <br/>";
                foreach(SessionDays day in sessionDetail.listOfSessionDays)
                {
                    stats += day.evt_date.ToString("d MMM yyyy") + ",  ";
                }

                stats += "<br/> number of required days :  " + sessionDetail.compulsoryDays;
            }

            stats += "<br/> <hr>";
            

            foreach (ShortListedVol volunteer in listOfVolunteers)
            {
                stats += " <br /><br />";
                volunteer.eventId = eventDetails.eventId;               
                stats += "volunteer email: " + volunteer.email;

                int typeWeight = determineEventInterestWeight(volunteer, eventDetails, eventDetails.listOfWeights[1], ref stats);

                stats += "<br/> unavliable dates: ";
                foreach(DateTime date in volunteer.listOfDates)
                {
                    stats += date.ToString("d MMM yyyy") + ", " ;
                }
                

                if (shortListVolunteers(eventDetails, volunteer, typeWeight, ref stats) == true)
                {
                    // send an insert statement towards the database
                    EventMatchingDL.createEventInvitation(databaseConnection, volunteer, connectionString);
                    EventMatchingDL.createNotificationInvite(databaseConnection, eventDetails.eventName, eventDetails.eventId, volunteer.VOL_ID);
                    matched += volunteer.email + "<br/>";
                }

                stats += "<br/> <hr>";
            }
            System.Diagnostics.Debug.WriteLine(stats);
            writeEmail(stats, matched);
        }

        /*
         * This is to send email 
         */
        private static void writeEmail(String statistic, string matched)
        {
            statistic += matched;
            using (MailMessage message = new MailMessage("sgserve8mc@gmail.com", "web@onethird.co"))
            {
                message.Subject = "Testing";
                message.Body = statistic;
                message.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("sgserve8mc@gmail.com", "8mcdjjshwj");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(message);
            }
        }


        /*
        *  This method interatively retrieves a volunteer sechdeule
        */
        private static void volDateMainThread(List<ShortListedVol> listOfShortListed)
        {
            foreach (ShortListedVol volunteer in listOfShortListed)
            {
                Database_Connect databseConfig = new Database_Connect("CONSOLE_VMS");
                SqlConnection databaseConnection = databseConfig.getConnection();
                EventMatchingDL.retrieveVolunteerSchedule(databaseConnection, volunteer);
                databseConfig.closeDatabase();
            }

        }

        /*
         *  This method checks if a volunteer is free to join the event as well as having the nessary traits to join
         */
        private static Boolean shortListVolunteers(EventDetails eventDetails, ShortListedVol volunteer, int typeWeight, ref String stats)
        {
            Boolean shortListedVolunteer = false;
            volunteer.listOfAcceptedSession = new List<AcceptedSession>();
            foreach (SessionDetails eventSession in eventDetails.listOfSession)
            {
                stats += "<br/><br/> <u>" + eventSession.sessionName + " </u>";
                AcceptedSession acceptSession = new AcceptedSession { sessionId = eventSession.sessionId, sessionRoles = "", sessionDates = "", percent = "" };
                Boolean passTraitsCheck = checkVolRole(eventSession, volunteer, acceptSession, typeWeight, eventDetails.listOfWeights, eventDetails.passingWeight, ref stats);

                // this means that volunteer is unable to match any of the event traits 
                if (passTraitsCheck == false)
                    continue;

                // this mean that volunteer is unable to match any of the event dates
                if (checkVolunteerDate(eventSession, volunteer, acceptSession, ref stats) == false)
                    continue;

                shortListedVolunteer = true;
                volunteer.listOfAcceptedSession.Add(acceptSession);
            }
            return shortListedVolunteer;
        }

        /*
         * This method checks the amount of weigtage percentrage a volunteer has against the  event interest 
         */
        private static int determineEventInterestWeight(ShortListedVol volunteer, EventDetails eventDetails, int typeWeight, ref String stats)
        {
            int count = 0;
            foreach (int cause in eventDetails.listOfEventCause)
            {
                int startIndex = 0;
                int endIndex = volunteer.listOfEventInterest.Count - 1;
                int median;
                while (startIndex <= endIndex)
                {
                    median = (startIndex + endIndex) / 2;

                    if (cause == volunteer.listOfEventInterest[median])
                    {
                        stats += "<br/> Event interest: "+ eventDetails.listOfEventCauseName[count] + "<br/> weight: " + typeWeight;
                        return typeWeight;
                    }

                    if (cause > volunteer.listOfEventInterest[median])
                        startIndex = median + 1;

                    if (cause < volunteer.listOfEventInterest[median])
                        endIndex = median - 1;
                }
                count++;
            }
            stats += "<br/> event interest : 0";
            return 0;
        }

        /*
         *  This method checks and define the number of session role in which a volunteer is eligible to join
         */
        private static Boolean checkVolRole(SessionDetails eventSession, ShortListedVol volunteer, AcceptedSession acceptSession, int typeWeight, List<int> listOfWeight, int weightTotal, ref String stats)
        {
            Boolean volunteerMatchRole = false;
            foreach (SessionRoles sessionRole in eventSession.listOfSessionRoles)
            {
                stats += "<br/> <br/> <u> session role :  " + sessionRole.roleName + " </u> <br/> <br/>";
                int traitsWeight = performVolRoleCheck(sessionRole.personalityTraits, volunteer.traits, listOfWeight[0], ref stats);
                int roleInterestWeight = determineRoleInterestWeight(listOfWeight[2], sessionRole.roleId, volunteer.listOfEventRoleInterest, ref stats);
                int skillWeight = performSkillsCheck(volunteer.skillSets, sessionRole.skillSets, listOfWeight[3], ref stats);


                int attaintedWeigtage = (traitsWeight + roleInterestWeight + skillWeight + typeWeight);

                stats += "<br/> total weight: " + attaintedWeigtage;
                if (weightTotal <= attaintedWeigtage)
                {
                    acceptSession.sessionRoles += sessionRole.roleId + ",";
                    acceptSession.percent += attaintedWeigtage + ",";
                    volunteerMatchRole = true;
                    stats += ", passed";
                }
                else
                {
                    stats += ", failed";
                }

            }
            return volunteerMatchRole;
        }


        /*
         * This method performs the check to see if volunteer has the nesscary traits 
         * This method also determines whether a volunteer pass the weigtage check that was set by the event organiser
         */
        private static int performVolRoleCheck(String roleTraits, String volTraits, int traitsWeight, ref string stats)
        {

            int numOfTraitsMatch = 4;
            for (int count = 0; count < 4; count++)
            {
                if (roleTraits[count].Equals(volTraits[count]) == false)
                {
                    if(roleTraits[count].Equals("X") == false)
                        numOfTraitsMatch--;
                }
            }
            stats += " traits: " + volTraits + " vs " + roleTraits;
            traitsWeight = (traitsWeight * numOfTraitsMatch) / 4;
            stats += " = " + traitsWeight;
            return traitsWeight;

        }

        /*
         * This method checks if volunteer has an interest in a specific event role 
         * This check is done with binary search by comparing the term_id of a sessionRole against the list of vol event role interest term_id 
         */
        private static int determineRoleInterestWeight(int roleWeight, int sessionRoleId, List<int> volEventRoleInterest, ref String stats)
        {
            int startIndex = 0;
            int endIndex = volEventRoleInterest.Count - 1;
            int median;
            while (startIndex <= endIndex)
            {
                median = (startIndex + endIndex) / 2;

                if (sessionRoleId == volEventRoleInterest[median])
                {
                    stats += "<br/> interested in role = " + roleWeight;
                    return roleWeight;
                }


                if (sessionRoleId > volEventRoleInterest[median])
                    startIndex = median + 1;

                if (sessionRoleId < volEventRoleInterest[median])
                    endIndex = median - 1;
            }

            stats += "<br/> role interest : 0";
            return 0;
        }


        /*
         * This method checks if volunteer has the required skills set to join an event
         * This method will only be called when volunteer has passed the traits check
         */
        private static int performSkillsCheck(List<int> volSkillsets, String roleSkillSets, int skillWeight, ref String stats)
        {
            if (volSkillsets.Count == 0)
                return 0;

            if (roleSkillSets == null)
                return skillWeight;

            String[] listOfRoleSkillSets = roleSkillSets.Split(',');
            int numOfMatch = 0;
            foreach (String roleSkill in listOfRoleSkillSets)
            {
                if (binarySearchSkill(Int32.Parse(roleSkill), volSkillsets) == true)
                {
                    numOfMatch++;
                }

            }

            skillWeight = (skillWeight * numOfMatch) / listOfRoleSkillSets.Length;
            stats += "<br/> skillwieght: " + skillWeight;
            return skillWeight;
        }

        /*
         * This method utlise binary search to check if if the volunteer has a skillset for a speicifc event role
         * This method compares skill Id
         */
        private static Boolean binarySearchSkill(int roleSkill, List<int> volSkillSets)
        {
            int startIndex = 0;
            int endIndex = volSkillSets.Count - 1;
            int median;
            while (startIndex <= endIndex)
            {
                median = (startIndex + endIndex) / 2;

                if (roleSkill == volSkillSets[median])
                    return true;

                if (roleSkill > volSkillSets[median])
                    startIndex = median + 1;

                if (roleSkill < volSkillSets[median])
                    endIndex = median - 1;
            }

            return false;
        }

        /*
         * This method can only be called when a volunteer is determined to have fit at least event session role citeria
         *  This method checks if a volunteer is able to attend any of the event sesion days 
         *  This can be enhance to incorporate a volunteers on schedule or date where he dont want to join
         */
        private static Boolean checkVolunteerDate(SessionDetails eventSession, ShortListedVol volunteer, AcceptedSession acceptSession, ref String stats)
        {
            stats += "<br/> <br/> <u>matched schedule </u> <br/>";
            int numOfAcceptedDays = 0;
            foreach (SessionDays day in eventSession.listOfSessionDays)
            {
                if (checkSpecificDate(day, volunteer.listOfDates) == true)
                {
                    acceptSession.sessionDates += day.evt_date.ToString("d/MM/yyyy") + ",";
                    numOfAcceptedDays++;
                    stats += day.evt_date.ToString("d MMM yyyy") + ",  ";
                }
            }

            // check if number of compulsory sessions is hit
            if (eventSession.compulsoryDays >= numOfAcceptedDays)
                return false;
            else
                return true;
        }


        /*
         *  This method utlise binary search to determine if a volunteer is free for the event session date
         *  If there is a match, it means that volunteer is not able to make it to this particular event session
         */
        public static Boolean checkSpecificDate(SessionDays day, List<DateTime> listOfDates)
        {
            int startIndex = 0;
            int endIndex = listOfDates.Count - 1;
            int median;
            while (startIndex <= endIndex)
            {
                median = (startIndex + endIndex) / 2;
                int dateComparison = DateTime.Compare(day.evt_date, listOfDates[median]);

                if (dateComparison == 0)
                    return false;

                // event date is later then date list
                if (dateComparison > 0)
                    startIndex = median + 1;

                // event date is lesser then date list
                if (dateComparison < 0)
                    endIndex = median - 1;
            }
            return true;
        }
    }
}