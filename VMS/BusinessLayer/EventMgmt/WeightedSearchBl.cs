﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.Models.EventModels;
using VMS.Models.AdminModels;
using System.Data.SqlClient;
using System.Threading;
using VMS.AccountMgmt;

namespace VMS.EventMgmt
{
    public class WeightedSearchBl
    {
        private static Semaphore mutex;

        public static void weightedSearch(String userId, List<String> listOfConnectionString)
        {
            List<EventDetails> listOfShortListedEvent = getListOfShortListedEvent(listOfConnectionString);

            List<Skill> listOfVolSkills;
            Thread skillThread = new Thread(delegate ()
            {
                Database_Connect databaseConfig = new Database_Connect("CONSOLE_VMS");
                SqlConnection sqlConnection = databaseConfig.getConnection();
               // listOfVolSkills = AccountMgmtDL.getVolSkills(sqlConnection,userId);
            });
            skillThread.Start();
        }

        /*
         * This method gets different types of data that benfits a volunteer.
         *  
         */
        private static void getVolunteerDetails()
        {
           
        }

        /*
         *  This method creates worker thread to send tcps calls to thhe database in parallel 
         */
        private static List<EventDetails> getListOfShortListedEvent(List<String> listOfConnectionString)
        {
            DateTime currentDate = DateTime.Now;
            List<Thread> listOfThreads = new List<Thread>();
            List<EventDetails> listOfShortListedEvent = new List<EventDetails>();
            mutex = new Semaphore(1, 1);
            foreach(String connectionString in  listOfConnectionString)
            {
                Thread thread = new Thread(delegate ()
                {
                    getListOfShortListedEventWorker(listOfShortListedEvent, connectionString, currentDate);
                });
                thread.Start();
                listOfThreads.Add(thread);
            }

            for(int threadCount =0; threadCount < listOfThreads.Count; threadCount++)
                listOfThreads[threadCount].Join();

            return listOfShortListedEvent;
        }

        /*
         *  This method is a worker thread which is use to grab a list of shortlisted event from a specific connection string
         */
        private static void getListOfShortListedEventWorker(List<EventDetails> listOfShortListedEvent, String connectionString, DateTime currentDate)
        {
            Database_Connect databaseConfig = new Database_Connect(connectionString);
            SqlConnection databaseConnection = databaseConfig.getConnection();

            List<EventDetails> listOfEvents =  WeightedSearchDL.retrieveShortListedEvents(databaseConnection, currentDate);

            // usage of binary semaphore to control access of threads
            mutex.WaitOne();
            foreach (EventDetails eventDetails in listOfEvents)
                listOfShortListedEvent.Add(eventDetails);
            mutex.Release();
        }

        /*
         * This method conducts a weightage check 
         */
        private static void weightCheck(List<EventDetails> listOfShortListedEvent)
        {
            foreach(EventDetails eventDetail in listOfShortListedEvent)
            {
                //eventDetail.weight += checkBenficiaryWeight(eventDetail.listOfBeneficiary, );
            }
        }

        /*
         * This method checks how much does a volunteer matches the personality traits
         */
        private static void checkEventRoleWeight(SessionRoles sessonRole, String volTraits, String volSkillSets )
        {

        }

        /*
       * This method checks how much does a volunteer matches in terms of benficiar
       * Ensure that volunterBenfiicary is retrieved in a sorted order from database
       * Weightage of benifiary match is 10% 
       */
        private static int checkBenficiaryWeight(string eventBeneficiary, List<int> volunteerBeneficiary)
        {
            int[] eventBeneficiaryList = Array.ConvertAll(eventBeneficiary.Split(','), int.Parse);
            int numOfMatch =0;

            foreach(int beneficiaryId in eventBeneficiaryList)
            {
                int startIndex = 0;
                int endIndex = volunteerBeneficiary.Count - 1;
                int median;
                while (startIndex <= endIndex)
                {
                    median = (startIndex + endIndex) / 2;

                    if (beneficiaryId == volunteerBeneficiary[median])
                        numOfMatch++;

                    if (beneficiaryId > volunteerBeneficiary[median])
                        startIndex = median + 1;

                    if (beneficiaryId < volunteerBeneficiary[median])
                        endIndex = median - 1;
                }
            }
            return 10 * (numOfMatch / volunteerBeneficiary.Count);
        }


    }
}