﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using VMS.Database;
using VMS.DataLayer.VolBiddingMgmt;
using VMS.DataLayer.SponsorMgmt;
using VMS.Models.VolBidModels;

namespace VMS.BusinessLayer.VolBiddingMgmt
{
    public class BidItemBL
    {
        // 1.4) Publish sponsored items to be bid by volunteers
        // trigers updating of items avaliable
        public static String createBidItem(String itemID, int allocatingAmt, String description, String startDate)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity avaliable & current quantity on bidding 
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);
            int qtyAva = item.QTY_AVAILABLE - allocatingAmt;
            int qtyBid = item.QTY_ON_BIDDING + allocatingAmt;

            String reply = VirtualInventoryDL.updateQuantity(dbConnection, itemID, qtyAva, qtyBid);

            reply =  BidItemDL.createBidItem(dbConnection, itemID, description, allocatingAmt, startDate);
            database.closeDatabase();
            return reply;
        }

        // 1.5) Edit sponsored items bid
        // trigers updating of items avaliable
        public static String updateBidItem(String bidItemID, String description, String startDate)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String reply = BidItemDL.updateBidItem(dbConnection, bidItemID, description, startDate);
            database.closeDatabase();
            return reply;
        }


        // 1.6) Remove sponsored items bid
        // trigers updating of items avaliable
        public static String deleteBidItem(String bidItemID, int returningAmt)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity avaliable & current quantity on bidding 
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, bidItemID);
            int qtyAva = item.QTY_AVAILABLE + returningAmt;
            int qtyBid = item.QTY_ON_BIDDING - returningAmt;

            VirtualInventoryDL.updateQuantity(dbConnection, bidItemID, qtyAva, qtyBid);
            String reply = BidItemDL.deleteBidItem(dbConnection, bidItemID);
            database.closeDatabase();
            return reply;
        }

        // Retrieve all items that are current
        public static List<BidItem> getAllCurrentBidItems()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidItem> item = BidItemDL.getAllCurrentBidItems(dbConnection);
            database.closeDatabase();
            return item;
        }

        // Retrieve all items that are published
        public static List<BidItem> getAllPublishedBidItems()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidItem> item = BidItemDL.getAllPublishedBidItems(dbConnection);
            database.closeDatabase();
            return item;
        }

        // Retrieve a published bidding item
        public static List<BidItem> getBidItemDetails(String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidItem> item = BidItemDL.getBidItemDetails(dbConnection, bidItemID);
            database.closeDatabase();
            return item;
        }

        // Retrieve all items
        public static List<BidItem> getAllBidItems()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidItem> item = BidItemDL.getAllBidItems(dbConnection);
            database.closeDatabase();
            return item;
        }

    }
}