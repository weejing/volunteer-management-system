﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using VMS.Models.VolBidModels;
using VMS.Database;
using VMS.DataLayer.SponsorMgmt;
using VMS.DataLayer.VolBiddingMgmt;

namespace VMS.BusinessLayer.VolBiddingMgmt
{
    public class BiddingTransactionBL
    {
        // Add points for event
        public static String addPointsForCompleteEvent(String userID, int hoursCompleted, String userEventID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get userEventID
            //String userEventID = "99e1420c-389a-4bfa-9f7f-313831dd2455";

            // retrieve current point
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);

            int pointsGained = hoursCompleted * 5;

            // add transaction record
            String reply = BiddingTransactionDL.createTransactionRecordForEvent(dbConnection, userID, pointsGained, balance+pointsGained, userEventID);

            // update points
            reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance+pointsGained, userID);

            database.closeDatabase();
            return reply;
        }

        // Add points for spin wheel
        public static String addPointsForSpinWheel(String userID, int pointsEarned)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // retrieve current point
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);

            // add transaction record
            String reply = BiddingTransactionDL.createTransactionRecordForBonusSpin(dbConnection, userID, pointsEarned, balance+pointsEarned);

            // update points
            reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance+pointsEarned, userID);

            database.closeDatabase();
            return reply;
        }

        // Update VIA Hour
        public static String updateVolVIAhours(int hoursCompleted, String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            int currentVIAhours = BiddingTransactionDL.getVolunteerCurrentVIAHours(dbConnection, userID);

            String result = BiddingTransactionDL.updateVolVIAhours(dbConnection, currentVIAhours + hoursCompleted, userID);
            database.closeDatabase();
            return result;
        }

        // Update points for changes in bidding
        public static String updatePointsForBidChanges(String userID, int bidAmount, int changeInBidAmt, String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // retrieve current point
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);

            // add transaction record
            String reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, userID, -changeInBidAmt, balance-changeInBidAmt, bidItemID, "update bidding record");

            // update bidding record
            reply = BiddingTransactionDL.updateBidItemRecord(dbConnection, bidAmount, bidItemID, userID);

            if (BiddingTransactionDL.checkIfBiddersMoreThanQuantity(dbConnection, bidItemID))
            {
                // retrieve quantity
                int quantity = BiddingTransactionDL.getQuantityForBidItem(dbConnection, bidItemID);

                // retrieve minimum bid points
                int minPoint = BiddingTransactionDL.getMinimumBidPointsForBidItem(dbConnection, bidItemID, quantity);

                // update minimum bid points
                reply = BiddingTransactionDL.updateBidItemMinPoint(dbConnection, minPoint+1, bidItemID);
            }

            // update points
            reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance-changeInBidAmt, userID);
            database.closeDatabase();
            return reply;
        }


        // Update points for withdrawing from a bid
        public static String withdrawBidandUpdatePoints(String userID, int withdrawBidAmount, String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // retrieve current point
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);

            // update / remove bidding record
            String reply = BiddingTransactionDL.removeBidItemRecord(dbConnection, bidItemID, userID);

            balance = balance + withdrawBidAmount;

            // create transaction record
            reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, userID, withdrawBidAmount, balance, bidItemID, "withdraw bidding record");

            // retrieve minimum bid points and number of bidders
            BidItem item = BiddingTransactionDL.getBidItemDetails(dbConnection, bidItemID);

            // minus number of bidders
            reply = BiddingTransactionDL.updateBidItemNumBidders(dbConnection, item.NUM_BIDDERS - 1, bidItemID);

            int minPoint = 0;

            if (BiddingTransactionDL.checkIfBiddersMoreThanQuantity(dbConnection, bidItemID))
            {
                // retrieve quantity
                int quantity = BiddingTransactionDL.getQuantityForBidItem(dbConnection, bidItemID);

                // retrieve minimum bid points
                minPoint = BiddingTransactionDL.getMinimumBidPointsForBidItem(dbConnection, bidItemID, quantity);

                // update minimum bid points
                reply = BiddingTransactionDL.updateBidItemMinPoint(dbConnection, minPoint+1, bidItemID);
            }
            else
            {
                reply = BiddingTransactionDL.updateBidItemMinPoint(dbConnection, 1, bidItemID);
            }

            // update points
            reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance, userID);
            database.closeDatabase();
            return reply;
        }
        

        // Place bid
        public static String createPlaceBid(String userID, int bidAmount, String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // retrieve current point
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);

            // add transaction record
            String reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, userID, bidAmount, balance-bidAmount, bidItemID, "place bid");

            // create bidding record
            reply = BiddingTransactionDL.createBiddingRecord(dbConnection, userID, bidItemID, bidAmount);

            // retrieve minimum bid points and number of bidders
            BidItem item = BiddingTransactionDL.getBidItemDetails(dbConnection, bidItemID);

            // add number of bidders
            reply = BiddingTransactionDL.updateBidItemNumBidders(dbConnection, item.NUM_BIDDERS + 1, bidItemID);

            if (BiddingTransactionDL.checkIfBiddersMoreThanQuantity(dbConnection, bidItemID))
            {
                // retrieve quantity
                int quantity = BiddingTransactionDL.getQuantityForBidItem(dbConnection, bidItemID);

                // retrieve minimum bid points
                int minPoint = BiddingTransactionDL.getMinimumBidPointsForBidItem(dbConnection, bidItemID, quantity);

                // update minimum bid points
                reply = BiddingTransactionDL.updateBidItemMinPoint(dbConnection, minPoint + 1, bidItemID);
            }

            // check if number of bidders >= quantity
            /*if (BiddingTransactionDL.checkIfBiddersMoreThanQuantity(dbConnection, bidItemID))
            {
                if (bidAmount > item.MIN_POINT)
                {
                    // update minimum bid points
                    reply = BiddingTransactionDL.updateBidItemMinPoint(dbConnection, bidAmount + 1, bidItemID);
                }
            }*/

            // update volunteer bid points
            reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance-bidAmount, userID);
            database.closeDatabase();
            return reply;
        }


        // Retrieve points transaction (by user)
        public static List<BidPointTransaction> getVolBidPointsTransaction(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<BidPointTransaction> listOfTransactions = BiddingTransactionDL.getVolBidPointsTransaction(dbConnection, userID);
            database.closeDatabase();
            return listOfTransactions;
        }

        public static String canUserSpinWheel(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String reply = BiddingTransactionDL.getLastSpinWheelDateByUser(dbConnection, userID);
            database.closeDatabase();

            if (reply.Equals("no spin"))
            {
                return "true";
            }
            else
            {
                if (System.DateTime.Compare(System.DateTime.Now, System.DateTime.Parse(reply).AddDays(7)) < 0)
                {
                    return "false";
                }
                else
                {
                    return "true";
                }
            }
        }


        // Retrieve bidding items history (by user)
        public static List<BidRecord> getVolAllBidItemRecord(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidRecord> listOfRecords = BiddingTransactionDL.getVolAllBidItemRecord(dbConnection, userID);
            database.closeDatabase();
            return listOfRecords;
        }

        // Retrieve bidding items that won (by user)
        public static List<BidRecord> getVolBidItemRecordWon(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidRecord> listOfRecords = BiddingTransactionDL.getVolBidItemRecordWon(dbConnection, userID);
            database.closeDatabase();
            return listOfRecords;
        }

        // Retrieve current bidding items (by user)
        public static List<BidRecord> getVolCurrentBidItemRecord(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidRecord> listOfRecords = BiddingTransactionDL.getVolCurrentBidItemRecord(dbConnection, userID);
            database.closeDatabase();
            return listOfRecords;
        }

        // Retrieve a current bid item details (by user)
        public static List<BidRecord> getVolCurrentBidItemRecordDetails(String userID, String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidRecord> listOfRecords = BiddingTransactionDL.getVolCurrentBidItemRecordDetails(dbConnection, userID, bidItemID);
            database.closeDatabase();
            return listOfRecords;
        }

        // Retrieve a bid item bidding details (by admin)
        public static List<BidRecord> getBidItemRecordDetails(String bidItemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<BidRecord> listOfRecords = BiddingTransactionDL.getBidItemRecordDetails(dbConnection, bidItemID);
            database.closeDatabase();
            return listOfRecords;
        }


        // getVolunteerCurrentBidPoints
        public static int getVolunteerCurrentBidPoints(String userID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, userID);
            database.closeDatabase();
            return balance;
        }


        // System Determine winner
        public static void systemDetermineWinner()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            BiddingTransactionDL.updateBidItemStatusToCheck(dbConnection);
            List<String> listOfItemsID = BiddingTransactionDL.getBidItemThatJustEnd(dbConnection);

            foreach (String item in listOfItemsID)
            {
                // get list of bid item recording
                List<BidRecord> listOfBidsPlaced = BiddingTransactionDL.getBidItemRecordDetails(dbConnection, item);

                // retrieve quantity
                int quantity = BiddingTransactionDL.getQuantityForBidItem(dbConnection, item);

                // determine the minimum bid point to win
                int minPoint = BiddingTransactionDL.getMinimumBidPointsForBidItem(dbConnection, item, quantity);

                // determine winner
                // change status of all winners to win
                String reply = BiddingTransactionDL.updateBidItemRecordSelectWinners(dbConnection, item, minPoint, quantity);

                // change the rest of the bidders status to unsuccessful
                reply = BiddingTransactionDL.updateBidItemRecordSelectLosers(dbConnection, item, minPoint);

                // select winners
                List<BidRecord> listOfWinners = BiddingTransactionDL.getWinnersFromBiddingRecord(dbConnection, item);

                foreach (BidRecord winner in listOfWinners)
                {
                    // retrieve current point
                    int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, winner.USR_ID);

                    int changes = winner.BID_POINTS_AMOUNT - minPoint;
                    balance = balance + changes;

                    // refund extra points for winners
                    reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, winner.USR_ID, changes, balance, item, "Bid won, refund for extra bid points");
                    reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance, winner.USR_ID);
                }

                // select losers
                List<BidRecord> listOfLosers = BiddingTransactionDL.getLosersFromBiddingRecord(dbConnection, item);

                foreach (BidRecord loser in listOfLosers)
                {
                    // retrieve current point
                    int balance = BiddingTransactionDL.getVolunteerCurrentBidPoints(dbConnection, loser.USR_ID);

                    int changes = loser.BID_POINTS_AMOUNT;
                    balance = balance + changes;

                    // refund extra points for winners
                    reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, loser.USR_ID, changes, balance, item, "Bid unsuccessful, refund for bid points");
                    reply = BiddingTransactionDL.updateVolBidPoint(dbConnection, balance, loser.USR_ID);
                }
            }
            BiddingTransactionDL.updateBidItemStatusToEnd(dbConnection);
            database.closeDatabase();
        }

        /*
        public static void systemDetermineFakeWinner()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            String reply = BiddingTransactionDL.createTransactionRecordForBidding(dbConnection, "43FB457D-15E2-47C1-959B-EA0F10C71136", -1, -1, "33ECF750-08AF-4D84-8CE0-4AC1C2944BCC", "fake transaction, this is a test command");
            database.closeDatabase();
        }
        */

    }
}