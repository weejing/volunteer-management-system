﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentScheduler;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using System.Web.Hosting;
using VMS.BusinessLayer.VolBiddingMgmt;
using VMS.Database;
using VMS.DataLayer.VolBiddingMgmt;
using VMS.Models.VolBidModels;


namespace VMS.BusinessLayer.VolBiddingMgmt
{
    public class TimeTriggerBidsTransBL : IJob, IRegisteredObject
    {
        //<summary>
        //deals with time-triggered events

        private readonly object _lock = new object();
        private bool _shuttingDown;

        public TimeTriggerBidsTransBL()
        {
            // Register this job with the hosting environment.
            // Allows for a more graceful stop of the job, in the case of IIS shutting down.
            HostingEnvironment.RegisterObject(this);
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                    return;

                // Do work, son!
                BiddingTransactionBL.systemDetermineWinner();
                //BiddingTransactionBL.systemDetermineFakeWinner();
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }
    }
}