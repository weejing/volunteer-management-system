﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.HrMgmt;
using VMS.Models.HrModels;


namespace VMS.BusinessLayer.HrMgmt
{
    public class HrMgmtBL
    {
        /// <summary>BBL: Insert a new external volunteer details</summary>
        public static String createOneRecord(ExternalVolunteerDetails newInfo)
        {
            Database_Connect database = new Database_Connect(newInfo.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            string reply = HrMgmtDL.createOneRecord(dbConnection, newInfo);
            database.closeDatabase();
            return reply;
        }
        /// <summary>BBL: Check external volunteer exist</summary>
        public static Boolean checkVolunteerExist(string name, string contact, string email, string connectionstring)
        {
            Database_Connect database = new Database_Connect(connectionstring);
            SqlConnection dbConnection = database.getConnection();
            bool reply = HrMgmtDL.checkVolunteerExist(name, contact, email, dbConnection);
            database.closeDatabase();
            return reply;
        }
        /// <summary>BLL: Get list of external volunteer from database</summary>
        public static List<ExternalVolunteerDetails> getExternalVolList(string connectionstring)
        {

            Database_Connect database = new Database_Connect(connectionstring);
            SqlConnection dbConnection = database.getConnection();
            List<ExternalVolunteerDetails> evd = HrMgmtDL.getExternalVolList(dbConnection);
            database.closeDatabase();
            return evd;
        }
        /// <summary>BBL: Delete external volunteer</summary>
        public static int deleteExternalVol(string NAMELIST_ID, string connectionstring)
        {
            Database_Connect database = new Database_Connect(connectionstring);
            SqlConnection dbConnection = database.getConnection();
            int reply = HrMgmtDL.deleteExternalVol(dbConnection, NAMELIST_ID);
            database.closeDatabase();
            return reply;
        }

        /// <summary>BBL: get event info</summary>
        public static List<OpenEvents> getUpcomingEvents(string connectionstring)
        {
            Database_Connect database = new Database_Connect(connectionstring);
            SqlConnection dbConnection = database.getConnection();
            List<OpenEvents> oe = HrMgmtDL.getUpcomingEvents(dbConnection);
            database.closeDatabase();
            return oe;
        }
    }
}