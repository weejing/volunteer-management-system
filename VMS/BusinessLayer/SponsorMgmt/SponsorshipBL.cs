﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using VMS.Database;
using VMS.DataLayer.SponsorMgmt;

namespace VMS.BusinessLayer.SponsorMgmt
{
    public class SponsorshipBL
    {

        // 4) Submit sponsorship request (from vendor to SGServe)
        /*BL : */
        public static String addSponsorshipRequest(String itemID, int qty, String sponsorID, String collectionDesc, String sponsorshipDesc)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.addSponsorshipRequest(dbConnection, itemID, qty, sponsorshipDesc, collectionDesc, sponsorID);
            database.closeDatabase();
            return result;
        }

        public static String createItemToRecord(String itemName, String description, String categoryID, String sponsorID, String imagePath)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.createItemToRecord(dbConnection, itemName, description, categoryID, sponsorID, imagePath);
            database.closeDatabase();
            return result;
        }

        // A2) Update (accept / reject) sponsorship request (from SGServe to vendor)
        // once accept, create or update virtual inventory item [SPR_ITEM]
        /*BL : */
        public static String updateSponsorship(String status, String sponsorRecordID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.updateSponsorship(dbConnection, status, sponsorRecordID);
            database.closeDatabase();
            return result;
        }

        // A1) Retreive all pending request
        /*BL :  */
        public static List<SponsorshipRecord> getAllPendingRequest()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getAllPendingRequest(dbConnection);
            database.closeDatabase();
            return listOfRecords;
        }

        // 1) Retrieve all sponsored item requests
        public static List<SponsorshipRecord> getAllSponsoredRequest()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getAllSponsoredRequest(dbConnection);
            database.closeDatabase();
            return listOfRecords;
        }


        // 2) Retrieve all sponsored item requests of a category
        /*
        public static List<SponsorshipRecord> getSponsoredRequestByCategory(String categoryID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            return SponsorshipDL.getSponsoredRequestByCategory(dbConnection, categoryID);
        }
        */


        // A1) Retrieve all sponsored item requests of a vendor
        public static List<SponsorshipRecord> getSponsoredRequestByVendorID(String sponsorVendorID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getSponsoredRequestByVendorID(dbConnection, sponsorVendorID);
            database.closeDatabase();
            return listOfRecords;
        }

        // A1.2) Retrieve all sponsored item requests of a vendor by status
        public static List<SponsorshipRecord> getSponsoredRequestByVendorIDByStatus(String sponsorVendorID, String status)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getSponsoredRequestByVendorIDByStatus(dbConnection, sponsorVendorID, status);
            database.closeDatabase();
            return listOfRecords;
        }

        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        public static SponsorshipRecord getSponsoredRequestByRecordID(String recordID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            SponsorshipRecord itemRecord = SponsorshipDL.getSponsoredRequestByRecordID(dbConnection, recordID);
            database.closeDatabase();
            return itemRecord;
        }


        // A1) & 3) & 5) Retrieve a specific sponsored item request (used by both vendor & SGServe)
        public static SponsorshipRecord getSponsoredRequestByItem(String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            SponsorshipRecord itemRecord = SponsorshipDL.getSponsoredRequestByItem(dbConnection, itemID);
            database.closeDatabase();
            return itemRecord;
        }

        public static String getItemIDbyRequestID(String requestID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String itemID = SponsorshipDL.getItemIDbyRequestID(dbConnection, requestID);
            database.closeDatabase();
            return itemID;
        }

        public static SponsorshipRecord getSponsorshipItemDetails(String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            SponsorshipRecord itemRecord = SponsorshipDL.getSponsorshipItemDetails(dbConnection, itemID);
            database.closeDatabase();
            return itemRecord;
        }

        // 6) Update sponsorship request
        public static String updateSponsorshipRequest(String sponsorRecordID, String sponsorItemRecordID, int quantity, String sponsorshipDesc, String collectionDesc)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.updateSponsorshipRequest(dbConnection, sponsorRecordID, sponsorItemRecordID, quantity, sponsorshipDesc, collectionDesc);
            database.closeDatabase();
            return result;
        }

        // 6) Update sponsorship request item
        public static String updateSponsorshipItem(String sponsorItemRecordID, String itemName, String itemDesc, String itemCatID, String imagePath)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.updateSponsorshipItem(dbConnection, sponsorItemRecordID, itemName, itemDesc, itemCatID, imagePath);
            database.closeDatabase();
            return result;
        }


        // 7) Delete sposorship request (for vendor)
        // once accepted, cannot vendor cannot delete striaght from system, need to personally email to request



            // 7) Delete sposorship request (for SGServe admin)
            /*BL : */
        public static String deleteSponsorshipRequest(String sponsorRecordID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = SponsorshipDL.deleteSponsorshipRequest(dbConnection, sponsorRecordID);
            database.closeDatabase();
            return result;
        }


        public static String removeAcceptedSponsorshipRequest(String sponsorRecordID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // set sponsorship record status to removed
            String reply = SponsorshipDL.updateSponsorship(dbConnection, "removed", sponsorRecordID);

            SponsorshipRecord record = SponsorshipDL.getSponsoredRequestByItemR(dbConnection, sponsorRecordID);

            String itemID = VirtualInventoryDL.getItemID(dbConnection, record.SPONSOR_USR_ID, record.ITEM_NAME);

            int qty = VirtualInventoryDL.getItemQuantity(dbConnection, itemID);

            if (qty > record.QTY)
            {
                // update inventory item
                reply = VirtualInventoryDL.updateQuantityWithSponsorDetails(dbConnection, qty - record.QTY, record.ITEM_NAME, record.SPR_ITEM_CATEGORY_ID, record.SPONSOR_USR_ID, record.ITEM_DESCRIPTION);
            }
            else
            {
                reply = VirtualInventoryDL.deleteItemFromInventory(dbConnection, itemID);
            }
            database.closeDatabase();
            return reply;
        }


        public static int getItemOnBiddingQtyByRecord(String sponsorRecordID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            SponsorshipRecord record = SponsorshipDL.getSponsoredRequestByItemR(dbConnection, sponsorRecordID);

            String itemID = VirtualInventoryDL.getItemID(dbConnection, record.SPONSOR_USR_ID, record.ITEM_NAME);

            int qty = VirtualInventoryDL.getItemQuantityOnBidding(dbConnection, itemID);
            
            database.closeDatabase();
            return qty;
        }

        public static Boolean checkItemNameExist(String sponsorVendorID, String itemName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            Boolean result = SponsorshipDL.checkItemNameExist(dbConnection, sponsorVendorID, itemName);
            database.closeDatabase();
            return result;
        }

        public static String getItemID(String sponsorVendorID, String itemName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String itemID = SponsorshipDL.getItemID(dbConnection, sponsorVendorID, itemName);
            database.closeDatabase();
            return itemID;
        }

        public static List<SponsorshipRecord> getItemNameList(String sponsorVendorID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfNames = SponsorshipDL.getItemNameList(dbConnection, sponsorVendorID);
            database.closeDatabase();
            return listOfNames;
        }

        public static List<SponsorItemCategory> getItemCategoryList()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorItemCategory> listOfCategories = SponsorshipDL.getItemCategoryList(dbConnection);
            database.closeDatabase();
            return listOfCategories;
        }

        public static List<SponsorItemCategory> getItemCategoryListForSearch()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorItemCategory> listOfCategories = SponsorshipDL.getItemCategoryListForSearch(dbConnection);
            database.closeDatabase();
            return listOfCategories;

        }

        public static List<SponsorshipRecord> searchSponsorshipRequests(String sponsorName, String itemName, String itemCatID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getSponsoredRequestBySearch(dbConnection, sponsorName, itemName, itemCatID);
            database.closeDatabase();
            return listOfRecords;
        }

        public static List<SponsorshipRecord> searchSponsoredRequestByVendor(String itemName, String itemCatID, String sponsorVendorID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<SponsorshipRecord> listOfRecords = SponsorshipDL.getSponsoredRequestBySearchByVendor(dbConnection, itemName, itemCatID, sponsorVendorID);
            database.closeDatabase();
            return listOfRecords;
        }
        
    }
}