﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using VMS.Models.SponsorshipModels;
using VMS.Database;
using VMS.DataLayer.SponsorMgmt;
using VMS.DataLayer.VolBiddingMgmt;

namespace VMS.BusinessLayer.SponsorMgmt
{
    public class VirtualInventoryBL
    {
        // Create item
        public static String addOrUpdateItem(String itemName, int qty, String description, String categoryID, String sponsorID, String imagePath)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String itemID = "";

            // if exisiting item, update quantity
            if (VirtualInventoryDL.checkItemNameExist(dbConnection, sponsorID, itemName))
            {
                itemID = VirtualInventoryDL.getItemID(dbConnection, sponsorID, itemName);
                int currentQtyAva = VirtualInventoryDL.getItemQuantity(dbConnection, itemID);
                currentQtyAva = currentQtyAva + qty;
                int currentQtyBid = VirtualInventoryDL.getItemQuantityOnBidding(dbConnection, itemID);
                VirtualInventoryDL.updateQuantity(dbConnection, itemID, currentQtyAva, currentQtyBid);
            }
            // else, new item, create inventory
            else
            {
                VirtualInventoryDL.addItem(dbConnection, itemName, qty, description, categoryID, sponsorID, imagePath);
                itemID = VirtualInventoryDL.getItemID(dbConnection, sponsorID, itemName);
            }
            database.closeDatabase();
            return "item added / updated";
        }

        // Update item 
        public static String updateItem(String itemID, String itemName, String description)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = VirtualInventoryDL.updateItem(dbConnection, itemID, itemName, description);
            database.closeDatabase();
            return result;
        }

        // Update item quantity -HOW
        public static String updateQuantity(String itemID, int qtyAva, int qtyBid)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String result = VirtualInventoryDL.updateQuantity(dbConnection, itemID, qtyAva, qtyBid);
            database.closeDatabase();
            return result;
        }

        // Update item quantity
        public static String addQuantity(String itemID, int qtyAva, int qtyBid)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity avaliable & quantity on bidding 
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);

            String result = VirtualInventoryDL.updateQuantity(dbConnection, itemID, qtyAva, qtyBid);
            database.closeDatabase();
            return result;
        }

        // Update item quantity
        public static String transferQuantityToBidding(String itemID, int allocatingAmt)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity avaliable & current quantity on bidding 
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);
            int qtyAva = item.QTY_AVAILABLE - allocatingAmt;
            int qtyBid = item.QTY_ON_BIDDING + allocatingAmt;

            String result = VirtualInventoryDL.updateQuantity(dbConnection, itemID, qtyAva, qtyBid);
            database.closeDatabase();
            return result;
        }

        // Update item quantity
        public static String returnQuantityToInventory(String itemID, String bidItemID, int returningAmt)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity avaliable & current quantity on bidding 
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);
            int qtyAva = item.QTY_AVAILABLE + returningAmt;
            int qtyBid = item.QTY_ON_BIDDING - returningAmt;

            String reply = BidItemDL.deleteBidItem(dbConnection, bidItemID);

            reply = VirtualInventoryDL.updateQuantity(dbConnection, itemID, qtyAva, qtyBid);
            database.closeDatabase();
            return reply;
        }


        // Retrieve all items
        public static List<VirtualItemSponsor> getAllItems()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VirtualItemSponsor> listOfItems = VirtualInventoryDL.getAllItems(dbConnection);
            database.closeDatabase();
            return listOfItems;
        }

        // Retreive items by category
        public static List<VirtualItemSponsor> getItemsByCategory(String categoryID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VirtualItemSponsor> listOfItems = VirtualInventoryDL.getItemsByCategory(dbConnection, categoryID);
            database.closeDatabase();
            return listOfItems;
        }

        // Retreive items by search
        public static List<VirtualItemSponsor> searchItemsList(String itemName, String categoryID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<VirtualItemSponsor> listOfItems = VirtualInventoryDL.getItemsBySearch(dbConnection, itemName, categoryID);
            database.closeDatabase();
            return listOfItems;
        }

        // Retrieve individual item
        public static VirtualItemSponsor getIndidivudalItem(String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);
            database.closeDatabase();
            return item;
        }

        public static int getItemQuantity(String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int quantity = VirtualInventoryDL.getItemQuantity(dbConnection, itemID);
            database.closeDatabase();
            return quantity;
        }

        public static int getItemQuantityOnBidding(SqlConnection dataConnection, String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int quantity = VirtualInventoryDL.getItemQuantityOnBidding(dbConnection, itemID);
            database.closeDatabase();
            return quantity;
        }

        public static Boolean checkItemNameExist(String sponsorVendorID, String itemName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            Boolean result = VirtualInventoryDL.checkItemNameExist(dbConnection, sponsorVendorID, itemName);
            database.closeDatabase();
            return result;
        }

        public static String getItemID(String sponsorVendorID, String itemName)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            String itemID = VirtualInventoryDL.getItemID(dbConnection, sponsorVendorID, itemName);
            database.closeDatabase();
            return itemID;
        }

        /*
        public static String removeSponsoredItem(String itemID)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();

            // get current quantity
            VirtualItemSponsor item = VirtualInventoryDL.getIndidivudalItem(dbConnection, itemID);

            // get sponsor record ID from ...


            String result = VirtualInventoryDL.updateQuantity(dbConnection, itemID, 0, item.QTY_ON_BIDDING);
            //result = SponsorshipBL.updateSponsorship("removed", item.SPR_ITEM_ID);

            database.closeDatabase();
            return "a";
        }
        */

    }
}