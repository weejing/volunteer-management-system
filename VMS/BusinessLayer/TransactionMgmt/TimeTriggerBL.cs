﻿using FluentScheduler;
using System;
using System.Web;
using System.Web.Hosting;
using VMS.BusinessLayer.RelationshipMgmt;
using VMS.DataLayer.TransactionMgmt;

namespace VMS.BusinessLayer.TransactionMgmt
{
    public class  TimeTriggerBL : IJob, IRegisteredObject
    {
        //as of now testing
        //<summary>
        //deals with time-triggered events

        private readonly object _lock = new object();
        private bool _shuttingDown;

        public TimeTriggerBL()
        {
            // Register this job with the hosting environment.
            // Allows for a more graceful stop of the job, in the case of IIS shutting down.
            HostingEnvironment.RegisterObject(this);
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                    return;

                // Do work, son!
                TransactionBL.createRecurringTransaction();
                TransactionBL.revoke_access();
                RelationshipBL.discountAction();
                //TenderingMgmtBL.autoUpdateTenderStatusStart();
                //TenderingMgmtBL.autoUpdateTenderStatusEnd();


            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

    }
}
