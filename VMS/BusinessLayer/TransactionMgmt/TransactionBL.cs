﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VMS.Database;
using VMS.Models.TransactionModels;
using VMS.DataLayer.TransactionMgmt;
using System.Globalization;
using VMS.Models.TenderingModels;
using System.Threading;
using VMS.BusinessLayer.RelationshipMgmt;
using VMS.BusinessLayer.VolBiddingMgmt;
using PayPal.PayPalAPIInterfaceService;
using PayPal.PayPalAPIInterfaceService.Model;

namespace VMS.BusinessLayer.TransactionMgmt
{
    public class TransactionBL
    {

        /// <summary>BBL: Insert new payment</summary>
        public static String insertNewPayment(OrgPayment newPayment)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            String data = TransactionDL.insertPayment(dbConnection, newPayment);
            database.closeDatabase();
            return data;
        }

        public static BillingStatus checkBillStatus(BillingStatus bill)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            BillingStatus data = TransactionDL.checkBillStatus(dbConnection, bill);
            database.closeDatabase();
            return data;
        }

        //TIME TRIGGER EVENTS
        //====================
        public static void revoke_access()
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                //check invoice
                List<String> revoke_list;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                string date = DateTime.Now.AddDays(-1).ToShortDateString();
                revoke_list = TransactionDL.getOrganisationRevokeListDL(dbConnection, date);

                //revoke access
                for (int i = 0; i < revoke_list.Count; i++)
                {
                    TransactionDL.revokeOrgDL(dbConnection, revoke_list[i]);
                }

                database.closeDatabase();
            }
            catch (Exception ex)
            {

            }
        }
        public static void createRecurringTransaction()
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<Transaction> list = TransactionDL.getRecurringOrg(dbConnection);
                if (list != null)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        Transaction item = list[i];
                        //check if paypal profile suspended
                        paypalGetTransaction(item);

                        if (item.PAYPAL_ID != null)
                        {
                            DateTime date = new DateTime();
                            DateTime.TryParse(item.PAYMENT_TIME, out date);
                            string startDate = date.ToShortDateString();

                            int per = 0;
                            Int32.TryParse(item.PER, out per);

                            string endDate = date.AddDays(1).AddMonths(per).ToShortDateString();

                            //the following block is for system release 3
                            if (item.ORG_ID.Equals("A87B5B58-28D0-40CC-8311-89250D8E6D8D"))
                            {
                                endDate = date.AddDays(10).ToShortDateString();
                            }

                            TransactionDL.createRecurringTransaction(dbConnection, item, startDate, endDate);
                        }
                    }
                }
                database.closeDatabase();
            }
            catch (Exception ex)
            {

            }
        }

        public static void paypalGetTransaction(Transaction item)
        {
            try
            {
                string profileID = item.PROFILE_ID.Split(' ')[0];
                //profileID = "I-EBLM97SHR1CX";
                if (profileID == null || profileID.Equals(""))
                {
                    return;
                }
                //string startDate = DateTime.Now.AddDays(-100).ToString("yyyy-MM-ddTHH:mm:ss");
                string startDate = DateTime.Now.AddDays(-10).ToString("yyyy-MM-ddTHH:mm:ss");
                string endDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                TransactionSearchRequestType request = new TransactionSearchRequestType();

                request.StartDate = startDate;
                request.EndDate = endDate;
                request.ProfileID = profileID;

                // Invoke the API
                TransactionSearchReq wrapper = new TransactionSearchReq();
                wrapper.TransactionSearchRequest = request;


                Dictionary<string, string> configurationMap = new Dictionary<string, string>();
                configurationMap.Add("mode", "sandbox");
                configurationMap.Add("account1.apiUsername", "jaslyn94-facilitator_api1.gmail.com");
                configurationMap.Add("account1.apiPassword", "VZ389V858JMW8K6L");
                configurationMap.Add("account1.apiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31A0wlVzdpXMu3x5ybypPeXfN64.xZ");

                // Create the PayPalAPIInterfaceServiceService service object to make the API call
                PayPalAPIInterfaceServiceService service = new PayPalAPIInterfaceServiceService(configurationMap);

                // # API call 
                // Invoke the TransactionSearch method in service wrapper object  
                TransactionSearchResponseType transactionDetails = service.TransactionSearch(wrapper);

                // Check for API return status
                processResponse(service, transactionDetails, item);


            }
            catch (Exception ex)
            {

            }
        }


        private static void processResponse(PayPalAPIInterfaceServiceService service, TransactionSearchResponseType response, Transaction item)
        {
            try
            {
                if (!response.Ack.Equals(AckCodeType.FAILURE))
                {
                    DateTime date = new DateTime();
                    for (int i = 0; i < response.PaymentTransactions.Count; i++)
                    {
                        PaymentTransactionSearchResultType result = response.PaymentTransactions[i];
                        if (result.Status.Equals("Completed"))
                        {
                            DateTime new_date = new DateTime();
                            string temp_timestamp = result.Timestamp;
                            DateTime.TryParse(temp_timestamp, out new_date);

                            if (new_date > date)
                            {
                                item.PAYMENT_TIME = result.Timestamp;
                                item.AMT = result.GrossAmount.value;
                                item.PAYPAL_FEE = result.FeeAmount.value;
                                item.PAYPAL_ID = result.TransactionID;
                                DateTime.TryParse(result.Timestamp, out date);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //PAYMENT PLANS -RELATED CODES
        //===============================
        //add payment plan
        public static List<OrgPayment> addPlanBL(string planName, string amount, string duration, string feature1, string feature2, string feature3)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //html feature to add in 
            string featuresHtml = feature1 + "<br>" + feature2 + "<br>" + feature3;
            TransactionDL.addPlanDL(dbConnection, planName, amount, duration, featuresHtml);
            List<OrgPayment> data = TransactionBL.getAvaliablePaymentPlan("0");
            database.closeDatabase();
            return data;
        }
        //update payment plan
        public static List<OrgPayment> updatePaymentPlanBL(string id, string feature1, string feature2, string feature3)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //html feature to add in 
            string featuresHtml = feature1 + "<br>" + feature2 + "<br>" + feature3;
            TransactionDL.updatePaymentPlanDL(dbConnection, id, featuresHtml);
            List<OrgPayment> planList = TransactionBL.getAvaliablePaymentPlan("0");

            database.closeDatabase();
            return planList;
        }
        //update payment plan status
        public static List<OrgPayment> updatePaymentPlanStatusBL(string id, string status)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //prepare parameter
            status = status.Split(' ')[0];
            if (status.Equals("1"))
            {
                status = "0";
            }
            else
            {
                status = "1";
            }

            TransactionDL.updatePaymentPlanStatusDL(dbConnection, id, status);
            List<OrgPayment> data = TransactionBL.getAvaliablePaymentPlan("0");

            database.closeDatabase();
            return data;
        }

        // <summary>BBL: Get all possible payment plan</summary>
        public static List<OrgPayment> getAvaliablePaymentPlan(string type)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<OrgPayment> planList = TransactionDL.getAvaliablePaymentPlanDL(dbConnection, type);
                if (type.Equals("0"))
                {
                    for (int i = 0; i < planList.Count; i++)
                    {
                        string payID = planList[i].PLAN_TYPE_ID;
                        planList[i].NUM_SUB = TransactionDL.getPlanSubscriberDL(dbConnection, payID);
                    }
                }
                database.closeDatabase();
                return planList;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }

        //TRANSACTIONS  
        //create new transactions
        public static Transaction createNewTransaction(OrgPayment OrgPayment)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                int per = Convert.ToInt32(OrgPayment.PER);

                //determine if this is the first payment of organisation
                if (OrgPayment.FIRST_PAYMENT.Equals("1"))
                {
                    DateTime date = DateTime.Now;
                    OrgPayment.FROM_DATE = date.ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
                    OrgPayment.TO_DATE = date.AddMonths(per).ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
                }
                else
                {
                    //get latest transaction
                    Transaction latestTransaction = TransactionDL.getOrgLatestTransactionDL(dbConnection, OrgPayment.ORG_ID);
                    DateTime lastDate = new DateTime();
                    lastDate = DateTime.Now;

                    if (latestTransaction != null)
                    {
                        DateTime.TryParseExact(latestTransaction.TO_DATE,
                                               "dd'/'MM'/'yyyy",
                                               CultureInfo.InvariantCulture,
                                               DateTimeStyles.None,
                                               out lastDate);
                        //DateTime.TryParse(latestTransaction.TO_DATE, out lastDate);
                        lastDate = lastDate.AddDays(1);
                        if (lastDate < DateTime.Now)
                        {
                            lastDate = DateTime.Now;
                        }
                    }

                    OrgPayment.FROM_DATE = lastDate.ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
                    OrgPayment.TO_DATE = lastDate.AddMonths(per).AddDays(1).ToString("G", CultureInfo.CreateSpecificCulture("en-us"));
                }

                string transaction = TransactionDL.createNewTransaction(dbConnection, OrgPayment);
                if (transaction.Equals("Not Success"))
                {
                    database.closeDatabase();
                    return new Transaction();
                }
                if (OrgPayment.CRM_ID != null)
                {
                    TransactionDL.updateCRMStatusDL(dbConnection, 0, OrgPayment.CRM_ID);
                }

                TransactionDL.updateOrgStatusDL(dbConnection, OrgPayment.ORG_ID);
                Transaction payment = TransactionDL.getOrgLatestTransactionDL(dbConnection, OrgPayment.ORG_ID);
                database.closeDatabase();
                return payment;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        // <summary>BBL: Show transaction list</summary>
        // 0 - show the whole list
        // 1 - show search results
        public static List<OrgPayment> getTransactionsListBL(int retrive, String name, String start0, String end0)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                List<OrgPayment> payment_list = null;

                if (retrive == 0)
                {
                    payment_list = TransactionDL.getAllTransactionListDL(dbConnection);

                }
                else if (retrive == 1 && start0 == null && end0 == null)
                {
                    String start = getDate(start0);
                    String end = getDate(end0);
                    payment_list = TransactionDL.getTransactionsSearchDL(dbConnection, 0, name, null, null);
                }
                else if (retrive == 1)
                {
                    // Format 01/04/2016 to 2016-04-01
                    String start = getDate(start0);
                    String end = getDate(end0);

                    if (name == null)
                    {
                        payment_list = TransactionDL.getTransactionsSearchDL(dbConnection, 1, null, start, end);
                    }
                    else
                    {
                        payment_list = TransactionDL.getTransactionsSearchDL(dbConnection, 2, name, start, end);
                    }
                }
                database.closeDatabase();
                return payment_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: Show individual payment </summary>
        public static OrgPayment getIndividualTransactionBL(String org_pay_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                OrgPayment payment = TransactionDL.getIndividualTransactionDL(dbConnection, org_pay_id);
                database.closeDatabase();
                return payment;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }

        //RELATIONSHIP-RELATED METHODS
        //============================
        //get all organisation list
        public static List<OrganisationDetails> getOrganisationListBL()
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                List<OrganisationDetails> org_list = TransactionDL.getOrganisationListDL(dbConnection);
                database.closeDatabase();
                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        //get organisation search results
        public static List<OrganisationDetails> getOrganisationSearchBL(string name, string email, string type, string check)
        {
            try
            {
                //connect to database and get results
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();
                List<OrganisationDetails> org_list = TransactionDL.getOrganisationSearchDL(dbConnection, name, email, type, check);
                database.closeDatabase();
                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        //get organisation details
        public static OrgPayment getOrgDetailsBL(String org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                OrgPayment orgdetail = TransactionDL.getOrgDetailsDL(dbConnection, org_id);
                database.closeDatabase();
                return orgdetail;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: Show payment related to given organisation</summary>
        public static List<OrgPayment> getOrgTransactionBL(string org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<OrgPayment> org_list = TransactionDL.getOrgTransactionDL(dbConnection, org_id);
                database.closeDatabase();
                return org_list;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: Get latest possible payment plan</summary>
        public static List<CRM> getOrgDiscountDataBL(String org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<CRM> crmList = TransactionDL.getOrgDiscountDataDL(dbConnection, org_id);
                database.closeDatabase();
                return crmList;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: Return CRM Table</summary>
        public static List<CRM> getUpdatedCRMBL(int retrieve, String org_id, String amt, String date, String crm_id, String plan_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                if (retrieve == 0)
                {
                    TransactionDL.insertCRMDL(dbConnection, org_id, amt, date, plan_id);
                }
                else if (retrieve == 2)
                {
                    TransactionDL.deleteCRMDL(dbConnection, crm_id);

                }

                List<CRM> crm_list = TransactionDL.showCRMDL(dbConnection, org_id);
                database.closeDatabase();
                return crm_list;

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: update and return verification</summary>
        public static String updateVerificationStatusBL(string org_id, string verify)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                TransactionDL.updateVerificationStatusDL(dbConnection, org_id, verify);
                String ans = TransactionDL.getVerificationStatusDL(dbConnection, org_id);
                database.closeDatabase();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        // <summary>BBL: Show payment related to given organisation</summary>
        public static List<string> getOrgPlanInfoBL(string org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                string ans1 = TransactionDL.getOrgPlanInfoDL(dbConnection, org_id);
                string ans2 = TransactionDL.getOrgPlanInfoDL(dbConnection, org_id);

                List<string> ans = new List<string>();
                ans.Add(ans1);
                ans.Add(ans2);
                database.closeDatabase();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }
        //get profile ID
        public static string getProfileBL(string org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                Transaction item = TransactionDL.getOrgLatestTransactionDL(dbConnection, org_id);
                database.closeDatabase();
                return item.PROFILE_ID;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }

        //NPO ACTION
        // cancel payment
        public static string cancelPaymentBL(string org_id)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                string ans = TransactionDL.cancelPaymentDL(dbConnection, org_id);
                database.closeDatabase();
                return ans;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("---Issue Start---");
                System.Diagnostics.Debug.WriteLine("---TransactionDL.Class---");
                System.Diagnostics.Debug.WriteLine("---Method: getRecentPaymentListDL---");
                System.Diagnostics.Debug.WriteLine("---Date: " + DateTime.Now.ToString() + " ---");
                System.Diagnostics.Debug.WriteLine(e.StackTrace.ToString());
                System.Diagnostics.Debug.WriteLine("---End---");

                return null;
            }

        }

        //BILLING-RELATED METHODS
        //=========================
        //Pre-condition: 
        // (current_month, current_year, search_type)
        // 0, 0 - current month and year
        //for search type
        //-1 - earlier than that date
        // 0 - exact month
        // 1 - later than that date
        // 2 - show all records
        //TO DO: get all the transactions
        // <summary>BBL: Show billing list</summary>
        public static List<BillingStatus> getBillingStatusListBL(int month, int year, int search_type)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            int current_month = month;
            int current_year = year;

            if (month == 0 && year == 0)
            {
                DateTime now = DateTime.Now;
                current_month = now.Month;
                current_year = now.Year;
            }

            List<BillingStatus> payment_list = TransactionDL.getBillingStatusListDL(dbConnection, current_month, current_year, search_type);
            database.closeDatabase();
            return payment_list;
        }
        //create billing invoice
        public static void create_billing_invoice()
        {
            //create the invoice along with check invoice
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            //generate parameter
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, today.Day);
            String first = month.AddMonths(-1).ToString();

            List<OrgPayment> unpaid = TransactionDL.getDueOrganisationDL(dbConnection, first);

            if (unpaid.Count == 0)
            {
                return;
            }

            //generate billing in sql
            List<String> bill_id_list = new List<String>(); //unused
            for (int i = 0; i < unpaid.Count; i++)
            {
                OrgPayment temp = unpaid[i];
                string org_id = temp.ORG_ID;
                string str_base_time = temp.TO_DATE;
                DateTime base_time = DateTime.Parse(str_base_time);
                string from_sub = base_time.AddSeconds(1).ToString();
                string to_sub;
                if (temp.PLAN_TYPE.Equals("YEARLY"))
                {
                    to_sub = base_time.AddYears(1).AddSeconds(1).ToString();
                }
                else
                {
                    to_sub = base_time.AddMonths(1).AddSeconds(1).ToString();
                }

                string str_temp = TransactionDL.generateBillingDL(dbConnection, org_id, from_sub, to_sub);
                if (!str_temp.Equals("-1"))
                {
                    bill_id_list.Add(str_temp);
                }

            }


            //notify the NPOs method

        }

        //Pre-condition: 
        // (current_month, current_year, search_type)
        // 0, 0 - current month and year
        //for search type
        //-1 - earlier than that date
        // 0 - exact month
        // 1 - later than that date
        // 2 - show all records
        //TO DO: get all the transactions
        // <summary>BBL: Show unpaid billing list</summary>
        public static List<BillingStatus> getUnpaidBillListBL(int month, int year, int search_type)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            int current_month = month;
            int current_year = year;

            if (month == 0 && year == 0)
            {
                DateTime now = DateTime.Now;
                current_month = now.Month;
                current_year = now.Year;
            }

            List<BillingStatus> payment_list = TransactionDL.getUnpaidBillListDL(dbConnection, current_month, current_year, search_type);
            database.closeDatabase();
            return payment_list;
        }
        public static List<OrgPayment> getOrgBillDetailsBL(String org_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<OrgPayment> payment_list = TransactionDL.getOrgBillDetailsDL(dbConnection, org_id);
            database.closeDatabase();
            return payment_list;
        }

        //SUPPORTING METHODS
        //return Date String
        public static String getDate(String date_given)
        {
            if (date_given == null || date_given.Equals(""))
            {
                return "";
            }
            // Format 01/04/2016 to 2016-04-01
            String[] date_format = date_given.Split(' ');

            String day = date_format[0];
            String month = get_month_int(date_format[1]);
            String year = date_format[2];
            return year + "-" + month + "-" + day;
        }
        //get Day in string
        protected static String getday_int(String string_day)
        {
            int int_day = Int32.Parse(string_day);
            if (int_day < 10)
            {
                return "0" + string_day;
            }
            return string_day;
        }
        //get month in strig in numeric forms
        protected static String get_month_int(String string_month)
        {
            switch (string_month)
            {
                case ("Jan"):
                    return "01";
                case ("Feb"):
                    return "02";
                case ("Mar"):
                    return "03";
                case ("Apr"):
                    return "04";
                case ("May"):
                    return "05";
                case ("Jun"):
                    return "06";
                case ("Jul"):
                    return "07";
                case ("Aug"):
                    return "08";
                case ("Sep"):
                    return "09";
                case ("Oct"):
                    return "10";
                case ("Nov"):
                    return "11";
                case ("Dec"):
                    return "12";
            }
            return "-1";
        }

        // <summary>BBL: check if need to change the year. 
        // True: Change Year 
        // False: Year remain the same </summary>
        public static Boolean checkMonthYear(int month, int year)
        {
            if (month > 12)
            {
                return true;
            }
            return false;
        }



        public static string getTimeTriggerEvents(String key)
        {
            try
            {
                bool successful = true;
                switch (key)
                {
                    case "1":
                        TransactionBL.createRecurringTransaction();
                        break;
                    case "2":
                        TransactionBL.revoke_access();
                        break;
                    case "3":
                        RelationshipBL.discountAction();
                        break;
                    case "4":
                        //TenderingMgmtBL.autoUpdateTenderStatusStart();
                        //TenderingMgmtBL.autoUpdateTenderStatusEnd();
                        break;
                    case "5":
                        BiddingTransactionBL.systemDetermineWinner();
                        break;
                    case "6":
                        //wee jing
                        break;
                }

                if (successful)
                {
                    return "success";
                }

            }
            catch (Exception ex)
            {

            }
            return "error";
        }
    }
}