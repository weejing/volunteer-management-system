﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AuditMgmt;
using VMS.Models.AuditMgmtModels;

namespace VMS.BusinessLayer.AuditMgmt
{
    public class AuditMgmtBL
    {
        /// <summary>BBL: get organisation information</summary>
        public static List<AuditTrail> getAuditListSaaS(string connection_str)
        {
            if (connection_str == null || connection_str.Equals(""))
            {
                connection_str = "Console_VMS";
            }
            Database_Connect database = new Database_Connect(connection_str);
            SqlConnection dbConnection = database.getConnection();

            List<AuditTrail> data =  AuditMgmtDL.getAuditListSaaS(dbConnection);

            database.closeDatabase();
            return data;
        }

        //moduleType, moduleFeature, action, description, systemUser, userID
        public static void createAuditObj(int moduleType, int moduleFeatureType, string action, string description, string systemUser, string userID, string connection)
        {
            try
            {
                string moduleName = getModuleName(moduleType);
                string moduleFeature = getModuleFeatureName(moduleFeatureType);
                systemUser = getSystemUserName(systemUser, connection);
                AuditTrail audit = new AuditTrail();
                audit.MODULE_NAME = moduleName;
                audit.MODULE_FEATURE = moduleFeature;
                audit.ACTION = action;
                audit.DESCRIPTION = description;
                audit.SYSTEM_USR = systemUser;
                audit.USR_ID = userID;

                //organisation database string
                if (connection == null || connection.Equals(""))
                {
                    connection = "Console_VMS";
                }
                Database_Connect databaseOrg = new Database_Connect(connection);
                audit.dbConnection = databaseOrg.getConnection();

                AuditMgmtDL.createAuditTrailSaaS(audit);
                databaseOrg.closeDatabase();
            }
            catch (Exception ex)
            {

            }
        }

        public static string getSystemUserName(string systemUser, string connection)
        {
            try
            {
                if (!systemUser.Equals(""))
                {
                    return systemUser;
                }
                if (connection == null || connection.Equals(""))
                {
                    return "Internal";
                }
                if (systemUser.Equals(""))
                {
                    return "NPO";
                }
            } catch (Exception ex)
            {

            }
            return systemUser;
        }

        //note to all: this list is comprehensive. Similar to votc
        public static string getModuleName(int moduleType)
        {
            switch (moduleType)
            {
                case 1:
                    return "Core Infrastructure";
                case 2:
                    return "Event Management";
                case 3:
                    return "Human Resource Management";
                case 4:
                    return "Inventory Management";
                case 5:
                    return "Admin Management";
                case 6:
                    return "Vendor Management";
                case 7:
                    return "Community Management";
                case 8:
                    return "Volunteer Relationship Management";
                case 9:
                    return "Operation Management";
            }
            return "";
        }

        //note to all: this list is comprehensive. Similar to votc
        public static string getModuleFeatureName(int moduleFeatureType)
        {
            switch (moduleFeatureType)
            {
                case 1:
                    return "Account Module";
                case 2:
                    return "Notification Module";
                case 3:
                    return "Programme Module";
                case 4:
                    return "Feedback Module";
                case 5:
                    return "Manpower Module";
                case 6:
                    return "Inventory Control Module";
                case 7:
                    return "Event Logistic Control";
                case 8:
                    return "Dashboard Reporting Module";
                case 9:
                    return "Event Role Module";
                case 10:
                    return "Billing Module";
                case 11:
                    return "Tendering Module";
                case 12:
                    return "Bidding Module";
                case 13:
                    return "Search and Explore Module";
                case 14:
                    return "Portfolio Module";
                case 15:
                    return "Newsfeed Module";
                case 16:
                    return "Volunteer Point Module";
                case 17:
                    return "Volunteer Reward Module";
                case 18:
                    return "SaaS Sales Module";
                case 19:
                    return "VWOs Relationship Module";
                case 20:
                    return "Admin Partnership Module";
                case 21:
                    return "Virtual Inventory Module";
                case 22:
                    return "Attendance Module";
                case 23:
                    return "Feedback Submission Module";
                case 24:
                    return "Admin Module";
            }
            return "";
        }
    }
}