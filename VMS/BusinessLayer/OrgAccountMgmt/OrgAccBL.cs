﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using VMS.AccountMgmt;
using VMS.Database;
using VMS.DataLayer.OrgAccountMgmt;
using VMS.Models.AccountModels;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.OrgAccountMgmt
{
    public class OrgAccBL
    {
        /*Create Account */
        public static string createAccount(OrgEmployee employee)
        {
            Database_Connect database = new Database_Connect(employee.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            //hashed password
            DateTime createDatetimeTemp = DateTime.Now;
            string stringDate = createDatetimeTemp.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
            DateTime regDate = new DateTime();
            regDate = DateTime.ParseExact(stringDate, "MM/dd/yyyy hh:mm:ss.fff tt", null);
            employee.REGISTRATION_DATE = regDate;
            employee.PASSWORD = AuthenticateUserBL.generateHashPassword(employee.PASSWORD + regDate);
            string id= OrgAccDL.createAccount(dbConnection, employee);
            //role access rights
            OrgAccDL.createEmpRoles(dbConnection, id, employee.roleList);
            string resetPwCode = Guid.NewGuid().ToString();
            AccountMgmtDL.updateResetPWCode(dbConnection, "TABLE_ORG_USR", employee.EMAIL, resetPwCode);
            if ((employee.CONNECTION_STRING)=="CONSOLE_VMS") {
                sendEmailtoSetPw("SGServe Management", (employee.URL + "/setPassword.aspx"), resetPwCode, employee.EMAIL, employee.NAME,"login.aspx");
            }
            else {
                sendEmailtoSetPw(employee.CONNECTION_STRING, (employee.URL+"/setPassword"), resetPwCode, employee.EMAIL, employee.NAME,"ologin");
            }
            database.closeDatabase();
            return id;
        }

        /*Update Account detail*/
        public static int updateAccDetail( OrgEmployee emp)
        {
            Database_Connect database = new Database_Connect(emp.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            //delete usr role
            OrgAccDL.deleteUsrRoles(dbConnection, emp.USR_ID);
            //insert usr role
            OrgAccDL.createEmpRoles(dbConnection, emp.USR_ID, emp.roleList);
            int count = OrgAccDL.updateAccDetail(dbConnection, emp);
            database.closeDatabase();
            return count;
        }

        /*Delete Account*/
        public static int deleteAcc(string connection_string, string USR_ID)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int count = OrgAccDL.deleteAcc(dbConnection, USR_ID);
            database.closeDatabase();
            return count;
        }

        /*Get list of employees*/
        public static List<OrgEmployee> getEmployees(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<OrgEmployee>  empList=OrgAccDL.getEmployees(dbConnection);
            foreach (OrgEmployee emp in empList) {
                string roleNames="";
                List<Role> roles = OrgAccDL.getEmpRoles(dbConnection, emp.USR_ID);
                foreach (Role role in roles)
                {
                    roleNames = roleNames + "," + role.ROLE_NAME;
                }
                if (roleNames.StartsWith(","))
                {
                    roleNames = roleNames.Substring(1);
                }
                emp.roleNames = roleNames;
            }
            database.closeDatabase();
            return empList;
        }

        /*get employee detail*/
        public static OrgEmployee getEmpParticular(string connection_string, string USR_ID) {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            OrgEmployee employee = OrgAccDL.getEmpParticular(dbConnection, USR_ID);
            employee.roleList = OrgAccDL.getEmpRoles(dbConnection, USR_ID);
            database.closeDatabase();
            //get organization status
            Database_Connect database2 = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection2 = database2.getConnection();
           employee.ORG_STATUS= OrgAccDL.getOrgStatus(dbConnection2, connection_string);
            database2.closeDatabase();
            return employee;
        }

        /*Search for employees*/
        public static List<OrgEmployee> searchEmployees(string connection_string, string name)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<OrgEmployee>  empList=OrgAccDL.getEmpByNameSearch(dbConnection, name);
            foreach (OrgEmployee emp in empList)
            {
                string roleNames = "";
                List<Role> roles = OrgAccDL.getEmpRoles(dbConnection, emp.USR_ID);
                foreach (Role role in roles)
                {
                    roleNames = roleNames + "," + role.ROLE_NAME;
                }
                if (roleNames.StartsWith(","))
                {
                    roleNames = roleNames.Substring(1);
                }
                emp.roleNames = roleNames;
            }
            database.closeDatabase();
            return empList;
        }

        /*Check existing employee email*/
        public static Boolean checkEmailExist(string connection_string, string EMAIL)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            //use the common method in AccountMgmtDL to cehck
            Boolean exist = OrgAccDL.checkEmailExist(dbConnection, "TABLE_ORG_USR", EMAIL);
            database.closeDatabase();
            return exist;
        }

        /*Employee request to reset password*/
        public static Boolean reqResetPw(string connection_string,string url, string email)
        {
            Boolean reqSuccess = false;
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            if (checkEmailExist(connection_string,email) == true)
            {
                //update resetpw_code
                string resetPwCode = Guid.NewGuid().ToString();
                AccountMgmtDL.updateResetPWCode(dbConnection, "TABLE_ORG_USR", email, resetPwCode);
                SendResetPwEmail(connection_string,url, resetPwCode, email);
                reqSuccess = true;
            }
            database.closeDatabase();
            return reqSuccess;
        }

        /*Employee reset password */
        public static Boolean resetPW(string connection_string, string reset_code, string newpassword)
        {   //validate reset_code is it GUID
            Guid guidOutput;
            bool isGuid = Guid.TryParse(reset_code, out guidOutput);
            Boolean resetPwSuccess = false;
            if (isGuid == true)
            {
                Database_Connect database = new Database_Connect(connection_string);
                SqlConnection dbConnection = database.getConnection();
                string salt = OrgAccDL.getSaltByResetCode(dbConnection, "TABLE_ORG_USR",reset_code);
                if (salt != "")
                {
                    string newHashedPw = AuthenticateUserBL.generateHashPassword(newpassword + salt);
                    AccountMgmtDL.updateResetPW(dbConnection, "TABLE_ORG_USR", reset_code, newHashedPw);
                    resetPwSuccess = true;
                }
                database.closeDatabase();
            }
            return resetPwSuccess;
        }

        /*Employee change password*/
        public static Boolean changePw(string connection_string, String email, string newpassword, string oldpassword)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            String salt = OrgAccDL.getSalt(dbConnection, "TABLE_ORG_USR", email);
            String hashedNewPassWord = AuthenticateUserBL.generateHashPassword(newpassword + salt);
            String hashedOldPassWord = AuthenticateUserBL.generateHashPassword(oldpassword + salt);
            Boolean changed = AccountMgmtDL.updatePW(dbConnection, "TABLE_ORG_USR", email, hashedNewPassWord, hashedOldPassWord);
            database.closeDatabase();
            return changed;

        }

        private static void sendEmailtoSetPw(string connection_string, string url, string activationCode, string email, string name,string loginLink)
        {
            using (MailMessage mm = new MailMessage("admin@sgserve.com", email))
            {
                mm.Subject = connection_string + "(VMS) - Set Password";
                string body = "Hello " + name + ",";
                body += "<br /><br />Your VMS account has been set up.";
                body += "<br />Please click <a href = " + url + "?ResetPwCode=" + activationCode + ">here</a> to set your password";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.zoho.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("admin@sgserve.com", "sgserve1991_iAM");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        private static void SendResetPwEmail(string connection_string,string url, string activationCode, string email)
        {
            using (MailMessage mm = new MailMessage("admin@sgserve.com", email))
            {
                mm.Subject = connection_string+"(VMS) - Reset Password";
                string body = "Hello, ";
                body += "<br /><br />Please click the following link to reset password.";
                body += "<br />Click <a href = " + url + "?ResetPwCode=" + activationCode + ">here</a> to reset password.";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.zoho.com";
                smtp.EnableSsl = true;

                NetworkCredential NetworkCred = new NetworkCredential("admin@sgserve.com", "sgserve1991_iAM");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

    }
}