﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VMS.DataLayer.ReportingMgmt;
using VMS.Database;
using System.Data.SqlClient;
using VMS.Models.ReportingModels;
using VMS.DataLayer.TransactionMgmt;
using System.Threading.Tasks;
using System.Threading;

namespace VMS.BusinessLayer.ReportingMgmt
{
    public class ReportingBL
    {
        //Account Management
        //============================
        // <summary>BBL: Show this volunteer high level view</summary>
        public static AVolunteerReport getVolunteerDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            AVolunteerReport data = ReportingDL.getVolunteerDataDL(dbConnection);
            database.closeDatabase();
            return data;
        }

        //VOLUNTEER REPORTING
        //get volunteer sign up data
        public static List<ReportVolunteers> getVolNumDataBL(string orgID)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            DateTime date = DateTime.Now;
            date = date.AddDays(-30);
            string dateStr = date.ToString("MM/dd/yyyy");
            List<ReportVolunteers> ans = ReportingDL.getVolNumDataDL(dbConnection, dateStr, orgID);

            database.closeDatabase();
            return ans;
        }

        //get volunteer interest breakdown
        public static List<ReportVolunteers> getVolInterestDataBL(string orgID)
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<ReportVolunteers> list = ReportingDL.getVolInterestDataDL(dbConnection, orgID);
                List<ItemSummary> interest = ReportingDL.getInterestListDataDL(dbConnection);

                for (int i = 0; i < list.Count; i++)
                {
                    ReportVolunteers item = list[i];
                    string interestItem = item.VOLUNTEER_INTEREST;
                    ItemSummary itemToRemove = interest.Single(r => r.ITEM_DESC.Equals(interestItem));
                    interest.Remove(itemToRemove);
                }

                if (interest.Count > 0)
                {
                    for (int i = 0; i < interest.Count; i++)
                    {
                        string interestDesc = interest[i].ITEM_DESC;
                        ReportVolunteers vItem = new ReportVolunteers();
                        vItem.VOLUNTEER_INTEREST_CAT = interest[i].ITEM_NAME;
                        vItem.VOLUNTEER_INTEREST = interest[i].ITEM_DESC;
                        vItem.VOLUNTEER_COUNT = "0";
                        list.Add(vItem);
                    }
                }

                database.closeDatabase();
                return list;
            }
            catch (Exception ex)
            {

            }
            return null;
        }


        //Transaction Management
        //============================
        // <summary>BBL: Show this volunteer high level view</summary>
        public static List<ReportTransaction> getTransactionDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            DateTime date = DateTime.Now;
            date = date.AddMonths(-1);
            string dateStr = date.ToString("MM/dd/yyyy");

            List<ReportTransaction> data = ReportingDL.getTransactionDataDL(dbConnection, dateStr);

            database.closeDatabase();
            return data;
        }
        public static List<ReportNPO> getOrgNumDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            ReportNPO first = ReportingDL.getOrgNumDataDL(dbConnection);
            List<ReportNPO> list = ReportingDL.getOrgTypeDataDL(dbConnection);

            for (int i = 0; i < list.Count; i++)
            {
                ReportNPO item = list[i];
                ReportingDL.getOrgVolNumDataDL(dbConnection, item);
            }
            list.Insert(0, first);
            database.closeDatabase();
            return list;
        }

        //TRANSACTION-RELATED METHODS
        //============================
        // <summary>BBL: Show this month summary figures</summary>
        public static List<int> getPaymentTrendBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            DateTime now = DateTime.Now;
            int current_month = now.Month;
            int current_year = now.Year;
            List<int> trend_data = new List<int>();

            for (int i = 0; i < 12; i++)
            {
                trend_data.Add(TransactionDL.getPaymentTrendDL(dbConnection, current_month, current_year));
                current_month++;
                //if (checkMonthYear(current_month, current_year))
                //{
                //    current_month = 1;
                //    current_year++;
                //}
            }

            database.closeDatabase();
            return trend_data;
        }

        //EVENT RELATED METHOD
        public static List<ReportEvent> getEventDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<string> conn = ReportingDL.getNPOConnect(dbConnection);
            List<ReportEvent> list = ReportingBL.createEventObjBL(0, 30);
            database.closeDatabase();

            for (int i = 0; i < conn.Count; i++)
            {
                Database_Connect database_npo = new Database_Connect(conn[i]);
                SqlConnection dbConnection_npo = database_npo.getConnection();

                for (int j = 0; j < list.Count; j++)
                {
                    ReportEvent item = list[j];
                    string message = ReportingDL.getEventObjDL(dbConnection_npo, item);
                    if (message.Equals("error"))
                    {
                        break;
                    }
                }

                database_npo.closeDatabase();
            }

            return list;
        }
        public static List<ReportEvent> createEventObjBL(int type, int days)
        {
            List<ReportEvent> list = new List<ReportEvent>();
            if (type == 0)
            {
                for (int i = 0; i < days; i++)
                {
                    ReportEvent item = new ReportEvent();

                    DateTime date = DateTime.Now;
                    date = date.AddDays(-days + i);

                    item.DATE = date;
                    item.EVENT_DATE = date.ToString("dd'/'MM");
                    item.EVENT_CREATED = 0;

                    list.Add(item);
                }
            }
            return list;
        }
        //event turn up
        public static List<ReportEvent> getEventTurnUpDataBL()
        {
            try
            {
                Database_Connect database = new Database_Connect("Console_VMS");
                SqlConnection dbConnection = database.getConnection();

                List<string> conn = ReportingDL.getNPOConnect(dbConnection);
                database.closeDatabase();

                List<ReportEvent> list = new List<ReportEvent>();

                for (int i = 0; i < conn.Count; i++)
                {
                    getEventTurnUpDetailsDataBL(conn[i], list);
                }

                return list;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static void getEventTurnUpDetailsDataBL(string conn, List<ReportEvent> list)
        {
            try
            {
                Database_Connect database_npo = new Database_Connect(conn);
                SqlConnection dbConnection_npo = database_npo.getConnection();

                DateTime dateT = DateTime.Now;
                dateT = dateT.AddDays(-30);
                string date = dateT.ToShortDateString();
                ReportingDL.getEventTurnUpDataDL(dbConnection_npo, list, date);

                database_npo.closeDatabase();
            }
            catch (Exception ex)
            {

            }
        }
        //npo reporting
        public static List<ReportEvent> getNPOEventDataBL(string connectionStr)
        {
            DateTime date = DateTime.Now;
            string dateStr = date.ToShortDateString();

            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<ItemSummary> eventCause = ReportingDL.getEventCauseList();
            List<ReportEvent> list = ReportingDL.getGeneralEventDataDL(dbConnection, dateStr);
            database.closeDatabase();


            Database_Connect database_npo = new Database_Connect(connectionStr);
            SqlConnection dbConnection_npo = database_npo.getConnection();
            ReportingDL.getNPOEventDataDL(dbConnection_npo, list, eventCause);
            database_npo.closeDatabase();

            return list;
        }
        public static List<ReportEvent> getNPOEventTurnUpBL(string orgID, string connectionStr)
        {
            Database_Connect database_npo = new Database_Connect(connectionStr);
            SqlConnection dbConnection_npo = database_npo.getConnection();

            string date = DateTime.Now.AddMonths(-6).ToShortDateString();
            List<ReportEvent> list = ReportingDL.getEventTurnOut1DL(dbConnection_npo, date);
            database_npo.closeDatabase();
            return list;
        }

        //VENDOR
        public static List<ReportVendor> getVendorDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            List<ReportVendor> list = ReportingDL.getVendorDataDL(dbConnection);
            ReportingDL.getVendorSponsoredDL(dbConnection, list);
            ReportingDL.getVendorRatingDL(dbConnection, list);
            database.closeDatabase();

            return list;
        }


        public static List<ReportOverview> getOverviewDataBL()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();

            string date = DateTime.Now.AddDays(-30).ToShortDateString();

            List<ReportOverview> vendor_list = ReportingDL.getOverviewDataDL_vendor(dbConnection, date);
            List<ReportOverview> npo_list = ReportingDL.getOverviewDataDL_npo(dbConnection, date);
            List<ReportOverview> vol_list = ReportingDL.getOverviewDataDL_volunteer(dbConnection, date);

            List<ReportOverview> report = new List<ReportOverview>();
            DateTime start = DateTime.Now;
            for (int i = 0; i < 30; i++)
            {
                string datetemp = start.ToString("MM-dd-yy");
                start = start.AddDays(1);

                ReportOverview item = new ReportOverview();
                item.DATE = datetemp;
                
                ReportOverview vendor = vendor_list.Find(x => x.DATE.Equals(datetemp));
                ReportOverview npo = vendor_list.Find(x => x.DATE.Equals(datetemp));
                ReportOverview vol = vendor_list.Find(x => x.DATE.Equals(datetemp));

                if (vendor != null)
                {
                    item.VENDOR_NUM = vendor.NUM;
                }
                if (npo != null)
                {
                    item.ORG_NUM = vendor.NUM;
                }
                if (vol != null)
                {
                    item.VOL_NUM = vendor.NUM;
                }
                report.Add(item);
            }

            database.closeDatabase();
            //ReportVendor item = list.Find(x => x.USR_ID.Equals(name));
            System.Diagnostics.Debug.Write("database.closeDatabase();");
            return report;
            System.Diagnostics.Debug.Write("report");
        }
    }
}