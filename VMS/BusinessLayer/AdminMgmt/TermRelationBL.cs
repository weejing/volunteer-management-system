﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class TermRelationBL
    {

        public static TermRelation getTermRelByTermID(string term_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TermRelation termRelation = TermRelationDL.getTermRelByTermID(dbConnection, term_id);
            database.closeDatabase();
            return termRelation;
        }


        public static List<TermRelation> getTermRelListByVocab(string vocab_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TermRelation>  termRelList =TermRelationDL.getTermRelListByVocab(dbConnection, vocab_id);
            database.closeDatabase();
            return termRelList;
        }

        public static List<TermRelation> getTermRelListByEntityId(string entity_id, string entity_type)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TermRelation> termRelList= TermRelationDL.getTermRelListByEntityId(dbConnection, entity_id, entity_type);
            database.closeDatabase();
            return termRelList;
        }

        public static List<TermRelation> getTermRelListByEntityIdAndVocab(string entity_id, string entity_type,int vocab_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<TermRelation> termRelList = TermRelationDL.getTermRelListByEntityIdAndVocab(dbConnection, entity_id, entity_type, vocab_id);
            database.closeDatabase();
            return termRelList;
        }
        /*Get term relation*/
        public static TermRelation getTermRelation(string term_relation_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TermRelation termRel = TermRelationDL.getTermRelation(dbConnection, term_relation_id);
            database.closeDatabase();
            return termRel;
        }

        /*Create term relation */
        public static int createTermRelation(TermRelation termRelation)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int id = TermRelationDL.createTermRelation(dbConnection, termRelation);
            database.closeDatabase();
            return id;
        }

        public static void createRelListForSameEntity(List<TermRelation> termRelationList, string entity_type, string entity_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TermRelationDL.createRelListForSameEntity(dbConnection, termRelationList, entity_type, entity_id);
            database.closeDatabase();
        }

        /*Delete relation*/
        public static int deleteTermRelation(string term_relation_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = TermRelationDL.deleteTermRelation(dbConnection, term_relation_id);
            database.closeDatabase();
            return count;
        }

        public static int deleteTermRelByEntityID(string entity_id,string entity_type)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = TermRelationDL.deleteTermRelByEntityID(dbConnection, entity_id, entity_type);
            database.closeDatabase();
            return count;
        }
    }
}