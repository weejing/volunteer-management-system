﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class BadgeBL
    {
        public static List<Badge> getMinHourBadgeList()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<Badge> badgeList = BadgeDL.getMinHourBadgeList(dbConnection);
            database.closeDatabase();
            return badgeList;
        }

        public static List<Badge> getPublishedBadgeList()
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<Badge> badgeList = BadgeDL.getPublishedBadgeList(dbConnection);
            database.closeDatabase();
            return badgeList;
        }

        public static Badge getBadge(string badge_id)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            Badge badge = BadgeDL.getBadge(dbConnection, badge_id);
            database.closeDatabase();
            return badge;
        }

        public static string createBadge(Badge badge)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            string id = BadgeDL.createBadge(dbConnection, badge);
            database.closeDatabase();
            return id;
        }

        

            public static int updateStatus(Badge badge)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count =BadgeDL.updateStatus(dbConnection, badge);
            database.closeDatabase();
            return count;
        }

        public static int updateBadge(Badge badge)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count= BadgeDL.updateBadge(dbConnection, badge);
            database.closeDatabase();
            return count;
        }

        public static int updateBadgeNoPhoto(Badge badge)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = BadgeDL.updateBadgeNoPhoto(dbConnection, badge);
            database.closeDatabase();
            return count;
        }

        public static Boolean deleteBadge(string badge_id)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            Boolean deleted = BadgeDL.deleteBadge(dbConnection, badge_id);
            database.closeDatabase();
            return deleted;
        }
    }
}