﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class RoleBL
    {
        /*Get list of roles within the organization*/
        public static List<Role> getRoleList(String CONNECTION_STRING)
        {
            Database_Connect database = new Database_Connect(CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            List<Role>  roleList=RoleDL.getRoleList(dbConnection);
            database.closeDatabase();
            return roleList;
        }

        /*Get role*/
        public static Role getRole(string role_id, String CONNECTION_STRING)
        {
            Database_Connect database = new Database_Connect(CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            Role role= RoleDL.getRole(dbConnection, role_id);
           role.roleModList= RoleDL.getRoleModList(dbConnection, role.ROLE_ID);
            database.closeDatabase();
            return role;
        }

        /*Create term */
        public static string createRole(Role role)
        {

            Database_Connect database = new Database_Connect(role.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            string id = RoleDL.createRole(dbConnection, role);
            database.closeDatabase();
            return id;
        }

        /*Update role*/
        public static int updateRole(Role role)
        {
            Database_Connect database = new Database_Connect(role.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            int count = RoleDL.updateRole(dbConnection, role);
            database.closeDatabase();
            return count;
        }

        /*Delete role*/
        public static int deleteRole( Role role)
        {
            Database_Connect database = new Database_Connect(role.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            int count = RoleDL.deleteRole(dbConnection, role.ROLE_ID);
            database.closeDatabase();
            return count;
        }

        #region Role <-> Modules
        
        /*Get role pages access rights*/
        public static List<Page> getPageAccessRights(string usr_id, string CONNECTION_STRING)
        {
            Database_Connect database = new Database_Connect(CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            List<Page> pageList= RoleDL.getPageAccessRights(dbConnection, usr_id);
            database.closeDatabase();
            return pageList;
        }

        public static List<Page> getBillPage(string CONNECTION_STRING)
        {
            Database_Connect database = new Database_Connect(CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            List<Page> pageList = RoleDL.getBillPage(dbConnection);
            database.closeDatabase();
            return pageList;
        }

        /*Get role mods access rights*/
        public static List<Module> getRoleModList(string role_id,string CONNECTION_STRING)
        {
            Database_Connect database = new Database_Connect(CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            List<Module> roleModList= RoleDL.getRoleModList(dbConnection, role_id);
            database.closeDatabase();
            return roleModList;
        }

        /*Assign role access rights to the selected mods */
        public static void createRoleMod(Role role)
        {
            Database_Connect database = new Database_Connect(role.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            RoleDL.deleteRoleMods(dbConnection, role.ROLE_ID);
            RoleDL.createRoleMod(dbConnection, role);
            database.closeDatabase();
        }

        #endregion
    }
}