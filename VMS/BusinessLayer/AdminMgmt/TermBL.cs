﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class TermBL
    {
        #region Interest form 
        public static int getVocabID(String vocab_name)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int id = VocabDL.getVocabID(dbConnection, vocab_name);
            database.closeDatabase();
            return id;
        }

        public static List<Vocab> getVocabList()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<Vocab> vocabList= VocabDL.getVocabForIntFormList(dbConnection);
            database.closeDatabase();
            return vocabList;
        }

        public static List<Term> getTermListByVocabName(string vocab_name)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int vocab_id = getVocabID(vocab_name);
            List<Term> termList= TermDL.getSpecificTermList(dbConnection, vocab_id);
            database.closeDatabase();
            return termList;
        }

        public static List<Term> getTermList(int vocab_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<Term> termList= TermDL.getSpecificTermList(dbConnection, vocab_id);
            database.closeDatabase();
            return termList;
        }

        /*Get term*/
        public static Term getTerm(string term_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            Term term = TermDL.getTerm(dbConnection, term_id);
            database.closeDatabase();
            return term;
        }

        /*Create term */
        public static Boolean createTerm(Term term)
        {
            Boolean status;
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            //check for same name 
            Boolean existName = TermDL.getTermByName(dbConnection, term.TERM_NAME);
            if (existName == false)
            {
                string id = TermDL.createTerm(dbConnection, term);
                string vocabName = "Event role";
                int vocab_id = VocabDL.getVocabID(dbConnection, vocabName);
                if (vocab_id == term.VOCAB_ID)
                {
                    EventRole eventRole = new EventRole();
                    eventRole.ROLE_ID = Convert.ToInt32(id);
                    eventRole.TRAITS = "XXXX";
                    eventRole.TERM_SKILL_IDS = null;
                    eventRole.CONNECTION_STRING = "CONSOLE_VMS";
                    EventRoleBL.createEvtRoleTraits(eventRole);
                }
                status = true;
            }
            else {
                status = false;
            }
            database.closeDatabase();
            return status;
        }

        /*Update term*/
        public static int updateTerm(Term term)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = TermDL.updateTerm(dbConnection, term);
            database.closeDatabase();
            return count;
        }

        /*Delete term*/
        public static Boolean deleteTerm(int term_id)
        {
            Boolean deleted;
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            TermRelation relation=TermRelationBL.getTermRelByTermID(term_id.ToString());
            if (relation !=null) {
                deleted = false;
            } else {
                TermDL.deleteTerm(dbConnection, term_id);
                deleted = true;
            }
            database.closeDatabase();
            return deleted;
        }
        #endregion
    }
}