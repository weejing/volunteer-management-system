﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class PageBL
    {
        /*Get parent node*/
        public static List<Module> getModuleList(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Module> modList = PageDL.getModuleList(dbConnection);
            database.closeDatabase();
            return modList;
        }

        /*Get list of pages*/
        public static List<Page> getPageList(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<Page> pageList = PageDL.getPageList(dbConnection);
            database.closeDatabase();
            return pageList;
        }
    }
}