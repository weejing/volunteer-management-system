﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class EventRoleBL
    {
        static String concat_skill_name = null;
        /*Get all event role*/
        public static List<EventRole> getEvtRoleList(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int vocab_id = TermBL.getVocabID("event role");
            // get skill master lists
            List<Term> allMasterSkillList = TermBL.getTermListByVocabName("skill");
            List<EventRole> evtRoleList= EventRoleDL.getEvtRoleList(dbConnection, vocab_id);
            foreach (EventRole evtRole in evtRoleList) {
                evtRole.CONCAT_SKILL_NAME= strSkillIDstoSkillName(evtRole.TERM_SKILL_IDS, allMasterSkillList);
            }
            database.closeDatabase();
            return evtRoleList;
        }

        /*Get event role traits*/
        public static EventRole getEvtRoleSkills(int term_id,string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            EventRole evtRole= EventRoleDL.getEvtRole(dbConnection, term_id);
            List<Term> roleSkills=getRoleSkills(evtRole.TERM_SKILL_IDS);
            evtRole.EVTROLESKILLLIST = roleSkills;
            evtRole.CONCAT_SKILL_NAME = concat_skill_name;
            return evtRole;
        }

        /*Update event role traits*/
        public static int updateEvtRole(EventRole evtRole)
        {
            int count;
            Database_Connect database = new Database_Connect(evtRole.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            evtRole.TERM_SKILL_IDS = listToStrSkillID(evtRole.EVTROLESKILLLIST);
            EventRole er = getEvtRoleSkills(evtRole.ROLE_ID, evtRole.CONNECTION_STRING);
            count= EventRoleDL.updateEvtRole(dbConnection, evtRole);
            database.closeDatabase();
            return count;
        }

        public static List<Term> getRoleSkills(string role_skill_ids) {
            concat_skill_name = "";
            //get all skills
            List<Term> allMasterSkillList= TermBL.getTermListByVocabName("skill");
            List<Term> roleSkillList=new List<Term>();
            if (role_skill_ids !=null) {
                string[] skillidArr = role_skill_ids.Split(',');
                foreach (string skillid in skillidArr) {
                    foreach (Term skillMaster in allMasterSkillList) {
                        if (skillMaster.TERM_ID.ToString().Equals(skillid)) {
                            roleSkillList.Add(skillMaster);
                            //add into concat_skill_name
                            concat_skill_name = concat_skill_name + "," + skillMaster.TERM_NAME;
                        }
                    }
                }
                if (concat_skill_name.StartsWith(","))
                {
                    concat_skill_name = concat_skill_name.Substring(1);
                }
            }
            return roleSkillList;          
        }


        public static string listToStrSkillID(List<Term> allRoleSkillList) {
            string concat_skill_ids = "";
            if (allRoleSkillList != null) {
                foreach (Term skill in allRoleSkillList) {
                    concat_skill_ids = concat_skill_ids + "," + skill.TERM_ID;
                }
                if (concat_skill_ids.StartsWith(","))
                {
                    concat_skill_ids = concat_skill_ids.Substring(1);
                }
            }
            return concat_skill_ids;
        }

        /*Covert string of concacted skill_ids to concated skill name*/
        public static string strSkillIDstoSkillName(string concated_skill_id, List<Term> allMasterSkillList)
        {
            string concat_skill_name = "";
            string[] skillidArr = concated_skill_id.Split(',');
            if (concated_skill_id != null || concated_skill_id !="")
            {
                foreach (string skillid in skillidArr)
                {
                    foreach (Term skill in allMasterSkillList)
                    {
                        if (skill.TERM_ID.ToString().Equals(skillid))
                        {
                            concat_skill_name = concat_skill_name + "," + skill.TERM_NAME;
                        }
                    }
                }
                if (concat_skill_name.StartsWith(","))
                {
                    concat_skill_name = concat_skill_name.Substring(1);
                }
            }
            return concat_skill_name;
        }

        /*Create event role traits & skills */
        public static void createEvtRoleTraits(EventRole evtRole)
        {
            Database_Connect database = new Database_Connect(evtRole.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            EventRoleDL.createEvtRoleTraits(dbConnection, evtRole);
            database.closeDatabase();
        }

        public static List<EventRole> getEvtRolesID(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<EventRole> evtRoleList =EventRoleDL.getEvtRolesID(dbConnection);
            database.closeDatabase();
            return evtRoleList;
        }
        #region Organiser Event Role
        //Organiser get event roles (org copy + those that org have not modified from master copy)
        public static List<EventRole> OrgGetEvtRoleList(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            //get OneThird eventroleskills
            List<EventRole> oneThirdEventRoles = getEvtRoleList("CONSOLE_VMS");
            //get Org eventroleskills
            List<EventRole> orgEventRolesID = getEvtRolesID(connection_string);
            //replace onethird eventroleskills with existing record in org eventroleskills
            foreach (EventRole orgEventRole in orgEventRolesID) 
            {
                foreach (EventRole oneThirdEventRole in oneThirdEventRoles)
                {
                    if (oneThirdEventRole.ROLE_ID== orgEventRole.ROLE_ID) {
                        oneThirdEventRole.TRAITS = orgEventRole.TRAITS;
                        oneThirdEventRole.TERM_SKILL_IDS = orgEventRole.TERM_SKILL_IDS;
                    }
                }
              }
            //inner join with term table to get event role name & skill name
            // get skill master lists
            List<Term> allMasterSkillList = TermBL.getTermListByVocabName("skill");
            foreach (EventRole evtRole in oneThirdEventRoles)
            {
                evtRole.CONCAT_SKILL_NAME = strSkillIDstoSkillName(evtRole.TERM_SKILL_IDS, allMasterSkillList);
            }
            database.closeDatabase();
            return oneThirdEventRoles;

        }

        public static int OrgUpdateEvtRole(EventRole evtRole)
        {
            int count=0;
            Database_Connect database = new Database_Connect(evtRole.CONNECTION_STRING);
            SqlConnection dbConnection = database.getConnection();
            evtRole.TERM_SKILL_IDS = listToStrSkillID(evtRole.EVTROLESKILLLIST);
            //check if record exist in organiser table not, if yes insert, else update
            EventRole role= EventRoleDL.getEventRoleSkill(dbConnection, evtRole.ROLE_ID);
            if (role.ROLE_ID.ToString().Equals("0"))
            {
                //insert
                EventRoleDL.createEvtRoleTraits(dbConnection, evtRole);
            }
            else {
                count = EventRoleDL.updateEvtRole(dbConnection, evtRole);
            }
            database.closeDatabase();
            return count;
        }

        public static EventRole OrgGetEvtRoleSkills(int term_id, string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            //get from organiser table
            EventRole role = EventRoleDL.getEventRoleSkill(dbConnection, term_id);
            if (role.ROLE_ID.ToString().Equals("0"))
            {
                //get from onethird list
                role = getEvtRoleSkills(term_id, "CONSOLE_VMS");
            } else {
                role.ROLE_NAME = TermBL.getTerm(term_id.ToString()).TERM_NAME;
                role.EVTROLESKILLLIST = getRoleSkills(role.TERM_SKILL_IDS);
                role.CONCAT_SKILL_NAME = concat_skill_name;
            }
            database.closeDatabase();
            return role;
        }
        #endregion
    }
}