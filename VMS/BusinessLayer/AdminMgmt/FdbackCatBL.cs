﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class FdbackCatBL
    {
        public static List<FdbackCat> getFdbackCatList(string connection_string)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            List<FdbackCat> catList = FdbackCatDL.getFdbackCatList(dbConnection);
            database.closeDatabase();
            return catList;
        }

        public static FdbackCat getCat(string connection_string,string cat_id)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            FdbackCat cat = FdbackCatDL.getCat(dbConnection, cat_id);
            database.closeDatabase();
            return cat;
        }

        public static Boolean createCat(string connection_string, FdbackCat cat)
        {
            Boolean created=false;
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            Boolean existName = FdbackCatDL.getCatByName(dbConnection, cat.CATEGORY);
            if (existName == false) {             
                FdbackCatDL.createCat(dbConnection, cat);
                created = true;
            }          
            database.closeDatabase();
            return created;
        }



        public static int updateCat(string connection_string, FdbackCat cat)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int count = FdbackCatDL.updateCat(dbConnection, cat);
            database.closeDatabase();
            return count;
        }

        public static int deleteCat(string connection_string,string cat_id)
        {
            Database_Connect database = new Database_Connect(connection_string);
            SqlConnection dbConnection = database.getConnection();
            int deleted = FdbackCatDL.deleteCat(dbConnection, cat_id);
            database.closeDatabase();
            return deleted;
        }
    }
}