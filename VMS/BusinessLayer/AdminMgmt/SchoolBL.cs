﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class SchoolBL
    {
        /*DAL: Get list of schools*/
        public static List<School> getSchools(string SCH_LVL)
        { 
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<School> schList =SchoolDL.getSchools(dbConnection, SCH_LVL);
            database.closeDatabase();
            return schList;
        }

        /*Get school name*/
        public static School getSchByID(string schl_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            School sch = SchoolDL.getSchByID(dbConnection, schl_id);
            database.closeDatabase();
            return sch;
        }

        /*Search list of schools*/
        public static List<School> searchSchools(string SCH_LVL, string SCHL_NAME)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<School> schList =SchoolDL.searchSchools(dbConnection, SCH_LVL, SCHL_NAME);
            database.closeDatabase();
            return schList;
        }


        public static string createSchool(School school)
        {
            string id;
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            string lvlid = SchoolDL.getLvlIDByName(dbConnection, school.SCH_LVL);
            school.LVL_OF_EDU_ID = lvlid;
            id =SchoolDL.createSchool(dbConnection, school);
            database.closeDatabase();
            return id;
        }


        public static int editSchool(School school)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            string lvlid = SchoolDL.getLvlIDByName(dbConnection, school.SCH_LVL);
            school.LVL_OF_EDU_ID = lvlid;
            int count= SchoolDL.editSchool(dbConnection, school);
            database.closeDatabase();
            return count;
        }


        public static int deleteSchool(string schl_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count = SchoolDL.deleteSchool(dbConnection, schl_id);
            database.closeDatabase();
            return count;
        }


    }
}