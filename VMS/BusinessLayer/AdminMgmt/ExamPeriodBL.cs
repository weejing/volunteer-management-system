﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class ExamPeriodBL
    {
        public static string createExamPeriod(ExamPeriod examPeriod)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            string lvlid = SchoolDL.getLvlIDByName(dbConnection, examPeriod.SCH_LVL);
            examPeriod.LVL_OF_EDU_ID = lvlid;
            string id = ExamPeriodDL.createExamPeriod(dbConnection, examPeriod);
            database.closeDatabase();
            return id;           
        }

        /*Update exam detail*/
        public static int updateExamPeriod(ExamPeriod examPeriod)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            string lvlid = SchoolDL.getLvlIDByName(dbConnection, examPeriod.SCH_LVL);
            examPeriod.LVL_OF_EDU_ID = lvlid;
            int count = ExamPeriodDL.updateExamPeriod(dbConnection, examPeriod);
            database.closeDatabase();
            return count;
        }

        /*Delete exam record*/
        public static int deleteExamPeriod(string exam_period_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            int count= ExamPeriodDL.deleteExamPeriod(dbConnection, exam_period_id); ;
            database.closeDatabase();
            return count;
        }

        /*get exam detail*/
        public static ExamPeriod getExamPeriod(string exam_period_id)
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            ExamPeriod examPeriod = ExamPeriodDL.getExamPeriod(dbConnection, exam_period_id);
            database.closeDatabase();
            return examPeriod;
        }

        /*Get list of exam details*/
        public static List<ExamPeriod> getExamPeriodList()
        {
            Database_Connect database = new Database_Connect("Console_VMS");
            SqlConnection dbConnection = database.getConnection();
            List<ExamPeriod> examList=ExamPeriodDL.getExamPeriodList(dbConnection);
            database.closeDatabase();
            return examList;
        }
    }
}