﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using VMS.Database;
using VMS.DataLayer.AdminMgmt;
using VMS.EventMgmt;
using VMS.Models.AdminModels;

namespace VMS.BusinessLayer.AdminMgmt
{
    public class VolBadgeBL
    {
        //check to issue badge : return newlyadded badge to show user their new badge
        public static List<Badge> issueBadges(string userId)
        {
            Database_Connect database = new Database_Connect("CONSOLE_VMS");
            SqlConnection dbConnection = database.getConnection();
            //get volunteer joined events
            int volRegUniqueEvtCount = VolBadgeDL.getVolRegEvtCount(dbConnection, userId);
            List<KeyValuePair<String, List<KeyValuePair<String,String>>>> listOfMyEvents = VolBadgeDL.getVolEvents(dbConnection, userId, volRegUniqueEvtCount);
           
            List<Thread> listOfThreads = new List<Thread>();
            Semaphore mutex = new Semaphore(1, 1);
             int totalhours=0;
            List<VolEvtCause> causeCountList = new List<VolEvtCause>();
            foreach (KeyValuePair<String, List<KeyValuePair<String, String>>> myevent in listOfMyEvents)
            {
                Thread thread = new Thread(delegate ()
                {   //go to EACH database instance
                 getListOfAttendedEventsWorkerThread(myevent.Key, userId, mutex, ref totalhours, causeCountList);
                });
                thread.Start();
                listOfThreads.Add(thread);
            }
            foreach (Thread thread in listOfThreads)
            {
                thread.Join();
            }
            //compare to see what badge volunteer can get with it's totalhours & cause
            int testhour = totalhours;
            //add up qty for same causeId
            causeCountList.GroupBy(i => i.CAUSE_ID).Select(g => new { CAUSE_ID = g.Key, QTY = g.Sum(i => i.QTY) });
            List<Badge> causeBadgesList = new List<Badge>();
            List<Badge> minHBadgesList=VolBadgeDL.getVolNewMinHBadges(dbConnection, userId, totalhours);
            if (causeCountList.Any()) {
                causeBadgesList = VolBadgeDL.getVolNewCauseHBadges(dbConnection, userId, causeCountList);
            }
            if (causeBadgesList.Any()) {
                minHBadgesList.AddRange(causeBadgesList);
            }
            if (minHBadgesList.Any()) {
                //insert into database
                VolBadgeDL.createVolBadge(dbConnection, minHBadgesList, userId);
            }
            database.closeDatabase();
            return minHBadgesList;
        }

        private static void getListOfAttendedEventsWorkerThread(string connectionString, string usr_id, Semaphore mutex,ref int totalhours, List<VolEvtCause> causeCountList)
        {
            int npoHours = 0;
            List<VolEvtCause> causeCountNPOList = new List<VolEvtCause>();
            try
            {
                Database_Connect databaseConifg = new Database_Connect(connectionString);
                SqlConnection dataConnection = databaseConifg.getConnection();

                npoHours = VolBadgeDL.getCompletedVIA(dataConnection, usr_id);
                causeCountNPOList=VolBadgeDL.getVolCauseCount(dataConnection, usr_id);
                databaseConifg.closeDatabase();
                // write to shared List
                mutex.WaitOne();                
                if (causeCountNPOList.Any()) {
                    causeCountList.AddRange(causeCountNPOList);
                }
                totalhours = totalhours + npoHours;
                mutex.Release();

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(connectionString + "does not exist");
            }
        }

        public static List<Badge> displayVolBadge(string userId)
        {
            Database_Connect databaseConifg = new Database_Connect("CONSOLE_VMS");
            SqlConnection dataConnection = databaseConifg.getConnection();
            List<Badge> allBadgeList = BadgeDL.getPublishedBadgeList(dataConnection);
            List<Badge> volBadgeList = VolBadgeDL.getVolBadges(dataConnection, userId);

            List<Thread> listOfThreads = new List<Thread>();
            Semaphore mutex = new Semaphore(1, 1);

            foreach (Badge volBadge in volBadgeList)
            {
                Thread thread = new Thread(delegate ()
                {   //go to EACH database instance
                    getWorkThread(mutex, allBadgeList,volBadge.BADGE_ID);
                });
                thread.Start();
                listOfThreads.Add(thread);
            }
            foreach (Thread thread in listOfThreads)
            {
                thread.Join();
            }

            databaseConifg.closeDatabase();
            return allBadgeList;
        }

        private static void getWorkThread(Semaphore mutex, List<Badge> allBadgeList, string badge_id)
        {
            foreach (Badge masterBadge in allBadgeList)
            {
                if (masterBadge.BADGE_ID == badge_id)
                {
                    masterBadge.VOL_HAS = 1;
                }
            }
        }
    }
}