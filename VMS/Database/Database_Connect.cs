﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;


namespace VMS.Database
{
    public class Database_Connect
    {
        private SqlConnection databaseConnection;

        public Database_Connect(String db_name)
        {
            this.databaseConnection = new SqlConnection("user id =sgserve1;" + "password =Hello123;" +
                                                    "server=sgserve1.database.windows.net;" +
                                                    "Trusted_Connection =false;" + "Encrypt =true;" + "database=" + db_name +";" + "connection timeout=500;MultipleActiveResultSets=True;");
            try
            {
                databaseConnection.Open();
                System.Diagnostics.Debug.WriteLine("Success: " + db_name);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("affected db_name: " + db_name);
                System.Diagnostics.Debug.WriteLine(e.StackTrace);
            }
        }

        // returns a sql databasConnection
        public SqlConnection getConnection()
        {
            return this.databaseConnection;
        }

        // close a database connection 
        public void closeDatabase()
        {
            try
            {
                this.databaseConnection.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

    }
}